<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Application_PLugin_Layout

 */
class Application_Plugin_Login extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
			
            $auth =  Zend_Auth::getInstance();
            $controllerName = $request->getControllerName();
			
            $controller = $request->getControllerName();
            $action = $request->getActionName();
			
            if($auth->hasIdentity())
            {
                $permisos = Zend_Auth::getInstance()->getIdentity()->permisos;
                $permisos = explode("|", $permisos);

                echo $permiso = strtoupper($this->getPermissionName($action)."_".$controller);

                if($action == "logout")
                {
                        Zend_Auth::getInstance()->clearIdentity();
                }

                if(!in_array($permiso, $permisos) && !$request->isXmlHttpRequest())
                {
                        $request->setModuleName("default");
                        $request->setControllerName("index");
                        $request->setActionName("index");
                }
			
            }  //fue identificado
            else 
            {
                $request->setModuleName("default");
                $request->setControllerName("index");
                $request->setActionName("index");
            }	
			    
    } //function
	
    public function  getPermissionName($action)
    {
            switch($action)
            {
                    case "index"	: return "ver"; break;
                    case "agregar" : return "agregar"; break;
                    case "eliminar" : return "eliminar"; break;
                    case "exportar" : return "ver"; break;
                    case "imprimir" : return "ver"; break;
                    default: return $action; break;	
            }
    }
}

?>
