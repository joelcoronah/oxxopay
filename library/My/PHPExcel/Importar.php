<?php
require_once 'PHPExcel.php';
require_once 'PHPExcel/Reader/Excel2007.php';

class My_PHPExcel_Importar extends PHPExcel
{
	private $objPHPExcel;
	
	public function __construct()
	{
		$this->objPHPExcel = new PHPExcel_Reader_Excel2007();
	}
	
	public  function load($archivo, $plaza, $usuario, $comunidad,$cantidad)
	{
		$o=$this->objPHPExcel->load($archivo);
		
		$o->setActiveSheetIndex(0);
		
		for($i=2; $i<=$cantidad; $i++)
		{
			$cliente= new Cliente();
				$cliente->contrato= $o->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
				$cliente->fecha_contratacion= $o->getActiveSheet()->getCell('M'.$i)->getValue();
				$cliente->nombre= $o->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
				$cliente->calle= $o->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
				$cliente->no_exterior= $o->getActiveSheet()->getCell('E'.$i)->getCalculatedValue();
				$cliente->no_interior= $o->getActiveSheet()->getCell('F'.$i)->getCalculatedValue();
				$cliente->colonia= $o->getActiveSheet()->getCell('G'.$i)->getCalculatedValue();
				$cliente->comunidad_id= $comunidad;
				$cliente->telefono= $o->getActiveSheet()->getCell('I'.$i)->getCalculatedValue();
				$cliente->celular1= $o->getActiveSheet()->getCell('J'.$i)->getCalculatedValue();
				$cliente->celular1= $o->getActiveSheet()->getCell('K'.$i)->getCalculatedValue();
				$cliente->email= $o->getActiveSheet()->getCell('L'.$i)->getCalculatedValue();
				$cliente->televisores= $o->getActiveSheet()->getCell('Y'.$i)->getCalculatedValue();
				$cliente->etiqueta= $o->getActiveSheet()->getCell('X'.$i)->getCalculatedValue();
				$cliente->nodo= $o->getActiveSheet()->getCell('Z'.$i)->getCalculatedValue();
				$cliente->poste= $o->getActiveSheet()->getCell('AA'.$i)->getCalculatedValue();
				
				switch($o->getActiveSheet()->getCell('P'.$i)->getCalculatedValue())
				{
					case "ACTIVO": $status=1; break;
					case "DESCONECTAR": $status=2; break;
					case "CORTAR": $status=3; break;
					case "RETIRAR": $status=4; break;
					case "BAJA": $status=5; break;
					case "POR INSTALAR": $status=6; break;
					case "CORTADO": $status=7; break;
					case "DESCONECTADO":  $status=8; break;
					case "RETIRADO":  $status=9; break;
					case "POR REINSTALAR":  $status=10; break;
					case "POR RECONECTAR":  $status=11; break;
				}
				
				$cliente->plaza_id= $plaza;
				$cliente->status= $status;
			$cliente->save();
			
			$clienteServicio= new ClienteServicio();
				$clienteServicio->cliente_id=$cliente->id;
				$clienteServicio->servicio_id=1;	
			$clienteServicio->save();
			
			$ultimoMes=substr($o->getActiveSheet()->getCell('AK'.$i)->getCalculatedValue(),4,4)."-".substr($o->getActiveSheet()->getCell('AK'.$i)->getCalculatedValue(),2,2)."-".substr($o->getActiveSheet()->getCell('AK'.$i)->getCalculatedValue(),0,2);
			$cobranza= new Cobranza();
				$cobranza->cliente_id=$cliente->id;	
				$cobranza->fecha_fin=$ultimoMes;
				$cobranza->plaza_id=$plaza;
				$cobranza->usuario_id=$usuario;
				$cobranza->subtotal=$o->getActiveSheet()->getCell('AE'.$i)->getCalculatedValue();
				$cobranza->total=$o->getActiveSheet()->getCell('AE'.$i)->getCalculatedValue();
			$cobranza->save();	
					
				
		}
	}
	
	
	public  function loadalterno($archivo, $plaza, $usuario, $comunidad,$cantidad)
	{

		//echo getcwd(); exit;
		$o=$this->objPHPExcel->load($archivo);
		
		$o->setActiveSheetIndex(0);
		
		for($i=2; $i<=$cantidad; $i++)
		{
			$cliente= new Cliente();
				$cliente->contrato= $o->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
				$cliente->fecha_contratacion= $o->getActiveSheet()->getCell('L'.$i)->getValue();
				$cliente->nombre= $o->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
				$cliente->calle= $o->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
				$cliente->no_exterior= $o->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
				$cliente->no_interior= $o->getActiveSheet()->getCell('E'.$i)->getCalculatedValue();
				$cliente->colonia= $o->getActiveSheet()->getCell('F'.$i)->getCalculatedValue();
				$cliente->comunidad_id= $comunidad;
				$cliente->telefono= $o->getActiveSheet()->getCell('H'.$i)->getCalculatedValue();
				$cliente->celular1= $o->getActiveSheet()->getCell('I'.$i)->getCalculatedValue();
				$cliente->celular1= $o->getActiveSheet()->getCell('J'.$i)->getCalculatedValue();
				$cliente->email= $o->getActiveSheet()->getCell('K'.$i)->getCalculatedValue();
				$cliente->televisores= $o->getActiveSheet()->getCell('Y'.$i)->getCalculatedValue();
				$cliente->etiqueta= $o->getActiveSheet()->getCell('X'.$i)->getCalculatedValue();
				$cliente->nodo= $o->getActiveSheet()->getCell('Z'.$i)->getCalculatedValue();
				$cliente->poste= $o->getActiveSheet()->getCell('AA'.$i)->getCalculatedValue();
				
				switch($o->getActiveSheet()->getCell('O'.$i)->getCalculatedValue())
				{
					case "ACTIVO": $status=1; break;
					case "DESCONECTAR": $status=2; break;
					case "CORTAR": $status=3; break;
					case "RETIRAR": $status=4; break;
					case "BAJA": $status=5; break;
					case "POR INSTALAR": $status=6; break;
					case "CORTADO": $status=7; break;
					case "DESCONECTADO":  $status=8; break;
					case "RETIRADO":  $status=9; break;
					case "POR REINSTALAR":  $status=10; break;
					case "POR RECONECTAR":  $status=11; break;
				}
				
				$cliente->plaza_id= $plaza;
				$cliente->status= $status;
			$cliente->save();
			
			$clienteServicio= new ClienteServicio();
				$clienteServicio->cliente_id=$cliente->id;
				$clienteServicio->servicio_id=1;	
			$clienteServicio->save();
			
			$ultimoMes=substr($o->getActiveSheet()->getCell('AH'.$i)->getCalculatedValue(),4,4)."-".substr($o->getActiveSheet()->getCell('AH'.$i)->getCalculatedValue(),2,2)."-".substr($o->getActiveSheet()->getCell('AH'.$i)->getCalculatedValue(),0,2);
			$cobranza= new Cobranza();
				$cobranza->cliente_id=$cliente->id;	
				$cobranza->fecha_fin=$ultimoMes;
				$cobranza->plaza_id=$plaza;
				$cobranza->usuario_id=$usuario;
				$cobranza->subtotal=$o->getActiveSheet()->getCell('AG'.$i)->getCalculatedValue();
				$cobranza->total=$o->getActiveSheet()->getCell('AG'.$i)->getCalculatedValue();
			$cobranza->save();	
					
				
		}
	}	
	
}
