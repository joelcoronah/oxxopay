<?php
class My_Comun
{
    function __construct(){}
  
    ### Método para obtener los datos de una tabla
    public static function obtener($modelo, $id)
    {
        $q=Doctrine_Query::create()->from($modelo)->where('id='.$id);
        return $q->execute()->getFirst();
    }
    
    public static function obtenerFiltro($modelo, $filtro=" 1=1 ", $orden = "", $inner=1, $groupBy=null, $select=null)
    {
        if($groupBy != null)
        {
            ### Creamos la consulta con el filtro pero sin parámetros de paginación para obtener el total de registros.
            $con = Doctrine_Manager::getInstance()->connection();
            $q = "
                    SELECT MAX( s.id ) AS id
                    FROM sms AS s
                    INNER JOIN cliente AS c ON s.cliente_id = c.id
                    WHERE ".$filtro."
                    GROUP BY c.id
                    ORDER BY s.created_at DESC
                ";
            $q = $con->execute($q)->fetchAll();

            $ids = "";

            if(count($q) > 0)
            {
                foreach ($q as $registro)
                $ids .= $registro['id'].",";
                $ids = trim($ids, ',');
                $q=My_Comun::createQuery($modelo)->where("id IN(".$ids.") AND estatus_envio='noEnviado'");
            }
            else
            {
                $q=My_Comun::createQuery($modelo)->where("1=2");
            }
        }
        else
        {
            if($orden != "")
                $q=Doctrine_Query::create()->from($modelo)->where($filtro)->orderBy($orden);
            else
                $q=Doctrine_Query::create()->from($modelo)->where($filtro);
        }
        
        return $q->execute();
    }

    
    /**
     * Elimina un registro del modelo especificado.
     *
     * @param string $modelo Tabla
     * @param int $id ID del registro a eliminar
     * @return mixed Resultado de �xito/error al tratar de eliminar el registro
     * @access public
     * @static
     */
    public static function delete($modelo, $id)
    {   
        ### Por default la respuesta tiene el valor false
        $respuesta=false;

        ### Instanciamos la conexión para generar una transacción
        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->beginTransaction();
        try
        { 
            ### Intentamos eliminar el registro
            $q = Doctrine_Query::create()->delete($modelo)->where('id='.$id);
            $respuesta=$q->execute();
            
            ### Hacemo commit a la transacción
            $conn->commit();
        }
        catch (Exception $e)
        { 
            ### Rollback en caso de algún problema
            $conn->rollBack(); 
            ### Entregamos un mensaje de error
            return $e->getPortableCode();
        }
        ### Retornamos la respuesta
        return $respuesta;
    }
    

    
    public static function habilitar($modelo,$id)
    {
        $q = Doctrine_Query::create()->update($modelo.' a')->set('activo','1')->where('a.id='.$id);
        $q->execute();
    }

    public static function deshabilitar($modelo,$id)
    {
        
        $q = Doctrine_Query::create()->update($modelo.' a')->set('status','0')->where('a.id='.$id);
        $q->execute();
        
        $qRegistro= Doctrine_Query::create()->from($modelo)->where('id='.$id);
        $d=$qRegistro->execute()->getFirst();
        
        $accion="Eliminó el registro: ".$d->nombre;
        Bitacora::registrarEvento($modelo, $accion, Zend_Auth::getInstance()->getIdentity()->nombre);
        
    }
    
    
    /**
     * Guarda un registro del modelo especificado.
     *
     * @param string $modelo Modelo
     * @param array $data arreglo de los datos a guardar
     * @param array $campo_unico arreglo asociativo campo=>valor de los datos a validar cómo únicos
     * @param integer $id del registro en caso de que se esté actualizando
     * @return mixed Resultado de �xito/error al tratar de eliminar el registro
     * @access public
     * @static
     */ 
    public static function guardar($model, $data = array(), $campo_unico = null, $id = "", $bitacora="nombre")
    {
            $ModelTable = Doctrine_Core::getTable($model);

            if($campo_unico != null)
            {
                    if(is_array($campo_unico))
                    {
                            foreach($campo_unico as $validar)
                            {
                                    $campo = $data[$validar];
                                    $campo_unico = ucfirst(str_replace("-", "", $validar));
                                    $findBy = "findOneBy$campo_unico";
                                    $Model = $ModelTable->{$findBy}($campo);
                                    if((@$Model->id != $id)&&(@$Model !== false))
                                    {
                                        return -1; 
                                        exit;
                                    }
                            }
                    }
                    else 
                    {
                            $campo = $data[$campo_unico];
                            $campo_unico = ucfirst($campo_unico);
                            $findBy = "findOneBy$campo_unico";
                            $Model = $ModelTable->{$findBy}($campo);
                            if((@$Model->id != (int)$id)&&(@$Model !== false)){return -1; exit;}
                    }
            }

            if(!is_numeric($id)){$Model = new $model(); unset($data['id']);}
            else{$Model = $ModelTable->findOneById((int)$id);}
            
            $data=str_replace(array("'","\"",),array("´","´"),$data);
            
            $Model->fromArray($data);   

            $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
            $conn->beginTransaction();
            try
            {   
                $Model->save(); 
                
                $conn->commit(); 

                if($bitacora!="")
                {
                    if(is_numeric($id))
                       $accion="Actualizó el registro: ".$Model[$bitacora];
                    else    
                       $accion="Inserto el registro: ".$Model[$bitacora];
                    Bitacora::registrarEvento($model, $accion, Zend_Auth::getInstance()->getIdentity()->nombre);
                }

                return $Model->id;
            }
            catch(Exception $e)
            { 
                    $conn->rollBack(); /*throw $e;*/ return $e;
            }
    }
    
    public static function registrosGrid($modelo, $filtro, $inner=1, $groupBy=null, $orderBy=null, $select=null)
    {
        ### Incializamos el arreglo de registros
        $registros=array();
        
        ### Recibimos los parámetros de paginación y ordenamiento.
        if (isset($_POST['page']) != ""){$page = $_POST['page'];}
        if (isset($_POST['sortname']) != ""){$sortname =$_POST['sortname'];}
        if (isset($_POST['sortorder']) != ""){$sortorder = $_POST['sortorder'];}
        if (isset($_POST['qtype']) != ""){$qtype = $_POST['qtype'];}
        if (isset($_POST['query']) != ""){$query = $_POST['query'];}
        if (isset($_POST['rp']) != ""){$rp = $_POST['rp'];}
        
        
        $alias = $modelo;

        ### Codificamos el filtro para evitar problemas con IE      
        $filtro= (My_Utf8::is_utf8($filtro))?$filtro:utf8_encode($filtro);
        
        
        ### Creamos la consulta con el filtro pero sin parámetros de paginación para obtener el total de registros.
        //echo $filtro; exit;
        if($inner == 1)
        {
            $q=My_Comun::createQuery($modelo)->where($filtro);
        }
        else if($groupBy != null)
        {
            $q=My_Comun::prepararQuery($modelo." o");
            $q->select($select);
            foreach ($inner as $value) {
                $q->innerJoin($value);
            }
            $q->where($filtro);
            $q->groupBy($groupBy);
        }
        else
        {
            $q=My_Comun::prepararQuery($modelo." o");
            foreach ($inner as $value) {
                $q->innerJoin($value);
            }
            $q->where($filtro);
        }
        
        $registros['total']=$q->count();
        $paginas=ceil($registros['total']/$rp);
        if($page>$paginas)
            $page=1;
            
        ### Completamos la consulta con los datos de paginación y ordenamiento
        $offset = ($page-1)*$rp;
        $filtro .= ($qtype != '' && $query != '') ? " AND $alias.{$qtype} = '$query'" : '';
        if($inner == 1 ) $order = "$alias.{$sortname} $sortorder";
        else $order = "$o.{$sortname} $sortorder";

        //echo $order; exit;
        
        if($orderBy != null)
        {
            ### Ejecutamos la consulta
            $q = $q->orderBy($orderBy)->limit($rp)->offset($offset);
        }
        else
        {
            ### Ejecutamos la consulta
            $q = $q->orderBy($order)->limit($rp)->offset($offset);
        }
        
        //echo $q->getSqlQuery(); exit;

        $registros['registros']=$q->execute();  
        $registros['pagina']=$page;  

        return $registros;
    }

    public static function registrosGrid3($modelo, $filtro, $inner=1, $groupBy=null, $orderBy=null, $select=null)
    {
        ### Incializamos el arreglo de registros
        $registros=array();
        
        ### Recibimos los parámetros de paginación y ordenamiento.
        if (isset($_POST['page']) != ""){$page = $_POST['page'];}
        if (isset($_POST['sortname']) != ""){$sortname =$_POST['sortname'];}
        if (isset($_POST['sortorder']) != ""){$sortorder = $_POST['sortorder'];}
        if (isset($_POST['qtype']) != ""){$qtype = $_POST['qtype'];}
        if (isset($_POST['query']) != ""){$query = $_POST['query'];}
        if (isset($_POST['rp']) != ""){$rp = $_POST['rp'];}
        
        
        $alias = $modelo;

        ### Codificamos el filtro para evitar problemas con IE      
        $filtro= (My_Utf8::is_utf8($filtro))?$filtro:utf8_encode($filtro);
        
        
        ### Creamos la consulta con el filtro pero sin parámetros de paginación para obtener el total de registros.
        $con = Doctrine_Manager::getInstance()->connection();
        $q = "
                SELECT MAX( s.id ) AS id
                FROM sms AS s
                INNER JOIN cliente AS c ON s.cliente_id = c.id
                WHERE ".$filtro."
                GROUP BY c.id
                ORDER BY s.created_at DESC
            ";
        $q = $con->execute($q)->fetchAll();

        $ids = "";

        if(count($q) > 0)
        {
            foreach ($q as $registro)
            $ids .= $registro['id'].",";
            $ids = trim($ids, ',');
            $q=My_Comun::createQuery($modelo)->where("id IN(".$ids.") AND estatus_envio='noEnviado'");
        }
        else
        {
            $q=My_Comun::createQuery($modelo)->where("1=2");
        }
        
        $registros['total']=$q->count();
        $paginas=ceil($registros['total']/$rp);
        if($page>$paginas)
            $page=1;
            
        ### Completamos la consulta con los datos de paginación y ordenamiento
        $offset = ($page-1)*$rp;
        $filtro .= ($qtype != '' && $query != '') ? " AND $alias.{$qtype} = '$query'" : '';
        if($inner == 1 ) $order = "$alias.{$sortname} $sortorder";
        else $order = "$o.{$sortname} $sortorder";

        //echo $order; exit;
        
        if($orderBy != null)
        {
            ### Ejecutamos la consulta
            $q = $q->orderBy($orderBy)->limit($rp)->offset($offset);
        }
        else
        {
            ### Ejecutamos la consulta
            $q = $q->orderBy($order)->limit($rp)->offset($offset);
        }
        
        //echo $q->getSqlQuery(); exit;

        $registros['registros']=$q->execute();  
        $registros['pagina']=$page;  

        return $registros;
    }

    public static function registrosGrid2($q)
    {
        ### Incializamos el arreglo de registros
        $registros=array();
        
        ### Recibimos los parámetros de paginación y ordenamiento.
        if (isset($_POST['page']) != ""){$page = $_POST['page'];}
        if (isset($_POST['sortname']) != ""){$sortname =$_POST['sortname'];}
        if (isset($_POST['sortorder']) != ""){$sortorder = $_POST['sortorder'];}
        if (isset($_POST['qtype']) != ""){$qtype = $_POST['qtype'];}
        if (isset($_POST['query']) != ""){$query = $_POST['query'];}
        if (isset($_POST['rp']) != ""){$rp = $_POST['rp'];}
        
        

        //echo $q->getSqlQuery(); exit;
        
        $registros['total']=$q->count();
        $paginas=ceil($registros['total']/$rp);
        if($page>$paginas)
            $page=1;
            
        ### Completamos la consulta con los datos de paginación y ordenamiento
        $offset = ($page-1)*$rp;
        
        
        
        ### Ejecutamos la consulta
        $q = $q->limit($rp)->offset($offset);
        
        //echo $q->getSqlQuery(); exit;

        $registros['registros']=$q->execute();  
        $registros['pagina']=$page;  
        
        return $registros;
    }
    
    public static function grid2($registros, $grid)
    {
        if(count($grid)>0)
        $columnas=array_keys($grid[0]);
        $xml='<rows><page>'.$registros['pagina'].'</page><total>'.$registros['total'].'</total>';
        
        foreach($grid as $row)
        {
            $xml .= '<row id="'.$registro['id'].'">';
            foreach($columnas as $k=>$v)
            {
                $xml.='<cell><![CDATA['.$row[$v].']]></cell>';
            }
            $xml .= '</row>';
            
        }
        
        echo $xml.="</rows>";
        
    }
    
    public static function grid($model, $query_custom, $filtro, $columnas, $acciones_derecha=array())
    {
            $alias = null;

            if(!is_object($model)){$alias = ucwords($model);}

            if(!empty($_POST))
            {
                ### Cachamos los valores enviados por el grid
                if (isset($_POST['page']) != ""){$page = mysql_escape_string($_POST['page']);}
                if (isset($_POST['sortname']) != ""){$sortname = mysql_escape_string($_POST['sortname']);}
                if (isset($_POST['sortorder']) != ""){$sortorder = mysql_escape_string($_POST['sortorder']);}
                if (isset($_POST['qtype']) != ""){$qtype = mysql_escape_string($_POST['qtype']);}
                if (isset($_POST['query']) != ""){$query = mysql_escape_string($_POST['query']);}
                if (isset($_POST['rp']) != ""){$rp = mysql_escape_string($_POST['rp']);}
                
                
                $filtro= (My_Utf8::is_utf8($filtro))?$filtro:utf8_encode($filtro);
                                
                ### Determinamos el total de registros
                $t = My_Comun::createQuery($model)->where($filtro);
                //echo $t->getSqlQuery(); exit;
                $total=$t->count();
                
                $paginas=ceil($total/$rp);
                
                if($page>$paginas)
                    $page=1;
                
                
                
                
                
                ### Completamos la consulta con los datos enviados por el grid
                $offset = ($page-1)*$rp;
                $filtro .= ($qtype != '' && $query != '') ? " AND $alias.{$qtype} = '$query'" : '';
                $order = "$alias.{$sortname} $sortorder";
                
                
                
                ### Ejecutamos la consulta
                $q = My_Comun::createQuery($model)->where($filtro)->orderBy($order)->limit($rp)->offset($offset);
                //echo $q->getSqlQuery(); exit;
                
                $rs=$q->execute();    



                $xml='<rows><page>'.$page.'</page><total>'.$total.'</total>';
                foreach($rs as $registro)
                {
                    $xml .= '<row id="'.$registro['id'].'">';



                    //Columnas
                    foreach($columnas as $columna)
                    {   
                        $atributos = explode(".", $columna);
                        $r = My_Comun::mapAttr($registro, $atributos);
                        $xml.='<cell><![CDATA['.$r.']]></cell>';
                    }

                    foreach ($acciones_derecha as $accion)
                    {
                        if($accion['type'] == 'other')
                        {
                            $p = array();                   
                            $params = explode(",", $accion['params']);
                            foreach($params as $param){
                                    $atributos = explode(".", $param);
                                    if(is_array($rs)){$r=$registro[$param];}
                                    else{$r = My_Comun::mapAttr($registro, $atributos);}
                                    $p[] = "$r"; 
                            }

                            $caption = null;
                            $class = null;

                            if(isset($accion['caption'])){$caption = ucfirst($accion['caption']);}

                            if(isset($accion['class'])){$class = $accion['class'];}


                            $xml.='<cell><![CDATA['.call_user_func_array('sprintf', array_merge((array)$accion['action'], $p)).']]></cell>';
                        }
                    }

                    $xml.='</row>';
                }
                echo $xml.="</rows>";       
        }
    }
    
    public static function mapAttr($object, $atributos)
    {   
          $registro = $object;
          switch(count($atributos))
          {
                  case 1: @$r = $registro->{trim($atributos[0])}; break;
                  case 2: @$r = $registro->{trim($atributos[0])}->{trim($atributos[1])}; break;
                  case 3: @$r = $registro->{trim($atributos[0])}->{trim($atributos[1])}->{trim($atributos[2])}; break;
                  case 4: @$r = $registro->{trim($atributos[0])}->{trim($atributos[1])}->{trim($atributos[2])}->{trim($atributos[3])}; break;
                  case 5: @$r = $registro->{trim($atributos[0])}->{trim($atributos[1])}->{trim($atributos[2])}->{trim($atributos[3])}->{trim($atributos[4])}; break;
                  case 6: @$r = $registro->{trim($atributos[0])}->{trim($atributos[1])}->{trim($atributos[2])}->{trim($atributos[3])}->{trim($atributos[4])}->{trim($atributos[5])}; break;
                  case 7: @$r = $registro->{trim($atributos[0])}->{trim($atributos[1])}->{trim($atributos[2])}->{trim($atributos[3])}->{trim($atributos[4])}->{trim($atributos[5])}->{trim($atributos[6])}; break;
                  case 8: @$r = $registro->{trim($atributos[0])}->{trim($atributos[1])}->{trim($atributos[2])}->{trim($atributos[3])}->{trim($atributos[4])}->{trim($atributos[5])}->{trim($atributos[6])}->{trim($atributos[7])}; break;
                  case 9: @$r = $registro->{trim($atributos[0])}->{trim($atributos[1])}->{trim($atributos[2])}->{trim($atributos[3])}->{trim($atributos[4])}->{trim($atributos[5])}->{trim($atributos[6])}->{trim($atributos[7])}->{trim($atributos[8])}; break;
                  case 10: @$r = $registro->{trim($atributos[0])}->{trim($atributos[1])}->{trim($atributos[2])}->{trim($atributos[3])}->{trim($atributos[4])}->{trim($atributos[5])}->{trim($atributos[6])}->{trim($atributos[8])}->{trim($atributos[9])}; break;
                  default: @$r = $registro->{trim($atributos[0])}; break;
          }

          return $r;
    }
    
    public static function createQuery($model, $query = null)
        {
            $con = Doctrine_Manager::getInstance()->connection();
            if(is_null($query)){$q = Doctrine_Query::create()->from($model); return $q;} 
            else{$q = $con->execute($query)->fetchAll(); return $q;}
    }

    


        

    
    public static function formatoFecha($fecha)
        {
            $meses = array(1 => "enero",2 => "febrero",3 => "marzo",4 => "abril",5 => "mayo",6 => "junio",7 => "julio",8 => "agosto",9 => "septiembre",10 => "octubre",11 => "noviembre",12 => "diciembre");
            if($fecha!='')
            {
                $f = explode('-',$fecha);
                return $f[2].' de '.$meses[(int)$f[1]].' de '.$f[0];
            }
            else return $fecha;
    }
        
        public static function comboSucursal($idSucursal,$selected=0)
        {
           
            $q=Doctrine_Query::create()->from('Sucursal')->where('plaza_id='.$idSucursal);   
            //echo $q->getSqlQuery(); exit;
            
            $d=$q->execute();
            
            $options='<option value="0">Seleccione una sucursal</option>';
            
            foreach($d as $sucursal)
            {
                $s="";
                if($selected==$sucursal->id)
                   $s="selected";
                         
                $options.='<option value="'.$sucursal->id.'" '.$s.'>'.$sucursal->nombre.'</option>';
            }
            echo $options;
        }
        
        public static function comboMunicipio($idEstado,$selected=0)
        {
            $q=Doctrine_Query::create()->from('Municipio')->where('estado_id=?',$idEstado);
            $d=$q->execute();
            $options='<option value="0">Seleccione un municipio</option>';
            foreach($d as $municipio)
            {
                $s="";
                if($selected==$municipio->id)
                   $s="selected";
                         
                $options.='<option value="'.$municipio->id.'" '.$s.'>'.$municipio->nombre.'</option>';
            }
            echo $options;
        }
        
        public static function comboComunidad($idComunidad,$selected=0)
        {
            $q=Doctrine_Query::create()->from('Comunidad')->where('municipio_id=?',$idComunidad);
            $d=$q->execute();
            $options='<option value="0">Seleccione una comunidad</option>';
            foreach($d as $comunidad)
            {
                $s="";
                if($selected==$comunidad->id)
                   $s="selected";
                         
                $options.='<option value="'.$comunidad->id.'" '.$s.'>'.$comunidad->nombre.'</option>';
            }
            echo $options;
        }       
        
        public static function comboTecnico($idComunidad,$selected=0)
        {
            $q=Doctrine_Query::create()->from('Tecnico')->where(' plaza_id = '.$idComunidad);
            //echo $q->getSqlQuery(); exit;
            
            $d=$q->execute();
            $options='<option value="0">Seleccione un técnico</option>';
            foreach($d as $comunidad)
            {
                $s="";
                if($selected==$comunidad->id)
                   $s="selected";
                         
                $options.='<option value="'.$comunidad->id.'" '.$s.'>'.$comunidad->nombre.'</option>';
            }
            echo $options;
        }
        
        /*
        public static function comboUsuario($plaza_id, $selected=0, $tipo =0)
        {
            $q=Doctrine_Query::create()->from('Usuario')->where('plaza_id = '.$plaza_id);
            $usuarios=$q->execute();
            
            if($tipo == 0)
                $options='<option value="0">Todos los usuarios</option>';
            else
                $options='<option value="0">Seleccione un usuario</option>';
                        
            foreach($usuarios as $usuario)
            {
                $s="";
                if($selected==$usuario->id)
                   $s="selected";
                         
                $options.='<option value="'.$usuario->id.'" '.$s.'>'.$usuario->nombre.'</option>';
            }
            
            echo $options;
        }
        */
        
        
    public static function obtenerPromotores()
    {
        $data=array();
        $qPromotores=Doctrine_Query::create()->from('Usuario')->where('status=? AND promotor=1 AND plaza_id='.Usuario::plaza().'',1);
        $dPromotores=$qPromotores->execute();
        
        
        
        foreach($dPromotores as $promotor)
        {
            $data[]=array('tipo'=>1,'id'=>$promotor->id,'nombre'=>$promotor->nombre);
        }
        
        
        
        return $data;
    }

    public static function obtenerPromotoresApp()
    {
        $data=array();
        $qPromotores=Doctrine_Query::create()->from('Usuario')->where('status=? AND promotor=1 AND plaza_id='.Usuario::plaza().'',1);
        $dPromotores=$qPromotores->execute();
        
        return $dPromotores;
    }
    
    /*public static function obtenerRangoFechaDesdePeriodo($periodos, $ultimafecha="", $sumar=true) 
    {
        $fechas=array();
    if($ultimafecha=="")
    {
            $fechas['inicio']=date('Y')."-".date('m')."-01";
            $fechas['fin']= date("Y-m-t",strtotime(date("Y-m-01")." +  ".($periodos)." month "));
        }
        else
    {
            $desde=0;
            if($sumar)
            {
                $desde=1;
        $periodos++;
            }
            $fUltima=explode("-",$ultimafecha);
            $fechas['inicio']=date("Y-m-01",strtotime($fUltima[0]."-".$fUltima[1]."-01"." +  ".($desde)." month "));
            $fechas['fin']= date("Y-m-t",strtotime($fUltima[0]."-".$fUltima[1]." +  ".($periodos)." month "));
    }
                
    return $fechas;
    }*/
        
    public function numero_de_meses2($hoy,$fecha_anterior)
    {
        $meses=0;
        $harray = split("-", $hoy);
        $hoy = date("Y-m-01",strtotime($harray[0]."-".$harray[1]."-"."01"));
        $farray = split("-", $fecha_anterior);
        $fecha_anterior = date("Y-m-01",strtotime($farray[0]."-".$farray[1]."-"."01"));

        //echo "hoy : ".$hoy."  |<br>";
        while($fecha_anterior<=$hoy)
        {
                $meses++;
                //echo "<br>entroI |".$fecha_anterior."|"; 
                $fecha_anterior=date("Y-m-d", strtotime("$fecha_anterior +1 month"));  
                //echo "entroO |".$fecha_anterior."| meses".$meses." |";     
        }
        return $meses;
    }

    public function numero_de_meses1($hoy,$fecha_anterior)
    {
        $meses=0;
        while($fecha_anterior<=$hoy)
        {
                $meses++;
                $fecha_anterior=date("Y-m-d", strtotime("$fecha_anterior +1 month"));       
        }
        return $meses;
    }
    
    public function numero_de_meses($hoy,$fecha_anterior)
    {
        $restar=false;
        $meses=0;
        while($fecha_anterior<=$hoy)
        {
            $f=explode("-",$fecha_anterior);
            $meses++;
            $fecha_anterior=date("Y-m-01", strtotime($f[0]."-".$f[1]."-01"." +1 month"));       
            $restar=true;
        }
        if($restar)
            return $meses-1;
        else
            return $meses;
    }
    
    public static function obtenerRangoFechaDesdePeriodo($periodos, $ultimafecha="", $sumar=true) 
    {
        
            $fechas=array();
            if($ultimafecha=="")
            {
                $fechas['inicio']=date('Y')."-".date('m')."-01";
                $fechas['fin']= date("Y-m-t",strtotime(date("Y-m-01")." +  ".($periodos)." month "));
            }
            else
            {
                $desde=1;
                if($sumar)
                {
                    $desde=1;
                    $periodos++;
                }
                $fUltima=explode("-",$ultimafecha);
                $fechas['inicio']=date("Y-m-01",strtotime($fUltima[0]."-".$fUltima[1]."-01"." +  ".($desde)." month "));
                $fechas['fin']= date("Y-m-t",strtotime($fUltima[0]."-".$fUltima[1]." +  ".($periodos)." month "));
            }
                
        return $fechas;
    }

    public static function obtenerRangoFechaDesdePeriodo2($periodos, $ultimafecha="", $sumar=true) 
    {
        
            $fechas=array();
            if($ultimafecha=="")
            {
                $fechas['inicio']=date('Y')."-".date('m')."-01";
                $fechas['fin']= date("Y-m-t",strtotime(date("Y-m-01")." +  ".($periodos)." month "));
            }
            else
            {
                
                if($sumar)
                {
                    $desde=1;
                    $periodos++;
                }
                else
                {
                    $desde=0;
                    $periodos--;
                }
                $fUltima=explode("-",$ultimafecha);
                $fechas['inicio']=date("Y-m-01",strtotime($fUltima[0]."-".$fUltima[1]."-01"." +  ".($desde)." month "));
                $fechas['fin']= date("Y-m-t",strtotime($fUltima[0]."-".$fUltima[1]." +  ".($periodos)." month "));
            }
                
        return $fechas;
    }
    
    public static function folioobtener($plaza_id, $cobro)
    {
    
        try
        {
            
            /*
            $plaza = array();
            $plaza["3"] = 0;
            $plaza["5"] = 0;
            $plaza["6"] = 0;
            $plaza["7"] = 0;
            $plaza["8"] = 0;
            $plaza["9"] = 0;
            $plaza["10"] = 0;
            $plaza["11"] = 0;
            $plaza["12"] = 0;
            $plaza["13"] = 0;
            $plaza["14"] = 0;
            $plaza["15"] = 0;
            $plaza["17"] = 0;
            $plaza["19"] = 0;
            */
            
            $q=Doctrine_Query::create()->select("max(folio) as folio")->from("Cobranza")->where("plaza_id = ".$plaza_id);
            $folio = $q->execute()->getFirst()->folio;
            
            //if($folio == 0)
            //  $cobro->folio = $plaza["$plaza_id"];
            //else
                $cobro->folio = ($folio+1);
                
            $cobro->save();
            
            return true;
        }
        catch (Exception $e)
        {
            if($e->getPortableCode() == -5)
                $this->folioobtener($plaza_id, $cobro);
            else
                return false;
        }
    }
    
    // Función para el llenado del folio de la tabla Cobranza.
    /*
    public static function llenar()
    {
        
        try
        {
            
            $q = Doctrine_Query::create()->select("distinct(plaza_id) as plaza_id")->from("Cobranza")->where("plaza_id = 19");
            $plazas = $q->execute();
            
            $cadena = "";
                    
            foreach($plazas as $plaza)
            {
                $plaza_id = $plaza->plaza_id;
                $q = Doctrine_Query::create()->from("Cobranza")->where("plaza_id = ".$plaza_id)->orderBy("id desc");
                $registros = $q->execute();
                
                $contador = 0;
                
                foreach($registros as $registro)
                {
                    Doctrine_Query::create()->update('Cobranza')->set('folio', $contador)->where("id = ".$registro->id)->execute();
                    $contador = $contador - 1;
                }
            }
            
            return "Bien";
        }
        catch (Exception $e)
        {
            
            return $e->getMessage();
        }
    }    
    */

    public static function comboZona($idMunicipio,$selected=0)
    {
        $q=Doctrine_Query::create()->from('Zona')->where('status = 1 and municipio_id=?',$idMunicipio);
        $d=$q->execute();
        $options='<option value="0">Seleccione una zona</option>';
        foreach($d as $zona)
        {
            $s="";
            if($selected==$zona->id)
               $s="selected";
                     
            $options.='<option value="'.$zona->id.'" '.$s.'>'.$zona->nombre.'</option>';
        }
        echo $options;
    }

    public static function pushnotification($registration_id,$mensaje)
    {
        $apiKey='AIzaSyA2Ixzf-DD_3SnFoX8x6Pxhx9izrSUrsws';
        

        $messageData=$mensaje;
        $registrationIdsArray=array($registration_id);

        $headers = array("Content-Type:" . "application/json", "Authorization:" . "key=" . $apiKey);
        $data = array(
            'data' => $messageData,
            'registration_ids' => $registrationIdsArray
        );
     
        $ch = curl_init();
     
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers ); 
        curl_setopt( $ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send" );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
     
        $response = curl_exec($ch);
        if($response===FALSE){
            die("Curl failed: ".curl_error($ch));
        }
        curl_close($ch);
     
        //echo  $response; 
    }

    public static function prepararQuery($modelo)
    {
        $q=Doctrine_Query::create()->from($modelo);
        return $q;
    }

    public static function configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,$default)
    {
        $orden='';

        if($orden_primer_nivel>0)
        {
            switch($orden_primer_nivel)
            {
                case 1: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.Colonia.nombre ASC'; break;
                case 2: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.calle ASC'; break;
                case 3: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.no_exterior ASC'; break;
                case 4: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.etiqueta ASC'; break;
                case 5: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.nodo ASC'; break;
                case 6: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.poste ASC'; break;
            }
        }

        if($orden_segundo_nivel>0)
        {
            switch($orden_segundo_nivel)
            {
                case 1: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.Colonia.nombre ASC'; break;
                case 2: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.calle ASC'; break;
                case 3: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.no_exterior ASC'; break;
                case 4: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.etiqueta ASC'; break;
                case 5: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.nodo ASC'; break;
                case 6: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.poste ASC'; break;
            }
        }

        if($orden_tercer_nivel>0)
        {
            switch($orden_tercer_nivel)
            {
                case 1: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.Colonia.nombre ASC'; break;
                case 2: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.calle ASC'; break;
                case 3: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.no_exterior ASC'; break;
                case 4: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.etiqueta ASC'; break;
                case 5: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.nodo ASC'; break;
                case 6: (empty($orden))?$orden.='':$orden.=', '; $orden.='c.poste ASC'; break;
            }
        }

        if($orden=='')
            $orden=$default;


        return $orden;

    }

    public static function configurarAgrupamiento($query,$agrupamiento)
    {
        $grupos=array();
        switch($agrupamiento)
        {
            case 1: 
                //Por colonia
                $query->groupBy('c.colonia_id'); 
                $resultado=$query->execute(); 
                foreach($resultado as $r)
                {
                    //$grupos[]=$r->Cliente->Colonia->nombre;
                    if($r->Cliente->Colonia->nombre == "" || $r->Cliente->Colonia->nombre == null)
                        $grupos[]="INDEFINIDO";
                    else
                        $grupos[]=$r->Cliente->Colonia->nombre;
                }
            break;
            case 2: 
                //Por calle
                $query->groupBy('c.calle'); 
                $resultado=$query->execute();
                foreach($resultado as $r)
                {
                    //$grupos[]=$r->Cliente->calle;
                    if($r->Cliente->calle == "" || $r->Cliente->calle == null)
                        $grupos[]="INDEFINIDO";
                    else
                        $grupos[]=$r->Cliente->calle;
                }
            break;
            case 3: 
                $query->groupBy('c.etiqueta'); 
                $resultado=$query->execute();
                foreach($resultado as $r)
                {
                    //$grupos[]=$r->Cliente->etiqueta;
                    if($r->Cliente->etiqueta == "" || $r->Cliente->etiqueta == null)
                        $grupos[]="INDEFINIDO";
                    else
                        $grupos[]=$r->Cliente->etiqueta;
                }
            break;
            case 4: 
                $query->groupBy('c.nodo'); 
                $resultado=$query->execute();
                foreach($resultado as $r)
                {
                    //$grupos[]=$r->Cliente->nodo;
                    if($r->Cliente->nodo == "" || $r->Cliente->nodo == null)
                        $grupos[]="INDEFINIDO";
                    else
                        $grupos[]=$r->Cliente->nodo;
                }
            break;
            case 5: 
                $query->groupBy('c.poste'); 
                $resultado=$query->execute();
                foreach($resultado as $r)
                {
                    //$grupos[]=$r->Cliente->poste;
                    if($r->Cliente->poste == "" || $r->Cliente->poste == null)
                        $grupos[]="INDEFINIDO";
                    else
                        $grupos[]=$r->Cliente->poste;
                }
            break;
        }

        natsort($grupos);
        return array_unique($grupos);
    }

    public static function crearCarperasUsuario($registro)
    {
        $created = false;

        if(mkdir('../public/expediente_clientes/'. $registro .'/Registro/', 0777, true))
            if(mkdir('../public/expediente_clientes/'. $registro .'/Instalacion/', 0777, true))
                if(mkdir('../public/expediente_clientes/'. $registro .'/Desconexion/', 0777, true))
                    if(mkdir('../public/expediente_clientes/'. $registro .'/Retiro/', 0777, true))
                        if(mkdir('../public/expediente_clientes/'. $registro .'/Reinstalacion/', 0777, true))
                            if(mkdir('../public/expediente_clientes/'. $registro .'/Reconexion/', 0777, true))
                                if(mkdir('../public/expediente_clientes/'. $registro .'/Cambio de domicilio/Retiro/', 0777, true))
                                    if(mkdir('../public/expediente_clientes/'. $registro .'/Cambio de domicilio/Instalacion/', 0777, true))
                                        if(mkdir('../public/expediente_clientes/'. $registro .'/Extensiones/', 0777, true))
                                            if(mkdir('../public/expediente_clientes/'. $registro .'/Baja/', 0777, true))
                                                if(chmod('../public/expediente_clientes/'. $registro .'', 0777))
                                                    if(chmod('../public/expediente_clientes/'. $registro .'/Registro', 0777))
                                                        if(chmod('../public/expediente_clientes/'. $registro .'/Instalacion', 0777))
                                                            if(chmod('../public/expediente_clientes/'. $registro .'/Desconexion', 0777))
                                                                if(chmod('../public/expediente_clientes/'. $registro .'/Retiro', 0777))
                                                                    if(chmod('../public/expediente_clientes/'. $registro .'/Reinstalacion', 0777))
                                                                        if(chmod('../public/expediente_clientes/'. $registro .'/Reconexion', 0777))
                                                                            if(chmod('../public/expediente_clientes/'. $registro .'/Cambio de domicilio', 0777))
                                                                                if(chmod('../public/expediente_clientes/'. $registro .'/Cambio de domicilio/Retiro', 0777))
                                                                                    if(chmod('../public/expediente_clientes/'. $registro .'/Cambio de domicilio/Instalacion', 0777))
                                                                                        if(chmod('../public/expediente_clientes/'. $registro .'/Extensiones', 0777))
                                                                                            if(chmod('../public/expediente_clientes/'. $registro .'/Baja', 0777))
                                                                                                $created = true;
        return $created;
    }

    public static function obtenerCreditosSmsRestantes()
    {
        $prexml='<SMSWSS10><USER>sai</USER><PASS>sai123</PASS><SERVICE>12</SERVICE></SMSWSS10>';
        

        $ch = curl_init("http://www.masmensajes.com.mx/wss/smswss10.php");
        //curl_setopt($ch, CURLOPT_URL,);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"xml=".urlencode($prexml)."");
        curl_setopt($ch, CURLOPT_HEADER, false); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta= curl_exec ($ch);

        
        curl_close ($ch);

        $respuesta=explode("|",trim($respuesta));


        return str_replace("Credito:", "", $respuesta[2]);
    }
    
 }
?>