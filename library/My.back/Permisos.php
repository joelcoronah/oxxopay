<?php
class My_Permisos
{
	// ------------------------------ PERMISOS -----------------------------------------------------------------
	public static $definicion_permisos = array('VER_ROL','AGREGAR_ROL','MODIFICAR_ROL','ELIMINAR_ROL');
	// ---------------------------- FIN PERMISOS ---------------------------------------------------------------
	
	function __construct(){} // Constructor de la clase
	
	public static function revisaPermisos($llave, $permisos){
		if($permisos[$llave] == '1') return 1;
		else return 0;
	} // Funci�n "revisapermisos"
	
	public static function tienePermiso($permiso)
	{	
		$usuario = My_Comun::obtener("Usuario",Zend_Auth::getInstance()->getIdentity()->id);
		
		$permisos_usuario = explode('|',$usuario->permisos);
                
		
		if(in_array($permiso, $permisos_usuario))
		{
			return 1;
		}
		
		return 0;
	}
	
		
	
}