function openSubMenu() 
{
    $(this).find('ul').css('visibility', 'visible');	
}

function closeSubMenu() 
{
    $(this).find('ul').css('visibility', 'hidden');	
}

function mostrarAlertaMeta()
{
	$.ajax(
	{
        url:"/transacciones/metas/mostraralertameta",
        success:function result(data)
        {    
            $(".dialog-form").html(data);
            $(".dialog-form").dialog({
            height:'auto',
            width: 'auto',
            resizable: false,
            title: "Alerta: La meta del mes no ha sido definida",
            position: "center",
            modal: true,
            buttons: [{
	            text:"Definir metas",
	            click:function()
	            {
	               location.href="/transacciones/metas";
	            }
	          }]
            }); 
        }
    });
}

function mostrarAlertaConfirmarMeta()
{
    $.ajax(
    {
        url:"/transacciones/metas/mostraralertaconfirmarmeta",
        success:function result(data)
        {    
            $(".dialog-form").html(data);
            $(".dialog-form").dialog({
            height:'auto',
            width: 'auto',
            resizable: false,
            title: "Alerta",
            position: "center",
            modal: true,
            buttons: [{
                id:"btnConfirmarMeta",
                text:"Confirmar meta",
                click:function()
                {
                   $.ajax(
                   {
                        url:"/transacciones/metas/confirmarmeta",
                        success:function result(data)
                        {  
                            $("#btnConfirmarMeta").hide();
                            alert(data);
                            $(".dialog-form").html(data);
                        }
                    });
                }
              }]
            }); 
        }
    });
}

function mostrarAlertaMetasRetrasadasAdministrador()
{
    $("#btnFixed").show();
    /*$.ajax(
    {
        url:"/transacciones/metas/mostraralertaretrasadaadministrador",
        success:function result(data)
        {    
            $(".dialog-form-alerta").html(data);
            $(".dialog-form-alerta").dialog({
            height:'auto',
            width: 'auto',
            resizable: false,
            title: "Alerta",
            position: "center",
            modal: true/*,
            buttons: [{
                id:"btnConfirmarMeta",
                text:"Confirmar meta",
                click:function()
                {
                   $.ajax(
                   {
                        url:"/transacciones/metas/confirmarmeta",
                        success:function result(data)
                        {  
                            $("#btnConfirmarMeta").hide();
                            alert(data);
                            $(".dialog-form").html(data);
                        }
                    });
                }
              }]*/
       /*     }); 
        }
    });*/
}

function mostrarAlertaMetasRetrasadasPlaza()
{
    $.ajax(
    {
        url:"/transacciones/metas/mostraralertaretrasadaplaza",
        success:function result(data)
        {    
            $(".dialog-form-alerta").html(data);
            $(".dialog-form-alerta").dialog({
            height:'auto',
            width: 'auto',
            resizable: false,
            title: "Alerta",
            position: "center",
            modal: true/*,
            buttons: [{
                id:"btnConfirmarMeta",
                text:"Confirmar meta",
                click:function()
                {
                   $.ajax(
                   {
                        url:"/transacciones/metas/confirmarmeta",
                        success:function result(data)
                        {  
                            $("#btnConfirmarMeta").hide();
                            alert(data);
                            $(".dialog-form").html(data);
                        }
                    });
                }
              }]*/
            }); 
        }
    });
}


$(document).ready(function() 
{
        $('#menu > div').bind('mouseover', openSubMenu);
        $('#menu > div').bind('mouseout', closeSubMenu);



});


function configurarOrdenamiento(modulo)
{

    $(".dialog-form-ordenamiento").dialog({
        height:'auto',
        width: 'auto',
        resizable: false,
        title: "Configurar ordenamiento y agrupamiento",
        position: "center",
        modal: true,
        buttons: [{
            id:"btnOrdenar",
            text:"Aceptar",
            click:function()
            {
                switch(modulo)
                {
                    case "ordenesdesconexion": filtrarOrdendesconexion(); break;
                    case "ordenesdecorte": filtrarOrdenescorte(); break;
                    case "ordenesderetiro": filtrarOrdenretiro(); break;
                    case "bajas": filtrarOrden(); break;
                    default: filtrarOrdenes(); break;
                }
                $(".dialog-form-ordenamiento").dialog('close');
            }
        }]
    }); 

}
