var tipo;
function cobrar(id)
{    
	if(id==-1)
	{
		alert("No es posible realizar cobranza para un cliente que tiene una instalación pendiente.");
	}
	else
	{
		$("#snpCobro"+id).hide();
		$.ajax({
				url:"/transacciones/cobranza/aplicarpago/id/"+id,
				method:"post",
				success:function result(data)
				{    
					$(".dialog-form").html(data);
					$("#snpCobro"+id).show();
					$('#btnAnadirServiciosAdicionales').click(function()
					{
						$.ajax({
							url:"/transacciones/cobranza/anadirserviciosadicionales/id/"+$('#cliente_id').attr('value'),
							method:"post",
							success:function result(data)
							{   
								$(".dialog-form-aux").html(data);
								$(".dialog-form-aux").dialog({
								height:'auto',
								width: 700,
								resizable: false,
								title: "Agregar un nuevo servicio",
								position: [270,20],
								modal: true,
								buttons: [
										{
											text:"Agregar",
											click:
											function()
											{
												$.ajax(
												{
													type: 'POST',
													url:"/transacciones/cobranza/estableceservicioadicional/id/"+$('#servicio_id').attr('value'),
													success:function respuesta(res)
													{
															$(res).insertAfter($('#trEncabezadoAdicionales'));
															calculatotalamostrar();															
															$(".dialog-form-aux").dialog("destroy");
															$(".dialog-form-aux").dialog("close");
													}
												});
											}
										},
										{
											text:"Cancelar",
											click:function()
											{
												$(".dialog-form-aux").dialog("destroy");
												$(".dialog-form-aux").dialog("close");
											}
										}
									]
								}); 
	
							}
						});
						
					});
					
					
					$("#frmAplicarpago").validate(
					{
					   submitHandler: function(form)
					   {
						   
						 if(!$("#selectPagarMesesAdelantado").is('select') || ($("#selectPagarMesesAdelantado").is('select') && $("#selectPagarMesesAdelantado").attr('value')>0) || $(".hdnservicioadicional").length>0) 
						 {
							 $("#Aceptar").hide(); 
							 $(form).ajaxSubmit(
							 {
								success:    function(response)
								{ 
									alert("El pago ha sido aplicado correctamente.");
									
									var url = "/transacciones/recibo/imprimir/id/"+response;				 
									window.open(url);
				 
									//event.preventDefault();
									
									
									$('#flexgrid').flexReload();
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							 });
						 }
						 else
						 {
							 alert("Debe elegir el número de meses a pagar o añadir al menos un servicio adicional.");
						 }
					   }
					});
	
					$(".dialog-form").dialog({
							height:'auto',
							width: 'auto',
							resizable: false,
							title: "APLICAR PAGO ",
							position: [120,30],
							modal: true,
							buttons: [
									{
										id:"Aceptar",
										text:"Aplicar pago",
										click:
										function()
										{
												$("#frmAplicarpago").submit();
										}
									},
									{
										id:"Cancelar",
										text:"Cancelar",
										click:function()
										{
											$(".dialog-form").dialog("destroy");
											$(".dialog-form").dialog("close");
										}
									}
							]
							});                     
				}
		});
	}
}

function cobrarAdicional(id)
{    
		$("#snpCobro"+id).hide();
		$.ajax({
				url:"/transacciones/cobranza/aplicarpagoadicional/id/"+id,
				method:"post",
				success:function result(data)
				{    
					$(".dialog-form").html(data);	
					$("#snpCobro"+id).show();				
					$("#frmAplicarpago").validate(
					{
					   submitHandler: function(form)
					   {
							 $(form).ajaxSubmit(
							 {
								success:    function(response)
								{ 
									alert("El pago ha sido aplicado correctamente.");
									
									var url = "/transacciones/recibo/imprimir/id/"+response;				 
									window.open(url);
									$('#flexgrid').flexReload();
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							 });
		
					   }
					});
	
					$(".dialog-form").dialog({
							height:'auto',
							width: 'auto',
							resizable: false,
							title: "APLICAR PAGO ",
							position: [120,30],
							modal: true,
							buttons: [
									{
										id:"Aceptar",
										text:"Aplicar pago",
										click:
										function()
										{
												$("#frmAplicarpago").submit();
										}
									},
									{
										id:"Cancelar",
										text:"Cancelar",
										click:function()
										{
											$(".dialog-form").dialog("destroy");
											$(".dialog-form").dialog("close");
										}
									}
							]
							});                     
				}
		});
}

$(document).ready(function() 
{      
    $("#flexgrid").flexigrid(
    {        
        url: '/transacciones/cobranza/grid',
        dataType: 'xml',
        colModel : [
				{display: 'CONTRATO',  name:'fecha',width : 138, sortable : true, align: 'center'},
                {display: 'CLIENTE',  name:'fecha',width : 138, sortable : true, align: 'center'},
                {display: 'TELEFONO', name:'cliente',width : 185, sortable : true, align: 'center'},
                {display: 'CELULAR',  name:'telefono',width : 155, sortable : true, align: 'center'},                               
                {display: 'ESTATUS',  name:'celular', width : 170, sortable : false, align: 'center'},
                {display: 'REGISTRAR COBRO',  width : 120, sortable : false, align: 'center'}
                ],
        sortname: "created_at",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });  
        
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
	
	
	
	
});


function calculatotalamostrar()
{
	var total=0;
	$('.divMonto').each(function()
	{
			if($(this).html()!='')
				total+=parseFloat($(this).html().substr(1)); 
	});
	

	$('#divTotal').html('$'+total.toFixed(2));
}

function calculatotalamostrar2(total)
{
	total=total-$("#descuento").val();
	

	$('#divTotal').html('$'+total.toFixed(2));
}

function calculaMesesPorAdelantado(id,periodos)
{
	    $.ajax({
            url:"/transacciones/cobranza/calculamesesporadelantado/id/"+id+"/periodos/"+periodos,
			method:"post",
            success:function result(data)
            {    
				$('#bodyTablaPagoAdelantado').html(data);
				calculatotalamostrar();
			}
		});
}

function calculaMesesPorAdelantadoDesconectar(id,periodos)
{
	    $.ajax({
            url:"/transacciones/cobranza/calculamesesporadelantadodesconectar/id/"+id+"/periodos/"+periodos,
			method:"post",
            success:function result(data)
            {    
				$('#bodyTablaPagoAdelantado').html(data);
				calculatotalamostrar();
			}
		});
}

function eliminarFilaAdicional(id)
{
	$("#"+id).remove();
}

function filtrarCobranza()
{
    var filtro = "/transacciones/cobranza/grid";

    if($("#fcontrato").val() != "")
    {
            filtro += "/contrato/"+$("#fcontrato").val();
    }
	if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
    }
	if($("#ftelefono").val() != "")
    {
            filtro += "/telefono/"+$("#ftelefono").val();
    }
          
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}