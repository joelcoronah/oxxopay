var tipo;

$(document).ready(function()
{
    $("#festatus").multiselect({
        noneSelectedText: "Todos los estatus",
        click: function(event, ui)
        {

        	//ui.value es para obtener el valor
        	//ui.checked regresa true si está seleccionado o false si no lo está
			
			valores=$("#festatus_").val();
			
			if(ui.checked==true)
			{
				//agrega la plaza a la cadena.
				if(valores=='')
					valores=ui.value;
				else
					valores=$("#festatus_").val()+'_'+ui.value;
					
				$("#festatus_").attr('value',valores);	
			}
			else
			{
				
				//elimina la plaza de la cadena.
				valoresNuevo='';
				separacion=valores.split('_');
				
				for(i=0;i<separacion.length;i++)
					if(separacion[i]!=ui.value)
						if(valoresNuevo=='')
							valoresNuevo=separacion[i];
						else
							valoresNuevo+='_'+separacion[i];
							
				$("#festatus_").attr('value',valoresNuevo);
			}
        },
		checkAll:function(){
			
			//todas las plazas
			$("#festatus_").attr('value',$("#festatus__").val());
		}
		,
		uncheckAll:function(){
			
			//ninguna plaza
			$("#festatus_").attr('value','');
		}
    });
});

$(document).ready(function()
{
    $("#fplaza").multiselect({
        noneSelectedText: "Todas las plazas",
        click: function(event, ui)
        {

        	//ui.value es para obtener el valor
        	//ui.checked regresa true si está seleccionado o false si no lo está
			
			valores=$("#fplazas").val();
			
			if(ui.checked==true)
			{
				//agrega la plaza a la cadena.
				if(valores=='')
					valores=ui.value;
				else
					valores=$("#fplazas").val()+'_'+ui.value;
					
				$("#fplazas").attr('value',valores);	
			}
			else
			{
				
				//elimina la plaza de la cadena.
				valoresNuevo='';
				separacion=valores.split('_');
				
				for(i=0;i<separacion.length;i++)
					if(separacion[i]!=ui.value)
						if(valoresNuevo=='')
							valoresNuevo=separacion[i];
						else
							valoresNuevo+='_'+separacion[i];
							
				$("#fplazas").attr('value',valoresNuevo);
			}
        },
		checkAll:function(){
			
			//todas las plazas
			$("#fplazas").attr('value',$("#plazas").val());
		}
		,
		uncheckAll:function(){
			
			//ninguna plaza
			$("#fplazas").attr('value','');
		}
    });
});

function cambiarDomicilio(id)
{
	if(id==-1)
	{
		alert("Este cliente adeuda al menos un periodo. No es posible realizar el cambio de domicilio.");
	}
	else if(id==-2)
	{
		alert("Este cliente tiene una orden de instalación pendiente. No es posible realizar el cambio de domicilio.");
	}
	else if(id==-3)
	{
		alert("Este cliente tiene una orden de cambio de domicilio pendiente. No es posible realizar el cambio de domicilio.");
	}
	else if(id==-4)
	{
		alert("Este cliente tiene una orden de cambio de servicio pendiente. No es posible realizar el cambio de domicilio.");
	}
	else
	{
		 $.ajax({
				url:"/transacciones/clientes/cambiodomicilio/id/"+id,
				success:function result(data)
				{    
					$(".dialog-form").html(data);
					
				    $("#frmCliente").validate(
					{
					   submitHandler: function(form) 
					   {
						     $(form).ajaxSubmit(
							 {
								success:    function(response) 
								{ 
									$('#flexgrid').flexReload();
									alert("Se ha generado una orden de cambio de domicilio para este cliente");
									
									var url = "/transacciones/recibo/imprimir/id/"+response;				 
									window.open(url);
									
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							 });
					    }
					  });

					$(".dialog-form").dialog({
						height:'auto',
						width: 1000,
						resizable: false,
						title: "Cambio de domicilio",
						position: [170,0],
						modal: true,
						buttons: 
						[
							{
								text:"Agregar",
								click:
								function()
								{
									$("#frmCliente").submit();
								}
							},
							{
								text:"Cancelar",
								click:function()
								{
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							}
						]
					}); 
				}
		 });
	}
}

function cambiarServicio(id)
{
	if(id==-1)
	{
		alert("Este cliente adeuda al menos un periodo. No es posible realizar el cambio de servicio.");
	}
	else if(id==-2)
	{
		alert("Este cliente tiene una orden de instalación pendiente. No es posible realizar el cambio de servicio.");
	}
	else if(id==-3)
	{
		alert("Este cliente tiene una orden de cambio de domicilio pendiente. No es posible realizar el cambio de servicio.");
	}
	else if(id==-4)
	{
		alert("Este cliente tiene una orden de cambio de servicio pendiente. No es posible realizar un nuevo cambio de servicio.");
	}
	else if(id==-5)
	{
		alert("Este cliente tiene una orden de extension adicional pendiente. No es posible realizar el cambio de servicio.");
	}
	else
	{
		 $.ajax({
				url:"/transacciones/clientes/cambioservicio/id/"+id,
				success:function result(data)
				{    
					$(".dialog-form").html(data);
					
				    $("#frmCliente").validate(
					{

						rules:
			            {
			                nuevo_servicio_id: {required:true, min:1}
			            },
		                messages:
		                {
		                	nuevo_servicio_id: {required: "Debe elegir el nuevo servicio del cliente"}
		                },

					   submitHandler: function(form) 
					   {
					   		$("#cambiar,#cancelar").hide();
						    $(form).ajaxSubmit(
							{
								success:    function(response) 
								{ 
									$('#flexgrid').flexReload();
									alert("Se ha generado una orden de cambio de servicio para este cliente");
									
									var res = response.split(':');
									var url = "/transacciones/recibo/imprimir/id/"+res[0]+"/orden/"+res[1];	
									var url = "/transacciones/recibo/imprimir/id/"+response;				 
									window.open(url);
									
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							});
					    }
					  });

					$(".dialog-form").dialog({
						height:'auto',
						width: 1000,
						resizable: false,
						title: "Cambio de servicio",
						position: [170,0],
						modal: true,
						buttons: 
						[
							{
								id:"cambiar",
								text:"Cambiar servicio",
								click:
								function()
								{
									$("#frmCliente").submit();
								}
							},
							{
								id:"cancelar",
								text:"Cancelar",
								click:function()
								{
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							}
						]
					}); 
				}
		 });
	}
}

function anadirTv(id)
{
	if(id==-1)
	{
		alert("Este cliente adeuda al menos un periodo. No es posible añadir extensiones.");
	}
	else if(id==-2)
	{
		alert("Este cliente tiene una orden de instalación pendiente. No es posible añadir extensiones.");
	}
	else if(id==-3)
	{
		alert("Este cliente tiene una orden de extensiones adicionales pendiente. No es posible añadir extensiones.");
	}
	else
	{
		 $.ajax({
				url:"/transacciones/clientes/anadirtv/id/"+id,
				success:function result(data)
				{    
					$(".dialog-form").html(data);
					
				    $("#frmCliente").validate(
					{
						
					   submitHandler: function(form) 
					   {
						     $(form).ajaxSubmit(
							 {
								success:    function(response) 
								{ 
									$('#flexgrid').flexReload();
									alert("Se ha generado una orden de extensiones adicionales");
									var url = "/transacciones/recibo/imprimir/id/"+response;				 
									window.open(url);
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							 });
					    }
					  });

					$(".dialog-form").dialog({
						height:'auto',
						width: 1000,
						resizable: false,
						title: "AÑADIR TELEVISIONES",
						position: [170,0],
						modal: true,
						buttons: 
						[
							{
								text:"Agregar",
								click:
								function()
								{
									$("#frmCliente").submit();
								}
							},
							{
								text:"Cancelar",
								click:function()
								{
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							}
						]
					}); 
				}
		 });
	}
}

function solicitarBaja(id)
{
	if(id==-1)
	{
		alert("Este cliente adeuda al menos un periodo. No es posible solicitar su baja.");
	}
	else
	{
		 $.ajax({
				url:"/transacciones/clientes/baja/id/"+id,
				success:function result(data)
				{    
					$(".dialog-form").html(data);
					
				    $("#frmCliente").validate(
					{
					   submitHandler: function(form) 
					   {
						     $(form).ajaxSubmit(
							 {
								success:    function(response) 
								{ 
									alert("Se ha generado una orden de baja para este cliente");
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							 });
					    }
					  });

					$(".dialog-form").dialog({
						height:'auto',
						width: 1000,
						resizable: false,
						title: "Solicitud de baja",
						position: [170,0],
						modal: true,
						buttons: 
						[
							{
								text:"Aceptar",
								click:
								function()
								{
									$("#frmCliente").submit();
								}
							},
							{
								text:"Cancelar",
								click:function()
								{
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							}
						]
					}); 
				}
		 });
	}
}

function calculatotalamostrar()
{
	var total=0;
	$('.divMonto').each(function()
	{
			if($(this).html()!='')
				total+=parseFloat($(this).html().substr(1)); 
	});
	

	$('#divTotal').html('$'+total.toFixed(2));
}

function tvadicional(cantidad)
{
	if(cantidad>$('#limitetelevisores').attr('value'))
	{
		
		$.ajax({
            type: 'POST',
            url:"/transacciones/clientes/costotvadicional",
            success:
            function respuesta(res)
            {
				res=parseFloat(res);
                var tvs=cantidad-$('#limitetelevisores').attr('value');
				var totaltvs=tvs*res;
				$("#encabezadoCantidadTv").html(tvs);
				$("#encabezadoConceptoTv").html('Extensiones adicionales');
				$("#encabezadoComisionTv").html('$0.00');
				$("#encabezadoMontoTv").html("$"+res.toFixed(2));
				$("#encabezadoTotalTv").html("$"+totaltvs.toFixed(2));
				calculatotalamostrar();
            }
        });
		
	}
	else
	{
		calculatotalamostrar();
		$("#encabezadoCantidadTv").html('');
		$("#encabezadoConceptoTv").html('');
		$("#encabezadoComisionTv").html('');
		$("#encabezadoMontoTv").html("");
		$("#encabezadoTotalTv").html("");
	}
}

function agregarCliente(id)
{
	var resultado=0;
	
    $.ajax({
            url:"/transacciones/clientes/agregar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
				

				
				$('#tarifa_concepto_contratacion_id').change(function()
				{
					$.ajax(
					{
            			type: 'POST',
            			url:"/transacciones/clientes/establecerconceptocontratacion/id/"+$('#tarifa_concepto_contratacion_id').attr('value'),
            			success:function respuesta(res)
            			{
							
							$.ajax(
							{
								type: 'POST',
								url:"/transacciones/clientes/establecertvsconceptocontratacion/id/"+$('#tarifa_concepto_contratacion_id').attr('value'),
								success:function respuesta(data)
								{
									var opt="";
									for(var i=data; i<=5; i++)
									{
										opt+='<option value="'+i+'">'+i+'</option>';
									}
									$('#televisores').html(opt);
									$('#limitetelevisores').attr('value',data);
								}
							});
							
							
							r=res.split('_');
							$("#encabezadoCantidad").html('1');
							$("#encabezadoConcepto").html(r[0]);
							$("#encabezadoComision").html('$0.00');
							$("#encabezadoMonto").html("Base");
							$("#encabezadoTotal").html("$"+parseFloat(r[1]).toFixed(2));
							
							var total=0;
							$('.divMonto').each(function()
							{
									if($(this).html()!='')
										total+=parseFloat($(this).html().substr(1)); 
							});
							

							$('#divTotal').html('$'+total.toFixed(2));
							
							/**/
							
							
							
							
            			}
        			});
					
					
				});
				
				$('#btnAgregarServicio').click(function()
				{
					
					
					$.ajax(
					{
            			type: 'POST',
            			url:"/transacciones/clientes/agregarservicio/",
            			success:function respuesta(res)
            			{
							$(".dialog-form-aux").html(res);
							
							
							//Para distinguir entre servicio y servicio adicional
							$('#tipo_servicio').change(function()
							{
								$.ajax(
								{
									type: 'POST',
									url:"/transacciones/clientes/tiposervicio/id/"+$('#tipo_servicio').attr('value'),
									success:function respuesta(res)
									{
										$('#servicio_id').html(res);
									}
								});
								
								
							});
							
							
							$(".dialog-form-aux").dialog({
							height:'auto',
							width: 700,
							resizable: false,
							title: "Agregar un nuevo servicio",
							position: [270,20],
							modal: true,
							buttons: [
									{
										id:"btnDAgregarServicio",
										text:"Agregar",
										click:
										function()
										{
											var bandera=true;
											$('.hdnServicio').each(function()
											{
												if(this.value==$('#servicio_id').attr('value'))
												{
													bandera=false;
													return;
												}
											});
											
										   if(bandera)
										   {
										   		$("#btnDAgregarServicio").hide();
												$.ajax(
												{
													type: 'POST',
													url:"/transacciones/clientes/establecerserviciocontratacion/id/"+$('#servicio_id').attr('value'),
													success:function respuesta(res)
													{
															$(res).insertAfter($('#filaConceptoContratacion'));
															var comision=0;
															var total=0;
															$('.divComision').each(function()
															{
																comision+=parseFloat($(this).html().substr(1)); 
															});
															$('.divMonto').each(function()
															{
																	if($(this).html()!='')
																		total+=parseFloat($(this).html().substr(1)); 
															});
															
															
															
															$('#comision').attr('value','$'+comision.toFixed(2));
															$('#divTotal').html('$'+total.toFixed(2));
															


															$.ajax(
															{
																type: 'POST',
																url:"/transacciones/clientes/pidecelulares/id/"+$('#servicio_id').attr('value'),
																success:function respuesta(res2)
																{
																	$("#divCelulares").html(res2)
																}
															});

															$(".dialog-form-aux").dialog("destroy");
															$(".dialog-form-aux").dialog("close");
	
													}
												});
										   }
										   else
										   {
											   alert('Este servicio ya ha sido agregado.');
										   }
										}
									},
									{
										text:"Cancelar",
										click:function()
										{
											$(".dialog-form-aux").dialog("destroy");
											$(".dialog-form-aux").dialog("close");
										}
									}
								]
							}); 
            			}
        			});
				});
				
				

				
				
                $("#fecha_contratacion").datepicker({changeYear:true,dateFormat:'yy-mm-dd', maxDate:"+0d"});

                $("#frmCliente").validate(
                {
                   submitHandler: function(form) 
                   {
					  if($('#tarifa_concepto_contratacion_id').attr('value')==0)
					  {
						  alert('Debe elegir un concepto de contratación.');
					  }
					  else if($('.hdnTarifa').length<=0)
					  {
						  alert('Debe elegir al menos un servicio a contratar. Un servicio adicional no se puede considerar como un servicio a contratar.');
					  }
					  else if($(".numero_de_celular_movil").length>0)
					  {
					  	var bandera=true;
					  	$(".numero_de_celular_movil").each(function()
					  	{
					  			if($(this).attr("value")=="")
					  			{
					  				alert("Debe definir todos los números de celular.");
					  				return false;
					  			}
					  			else
								  {
									 $("#Aplicar,#Aceptar").hide(); 
									 $(form).ajaxSubmit(
									 {
										success:    function(response) 
										{ 
											if(response=="-1")
											{
												$("#Aplicar,#Aceptar").show(); 
												alert("Este número de contrato ya está dado de alta.");
											}
											else if(response=="-5")
											{
												$("#Aplicar,#Aceptar").show(); 
												alert("No hay vouchers suficientes para registrar estos celulares. Por favor contacte al administrador.");
											}
											else if(response=="-3")
											{
												$("#Aplicar,#Aceptar").show(); 
												alert("Hubo un error al tratar de guardar al cliente");
											}
											else
											{
												
												$('#flexgrid').flexReload();
												$("#Aplicar,#Aceptar").show(); 
												if(response==-2)
												{
													alert("Este recibo no se imprimirá pues se requiere autorización.");
												}
												else
												{
													var url = "/transacciones/recibo/imprimir/id/"+response;				 
													window.open(url);
												}
												if(tipo==1)
												{
													$('#frmCliente').resetForm();
													$('#divCuerpo1').html('');
													$("#encabezadoCantidad").html('');
													$("#encabezadoConcepto").html('');
													$("#encabezadoComision").html('');
													$("#encabezadoMonto").html('');
													$("#encabezadoTotal").html('');
													$('#divTotal').html('');
												}
												else
												{
													$(".dialog-form").dialog("destroy");
													$(".dialog-form").dialog("close");
												}
											}
										}
									 });
								  }


					  	});
					  }
					  else
					  {
						 $("#Aplicar,#Aceptar").hide(); 
						 $(form).ajaxSubmit(
						 {
							success:    function(response) 
							{ 
								if(response=="-1")
								{
									$("#Aplicar,#Aceptar").show(); 
									alert("Este número de contrato ya está dado de alta.");
								}
								else
								{
									
									$('#flexgrid').flexReload();
									$("#Aplicar,#Aceptar").show(); 
									if(response==-2)
									{
										alert("Este recibo no se imprimirá pues se requiere autorización.");
									}
									else
									{
										var url = "/transacciones/recibo/imprimir/id/"+response;				 
										window.open(url);
									}
									if(tipo==1)
									{
										$('#frmCliente').resetForm();
										$('#divCuerpo1').html('');
										$("#encabezadoCantidad").html('');
										$("#encabezadoConcepto").html('');
										$("#encabezadoComision").html('');
										$("#encabezadoMonto").html('');
										$("#encabezadoTotal").html('');
										$('#divTotal').html('');
									}
									else
									{
										$(".dialog-form").dialog("destroy");
										$(".dialog-form").dialog("close");
									}
								}
							}
						 });
					  }
					  
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 1000,
                        resizable: false,
                        title: "Registrar nuevo cliente",
                        position: [170,20],
                        modal: false,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
										
                                            tipo = 2;
                                            $("#frmCliente").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

///////////////////7
function editarCliente(id)
{
	var resultado=0;
	
    $.ajax({
            url:"/transacciones/clientes/editar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
				
				
                $("#fecha_contratacion").datepicker({changeYear:true,dateFormat:'yy-mm-dd', maxDate:"+0d"});

                $("#frmCliente").validate(
                {
                   submitHandler: function(form) 
                   {
					
							 $("#Aplicar,#Aceptar").hide(); 
							 $(form).ajaxSubmit(
							 {
								success:    function(response) 
								{ 
									if(response=="-1")
									{
										$("#Aplicar,#Aceptar").show(); 
										alert("Este número de contrato ya está dado de alta.");
									}
									else
									{
										
										$(".dialog-form").dialog("destroy");
										$(".dialog-form").dialog("close");
									}
								}
							 });
					  }
                   
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 1000,
                        resizable: false,
                        title: "Registrar nuevo cliente",
                        position: [170,20],
                        modal: false,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
										
                                            tipo = 2;
                                            $("#frmCliente").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });
}

/////////////////										  


function eliminarCliente(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/tecnicos/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                $('#flexgrid').flexReload();
            }
        });
    }
}


$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/clientes/grid',
        dataType: 'xml',
        colModel : [
				{display: 'CONTRATO',  name:'contrato',width : 60, sortable : true, align: 'center'},
                {display: 'CLIENTE',  name:'nombre',width : 150, sortable : true, align: 'center'},
				{display: 'DOMICILIO',  name:'calle',width : 150, sortable : true, align: 'center'},
				{display: 'PLAZA',  name:'calle',width : 40, sortable : true, align: 'center'},
                {display: 'TELEFONO',  name:'telefono',width : 90, sortable : true, align: 'center'},
                {display: 'CELULAR',  name:'celular1',width : 90, sortable : true, align: 'center'},
                {display: 'ETIQUETA',  name:'etiqueta',width : 70, sortable : true, align: 'center'},
                {display: 'NODO',  name:'nodo',width : 50, sortable : true, align: 'center'},
                {display: 'POSTE',  name:'poste',width : 70, sortable : true, align: 'center'},
				{display: 'ESTATUS',  name:'estatus',width : 90, sortable : false, align: 'center'},
				{display: '',   width : 40, sortable : false, align: 'center'},
                {display: '',   width : 40, sortable : false, align: 'center'},				
                {display: '',   width : 40, sortable : false, align: 'center'},
				{display: '',   width : 40, sortable : false, align: 'center'},
				{display: '',   width : 40, sortable : false, align: 'center'},
				{display: '',   width : 40, sortable : false, align: 'center'}
                ],
        sortname: "contrato",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1120,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    $('#festado').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/municipios/id/"+$('#festado').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fmunicipio').html(res);
            }
        });
    });

    $('#fmunicipio').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/comunidades/id/"+$('#fmunicipio').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fcomunidad').html(res);
            }
        });
    });
});

function filtrarClientes()
{
    var filtro = "/transacciones/clientes/grid";
    var imprimir="/transacciones/clientes/imprimir";
    var exportar="/transacciones/clientes/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }
    if($("#fplazas").val() != "")
    {
            filtro += "/plaza/"+$("#fplazas").val();
            imprimir += "/plaza/"+$("#fplazas").val();
            exportar += "/plaza/"+$("#fplazas").val();
    }
	if($("#festatus_").val() != "")
    {
            filtro += "/estatus/"+$("#festatus_").val();
            imprimir += "/estatus/"+$("#festatus_").val();
            exportar += "/estatus/"+$("#festatus_").val();
    }
	
	if($("#fetiqueta").val() != "")
    {
            filtro += "/etiqueta/"+$("#fetiqueta").val();
            imprimir += "/etiqueta/"+$("#fetiqueta").val();
            exportar += "/etiqueta/"+$("#fetiqueta").val();
    }
	
	if($("#fnodo").val() != "")
    {
            filtro += "/nodo/"+$("#fnodo").val();
            imprimir += "/nodo/"+$("#fnodo").val();
            exportar += "/nodo/"+$("#fnodo").val();
    }
	
	if($("#fposte").val() != "")
    {
            filtro += "/poste/"+$("#fposte").val();
            imprimir += "/poste/"+$("#fposte").val();
            exportar += "/poste/"+$("#fposte").val();
    }
    

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarClientes()
{
    $("#fnombre").attr('value','');
    
	//$("#fplazas").attr('value','');	
	$("#fplaza").multiselect("widget").find(":checkbox").each(function()
	{
		if(this.checked)
			this.click();
	});
	
	$("#festatus").multiselect("widget").find(":checkbox").each(function()
	{
		if(this.checked)
			this.click();
	});
	
	$("#fetiqueta").attr('value','');
	$("#fnodo").attr('value','');
	$("#fposte").attr('value','');


    var filtro = "/transacciones/clientes/grid";
    var imprimir="/transacciones/clientes/imprimir";
    var exportar="/transacciones/clientes/exportar";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);


    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}



function eliminarServicio(id)
{
	var comision=0;
	var total=0;
	$('#divCuerpo'+id).remove();
	
	$('.divComision').each(function()
	{
		comision+=parseFloat($(this).html().substr(1)); 
	});
	
	$('.divMonto').each(function()
	{
		total+=parseFloat($(this).html().substr(1)); 
	});

	$("#divCelulares").html("");
	
	
	$('#comision').attr('value','$'+comision.toFixed(2));
	$('#divTotal').html('$'+total.toFixed(2));
}

function estableceTipoTarifa(id, base)
{
	var identificador=id.split("_");
	
	if($('#'+id).attr('value')>0)
	{
		$.ajax({
				type: 'POST',
				url:"/transacciones/clientes/obtenermontotarifaespecial/id/"+$('#'+id).attr('value'),
				success:
				function respuesta(res)
				{
					
				    resp=res.split("_");
				   $('#precio'+identificador[1]).html(resp[0]);
				   
				   if(resp[1]==1)
				   {
					    $('#hdnautorizacion'+identificador[1]).attr('value',parseFloat(resp[1]).toFixed(2));
				   		$("#divCuerpo"+identificador[1]).addClass("requiereautorizacion");
				   }
				   else
				   {
					   $("#divCuerpo"+identificador[1]).removeClass("requiereautorizacion");
					   $('#hdnautorizacion'+identificador[1]).attr('value','');
				   }
				   
				}
		 });
	}
	else
	{
		$("#divCuerpo"+identificador[1]).removeClass("requiereautorizacion");
		$('#precio'+identificador[1]).html("$"+parseFloat(base).toFixed(2));
		$('#hdnautorizacion'+identificador[1]).attr('value','');
	}
	
}

function reasignarVoucher(id)
{
	if(confirm('¿Realmente quieres reasignar un voucher para este celular?'))
	{
		$("#btnEnviarSms, #btnReasignarVoucher").hide();
		$(".dialog-form-aux").html("<h3>Reasignando voucher, espere un momento...</h3>");
		$(".dialog-form-aux").dialog({
	        height:300,
	        width: 600,
	        resizable: false,
	        title: "Asignando voucher",
	        modal: true
		});

		$.ajax(
		{
			type: 'POST',
			url:"/transacciones/clientes/reasignarvoucher/id/"+id,
			success:function respuesta(res)
			{
				$(".dialog-form-aux").html('<h3>Se asignó el voucher '+res+'</h3><p class="alert alert-error"><b>Aviso</b> Te recordamos que es importante que desactives el voucher anterior del cliente para evitar que lo siga utilizando.</p>');
				$("#tdVoucher"+id).text(res);
				$("#btnEnviarSms, #btnReasignarVoucher").show();
			}
		});
	}

}

function enviarSms(id)
{
	if(confirm('¿Realmente quieres enviar por SMS este código de acceso?.\nTe recordamos que esto consumira un credito del saldo total de SMSs disponible.'))
	{
		$("#btnEnviarSms, #btnReasignarVoucher").hide();
		$(".dialog-form-aux").html("<h3>Enviando voucher, espere un momento...</h3>");
		$(".dialog-form-aux").dialog({
	        height:300,
	        width: 600,
	        resizable: false,
	        title: "Asignando voucher",
	        modal: true
		});

		$.ajax(
		{
			type: 'POST',
			url:"/transacciones/clientes/reenviarsms/id/"+id,
			success:function respuesta(res)
			{
				res=JSON.parse(res);
				$(".dialog-form-aux").html('<h3>Se ha enviado un mensaje al celular: '+res.celular+'</h3><p class="alert alert-error"><b>Aviso:</b>Quedan <b>'+res.mensajesrestantes+'</b> SMS disponibles.</p>');
				$("#btnEnviarSms, #btnReasignarVoucher").show();
			}
		});
	}

}