var tipo;
function validarMatriz()
{	
	var suma =0;
	var contador=1;
	var enviar =true;
	
	$('.porciento').each(function()
    {	
		//alert($("#"+this.id).attr('value'));    	
		if($("#"+this.id).val()>=0 && $("#"+this.id).val()<=100)
			suma+=parseFloat($("#"+this.id).attr('value'));
		
		else
			$("#"+this.id).attr('value',0);			
				
		if(contador==6){
			//alert(suma);			
			
			$("#"+this.id).removeClass('btn-success');	
			objetivo=this.id.split('_');
       		$('.divEstado_'+objetivo[1]).show();
			
								
			if(suma!=100){			
				$("#"+this.id).removeClass('btn-primary').addClass('btn-success');
       			
				objetivo=this.id.split('_');
       			$('.divEstado_'+objetivo[1]).show();				
				enviar =false;
				     						
			}
								
			contador=1;	
			suma=0;	
		}
				
		else
			contador++;
				       
    });	
	
	if(enviar)
    	$('#frmTarifas').submit();
		
	if(!enviar){	
		$(function() {
			$( "#dialog-message" ).dialog({
				modal: true,
				buttons: {
					Ok: function() {
						$( this ).dialog( "close" );
					}
				}
			});
		});		
	}
}

$(document).ready(function()
{
    var pasada=0;
    $('.botoneraTarifas input').each(function()
    {
        if(pasada==0)
        {
            $("#"+this.id).removeClass('btn-primary').addClass('btn-success');
            objetivo=this.id.split('_');
            $('.divEstado_'+objetivo[1]).show();
        }
        pasada++;
    });
		
    $('.botoneraTarifas input').click(function()
    {
        $('.botoneraTarifas input').removeClass('btn-success').addClass('btn-primary');
        $('.contenedorComisionesTarifas').hide();
			
        $("#"+this.id).removeClass('btn-primary').addClass('btn-success');
		objetivo=this.id.split('_');
		$('.divEstado_'+objetivo[1]).show();
    });
			
    $("#frmTarifas").validate();				
});