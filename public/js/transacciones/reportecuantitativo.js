var tipo;

function enviarFiltro()
{	
    $('#frmMetas').submit();
}

$(document).ready(function() 
{      
    $("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', onSelect:function(date){
         $( "#fhasta" ).datepicker( "option", "minDate", date );   
    }});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});	
    
    $('#fplaza').change(function()
    {        
        $.ajax({
            type: 'POST',
            url:"/transacciones/reportecuantitativo/sucursales/id/"+$('#fplaza').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });
    });   
     
});

function filtrarReporte()
{
    var exportar="/transacciones/reportecuantitativo2/exportar";
	
    if($("#fdesde").val() != "")
    {
        exportar += "/desde/"+$("#fdesde").val();
    }
	
    if($("#fhasta").val() != "")
    {
        exportar += "/hasta/"+$("#fhasta").val();
    }
	
    if($("#fplaza").val() > 0)
    {
        exportar += "/plaza/"+$("#fplaza").val();
    }

    if($("#fsucursal").val() > 0)
    {   
        exportar += "/sucursal/"+$("#fsucursal").val();
    }
    
    $('#frmMetas').submit();  
    $('#btnExportar').attr("href",exportar);
}

function filtrarReporte2()
{
    var exportar="/transacciones/reportecuantitativo2/exportar";
    
    if($("#fdesde").val() != "")
    {
        exportar += "/desde/"+$("#fdesde").val();
    }
    
    if($("#fhasta").val() != "")
    {
        exportar += "/hasta/"+$("#fhasta").val();
    }
    
    if($("#fplaza").val() > 0)
    {
        exportar += "/plaza/"+$("#fplaza").val();
    }

    if($("#fsucursal").val() > 0)
    {   
        exportar += "/sucursal/"+$("#fsucursal").val();
    }
    
    $('#frmMetas').submit();  
    $('#btnExportar').attr("href",exportar);
}

function limpiarGastos()
{
    $("#fdesde").attr('value','');
    $("#fhasta").attr('value','');
    $("#fplaza").attr('value',0);
    $("#fsucursal").attr('value',0);
}

function verDetalle(cual,fecha,plaza,tipo)
{
    $.ajax({
        url:"/transacciones/reportecuantitativo/detalle/reporte/"+cual+"/fecha/"+fecha+"/plaza/"+plaza+"/tipo/"+tipo+"/desde/"+$("#fdesde").val()+"/hasta/"+$("#fhasta").val(),
        method:"post",
        success:function result(data)
        {    
            $(".dialog-form").html(data);

            $(".dialog-form").dialog({
                height:800,
                width: 'auto',
                resizable: false,
                title: "REPORTE DETALLADO",
                position: "center",
                modal: true,
                buttons: [
                {
                    id:"Cerrar",
                    text:"Cerrar",
                    click:function()
                    {
                        $(".dialog-form").dialog("destroy");
                        $(".dialog-form").dialog("close");
                    }
                }
                ]
            });                     
        }
    });
}

function OCULTAR(tabla)
{
    $("#"+tabla).toggle();
}