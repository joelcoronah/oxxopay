var tipo;

$(document).ready(function() 
{   
   //Load...

});

function filtrar()
{
    var filtro = "/transacciones/reasignaciones/grid";

    if($("#ftipo").val() > 0)
        filtro += "/tipo/"+$("#ftipo").val();

    if($("#fagrupar").val() > 0)
        filtro += "/agrupar/"+$("#fagrupar").val();

    filtro+="/ordenar1/"+$("#fordenar1").val();
    filtro+="/ordenar2/"+$("#fordenar2").val();
    filtro+="/ordenar3/"+$("#fordenar3").val();
	
	if($("#ftusuario").val() == 0){
		if($("#ftipo").val() > 0){
			if($("#fagrupar").val() > 0){
				if($("#fplaza").val() == 0)
					alert("Debe seleccionar una plaza.");
				else{
					filtro += "/plaza/"+$("#fplaza").val();
					$.ajax({
						url:filtro,
						success:
						function respuesta(res)
						{
							$('#flexgrid').html(res);                
						}
					});
				}
			}else{
				alert("Debe seleccionar un grupo.");
			}
		}else{
			alert("Debe seleccionar un tipo.");
		}
	}else{
		//if(filtro == "/transacciones/reasignaciones/grid")
		if($("#ftipo").val() > 0){
			if($("#fagrupar").val() > 0){
				$.ajax({
					url:filtro,
					success:
					function respuesta(res)
					{
						$('#flexgrid').html(res);                
					}
				});
			}
			else
				alert("Debe seleccionar un grupo.");
		}
		else
			alert("Debe seleccionar un tipo.");
	}
}

function limpiar()
{
    
    $("#ftipo").attr('value','');
    $("#fagrupar").attr('value','');

    filtrar();
}

function guardar(tipo)
{
	if(confirm('Realmente quiere hacer esta asignacion?'))
	{
		$("#tipoasignacion").attr("value",tipo);
	    $("#frmReasignacion").ajaxSubmit(
	     {
	        success:    function(response) 
	        { 
	            filtrar();

	        }
	    });
	}	
}


function asignarIndividual(orden,tipo)
{
	if($("#tecnico_"+orden).val()==0)
	{
		alert("Debe elegir a un técnico");
	}
	else
	{
		$.ajax({
			url:"/transacciones/reasignaciones/asignarindividual/orden/"+orden+"/tipo/"+tipo+"/tecnico/"+$("#tecnico_"+orden).val(),
			success:
			function respuesta(res)
			{
				alert("La orden ha sido reasignada correctamente.");
			}
		});
	}
}