var tipo;
function descargarInstalacion(id)
{
    $.ajax({
            url:"/transacciones/ordenreinstalacion/descargarinstalacion/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
                $("#fecha_instalacion").datepicker({changeYear:true,dateFormat:'yy-mm-dd', maxDate:'+0'});


                $("#frmCliente").validate(
                {
                   submitHandler: function(form) 
                   {
					  if($('#cable_excedente').attr('value')!='' && $('#costo_excedente').attr('value')=='')
					  	alert("Debe indicar el monto a cobrar por el cable excedente");
					 else
					 {	
						 $(form).ajaxSubmit(
						 {
							success:    function(response) 
							{ 
								$('#flexgrid').flexReload();
	
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
							}
						 });
					 }
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Descargar reinstalación",
                        position: [240,100],
                        modal: true,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmCliente").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
   
                    
            }
    });

}


$(document).ready(function() 
{
    $("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', onSelect:function(date){
        $("#fhasta").datepicker( "option", "minDate", date );   
    }});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});   
    
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/ordenreinstalacion/grid',
        dataType: 'xml',
        colModel : [
		{display: 'FECHA CONTRATACIÓN',  name:'Cliente.fecha_contratacion',width : 130, sortable : true, align: 'center'},
		{display: 'CONTRATO',  name:'Cliente.contrato',width : 100, sortable : true, align: 'center'},
                {display: 'CLIENTE',  name:'Cliente.nombre',width : 200, sortable : true, align: 'center'},
                {display: 'PLAZA',  name:'Cliente.Plaza.nombre',width : 50, sortable : true, align: 'center'},
                {display: 'COLONIA',  name:'Cliente.Colonia.nombre',width : 50, sortable : true, align: 'center'},
                {display: 'TELEFONO',  name:'Cliente.telefono',width : 135, sortable : true, align: 'center'},
                {display: 'CELULAR',  name:'Cliente.celular1',width : 135, sortable : true, align: 'center'},
                {display: 'ESTATUS',  name:'Cliente.estatus',width : 120, sortable : false, align: 'center'},
		{display: 'DESCARGAR',  width : 70,  align: 'center'}
                ],
        sortname: "Cliente.fecha_contratacion",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
        
   $('#fplaza').change(function()
    {
        $.ajax({
                type: 'POST',                            
                url:"/transacciones/ordenreinstalacion/plazas/id/"+$('#fplaza').attr('value'),
                success:
                function respuesta(res)
                {
                    $('#fsucursal').html(res);
                }
        });
    }); 
});

function filtrarOrdenes()
{
    var filtro = "/transacciones/ordenreinstalacion/grid";
    var imprimir="/transacciones/ordenreinstalacion/imprimir";
    var exportar="/transacciones/ordenreinstalacion/exportar";

    if($("#fnombre").val() != "")
    {
        filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
        exportar += "/nombre/"+$("#fnombre").val();
    }
    
        if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
	imprimir += "/desde/"+$("#fdesde").val();
        exportar += "/desde/"+$("#fdesde").val();
    }
    
    if($("#fhasta").val() != "")
    {
     	filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();	
    }
    
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();   
        exportar += "/sucursal/"+$("#fsucursal").val();
    }
    
    if($("#fplaza").val() > 0)
    {
        filtro += "/plaza/"+$("#fplaza").val();
        imprimir += "/plaza/"+$("#fplaza").val();
        exportar += "/plaza/"+$("#fplaza").val();
    }

    if($("#orden_primer_nivel").val()>0)
    {
       filtro += "/orden_primer_nivel/"+$("#orden_primer_nivel").val();
       imprimir += "/orden_primer_nivel/"+$("#orden_primer_nivel").val();
       exportar += "/orden_primer_nivel/"+$("#orden_primer_nivel").val(); 
    }

    if($("#orden_segundo_nivel").val()>0)
    {
       filtro += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val();
       imprimir += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val();
       exportar += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val(); 
    }

    if($("#orden_tercer_nivel").val()>0)
    {
       filtro += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val();
       imprimir += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val();
       exportar += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val(); 
    }
    
    if($("#agrupamiento_primer_nivel").val()>0)
    {
       filtro += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val();
       imprimir += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val();
       exportar += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val(); 
    }
    
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarOrdenes()
{
    $("#fnombre").attr('value','');
    $("#fplaza").attr('value',0);
    $("#festado").attr('value',0);
    $("#fmunicipio").attr('value',0);
    $("#fcomunidad").attr('value',0);
    $("#fsucursal").attr('value',0);

    var filtro = "/transacciones/ordenreinstalacion/grid";
    var imprimir="/transacciones/ordenreinstalacion/imprimir";
    var exportar="/transacciones/ordenreinstalacion/exportar";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);


    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}