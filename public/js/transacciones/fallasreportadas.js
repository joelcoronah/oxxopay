var tipo;

function fallaReportada(id)
{	
    $.ajax({
            url:"/transacciones/fallasreportadas/editarfalla/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
				$("#fecha_solucion").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});	
					
				if($('#fecha_solucion').attr('value') != '0000-00-00'){
					$("#frmEditarFalla input[type='text']").attr('disabled',true);
					$("#frmEditarFalla select").attr('disabled',true);	
					$("#frmEditarFalla textarea").attr('disabled',true);	
				}
				
				$('#fecha_solucion').change(function()
				{
					if(!confirm('Ha definido una fecha de cierre para esta falla; ¿está seguro de querer reportarla como cerrada?'))
        			{
						 $("#fecha_solucion").attr('value','0000-00-00');		
					}
				});
				
								
				$("#frmEditarFalla").validate(
                {
               		submitHandler: function(form) 
                   	{ 

                        if($("#status_falla").val()==2 && ($('#fecha_solucion').val()=='' || $('#fecha_solucion').val()=='0000-00-00' ))
                        {
                            alert("Par marcar una falla como solucionada debe indicar la fecha de solución.");
                        }
                        else
                        {
    						$("#Aceptar").hide();
    						$(form).ajaxSubmit(
    						{
    							success: function(response) 
    							{ 
    									
    								$('#flexgrid').flexReload();
    								if(tipo==1)
    								{   
    									$('#frmEditarFalla').resetForm();
    								}
    								else
    								{
    									$(".dialog-form").dialog("destroy");
    									$(".dialog-form").dialog("close");
    								}
    									
    								$("#Aceptar").show(); 
    							}
    						});
                        }
					}
            	});
				
                $(".dialog-form").dialog({
                        height:'auto',
                        width: 1000,
                        resizable: false,
                        title: "FALLAS REPORTADAS ",
                        position: [170,20],
                        modal: false,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            if($('#fsolucion').val()!='0000-00-00'){
                                                $(".dialog-form").dialog("destroy");
                                                $(".dialog-form").dialog("close");
                                            }else{
                                                $("#frmEditarFalla").submit();
                                            }
                                            
                                    }
                                },
		                  {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
            		});                     
            }
    });

}

$(document).ready(function() 
{      
    $("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', onSelect:function(date){
         $( "#fhasta" ).datepicker( "option", "minDate", date );   
    }});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});	
    
    $("#flexgrid").flexigrid(
    {        
        url: '/transacciones/fallasreportadas/grid',
        dataType: 'xml',
        colModel : [
        		{display: 'FOLIO',  name:'id',width : 60, sortable : true, align: 'center'},
                {display: 'FECHA',  name:'fecha_falla',width : 60, sortable : true, align: 'center'},
        		{display: 'CONTRATO',  name:'Cliente.contrato',width : 90, sortable : true, align: 'center'},
                {display: 'CLIENTE',  name:'Cliente.nombre',width : 210, sortable : true, align: 'center'},
                {display: 'PLAZA',  name:'Cliente.Plaza.nombre',width : 50, sortable : true, align: 'center'},
                {display: 'TELEFONO', name:'Cliente.telefono',width : 100, sortable : true, align: 'center'},
                {display: 'CELULAR',  name:'Cliente.celular1',width : 90, sortable : true, align: 'center'},   
                {display: 'ESTATUS',  width : 130, sortable : false, align: 'center'},
		        {display: 'ULTIMO ESTATUS',  width : 130, sortable : false, align: 'center'},
                {display: 'VER FALLA', width : 70, sortable : false, align: 'center'}
                ],
        sortname: "Cliente.nombre",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1120,
        height: 400
    });  
    
   $('#fplaza').change(function()
   {
        $.ajax({
                type: 'POST',                            
                url:"/transacciones/fallasreportadas/plazas/id/"+$('#fplaza').attr('value'),
                success:
                function respuesta(res)
                {
                    $('#fsucursal').html(res);
                }
        });
    });      
        
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
	
});

function filtrarFallas()
{
    var filtro = "/transacciones/fallasreportadas/grid";
	var imprimir="/transacciones/fallasreportadas/imprimir";
    var exportar="/transacciones/fallasreportadas/exportar";

    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
	imprimir += "/desde/"+$("#fdesde").val();
        exportar += "/desde/"+$("#fdesde").val();
    }
    
    if($("#fhasta").val() != "")
    {
     	filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();		
    }
    
    if($("#fnombre").val() != "")
    {
    	filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
        exportar += "/nombre/"+$("#fnombre").val();
    }
	
    if($("#ffolio").val() != "")
    {
    	filtro += "/folio/"+$("#ffolio").val();
	imprimir += "/folio/"+$("#ffolio").val();
        exportar += "/folio/"+$("#ffolio").val()
    }
	
    if($("#fplaza").val() > 0)
    {
    	filtro += "/plaza/"+$("#fplaza").val();
	imprimir += "/plaza/"+$("#fplaza").val();
	exportar += "/plaza/"+$("#fplaza").val();
    }
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();   
        exportar += "/sucursal/"+$("#fsucursal").val();
    }
    
    filtro += "/falla/"+$("#ffalla").val();
    imprimir += "/falla/"+$("#ffalla").val();
    exportar += "/falla/"+$("#ffalla").val()
		
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);      
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}