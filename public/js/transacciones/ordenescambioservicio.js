var tipo;

function cancelar(id)
{
	if(confirm('Realmente desea cancelar esta orden de cambio de servicio?'))
	{	
		 $.ajax(
		 {
            url:"/transacciones/ordenescambioservicio/cancelar/id/"+id,
            success:function result(data)
            { 
				alert("La orden  ha sido cancelada.");
				$('#flexgrid').flexReload();
			}
		 });
	}
}

function descargar(id)
{
    $.ajax({
            url:"/transacciones/ordenescambioservicio/descargar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
				$("#fecha_retiro").datepicker({changeYear:true,dateFormat:'yy-mm-dd', maxDate:'+0'});
                $("#fecha_instalacion").datepicker({changeYear:true,dateFormat:'yy-mm-dd', maxDate:'+0'});


                $("#frmCliente").validate(
                {
                   submitHandler: function(form) 
                   {
                     $(form).ajaxSubmit(
                     {
                        success:    function(response) 
                        { 
                            $('#flexgrid').flexReload();

                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Descargar cambio servicio",
                        position: [165,20],
                        modal: false,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmCliente").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
   
                    
            }
    });

}


$(document).ready(function() 
{   
    $("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', onSelect:function(date){
         $("#fhasta").datepicker( "option", "minDate", date );   
    }});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});
    
    
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/ordenescambioservicio/grid',
        dataType: 'xml',
        colModel : [
		{display: 'FECHA DE SOLICITUD',  name:'created_at',width : 115, sortable : true, align: 'center'},
		{display: 'CONTRATO',  name:'Cliente.contrato',width : 100, sortable : true, align: 'center'},
                {display: 'CLIENTE',  name:'Cliente.nombre',width : 200, sortable : true, align: 'center'},
                {display: 'PLAZA',  name:'Cliente.Plaza.nombre',width : 50, sortable : true, align: 'center'},
                {display: 'TELEFONO',  name:'Cliente.telefono',width : 130, sortable : true, align: 'center'},
                {display: 'CELULAR',  name:'Cliente.celular1',width : 130, sortable : true, align: 'center'},
                {display: 'ESTATUS',  name:'status',width : 120, sortable : false, align: 'center'},
		{display: 'DESCARGAR',  width : 70,  align: 'center'},
		{display: 'CANCELAR',  width : 70,  align: 'center'}
                ],
        sortname: "Cliente.fecha_contratacion",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
     
   $('#fplaza').change(function()
    {
        $.ajax({
                type: 'POST',                            
                url:"/transacciones/ordenescambioservicio/plazas/id/"+$('#fplaza').attr('value'),
                success:
                function respuesta(res)
                {
                    $('#fsucursal').html(res);
                }
        });
    }); 
});

function filtrarOrdenes()
{
    var filtro = "/transacciones/ordenescambioservicio/grid";
    var imprimir="/transacciones/ordenescambioservicio/imprimir";

    if($("#fnombre").val() != "")
    {
        filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
    }
    
    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
	imprimir += "/desde/"+$("#fdesde").val();
    }
    
    if($("#fhasta").val() != "")
    {
     	filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();	
    }
      
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();   
    }
    if($("#fplaza").val() > 0)
    {
        filtro += "/plaza/"+$("#fplaza").val();
        imprimir += "/plaza/"+$("#fplaza").val();
    }

    if($("#orden_primer_nivel").val()>0)
    {
       filtro += "/orden_primer_nivel/"+$("#orden_primer_nivel").val();
       imprimir += "/orden_primer_nivel/"+$("#orden_primer_nivel").val();
    }

    if($("#orden_segundo_nivel").val()>0)
    {
       filtro += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val();
       imprimir += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val();
    }

    if($("#orden_tercer_nivel").val()>0)
    {
       filtro += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val();
       imprimir += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val();
    }
    
    if($("#agrupamiento_primer_nivel").val()>0)
    {
       filtro += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val();
       imprimir += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarOrdenes()
{
    $("#fnombre").attr('value','');
    $("#fdesde").attr('value','');
    $("#fhasta").attr('value','');
    $("#fsucursal").attr('value',0);
    $("#fplaza").attr('value',0);

    var filtro = "/transacciones/ordenescambioservicio/grid";
    var imprimir="/transacciones/ordenescambioservicio/imprimir";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}