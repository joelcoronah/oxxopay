var tipo;

function agregarGasto(id)
{
    $.ajax({
        url:"/transacciones/fallasyacometidas/agregar/id/"+id,
        success:function result(data)
        {    
            $(".dialog-form").html(data);				
            $("#fecha").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});		
				
            $("#frmFallasyAcometidas").validate(
            {
                submitHandler: function(form) 
                {
                    $("#Aplicar,#Aceptar").hide();  
                     $(form).ajaxSubmit(
                     {
                        success:    function(response) 
                        { 
                            $('#flexgrid').flexReload();
                            if(tipo==1)
                            {
                                $('#frmFallasyAcometidas').resetForm();
				$("#Aplicar,#Aceptar").show(); 
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							
                        }
                     });
                   }
                });

                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar registro de auditoría de acometidas ",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmFallasyAcometidas").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmFallasyAcometidas").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });
}


$(document).ready(function() 
{      
    $("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', onSelect:function(date){
         $( "#fhasta" ).datepicker( "option", "minDate", date );   
    }});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});	
    	
    $("#flexgrid").flexigrid(
    {        
        url: '/transacciones/fallasyacometidas/grid',
        dataType: 'xml',
        colModel : [
		{display: 'FECHA',  name:'fecha',width : 70, sortable : true, align: 'center'},
                {display: 'PLAZA',  name:'concepto',width : 70, sortable : true, align: 'center'},
                {display: 'TÉCNICO',  name:'concepto',width : 200, sortable : true, align: 'center'},
                {display: 'NODO', name:'monto',width : 70, sortable : true, align: 'center'},
                {display: 'POSTE',  name:'telefono',width : 70, sortable : true, align: 'center'},
                {display: 'DESCRIPCIÓN',  name:'telefono',width : 320, sortable : true, align: 'center'},
                {display: 'MODIFICAR',  name:'telefono',width : 70, sortable : true, align: 'center'},
                {display: 'ELIMINAR',  name:'telefono',width : 70, sortable : true, align: 'center'},
                ],
        sortname: "id",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });  
      
    $('#fsucursal').change(function()
    {        
        $.ajax({
            type: 'POST',
            url:"/transacciones/gastos/plazas/id/"+$('#fsucursal').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fplaza').html(res);
            }
        });
    });   
      
    $("#flexgrid").ready(function(){		
        $(".pReload").hide();
    });
		
});

function filtrarGastos()
{
    var filtro = "/transacciones/fallasyacometidas/grid";
    var imprimir="/transacciones/fallasyacometidas/imprimir";
    var exportar="/transacciones/fallasyacometidas/exportar";
	
    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
        imprimir += "/desde/"+$("#fdesde").val();
        exportar += "/desde/"+$("#fdesde").val();
    }
	
    if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();
    }
	
    if($("#fplaza").val() > 0)
    {
        filtro += "/plaza/"+$("#fplaza").val();
        imprimir += "/plaza/"+$("#fplaza").val();
        exportar += "/plaza/"+$("#fplaza").val();
    }

    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();   
        exportar += "/sucursal/"+$("#fsucursal").val();
    }
	
    //$('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarGastos()
{
    $("#fdesde").attr('value','');
    $("#fhasta").attr('value','');
    $("#fplaza").attr('value',0);
    $("#fsucursal").attr('value',0);
    $("#ftecnico").attr('value',0);

    var filtro = "/transacciones/fallasyacometidas/grid";

    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}
function eliminarAcometidas(){
    
}