var tipo;

function fallaReportada(id)
{	
    $.ajax({
        url:"/transacciones/comisionesporpagar/editarfalla/id/"+id,
        success:function result(data)
        {    
            $(".dialog-form").html(data);
            $("#fecha_solucion").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});		
			
	        $("#frmEditarFalla").validate(
            {
                submitHandler: function(form) 
               { 
                    $("#Aceptar").hide();
        			$(form).ajaxSubmit(
        			{
                        success: function(response) 
                        { 
        									
                            $('#flexgrid').flexReload();
            				if(tipo==1)
            				{
                                $('#frmEditarFalla').resetForm();
            				}
            				else
            				{
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
        									
        				    $("#Aceptar").show(); 
                        }
                    });
                }
            });
    				
            $(".dialog-form").dialog({
                height:'auto',
                width: 1000,
                resizable: false,
                title: "FALLAS REPORTADAS ",
                position: [170,20],
                modal: false,
                buttons: [
                    {
                        id:"Aceptar",
                        text:"Aceptar",
                        click:
                        function()
                        {
                                $("#frmEditarFalla").submit();
                        }
                    },
                    {
                        id:"Cancelar",
                        text:"Cancelar",
                        click:function()
                        {
                            $(".dialog-form").dialog("destroy");
                            $(".dialog-form").dialog("close");
                        }
                    }
                ]
        	});                     
        }
    });
}

function cobrar(id)
{	
    $.ajax({
            url:"/transacciones/comisionesporpagar/cantidad/id/"+id,
            success:function result(data)
            {        	
                if(confirm('¿En realidad desea pagar '+data+' pesos?'))
        		{
                    $.ajax({
                        url:"/transacciones/comisionesporpagar/pagar/id/"+id,
            			success:function result(data)
            			{
                            alert("Se realizo el pago con éxito ");
                            $('#flexgrid').flexOptions({url: "/transacciones/comisionesporpagar/grid"}).flexReload(); 
            			}
                    });
        		}   
            }
    });
}

$(document).ready(function() 
{      
    $("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', onSelect:function(date){
         $( "#fhasta" ).datepicker( "option", "minDate", date );   
    }});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});
    
    $("#flexgrid").flexigrid(
    {        
        url: '/transacciones/comisionesporpagar/grid',
        dataType: 'xml',
        colModel : [
            {display: 'PROMOTOR',  name:'Cliente.promotor',width : 180, sortable : true, align: 'center'},
            {display: 'CONCEPTO', width : 170, sortable : false, align: 'center'},
            {display: 'FECHA',  name:'Cliente.fecha_contratacion',width : 100, sortable : true, align: 'center'},
            {display: 'MONTO', name:'Cliente.comision',width : 100, sortable : true, align: 'center'},
            {display: 'PAGAR', width : 70, sortable : false, align: 'center'}
         ],
        sortname: "nombre",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });  
        
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
   $('#fplaza').change(function()
    {
        $.ajax({
                type: 'POST',                            
                url:"/transacciones/comisionesporpagar/plazas/id/"+$('#fplaza').attr('value'),
                success:
                function respuesta(res)
                {
                    $('#fsucursal').html(res);
                }
        });
    }); 
    
});

function filtrarFallas()
{
    var filtro = "/transacciones/comisionesporpagar/grid";
    var imprimir="/transacciones/comisionesporpagar/imprimir";
    var exportar="/transacciones/comisionesporpagar/exportar";

    if($("#fnombre").val() != "")
    {
    	filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
        exportar += "/nombre/"+$("#fnombre").val();
    }

    if($("#ftipo").val() != 0)
    {
    	filtro += "/tipo/"+$("#ftipo").val();
	imprimir += "/tipo/"+$("#ftipo").val();
	exportar += "/tipo/"+$("#ftipo").val();
    }
        
    if($("#fplaza").val() > 0)
    {
    	filtro += "/plaza/"+$("#fplaza").val();
	imprimir += "/plaza/"+$("#fplaza").val();
	exportar += "/plaza/"+$("#fplaza").val();
    }
    
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();   
        exportar += "/sucursal/"+$("#fsucursal").val();
    }
    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
	imprimir += "/desde/"+$("#fdesde").val();
        exportar += "/desde/"+$("#fdesde").val();
    }
    
    if($("#fhasta").val() != "")
    {
     	filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();		
    }
    
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);      
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}