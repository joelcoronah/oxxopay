var tipo;
function reportarFalla(id)
{
	var resultado=0;
	
    $.ajax({
            url:"/transacciones/reportarfallas/agregarfallas/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
				$("#fecha_falla").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});
		
				$("#frmReperteFallas").validate(
                {
               		submitHandler: function(form) 
                   	{ 
						$("#Aceptar").hide();
						$(form).ajaxSubmit(
						{
							success: function(response) 
							{ 
									
								$('#flexgrid').flexReload();
								if(tipo==1)
								{
									$('#frmReperteFallas').resetForm();
								}
								else
								{
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
									
								$("#Aceptar").show(); 
							}
						});
					}
            	});
				
                $(".dialog-form").dialog({
                        height:'auto',
                        width: 1000,
                        resizable: false,
                        title: "AGREGAR REPORTE DE FALLAS",
                        position: [170,20],
                        modal: false,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            $("#frmReperteFallas").submit();
                                    }
                                },
		                  {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
            		});                     
            }
    });

}

$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/reportarfallas/grid',
        dataType: 'xml',
        colModel : [
				{display: 'CONTRATO',  name:'nombre',width : 120, sortable : true, align: 'center'},
                {display: 'CLIENTE',  name:'nombre',width : 180, sortable : true, align: 'center'},
                {display: 'TELEFONO',  name:'telefono',width : 180, sortable : true, align: 'center'},
                {display: 'CELULAR',  name:'celular1',width : 180, sortable : true, align: 'center'},
                {display: 'ESTATUS',  name:'estatus',width : 180, sortable : false, align: 'center'},
                {display: 'REPORTAR FALLA',   width : 106, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    $('#festado').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/municipios/id/"+$('#festado').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fmunicipio').html(res);
            }
        });
    });

    $('#fmunicipio').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/comunidades/id/"+$('#fmunicipio').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fcomunidad').html(res);
            }
        });
    });
});

function filtrarFallas()
{
    var filtro = "/transacciones/reportarfallas/grid";

    if($("#fnombre").val() != "")
    {
   		filtro += "/nombre/"+$("#fnombre").val();
    }
	
    if($("#fplaza").val() > 0)
    {
            filtro += "/plaza/"+$("#fplaza").val();
    }
    
    if($("#festado").val() > 0)
    {
            filtro += "/estado/"+$("#festado").val();
    }
    
    if($("#fmunicipio").val() > 0)
    {
            filtro += "/municipio/"+$("#fmunicipio").val();
    }
    
    if($("#fcomunidad").val() > 0)
    {
            filtro += "/comunidad/"+$("#fcomunidad").val();
    }

    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarFallas()
{
    $("#fnombre").attr('value','');
    $("#fplaza").attr('value',0);
    $("#festado").attr('value',0)
    $("#fmunicipio").attr('value',0)
    $("#fcomunidad").attr('value',0)

    var filtro = "/transacciones/reportarfallas/grid";

    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}