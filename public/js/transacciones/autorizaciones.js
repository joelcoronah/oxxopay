var tipo;
function verautorizacion(id)
{	
    $.ajax({
            url:"/transacciones/autorizaciones/ver/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
				
				$("#frmAutorizacion").validate(
                {
                   submitHandler: function(form) 
                   {
					 
					 if($('#motivo').attr('value')!='')
					 {
						 if($('#dictamen').attr('value')==1)
						 	var mensaje="Realmente desea AUTORIZAR esta solicitud?";
						 else
						 	var mensaje="Realmente desea RECHAZAR esta solicitud?";	
						 if(confirm(mensaje))
						 {
							 $("#Aceptar").hide();
							 $(form).ajaxSubmit(
							 {
								success:    function(response) 
								{ 
									
									$('#flexgrid').flexReload();
									if(tipo==1)
									{
										$('#frmCiudad').resetForm();
									}
									else
									{
										$(".dialog-form").dialog("destroy");
										$(".dialog-form").dialog("close");
									}
									
									$("#Aceptar").show(); 
								}
							 });
						 }
					 }
					 else
					 {
						 if($('#dictamen').attr('value')==1)
						 {
							 alert("Debe especificar el motivo por el que está autorizando está solicitud.");
						 }
						 else
						 {
							 alert("Debe especificar el motivo por el que está rechazando está solicitud.");
						 }
					 }
                   }
                });

				

                $(".dialog-form").dialog({
                        height:'auto',
                        width: 1000,
                        resizable: false,
                        title: "DETALLE DE LA SOLICITUD DE AUTORIZACIÓN",
                        position: [170,20],
                        modal: false,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            $("#frmAutorizacion").submit();
                                    }
                                },
		                  {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
            }
    });

}
										  
$(document).ready(function() 
{   
   $('#fplaza').change(function()
    {
        $.ajax({
                type: 'POST',                            
                url:"/transacciones/autorizaciones/plazas/id/"+$('#fplaza').attr('value'),
                success:
                function respuesta(res)
                {
                    $('#fsucursal').html(res);
                }
        });
    }); 
    
        
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/autorizaciones/grid',
        dataType: 'xml',
        colModel : [
                {display: 'NÚMERO',  name:'id',width : 70, sortable : true, align: 'center'},
                {display: 'FECHA',  name:'fecha_solicitud',width : 120, sortable : true, align: 'center'},
                {display: 'CLIENTE',  name:'cliente',width : 190, sortable : true, align: 'center'},
                {display: 'USUARIO',  name:'usuario',width : 190, sortable : false, align: 'center'},
		{display: 'PLAZA',  name:'plaza',width : 100, sortable : false, align: 'center'},
		{display: 'ESTATUS',  name:'estatus',width : 100, sortable : false, align: 'center'},
                {display: 'VER',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "id",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });
    
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
});

function filtrarAutorizaciones()
{
    var filtro = "/transacciones/autorizaciones/grid";
    var imprimir="/transacciones/autorizaciones/imprimir";
    var exportar="/transacciones/autorizaciones/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }
    if($("#fplaza").val() > 0)
    {
            filtro += "/plaza/"+$("#fplaza").val();
            imprimir += "/plaza/"+$("#fplaza").val();
            exportar += "/plaza/"+$("#fplaza").val();
    }    
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();   
        exportar += "/sucursal/"+$("#fsucursal").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarAutorizaciones()
{
    $("#fnombre").attr('value','');
    $("#fplaza").attr('value',0);
    $("#fsucursal").attr('value',0);

    var filtro = "/transacciones/autorizaciones/grid";
    var imprimir="/transacciones/autorizaciones/imprimir";
    var exportar="/transacciones/autorizaciones/exportar";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}