var tipo;

function validarMatriz(identificador)
{	
	var bandera=false;

	$(".contenedorPlazaTarifas ").each(function()
	{
		//Si la bandera está levantada no tiene caso hacer nigun tipo de evaluación
		if(bandera)
			return;
			
		var numElementoVacios=0;
		var numElementoEspecialVacios=0;
		var idDiv=this.id;
		var idTr="";

		// Establecemos el background del div que vamos a evaluar en color blanco por default
		$('#'+idDiv).css("background-color","#fff");
		
		// Recorremos todos los campos base del div
		$("#"+this.id+" .base").each(function()
		{	
			if(this.value!='')
			{
				// Incrementamos el contador de elementos vacios
				numElementoVacios++;
			}
		});

		
		// Si el número elementos vacios es mayor a cero, pero menor a la totalidad de campos base, entonces detenemos el flujo y avisamos al usuario.
		if(numElementoVacios>0 && numElementoVacios<$("#"+this.id+" .base").length)
		{
			alert("No es posble definir solo algunos de los precios que conforman la tarifa base.");
			// Hacemos que el div se vuelva color rojo
			$('#'+idDiv).css("background-color","#FFBFBF");
			
			//Levantamos la bandera para ya no hacer validaciones
			bandera=true;
			return;
		}
		else if(numElementoVacios==$("#"+this.id+" .base").length)
		{
			//Ya que recorrimos  las tarifas base; vamos a evaluar las tarifas especiales
			$("#"+this.id+" .especial1mes").each(function()
			{	
				if(bandera)
					return;
					
				numElementoEspecialVacios=0;
				idTr=this.id;
				
				cadenaSplit=idTr.split('_');
				idTarifasAdicionales='especialAdcional_'+cadenaSplit[1]+'_'+cadenaSplit[2]+'_'+cadenaSplit[3];	
				
				
				$('#'+idTr).css("background-color","#fff");
				$('#'+idTarifasAdicionales).css("background-color","#fff");
				
				
				// Recorremos los de la tarifa especial a 1 mes
				$("#"+this.id+" .especial").each(function()
				{	
					if(this.value=='')
						numElementoEspecialVacios++;
				});
				
				
				// Recorremos los de la tarifa especial a varios meses
				$("#"+idTarifasAdicionales+" .especial").each(function()
				{	
					if(this.value=='')
						numElementoEspecialVacios++;
				});
				
				
				// Si el número elementos vacios es mayor a cero, pero menor a la totalidad de cmapos base, entonces detenemos el flujo y avisamos al usuario.
				if(numElementoEspecialVacios>0 && numElementoEspecialVacios<($("#"+this.id+" .especial").length + $("#"+idTarifasAdicionales+" .especial").length))
				{
					alert("No es posble definir solo algunos de los precios que conforman la tarifa especial.");
					// Hacemos que el div se vuelva color rojo
					$('#'+idTr).css("background-color","#FFBFBF");
					$('#'+idTarifasAdicionales).css("background-color","#FFBFBF");
					
					//Levantamos la bandera para ya no hacer validaciones
					bandera=true;
					return;
				}
			});
			
			
		}
	});
	
	if(bandera==false){
		//console.log($('#fservicio').val());
		$('#servicio_id').val($('#fservicio').val());
		$('#plaza').val($('#fplaza').val());
		$('#frmTarifas').submit();
	}
	
}

$(document).ready(function()
{
		var pasada=0;
		/*$('.botoneraTarifas input').each(function()
		{
				if(pasada==0)
				{
					$("#"+this.id).removeClass('btn-primary').addClass('btn-success');
					objetivo=this.id.split('_');
					$('.divEstado_'+objetivo[1]).show();
				}
				pasada++;
		});*/
		
		$('.botoneraTarifas input').click(function()
		{
			$('.botoneraTarifas input').removeClass('btn-success').addClass('btn-primary');
			$('.contenedorServicioTarifas').hide();
			
			$("#"+this.id).removeClass('btn-primary').addClass('btn-success');
			objetivo=this.id.split('_');
			$('.divEstado_'+objetivo[1]).show();
			
			
			
		});



		$("#fservicio").multiselect({
			        noneSelectedText: "Servicios",
			        click: function(event, ui)
			        {

			        	//ui.value es para obtener el valor
			        	//ui.checked regresa true si está seleccionado o false si no lo está
						
						valores=$("#fservicio").val();
						
						if(ui.checked==true)
						{
							//agrega la plaza a la cadena.
							if(valores=='')
								valores=ui.value;
							else
								valores=$("#fservicio").val()+'_'+ui.value;
								
							$("#fservicio").attr('value',valores);	
						}
						else
						{
							
							//elimina la plaza de la cadena.
							valoresNuevo='';
							separacion=valores.split('_');
							
							for(i=0;i<separacion.length;i++)
								if(separacion[i]!=ui.value)
									if(valoresNuevo=='')
										valoresNuevo=separacion[i];
									else
										valoresNuevo+='_'+separacion[i];
										
							$("#fservicio").attr('value',valoresNuevo);
						}
			        },
					checkAll:function(){
						
						//todas las plazas
						$("#fservicio").attr('value',$("#fservicio").val());
					}
					,
					uncheckAll:function(){
						
						//ninguna plaza
						$("#fservicio").attr('value','');
					}
			    });
	if($('#servicio_id').val()!=''){
		var data=$('#servicio_id').val();
		//Make an array
		var dataarray=data.split(",");
		// Set the value
		$("#fservicio").val(dataarray);
		// Then refresh
		$("#fservicio").multiselect("refresh");
		filtrarServicios();
	}
});
function cargarServiciosPlaza(id){
	$.ajax({
            type: 'POST',
            url:"/transacciones/servicioplaza/cargar-servicios-plaza",
            data:{plaza:id},
            success:
            function respuesta(res)
            {
				console.log(res);
				//$("#fservicio").multiselect("destroy").multiselect();
				
				$("#fservicio").html(res);
				$("#fservicio").multiselect("refresh");

				
            }
        });
}
function  filtrarServicios(){
	console.log($('#fservicio').val());
	if($('#fservicio').val()==''||$('#fservicio').val()== null){
		alert("Debe seleccionar al menos un servicio.");
	}else{
		$.ajax({
            type: 'POST',
            url:"/transacciones/servicioplaza/cargar-servicios",
            data:{plaza:$('#fplaza').val(),servicios:$('#fservicio').val()},
            success:
            function respuesta(res)
            {
				$("#contendorServicio").html('');
				$("#contendorServicio").html(res);
				
            }
        });
	}
}

