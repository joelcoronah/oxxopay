var tipo;
var map;
var latitud = 21.289374637098234;
var longitud = -102.08496081249996;

$(document).ready(function()
{

	$("#fpromotor").multiselect({noneSelectedText: "Todas los promotores"});
	$("#ftecnico").multiselect({noneSelectedText: "Todas los tecnicos"});


    $("#fplaza").multiselect({
        noneSelectedText: "Todas las plazas",
        click: function(event, ui)
        {

        	//ui.value es para obtener el valor
        	//ui.checked regresa true si está seleccionado o false si no lo está
			
			valores=$("#fplazas").val();
			
			if(ui.checked==true)
			{
				//agrega la plaza a la cadena.
				if(valores=='')
					valores=ui.value;
				else
					valores=$("#fplazas").val()+'_'+ui.value;
					
				$("#fplazas").attr('value',valores);	
			}
			else
			{
				
				//elimina la plaza de la cadena.
				valoresNuevo='';
				separacion=valores.split('_');
				
				for(i=0;i<separacion.length;i++)
					if(separacion[i]!=ui.value)
						if(valoresNuevo=='')
							valoresNuevo=separacion[i];
						else
							valoresNuevo+='_'+separacion[i];
							
				$("#fplazas").attr('value',valoresNuevo);
			}
			
			valores=$("#fplazas").val();
			
			//si hay valores
			if(valores!='')
			{
				if(valores.indexOf('_') > 0) //si hay más de un valor
				{
					//limpia y deshabilita el combo de sucursal.
					//$("#fpromotor").multiselect('destroy');
					//$("#ftecnico").multiselect('destroy');
					sucursal(-1);
					tecnico(-1);
					promotor(-1);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", true);
					
					$("#ftecnico").attr("value", 0);
					$("#ftecnico").attr("disabled", true);	
					
					$("#fpromotor").attr("value", 0);
					$("#fpromotor").attr("disabled", true);
				}
				else
				{
					//selecciona las sucursales de la única plaza seleccionada, y habilita el combo.
					////$("#fpromotor").multiselect('destroy');
					////$("#ftecnico").multiselect('destroy');
					sucursal(valores);
					tecnico(valores);
					promotor(valores);

					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", false);
					
					$("#ftecnico").attr("value", 0);
					$("#ftecnico").multiselect('enable');//attr("disabled", false);
					
					$("#fpromotor").attr("value", 0);
					$("#fpromotor").multiselect('enable');//.attr("disabled", false);
				}
			}
			else
			{
				//limpia y habilita el combo de sucursal.
				//$("#fpromotor").multiselect('destroy');
				//$("#ftecnico").multiselect('destroy');
				sucursal(-1);
				tecnico(-1);
				promotor(-1);
				$("#fsucursal").attr("value", 0);
				$("#fsucursal").attr("disabled", false);
				
				$("#ftecnico").attr("value", 0);
				$("#ftecnico").attr("disabled", false);
				
				$("#fpromotor").attr("value", 0);
				$("#fpromotor").attr("disabled", false);
			}
        },
		checkAll:function(){
			
			//todas las plazas
			$("#fplazas").attr('value',$("#plazas").val());
			
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1);
			tecnico(-1);
			promotor(-1);
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", true);	
			
			$("#ftecnico").attr("value", 0);
			$("#ftecnico").attr("disabled", true);
			
			$("#fpromotor").attr("value", 0);
			$("#fpromotor").attr("disabled", true);
		}
		,
		uncheckAll:function(){
			
			//ninguna plaza
			$("#fplazas").attr('value','');
		
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1);
			tecnico(-1);
			promotor(-1);
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", false);	
			
			$("#ftecnico").attr("value", 0);
			$("#ftecnico").attr("disabled", false);
			
			$("#fpromotor").attr("value", 0);
			$("#fpromotor").attr("disabled", false);
		}
    });
});

function sucursal(plaza_id)
{

	$.ajax({
            type: 'POST',
            url:"/transacciones/reportes/plazas/id/"+plaza_id,
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });	
}

function tecnico(plaza_id)
{
	
	$.ajax({
		type: 'POST',
		url:"/transacciones/reportes/usuarios/plaza_id/"+plaza_id+"/tipo_id/4",
		success:
		function respuesta(res)
		{
			var tecnicos='';
			$('#ftecnico').html(res);
			$("#ftecnico").multiselect({
				click: function(event, ui)
				{
					if(ui.checked==true)
					{
						if(tecnicos=='')
							tecnicos=ui.value;
						else
							tecnicos+='_'+ui.value;
					}
					else
					{
						tecnicos=tecnicos.replace("_"+ui.value,"");
						tecnicos=tecnicos.replace(ui.value,"");
					}

					$("#htecnico").attr("value",tecnicos);
				},
				uncheckAll:function()
				{
					$("#htecnico").attr("value",'');
				}
			});
			$("#ftecnico").multiselect("refresh");
		}
	});
}

function promotor(plaza_id)
{

	$.ajax({
		type: 'POST',
		url:"/transacciones/reportes/usuarios/plaza_id/"+plaza_id+"/tipo_id/3",
		success:
		function respuesta(res)
		{
			var promotores='';
			$('#fpromotor').html(res);
			$("#fpromotor").multiselect({
				click: function(event, ui)
				{
					if(ui.checked==true)
					{
						if(promotores=='')
							promotores=ui.value;
						else
							promotores+='_'+ui.value;
					}
					else
					{
						promotores=promotores.replace("_"+ui.value,"");
						promotores=promotores.replace(ui.value,"");
					}

					$("#hpromotor").attr("value",promotores);
				},
				uncheckAll:function()
				{
					$("#hpromotor").attr("value",'');
				}
			});
			$("#fpromotor").multiselect("refresh");
		}
	});
}

$(document).ready(function()
{       
    //$("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd', maxDate:'+0', minDate: $('#fdesde').attr('value') }); 
    //$("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd', maxDate:'+0' });
	
	$('#fdesde').datetimepicker({dateFormat:'yy-mm-dd'});
	$('#fhasta').datetimepicker({dateFormat:'yy-mm-dd'});
	
	var dia = new Date();
	var mes = dia.getMonth() + 1;
	var fecha = dia.getFullYear() + "-" + mes + "-" + dia.getDate()
	
	$('#fdesde').attr('value', fecha + " 00:00");
	//'2013-09-30 00:00');
	$('#fhasta').attr('value', fecha + " 23:59");
	//'2013-09-30 23:59');

	
	$('#fsucursal').change(function()
	{
		$.ajax({
			type: 'POST',
			url:"/transacciones/reportes/usuarios/plaza_id/"+$('#fplazas').val()+"/sucursal_id/"+$('#fsucursal').val()+"/tipo_id/4",
			success:
			function respuesta(res)
			{
				$('#ftecnico').html(res);
			}
		});
		
		$.ajax({
			type: 'POST',
			url:"/transacciones/reportes/usuarios/plaza_id/"+$('#fplazas').val()+"/sucursal_id/"+$('#fsucursal').val()+"/tipo_id/3",
			success:
			function respuesta(res)
			{
				$('#fpromotor').html(res);
			}
		});
		
	});
	
	
	var mexico = new google.maps.LatLng(latitud, longitud);
	
	var mapOptions = {
		zoom: 6,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: mexico
	};

	map = new google.maps.Map(document.getElementById('contenedor'), mapOptions);
	
	filtrar();
});


function limpiar()
{
    //$("#fdesde").attr('value','');
    //$("#fhasta").attr('value','');
	
	//$('#fdesde').attr('value', '2013-09-30 00:00');
	//$('#fhasta').attr('value', '2013-09-30 23:59');

	$('#fdesde').datetimepicker({dateFormat:'yy-mm-dd'});
	$('#fhasta').datetimepicker({dateFormat:'yy-mm-dd'});
	
	var dia = new Date();
	var mes = dia.getMonth() + 1;
	var fecha = dia.getFullYear() + "-" + mes + "-" + dia.getDate()
	
	$('#fdesde').attr('value', fecha + " 00:00");
	$('#fhasta').attr('value', fecha + " 23:59");
	
    $("#fplazas").attr('value','');	
	$("#fplaza").multiselect("widget").find(":checkbox").each(function()
	{
		if(this.checked)
			this.click();
	});
	
	$("#htecnico").attr("value",'');
	$("#ftecnico").attr('value',0);
	$("#hpromotor").attr("value",'');
	$("#fpromotor").attr('value',0);
	//$("#fpromotor").multiselect('destroy');
	//$("#ftecnico").multiselect('destroy');
	tecnico(-1);
	promotor(-1);

    $("#fsucursal").attr('value',0);
	$("#fsucursal").attr("disabled", false);
	
	$("#resultados").css('display','none');
	$("#contador").css('display','none');
	$("#contador").html('0');
	
	var mexico = new google.maps.LatLng(latitud, longitud);
	
	var mapOptions = {
		zoom: 6,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: mexico
	};
	
	map = new google.maps.Map(document.getElementById('mapa'), mapOptions);
	$('#fsucursal').change(function()
	{
		$.ajax({
			type: 'POST',
			url:"/transacciones/reportes/usuarios/plaza_id/"+$('#fplazas').val()+"/sucursal_id/"+$('#fsucursal').val()+"/tipo_id/4",
			success:
			function respuesta(res)
			{
				$('#ftecnico').html(res);
			}
		});
		
		$.ajax({
			type: 'POST',
			url:"/transacciones/reportes/usuarios/plaza_id/"+$('#fplazas').val()+"/sucursal_id/"+$('#fsucursal').val()+"/tipo_id/3",
			success:
			function respuesta(res)
			{
				$('#fpromotor').html(res);
			}
		});
		
	});
	
	
	var mexico = new google.maps.LatLng(latitud, longitud);
	
	var mapOptions = {
		zoom: 6,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: mexico
	};

	map = new google.maps.Map(document.getElementById('contenedor'), mapOptions);
	
	filtrar();
}

function filtrar()
{
	
	var filtro = "/transacciones/reportes/mapa";

    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
    }

    if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
    }
    
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
    }
    
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    }
	
	if($("#htecnico").val() !='')
    {
        filtro += "/tecnico/"+$("#htecnico").val();
    } 

    if($("#hpromotor").val() != '')
    {
        filtro += "/promotor/"+$("#hpromotor").val();
    } 
       
    mostrarMapa(filtro); 
}

function mostrarMapa(filtro) 
{
	
	$.ajax({
		type: 'POST',
		url: filtro,
		success:
		function respuesta(res)
		{
			//alert(filtro);
			$('#contenedor').html(res);
		}
	});
}


