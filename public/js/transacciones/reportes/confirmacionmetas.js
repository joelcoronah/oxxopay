var tipo;

$(document).ready(function()
{
	
	$("#fplaza").multiselect({
        noneSelectedText: "Todas las plazas",
        click: function(event, ui)
        {

        	//ui.value es para obtener el valor
        	//ui.checked regresa true si está seleccionado o false si no lo está
			
			valores=$("#fplazas").val();
			
			if(ui.checked==true)
			{
				//agrega la plaza a la cadena.
				if(valores=='')
					valores=ui.value;
				else
					valores=$("#fplazas").val()+'_'+ui.value;
					
				$("#fplazas").attr('value',valores);	
			}
			else
			{
				
				//elimina la plaza de la cadena.
				valoresNuevo='';
				separacion=valores.split('_');
				
				for(i=0;i<separacion.length;i++)
					if(separacion[i]!=ui.value)
						if(valoresNuevo=='')
							valoresNuevo=separacion[i];
						else
							valoresNuevo+='_'+separacion[i];
							
				$("#fplazas").attr('value',valoresNuevo);
			}
        },
		checkAll:function(){
			
			//todas las plazas
			$("#fplazas").attr('value',$("#plazas").val());
		}
		,
		uncheckAll:function(){
			
			//ninguna plaza
			$("#fplazas").attr('value','');
		}
    });
});

$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/reporteconfirmacionmetas/grid',
        dataType: 'xml',
        colModel : [
				{display: 'META',  name:'contrato',width : 100, sortable : true, align: 'center'},
                {display: 'PLAZA',  name:'nombre',width : 180, sortable : true, align: 'center'},
                {display: 'USUARIO',  name:'telefono',width : 200, sortable : true, align: 'center'},
				{display: 'ESTATUS',  name:'estatus',width : 100, sortable : false, align: 'center'},
                {display: 'FECHA DE CONFIRMACION',  name:'estatus',width : 200, sortable : false, align: 'center'}
                ],
        sortname: "Plaza.nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $( "#ffecha" ).datepicker(
    {
        beforeShowDay: function (date) 
        {
           if (date.getDate() == 1) 
               return [true, ''];
           else
               return [false, ''];
        },
        maxDate:"+0",
        dateFormat: "yy-mm-dd"
    });
});

function filtrar()
{
    var filtro = "/transacciones/reporteconfirmacionmetas/grid";

    if($("#ffecha").val() != "")
    {
        filtro += "/fecha/"+$("#ffecha").val();
    }
	
	if($("#festatus").val() != "")
    {
        filtro += "/estatus/"+$("#festatus").val();
    }

    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
    }
    
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{
	
	$("#ffecha").attr('value','');
    $("#festatus").attr('value',"");
	
	//$("#fplazas").attr('value','');	
	$("#fplaza").multiselect("widget").find(":checkbox").each(function()
	{
		if(this.checked)
			this.click();
	});
    
	filtrar(); 
}