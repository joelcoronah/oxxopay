var tipo;
//var hasta= Date();

$(document).ready(function()
{
    $("#fplaza").multiselect({
        noneSelectedText: "Todas las plazas",
        click: function(event, ui)
        {

        	//ui.value es para obtener el valor
        	//ui.checked regresa true si está seleccionado o false si no lo está
			
			valores=$("#fplazas").val();
			
			if(ui.checked==true)
			{
				//agrega la plaza a la cadena.
				if(valores=='')
					valores=ui.value;
				else
					valores=$("#fplazas").val()+'_'+ui.value;
					
				$("#fplazas").attr('value',valores);	
			}
			else
			{
				
				//elimina la plaza de la cadena.
				valoresNuevo='';
				separacion=valores.split('_');
				
				for(i=0;i<separacion.length;i++)
					if(separacion[i]!=ui.value)
						if(valoresNuevo=='')
							valoresNuevo=separacion[i];
						else
							valoresNuevo+='_'+separacion[i];
							
				$("#fplazas").attr('value',valoresNuevo);
			}
			
			valores=$("#fplazas").val();
			
			//si hay valores
			if(valores!='')
				if(valores.indexOf('_')>0) //si hay más de un valor
				{
					//limpia y deshabilita el combo de sucursal.
					sucursal(-1);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", true);
				}
				else
				{
					//selecciona las sucursales de la única plaza seleccionada, y habilita el combo.
					sucursal(valores);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", false);
				}
			else
			{
				//limpia y habilita el combo de sucursal.
				sucursal(-1);
				$("#fsucursal").attr("value", 0);
				$("#fsucursal").attr("disabled", false);
			}
        },
		checkAll:function(){
			
			//todas las plazas
			$("#fplazas").attr('value',$("#plazas").val());
			
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1);
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", true);
		}
		,
		uncheckAll:function(){
			
			//ninguna plaza
			$("#fplazas").attr('value','');
		
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1)
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", false);	
		}
    });
});

function sucursal(plaza_id)
{

	$.ajax({
            type: 'POST',
            url:"/transacciones/reportes/plazas/id/"+plaza_id,
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });	
}

$(document).ready(function()
{       
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd', maxDate:'+0', minDate: $('#fdesde').attr('value') }); 
    $("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd', maxDate:'+0' });  
    
	$("#flexgrid").flexigrid(
    {
        url: '/transacciones/reportes/gridexistencias',
        dataType: 'xml',
        colModel : [
                {display: 'CABLE', name:'cable', width : 160, sortable : true, align: 'center'},
                {display: 'CONECTOR', name:'conector', width : 160, sortable : true, align: 'center'},
                {display: 'DIVISOR2', name:'divisor2', width : 160, sortable : true, align: 'center'},
                {display: 'DIVISOR3', name:'divisor3', width : 160, sortable : true, align: 'center'},
				{display: 'DIVISOR4', name:'divisor4', width : 160, sortable : true, align: 'center'},
                {display: 'GRAPAS', name:'grapas', width : 160, sortable : true, align: 'center'},
                ],
        sortname: "cable",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });
    
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });   
});

function servicios()
{
    var filtro = "/transacciones/reportes/gridexistencias";
    var imprimir="/transacciones/reportes/imprimirexistencias";
    var exportar="/transacciones/reportes/exportarexistencias"; 	
	
    if($("#fdesde").val() != "")
    {
		filtro += "/desde/"+$("#fdesde").val();
        imprimir += "/desde/"+$("#fdesde").val();
        exportar += "/desde/"+$("#fdesde").val();
    }
    
    if($("#fhasta").val() != "")
    {
	filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();
    }
  	
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
        imprimir += "/plaza/"+$("#fplazas").val();
        exportar += "/plaza/"+$("#fplazas").val();
    }

    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();
    	exportar += "/sucursal/"+$("#fsucursal").val();    
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function filtrar()
{
    var filtro = "/transacciones/reportes/gridexistencias";

    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
    }

    if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
    }
    
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
    }
    
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    }
       
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{
    $("#fdesde").attr('value','');
    $("#fhasta").attr('value','');
	
    //$("#fplazas").attr('value','');	
	$("#fplaza").multiselect("widget").find(":checkbox").each(function()
	{
		if(this.checked)
			this.click();
	});
	
    $("#fsucursal").attr('value',0);
	$("#fsucursal").attr("disabled", false);

    var filtro = "/transacciones/reportes/gridpexistencias";
    var imprimir="/transacciones/reportes/imprimirexistencias";
    var exportar="/transacciones/reportes/exportarexistencias"; 
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}