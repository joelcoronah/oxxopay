var tipo;

$(document).ready(function()
{
    $("#fplaza").multiselect({
        noneSelectedText: "Todas las plazas",
        click: function(event, ui)
        {

        	//ui.value es para obtener el valor
        	//ui.checked regresa true si está seleccionado o false si no lo está
			
			valores=$("#fplazas").val();
			
			if(ui.checked==true)
			{
				//agrega la plaza a la cadena.
				if(valores=='')
					valores=ui.value;
				else
					valores=$("#fplazas").val()+'_'+ui.value;
					
				$("#fplazas").attr('value',valores);	
			}
			else
			{
				
				//elimina la plaza de la cadena.
				valoresNuevo='';
				separacion=valores.split('_');
				
				for(i=0;i<separacion.length;i++)
					if(separacion[i]!=ui.value)
						if(valoresNuevo=='')
							valoresNuevo=separacion[i];
						else
							valoresNuevo+='_'+separacion[i];
							
				$("#fplazas").attr('value',valoresNuevo);
			}
			
			valores=$("#fplazas").val();
			
			//si hay valores
			if(valores!='')
				if(valores.indexOf('_')>0) //si hay más de un valor
				{
					//limpia y deshabilita el combo de sucursal.
					sucursal(-1);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", true);	
				}
				else
				{
					//selecciona las sucursales de la única plaza seleccionada, y habilita el combo.
					sucursal(valores);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", false);
				}
			else
			{
				//limpia y habilita el combo de sucursal.
				sucursal(-1);
				$("#fsucursal").attr("value", 0);
				$("#fsucursal").attr("disabled", false);
			}
        },
		checkAll:function(){
			
			//todas las plazas
			$("#fplazas").attr('value',$("#plazas").val());
			
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1)
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", true);	
		}
		,
		uncheckAll:function(){
			
			//ninguna plaza
			$("#fplazas").attr('value','');
		
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1)
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", false);	
		}
    });
});

function sucursal(plaza_id)
{

	$.ajax({
            type: 'POST',
            url:"/transacciones/reportes/plazas/id/"+plaza_id,
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });	
}

$(document).ready(function() 
{  
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/reportes/gridllamadasdecobranza',
        dataType: 'xml',
        colModel : [
                {display: 'FECHA',  name:'updated_at',width : 150, sortable : true, align: 'center'},
                {display: 'CONTRATO', name:'contrato', width : 60, sortable : true, align: 'center'},
                {display: 'CLIENTE', name:'nombre', width : 200, sortable : true, align: 'center'},
                {display: 'PLAZA', name:'nombre', width : 120, sortable : true, align: 'center'},
                {display: 'TELEFONO', name:'telefono', width : 139, sortable : true, align: 'center'},
                {display: 'CELULAR', name:'celular1', width : 96, sortable : true, align: 'center'},
                {display: 'ESTATUS', name:'status', width : 142, sortable : true, align: 'center'}
                ],
        sortname: "updated_at",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    }); 
    
	/*
    $('#fplaza').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/transacciones/reportes/plazas/id/"+$('#fplaza').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });
    }); 
	*/
    
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });   
});

function servicios()
{
    var filtro = "/transacciones/reportes/gridllamadasdecobranza";
    var imprimir="/transacciones/reportes/imprimirllamadasdecobranza";
    var exportar="/transacciones/reportes/exportarllamadasdecobranza"; 	
	
    if($("#fnombre").val() != "")
    {
	filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
        exportar += "/nombre/"+$("#fnombre").val();
    }
    
    if($("#fcontrato").val() != "")
    {
	filtro += "/contrato/"+$("#fcontrato").val();
        imprimir += "/contrato/"+$("#fcontrato").val();
        exportar += "/contrato/"+$("#fcontrato").val();
    }
  	
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
        imprimir += "/plaza/"+$("#fplazas").val();
        exportar += "/plaza/"+$("#fplazas").val();
    }
    
    if($("#festatus").val() > 0)
    {	
    	filtro += "/estatus/"+$("#festatus").val();
    	imprimir += "/estatus/"+$("#festatus").val();
    	exportar += "/estatus/"+$("#festatus").val();    
    }
      
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();
    	exportar += "/sucursal/"+$("#fsucursal").val();    
    } 
   
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function filtrar()
{
    var filtro = "/transacciones/reportes/gridllamadasdecobranza";
	
    if($("#fnombre").val() != "")
    {
        filtro += "/nombre/"+$("#fnombre").val();
    }

    if($("#fcontrato").val() != "")
    {
        filtro += "/contrato/"+$("#fcontrato").val();
    }
    
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
    }
	
    if($("#festatus").val() > 0){
    	filtro += "/estatus/"+$("#festatus").val();
    }
    
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    } 
     
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{
    $("#fnombre").attr('value','');
    $("#fcontrato").attr('value','');
	
    //$("#fplazas").attr('value','');	
	$("#fplaza").multiselect("widget").find(":checkbox").each(function()
	{
		if(this.checked)
			this.click();
	});
	
    $("#festatus").attr('value',0);
    $("#fsucursal").attr('value',0);   
	$("#fsucursal").attr("disabled", false); 

    var filtro = "/transacciones/reportes/gridllamadasdecobranza";
    var imprimir="/transacciones/reportes/imprimirllamadasdecobranza";
    var exportar="/transacciones/reportes/exportarllamadasdecobranza"; 
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}