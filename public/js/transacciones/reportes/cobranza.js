var tipo;

$(document).ready(function()
{
    $("#fplaza").multiselect({
        noneSelectedText: "Todas las plazas",
        click: function(event, ui)
        {

        	//ui.value es para obtener el valor
        	//ui.checked regresa true si está seleccionado o false si no lo está
			
			valores=$("#fplazas").val();
			
			if(ui.checked==true)
			{
				//agrega la plaza a la cadena.
				if(valores=='')
					valores=ui.value;
				else
					valores=$("#fplazas").val()+'_'+ui.value;
					
				$("#fplazas").attr('value',valores);	
			}
			else
			{
				
				//elimina la plaza de la cadena.
				valoresNuevo='';
				separacion=valores.split('_');
				
				for(i=0;i<separacion.length;i++)
					if(separacion[i]!=ui.value)
						if(valoresNuevo=='')
							valoresNuevo=separacion[i];
						else
							valoresNuevo+='_'+separacion[i];
							
				$("#fplazas").attr('value',valoresNuevo);
			}
			
			valores=$("#fplazas").val();
			
			//si hay valores
			if(valores!='')
				if(valores.indexOf('_')>0) //si hay más de un valor
				{
					//limpia y deshabilita el combo de sucursal.
					sucursal(-1);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", true);	
				}
				else
				{
					//selecciona las sucursales de la única plaza seleccionada, y habilita el combo.
					sucursal(valores);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", false);
				}
			else
			{
				//limpia y habilita el combo de sucursal.
				sucursal(-1);
				$("#fsucursal").attr("value", 0);
				$("#fsucursal").attr("disabled", false);
			}
        },
		checkAll:function(){
			
			//todas las plazas
			$("#fplazas").attr('value',$("#plazas").val());
			
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1)
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", true);	
		}
		,
		uncheckAll:function(){
			
			//ninguna plaza
			$("#fplazas").attr('value','');
		
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1)
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", false);	
		}
    });
});

function sucursal(plaza_id)
{

	$.ajax({
            type: 'POST',
            url:"/transacciones/reportes/plazas/id/"+plaza_id,
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });	
}

$(document).ready(function() 
{  	
	$("#ffecha").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', onSelect:function(date){
         $("#fhasta").datepicker( "option", "minDate", date );   
    }});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});
	
	
	
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/reportes/gridcobranza',
        dataType: 'xml',
        colModel : [
				{display: 'FOLIO',  name:'id',width : 40, sortable : true, align: 'center'},
        		{display: 'FECHA',  name:'updated_at',width : 107, sortable : true, align: 'center'},
				{display: 'CONTRATO',  name:'Cliente.contrato',width : 70, sortable : true, align: 'center'},
                {display: 'CLIENTE', name:'Cliente.nombre', width : 220, sortable : true, align: 'center'},
				{display: 'PLAZA', name:'Cliente.nombre', width : 33, sortable : true, align: 'center'},				
                {display: 'CAJERO', name:'usuario_id', width : 170, sortable : true, align: 'center'},	
                {display: 'MONTO', name:'total', width : 50, sortable : true, align: 'center'},				
				{display: 'CONCEPTO', name:'usuario_id', width : 170, sortable : false, align: 'center'},
				{display: 'MES CUBIERTO', name:'usuario_id', width : 90, sortable : false, align: 'center'},
				{display: 'ESTATUS', name:'estatus', width : 60, sortable : false, align: 'center'},
				{display: 'CANCELADO POR',name:'cancelado_por', width : 100, sortable : false, align: 'center'},
                {display: '', width : 33, sortable : false, align: 'center'},
				{display: '', width : 33, sortable : false, align: 'center'},
                ],
        sortname: "id",
        sortorder: "DESC",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    }); 
    
	/*
    $('#fplaza').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/transacciones/reportes/plazas/id/"+$('#fplaza').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });
    }); */

    
    
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });   
});

function cancelarPago(id)
{
    $.ajax({
        url:"/transacciones/cobranza/cancelarpago/id/"+id,
        success:function result(data)
        {    
            $(".dialog-form").html(data);

                $("#frmCancelarpago").validate(
                {
                   submitHandler: function(form) 
                   {
					  if(confirm('Realmente desea cancelar este pago? Esta acción no podrá ser revertida')) 
					  {
						 $("#Aceptar").hide(); 
						 $(form).ajaxSubmit(
						 {
							success:    function(response) 
							{ 
								alert('El pago ha sido cancelado');
								$(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
								$('#flexgrid').flexReload();
								
								
							}
						 });
					  }
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Cancelar pago",
                        position: [240,100],
                        modal: true,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            $("#frmCancelarpago").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                                         
            }
    });
}

function servicios()
{
    var filtro = "/transacciones/reportes/gridcobranza";
    var imprimir="/transacciones/reportes/imprimircobranza";
    var exportar="/transacciones/reportes/exportarcobranza"; 	
	
    if($("#fnombre").val() != "")
    {
	filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
        exportar += "/nombre/"+$("#fnombre").val();
    }
    
	if($("#fcontrato").val() != "")
    {
		filtro += "/contrato/"+$("#fcontrato").val();
        imprimir += "/contrato/"+$("#fcontrato").val();
        exportar += "/contrato/"+$("#fcontrato").val();
    }   
	
    if($("#ffecha").val() != "")
    {
		filtro += "/fecha/"+$("#ffecha").val();
        imprimir += "/fecha/"+$("#ffecha").val();
        exportar += "/fecha/"+$("#ffecha").val();
    }
	
	if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
		imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();
    }
  	
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
        imprimir += "/plaza/"+$("#fplazas").val();
        exportar += "/plaza/"+$("#fplazas").val();
    }
    
    if($("#festatus").val() > 0)
    {	
    	filtro += "/estatus/"+$("#festatus").val();
    	imprimir += "/estatus/"+$("#festatus").val();
    	exportar += "/estatus/"+$("#festatus").val();    
    }
        
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();
    	exportar += "/sucursal/"+$("#fsucursal").val();    
    }
	
	if($("#fconcepto").val() > 0)
    {	
    	filtro += "/concepto/"+$("#fconcepto").val();
    	imprimir += "/concepto/"+$("#fconcepto").val();
    	exportar += "/concepto/"+$("#fconcepto").val();    
    }
       
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function filtrar()
{
    var filtro = "/transacciones/reportes/gridcobranza";
	
    if($("#fnombre").val() != "")
    {
        filtro += "/nombre/"+$("#fnombre").val();
    }

	if($("#fcontrato").val() != "")
    {
		filtro += "/contrato/"+$("#fcontrato").val();
    }   
	
    if($("#ffecha").val() != "")
    {
        filtro += "/fecha/"+$("#ffecha").val();
    }
   	
    if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
    }
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
    }
       
    if($("#fsucursal").val() > 0)
    {
        filtro += "/sucursal/"+$("#fsucursal").val();
    }
	
    if($("#festatus").val() > 0){
    	filtro += "/estatus/"+$("#festatus").val();
    }
	
	if($("#fconcepto").val() > 0)
    {	
    	filtro += "/concepto/"+$("#fconcepto").val();
    }
	
    
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{
    $("#fnombre").attr('value','');
    $("#ffecha").attr('value','');
	$("#fhasta").attr('value','');
	
    //$("#fplazas").attr('value','');	
	$("#fplaza").multiselect("widget").find(":checkbox").each(function()
	{
		if(this.checked)
			this.click();
	});
	
    $("#fsucursal").attr('value',0);
	$("#fsucursal").attr("disabled", false);
	
    $("#festatus").attr('value',0);
	$("#fconcepto").attr('value',0);

    var filtro = "/transacciones/reportes/gridcobranza";
    var imprimir="/transacciones/reportes/imprimircobranza";
    var exportar="/transacciones/reportes/exportarcobranza"; 
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}