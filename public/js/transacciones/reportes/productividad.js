var tipo;
//var hasta= Date();

$(document).ready(function()
{
    $("#fplaza").multiselect({
        noneSelectedText: "Todas las plazas",
        click: function(event, ui)
        {

        	//ui.value es para obtener el valor
        	//ui.checked regresa true si está seleccionado o false si no lo está
			
			valores=$("#fplazas").val();
			
			if(ui.checked==true)
			{
				//agrega la plaza a la cadena.
				if(valores=='')
					valores=ui.value;
				else
					valores=$("#fplazas").val()+'_'+ui.value;
					
				$("#fplazas").attr('value',valores);	
			}
			else
			{
				
				//elimina la plaza de la cadena.
				valoresNuevo='';
				separacion=valores.split('_');
				
				for(i=0;i<separacion.length;i++)
					if(separacion[i]!=ui.value)
						if(valoresNuevo=='')
							valoresNuevo=separacion[i];
						else
							valoresNuevo+='_'+separacion[i];
							
				$("#fplazas").attr('value',valoresNuevo);
			}
			
			valores=$("#fplazas").val();
			
			//si hay valores
			if(valores!='')
				if(valores.indexOf('_')>0) //si hay más de un valor
				{
					//limpia y deshabilita el combo de sucursal.
					sucursal(-1);
					tecnico(-1);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", true);
					
					$("#ftecnico").attr("value", 0);
					$("#ftecnico").attr("disabled", true);	
				}
				else
				{
					//selecciona las sucursales de la única plaza seleccionada, y habilita el combo.
					sucursal(valores);
					tecnico(valores);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", false);
					
					$("#ftecnico").attr("value", 0);
					$("#ftecnico").attr("disabled", false);
				}
			else
			{
				//limpia y habilita el combo de sucursal.
				sucursal(-1);
				tecnico(-1);
				$("#fsucursal").attr("value", 0);
				$("#fsucursal").attr("disabled", false);
				
				$("#ftecnico").attr("value", 0);
				$("#ftecnico").attr("disabled", false);
			}
        },
		checkAll:function(){
			
			//todas las plazas
			$("#fplazas").attr('value',$("#plazas").val());
			
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1);
			tecnico(-1);
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", true);	
			
			$("#ftecnico").attr("value", 0);
			$("#ftecnico").attr("disabled", true);
		}
		,
		uncheckAll:function(){
			
			//ninguna plaza
			$("#fplazas").attr('value','');
		
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1)
			tecnico(-1)
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", false);	
			
			$("#ftecnico").attr("value", 0);
			$("#ftecnico").attr("disabled", false);
		}
    });
});

function sucursal(plaza_id)
{

	$.ajax({
            type: 'POST',
            url:"/transacciones/reportes/plazas/id/"+plaza_id,
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });	
}

function tecnico(plaza_id)
{
	
	$.ajax({
		type: 'POST',
		url:"/transacciones/reportes/usuarios/plaza_id/"+plaza_id+"/tipo_id/4",
		success:
		function respuesta(res)
		{
			$('#ftecnico').html(res);
		}
	});  
}

$(document).ready(function()
{       
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd', maxDate:'+0', minDate: $('#fdesde').attr('value') }); 
    $("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd', maxDate:'+0' });  
    
	$('#fsucursal').change(function()
	{
		
		$.ajax({
			type: 'POST',
			url:"/transacciones/reportes/usuarios/plaza_id/"+$('#fplazas').val()+"/sucursal_id/"+$('#fsucursal').val()+"/tipo_id/4/tecnico_id/"+$('#ftecnico').val(),
			success:
			function respuesta(res)
			{
				$('#ftecnico').html(res);
			}
		});
	}); 
    
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/reportes/gridproductividad/',
        dataType: 'xml',
        colModel : [
                {display: 'FECHA', width : 55, sortable : false, align: 'center'},
                {display: 'TÉCNICO', name:'Cliente.nombre', width : 200, sortable : false, align: 'center'},
                {display: 'PLAZA', name:'plaza_id', width : 90, sortable : false, align: 'center'},

                {display: 'NÚM. DE PTS. T. INTER.', width : 150, sortable : false, align: 'center'},
                {display: 'NÚM. DE PTS. T. CABLE.', width : 150, sortable : false, align: 'center'},

                {display: '   NÚM. DE INST. INTER.', width : 124, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. INTER.', width : 124, sortable : false, align: 'center'},
                {display: '   NÚM. DE INST. CABLE', width : 124, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. CABLE', width : 124, sortable : false, align: 'center'},

                {display: 'NÚM. DE RECON. INTER.', width : 126, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. INTER.', width : 124, sortable : false, align: 'center'},       
                {display: 'NÚM. DE RECON. CABLE', width : 130, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. CABLE', width : 129, sortable : false, align: 'center'},  

                {display: 'NÚM. DE CAMB. DE DOM. INTER.', width : 165, sortable : false, align: 'center'},   
                {display: '-  PTS. GAN. INTER.', width : 124, sortable : false, align: 'center'},  
                {display: 'NÚM. DE CAMB. DE DOM. CABLE.', width : 168, sortable : false, align: 'center'},   
                {display: '-  PTS. GAN. CABLE', width : 124, sortable : false, align: 'center'},       

                {display: 'NÚM. DE ATEN. A FALL. INTER.', width : 162, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. INTER.', width : 124, sortable : false, align: 'center'},  
                {display: 'NÚM. DE ATEN. A FALL. CABLE.', width : 165, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. CABLE', width : 124, sortable : false, align: 'center'}, 

                {display: 'NÚM. DE EXT. ADI. INTER.', width : 133, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. INTER.', width : 124, sortable : false, align: 'center'},
                {display: 'NÚM. DE EXT. ADI. CABLE', width : 130, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. CABLE', width : 129, sortable : false, align: 'center'},

                {display: 'NÚM. DE RECUP. INTER.', width : 124, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. INTER.', width : 124, sortable : false, align: 'center'},
                {display: 'NÚM. DE RECUP. CABLE', width : 129, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. CABLE', width : 124, sortable : false, align: 'center'},

                {display: 'NÚM. DE AUDI. ACOMET. INTER.', width : 169, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. INTER.', width : 124, sortable : false, align: 'center'},
                {display: 'NÚM. DE AUDI. ACOMET. CABLE', width : 165, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. CABLE', width : 124, sortable : false, align: 'center'},

                {display: 'NÚM. DE REINS. INTER.', width : 124, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. INTER.', width : 124, sortable : false, align: 'center'},
                {display: 'NÚM. DE REINS. CABLE', width : 124, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. CABLE', width : 124, sortable : false, align: 'center'},

                {display: 'NÚM. DE RETIROS INTER.', width : 135, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. INTER.', width : 124, sortable : false, align: 'center'},
                {display: 'NÚM. DE RETIROS CABLE', width : 134, sortable : false, align: 'center'},
                {display: '-  PTS. GAN. CABLE', width : 134, sortable : false, align: 'center'},
                
                ],
        sortname: "id",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1121,
        height: 400
    });
    
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });   
});

function servicios()
{
    var filtro = "/transacciones/reportes/gridproductividad";
    var imprimir="/transacciones/reportes/imprimirproductividad";
    var exportar="/transacciones/reportes/exportarproductividad"; 	
	
    if($("#fdesde").val() != "")
    {
	filtro += "/desde/"+$("#fdesde").val();
        imprimir += "/desde/"+$("#fdesde").val();
        exportar += "/desde/"+$("#fdesde").val();
    }
    
    if($("#fhasta").val() != "")
    {
	filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();
    }
  	
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
        imprimir += "/plaza/"+$("#fplazas").val();
        exportar += "/plaza/"+$("#fplazas").val();
    }
  	
    if($("#ftecnico").val() > 0)
    {
        filtro += "/tecnico/"+$("#ftecnico").val();
        imprimir += "/tecnico/"+$("#ftecnico").val();
        exportar += "/tecnico/"+$("#ftecnico").val();
    }

    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();
    	exportar += "/sucursal/"+$("#fsucursal").val();    
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function filtrar()
{
    var filtro = "/transacciones/reportes/gridproductividad";

    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
    }

    if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
    }
    
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
    }
    
    if($("#ftecnico").val() > -1)
    {
        filtro += "/tecnico/"+$("#ftecnico").val();
    } 
    
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    }
       
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{
    $("#fdesde").attr('value','');
    $("#fhasta").attr('value','');
	
    //$("#fplazas").attr('value','');	
	$("#fplaza").multiselect("widget").find(":checkbox").each(function()
	{
		if(this.checked)
			this.click();
	});
	
    $("#ftecnico").attr('value',0);
    $("#fsucursal").attr('value',0);
	$("#fsucursal").attr("disabled", false);

    var filtro = "/transacciones/reportes/gridproductividad";
    var imprimir="/transacciones/reportes/imprimirproductividad";
    var exportar="/transacciones/reportes/exportarproductividad"; 
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}