var tipo;

$(document).ready(function()
{   

    $("#ffecha").datetimepicker({controlType: 'select', changeYear:true, dateFormat:'yy-mm-dd', timeFormat: 'HH:00:00', onSelect:function(date){
        $("#fhasta").datepicker( "option", "minDate", date );   
    }});    

    $("#fhasta").datetimepicker({controlType: 'select', changeYear:true, dateFormat:'yy-mm-dd', timeFormat: 'HH:59:59'});

});


$(document).ready(function() 
{  
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/reportes/gridenviosms',
        dataType: 'xml',
        colModel : [
                {display: 'FECHA', name:'created_at',width : 178, sortable : true, align: 'center'},
                {display: 'CLIENTE', name:'Cliente.nombre', width : 300, sortable : true, align: 'center'},
                {display: 'CELULAR', name:'Cliente.telefono', width : 300, sortable : true, align: 'center'},
                {display: 'ESTATUS DE ENVIO', name:'estatus', width : 300, sortable : false, align: 'center'}
                ],
        sortname: "created_at",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1121,
        height: 400
    });
        
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });   
});

function servicios()
{
    var filtro = "/transacciones/reportes/gridenviosms";
    var imprimir="/transacciones/reportes/imprimirenviosms";
    var exportar="/transacciones/reportes/exportarenviosms"; 	
	
    if($("#fnombre").val() != '')
    {
		filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
        exportar += "/nombre/"+$("#fnombre").val();
    }
  	
    if($("#fplaza").val() != '')
    {
        filtro += "/plaza/"+$("#fplaza").val();
        imprimir += "/plaza/"+$("#fplaza").val();
        exportar += "/plaza/"+$("#fplaza").val();
    } 
	
    if($("#festatus").val() != '')
    {
        filtro += "/estatus/"+$("#festatus").val();
        imprimir += "/estatus/"+$("#festatus").val();
        exportar += "/estatus/"+$("#festatus").val();
    } 

    if($("#fnotificacion").val() != '')
    {
        filtro += "/notificacion/"+$("#fnotificacion").val();
        imprimir += "/notificacion/"+$("#fnotificacion").val();
        exportar += "/notificacion/"+$("#fnotificacion").val();
    } 

    if($("#ffecha").val() != '')
    {
        filtro += "/desde/"+$("#ffecha").val();
        imprimir += "/desde/"+$("#ffecha").val();
        exportar += "/desde/"+$("#ffecha").val();
    }

    if($("#fhasta").val() != '')
    {
        filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();
    }
        
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function filtrar()
{
    var filtro = "/transacciones/reportes/gridenviosms";

    if($("#fnombre").val() != '')
        filtro += "/nombre/"+$("#fnombre").val();
    
    if($("#fplaza").val() != '')
        filtro += "/plaza/"+$("#fplaza").val();
    
    if($("#festatus").val() != '')
        filtro += "/estatus/"+$("#festatus").val();
    
    if($("#fnotificacion").val() != '')
    	filtro += "/notificacion/"+$("#fnotificacion").val();

    if($("#ffecha").val() != '')
        filtro += "/desde/"+$("#ffecha").val();

    if($("#fhasta").val() != '')
        filtro += "/hasta/"+$("#fhasta").val();
    
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{
    $("#fnombre").attr('value','');
    $("#fplaza").attr('value',0);	
    $("#festatus").attr('value',0);
    $("#fnotificacion").attr('value',0);
    
    var filtro = "/transacciones/reportes/gridenviosms";
    var imprimir="/transacciones/reportes/imprimirenviosms";
    var exportar="/transacciones/reportes/exportarenviosms"; 
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function actualizar()
{
    $('#btnActualizar').attr("href","/transacciones/reportes/actualizaenviosms");
}