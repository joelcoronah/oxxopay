var tipo;

$(document).ready(function()
{
	
    $("#fplaza").multiselect({
        noneSelectedText: "Todas las plazas",
        click: function(event, ui)
        {

        	//ui.value es para obtener el valor
        	//ui.checked regresa true si está seleccionado o false si no lo está
			
			valores=$("#fplazas").val();
			
			if(ui.checked==true)
			{
				//agrega la plaza a la cadena.
				if(valores=='')
					valores=ui.value;
				else
					valores=$("#fplazas").val()+'_'+ui.value;
					
				$("#fplazas").attr('value',valores);	
			}
			else
			{
				
				//elimina la plaza de la cadena.
				valoresNuevo='';
				separacion=valores.split('_');
				
				for(i=0;i<separacion.length;i++)
					if(separacion[i]!=ui.value)
						if(valoresNuevo=='')
							valoresNuevo=separacion[i];
						else
							valoresNuevo+='_'+separacion[i];
							
				$("#fplazas").attr('value',valoresNuevo);
			}
			
			valores=$("#fplazas").val();
			
			//si hay valores
			if(valores!='')
				if(valores.indexOf('_')>0) //si hay más de un valor
				{
					//limpia y deshabilita el combo de sucursal.
					sucursal(-1);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", true);	
				}
				else
				{
					//selecciona las sucursales de la única plaza seleccionada, y habilita el combo.
					sucursal(valores);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", false);
				}
			else
			{
				//limpia y habilita el combo de sucursal.
				sucursal(-1);
				$("#fsucursal").attr("value", 0);
				$("#fsucursal").attr("disabled", false);
			}
        },
		checkAll:function(){
			
			//todas las plazas
			$("#fplazas").attr('value',$("#plazas").val());
			
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1)
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", true);	
		}
		,
		uncheckAll:function(){
			
			//ninguna plaza
			$("#fplazas").attr('value','');
		
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1)
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", false);	
		}
    });

	$("#ffecha").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', onSelect:function(date){
         $("#fhasta").datepicker( "option", "minDate", date );   
    }});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});
		
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/reporteservicio/grid/fecha/'+$("#ffecha").val()+'/hasta/'+$("#fhasta").val(),
        dataType: 'xml',
        colModel : [
				{display: 'SERVICIO',  name:'id',width : 300, sortable : true, align: 'center'},
        		{display: 'CANTIDAD',  name:'updated_at',width : 300, sortable : true, align: 'center'},
				{display: 'VER',  name:'Cliente.contrato',width : 300, sortable : true, align: 'center'}
                ],
        sortname: "S.nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1122,
        height: 400
    }); 
    
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });   
});

function sucursal(plaza_id)
{
	
	if(plaza_id != -1){
	$.ajax({
            type: 'POST',
            url:"/transacciones/reportes/plazas/id/"+plaza_id,
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });	
		
		$.ajax({
            type: 'POST',
            url:"/transacciones/reporteservicio/vendedores/id/"+plaza_id,
            success:
            function respuesta(res)
            {
               $('#fvendedor').html(res);
            }
        });			
	}
}

function detalle(id)
{    
		$.ajax({
				url:"/transacciones/reporteservicio/detalle/id/"+id,
				method:"post",
				success:function result(data)
				{    
					$(".dialog-form").html(data);
					
					var filtro = "/transacciones/reporteservicio/detallegrid/id/"+id;  
	
    				if($("#ffecha").val() != "")
    				{
        				filtro += "/fecha/"+$("#ffecha").val();
    				}
   	
    				if($("#fhasta").val() != "")
    				{
        				filtro += "/hasta/"+$("#fhasta").val();
    				}
    				if($("#fplazas").val() != '')
    				{
        				filtro += "/plaza/"+$("#fplazas").val();
    				}
       
    				if($("#fsucursal").val() > 0)
    				{
        				filtro += "/sucursal/"+$("#fsucursal").val();
    				}	
	
					if($("#fvendedor").val() > 0)
    				{
        				filtro += "/vendedor/"+$("#fvendedor").val();
    				}	
					
					$("#flexgrid1").flexigrid(
    				{
        				url: filtro,
        				dataType: 'xml',
        				colModel : [
							{display: 'CONTRATO',  name:'id',width : 100, sortable : false, align: 'center'},
							{display: 'CLIENTE',  name:'id',width : 250, sortable : false, align: 'center'},
							{display: 'VENDEDOR',  name:'id',width : 250, sortable : false, align: 'center'}
                		],
        				sortname: "C.nombre",
        				sortorder: "asc",
        				usepager: true,
        				useRp: false,
        				rp: 10,
        				width: 640,
        				height: 350
    				}); 
    
    				$("#flexgrid1").ready(function(){
        				$(".pReload").hide();
    				});   
					
					$(".dialog-form").dialog({
							height:'auto',
							width: 'auto',
							resizable: false,
							title: "CLIENTES",
							position: "center",
							modal: true,
							buttons: [									
									{
										id:"Cancelar",
										text:"Cerrar",
										click:function()
										{
											$(".dialog-form").dialog("destroy");
											$(".dialog-form").dialog("close");
										}
									}
							]
							});                     
				}
		});
}

function servicios()
{
    var filtro = "/transacciones/reporteservicio/grid";
    var imprimir="/transacciones/reporteservicio/imprimir";
    var exportar="/transacciones/reporteservicio/exportar"; 	

    if($("#ffecha").val() != "")
    {
		filtro += "/fecha/"+$("#ffecha").val();
        imprimir += "/fecha/"+$("#ffecha").val();
        exportar += "/fecha/"+$("#ffecha").val();
    }
	
	if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
		imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();
    }
  	
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
        imprimir += "/plaza/"+$("#fplazas").val();
        exportar += "/plaza/"+$("#fplazas").val();
    }
        
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();
    	exportar += "/sucursal/"+$("#fsucursal").val();    
    }
     	  
	if($("#fvendedor").val() > 0)
    {	
    	filtro += "/vendedor/"+$("#fvendedor").val();
    	imprimir += "/vendedor/"+$("#fvendedor").val();
    	exportar += "/vendedor/"+$("#fvendedor").val();    
    }   
	   
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function filtrar()
{
    var filtro = "/transacciones/reporteservicio/grid";  
	
    if($("#ffecha").val() != "")
    {
        filtro += "/fecha/"+$("#ffecha").val();
    }
   	
    if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
    }
	
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
    }
       
    if($("#fsucursal").val() > 0)
    {
        filtro += "/sucursal/"+$("#fsucursal").val();
    }	
	
	if($("#fvendedor").val() > 0)
    {
        filtro += "/vendedor/"+$("#fvendedor").val();
    }
    
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{
    $("#ffecha").attr('value','');
	$("#fhasta").attr('value','');
	
    //$("#fplazas").attr('value','');	
	$("#fplaza").multiselect("widget").find(":checkbox").each(function()
	{
		if(this.checked)
			this.click();
	});
	
    $("#fsucursal").attr('value',0);
	$("#fsucursal").attr("disabled", false);
	
	$("#fvendedor").attr('value',0);
	$("#fvendedor").attr("disabled", false);
	
    var filtro = "/transacciones/reporteservicio/grid";
    var imprimir="/transacciones/reporteservicio/imprimir";
    var exportar="/transacciones/reporteservicio/exportar"; 
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}