var tipo;
$(document).ready(function()
{
    $("#fplaza").multiselect({
        noneSelectedText: "Todas las plazas",
        click: function(event, ui)
        {

        	//ui.value es para obtener el valor
        	//ui.checked regresa true si está seleccionado o false si no lo está
			
			valores=$("#fplazas").val();
			
			if(ui.checked==true)
			{
				//agrega la plaza a la cadena.
				if(valores=='')
					valores=ui.value;
				else
					valores=$("#fplazas").val()+'_'+ui.value;
					
				$("#fplazas").attr('value',valores);	
			}
			else
			{
				
				//elimina la plaza de la cadena.
				valoresNuevo='';
				separacion=valores.split('_');
				
				for(i=0;i<separacion.length;i++)
					if(separacion[i]!=ui.value)
						if(valoresNuevo=='')
							valoresNuevo=separacion[i];
						else
							valoresNuevo+='_'+separacion[i];
							
				$("#fplazas").attr('value',valoresNuevo);
			}
			
			valores=$("#fplazas").val();
			
			//si hay valores
			if(valores!='')
				if(valores.indexOf('_')>0) //si hay más de un valor
				{
					//limpia y deshabilita el combo de sucursal.
					sucursal(-1);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", true);	
				}
				else
				{
					//selecciona las sucursales de la única plaza seleccionada, y habilita el combo.
					sucursal(valores);
					$("#fsucursal").attr("value", 0);
					$("#fsucursal").attr("disabled", false);
				}
			else
			{
				//limpia y habilita el combo de sucursal.
				sucursal(-1);
				$("#fsucursal").attr("value", 0);
				$("#fsucursal").attr("disabled", false);
			}
        },
		checkAll:function(){
			
			//todas las plazas
			$("#fplazas").attr('value',$("#plazas").val());
			
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1)
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", true);	
		}
		,
		uncheckAll:function(){
			
			//ninguna plaza
			$("#fplazas").attr('value','');
		
			//limpia y deshabilita el combo de sucursal.
			sucursal(-1)
			$("#fsucursal").attr("value", 0);
			$("#fsucursal").attr("disabled", false);	
		}
    });
	$("#fplaza").multiselect("checkAll");
});

function sucursal(plaza_id)
{

	$.ajax({
            type: 'POST',
            url:"/transacciones/reportes/plazas/id/"+plaza_id,
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });	
}

$(document).ready(function() 
{  
    $("#ffecha").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});	
	
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/reportes/gridcortedecaja',
        dataType: 'xml',
        colModel : [
        {display: 'FECHA Y HORA',  name:'created_at',width : 101, sortable : false, align: 'center'},
        {display: 'CONTRATO',  name:'updated_at',width : 100, sortable : false, align: 'center'},
        {display: 'COLONIA',  name:'updated_at',width : 100, sortable : false, align: 'center'},
        {display: 'PLAZA',  name:'updated_at',width : 50, sortable : false, align: 'center'},
        {display: 'CONCEPTO',  name:'updated_at',width : 245, sortable : false, align: 'center'},
        {display: 'SERVICIO',  name:'servicio',width : 245, sortable : false, align: 'center'},
        {display: 'ENTRADA',  name:'Cliente.contrato',width : 100, sortable : false, align: 'center'},
        {display: 'SALIDA', name:'Cliente.nombre', width : 100, sortable : false, align: 'center'},
        {display: 'SALDO', name:'usuario_id', width : 95, sortable : false, align: 'center'},
        {display: 'ORIGEN', name:'origen', width : 95, sortable : false, align: 'center'},
                ],
        sortname: "id",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1120,
        height: 400
    });
    
    $("#flexgrid").ready(function(){
    /*    $.ajax({
            url:"/transacciones/reportes/calcular",
            success:function result(data)
            {    
		$("#fdepositar").attr('value',data);	  
	    }
 	});*/
		
        $(".pReload").hide();
    });  
});

function servicios()
{
    var filtro = "/transacciones/reportes/gridcortedecaja";
    var imprimir="/transacciones/reportes/imprimircortedecaja";
    var exportar="/transacciones/reportes/exportarcortedecaja";
    var calcular="/transacciones/reportes/calcular"; 
		
    if($("#ffecha").val() != "")
    {
	filtro += "/fecha/"+$("#ffecha").val();
        imprimir += "/fecha/"+$("#ffecha").val();
        exportar += "/fecha/"+$("#ffecha").val();
	calcular += "/fecha/"+$("#ffecha").val();
    }

    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
        imprimir += "/plaza/"+$("#fplazas").val();
        exportar += "/plaza/"+$("#fplazas").val();
	calcular += "/plaza/"+$("#fplazas").val();
    }

    if($("#fhasta").val() != "")
    {
	filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();
	calcular += "/hasta/"+$("#fhasta").val();
    }
    
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();
    	exportar += "/sucursal/"+$("#fsucursal").val(); 
        calcular += "/sucursal/"+$("#fsucursal").val();
    }    
	if($("#ftipo").val() >= 0)
    {   
        filtro += "/tipo/"+$("#ftipo").val();
        imprimir += "/tipo/"+$("#ftipo").val();
        exportar += "/tipo/"+$("#ftipo").val(); 
        calcular += "/tipo/"+$("#ftipo").val();
    } 

    if($("#fproviene").val() >= 0)
    {   
        filtro += "/proviene/"+$("#fproviene").val();
        imprimir += "/proviene/"+$("#fproviene").val();
        exportar += "/proviene/"+$("#fproviene").val(); 
        calcular += "/proviene/"+$("#fproviene").val();
    } 
		
    $.ajax({
        url:calcular,
        success:function result(data)
        {    
            $("#fdepositar").attr('value',data);	  
       	}
    });		
   
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function filtrar()
{
    var filtro = "/transacciones/reportes/gridcortedecaja";
    var calcular = "/transacciones/reportes/calcular";
		
    if($("#ffecha").val() != "")
    {
        filtro += "/fecha/"+$("#ffecha").val();
	calcular += "/fecha/"+$("#ffecha").val();
    }
	if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
	calcular += "/hasta/"+$("#fhasta").val();
    }
	
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
	calcular += "/plaza/"+$("#fplazas").val();
    }
    
    if($("#fsucursal").val() > 0)
    {
        filtro += "/sucursal/"+$("#fsucursal").val();
	calcular += "/sucursal/"+$("#fsucursal").val();
    }
    
	if($("#ftipo").val() >= 0)
    {
        filtro += "/tipo/"+$("#ftipo").val();
        calcular += "/tipo/"+$("#ftipo").val();
    }

    if($("#fproviene").val() >= 0)
    {
        filtro += "/proviene/"+$("#fproviene").val();
        calcular += "/proviene/"+$("#fproviene").val();
    }

    $.ajax({
        url:calcular,
        success:function result(data)
        {    
            $("#fdepositar").attr('value',data);	  
       	}
    });
		
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{
    //$("#fplazas").attr('value','');	
	$("#fplaza").multiselect("widget").find(":checkbox").each(function()
	{
		if(this.checked)
			this.click();
	});
	
    $("#ffecha").attr('value','');
    $("#fhasta").attr('value','');
	
    $("#fsucursal").attr('value',0);
	$("#fsucursal").attr("disabled", false);
	$("#ftipo").attr('value',0);
    $("#fproviene").attr('value',0);
	
    var filtro = "/transacciones/reportes/gridcortedecaja";
    var imprimir="/transacciones/reportes/imprimircortedecaja";
    var exportar="/transacciones/reportes/exportarcortedecaja"; 
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload();
    
    $.ajax({
        url:"/transacciones/reportes/calcular",
        success:function result(data)
        {    
            $("#fdepositar").attr('value',data);	  
	}
    });
}