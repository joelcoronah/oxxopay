var tipo;

$(document).ready(function()
{
	
	$("#fplaza").multiselect({
        noneSelectedText: "Todas las plazas",
        click: function(event, ui)
        {

        	//ui.value es para obtener el valor
        	//ui.checked regresa true si está seleccionado o false si no lo está
			
			valores=$("#fplazas").val();
			
			if(ui.checked==true)
			{
				//agrega la plaza a la cadena.
				if(valores=='')
					valores=ui.value;
				else
					valores=$("#fplazas").val()+'_'+ui.value;
					
				$("#fplazas").attr('value',valores);	
			}
			else
			{
				
				//elimina la plaza de la cadena.
				valoresNuevo='';
				separacion=valores.split('_');
				
				for(i=0;i<separacion.length;i++)
					if(separacion[i]!=ui.value)
						if(valoresNuevo=='')
							valoresNuevo=separacion[i];
						else
							valoresNuevo+='_'+separacion[i];
							
				$("#fplazas").attr('value',valoresNuevo);
			}
			
			valores=$("#fplazas").val();
			
			//si hay valores
			if(valores!='')
				if(valores.indexOf('_')>0) //si hay más de un valor
				{
					//limpia y deshabilita el combo de sucursal.
					tecnico(-1);
					$("#ftecnico").attr("value", 0);
					$("#ftecnico").attr("disabled", true);	
				}
				else
				{
					//selecciona los tecnicos de la única plaza seleccionada, y habilita el combo.
					tecnico(valores);
					$("#ftecnico").attr("value", 0);
					$("#ftecnico").attr("disabled", false);
				}
			else
			{
				//limpia y habilita el combo de tecnico.
				tecnico(-1);
				$("#ftecnico").attr("value", 0);
				$("#ftecnico").attr("disabled", false);
			}
        },
		checkAll:function(){
			
			//todas las plazas
			$("#fplazas").attr('value',$("#plazas").val());
			
			//limpia y deshabilita el combo de tecnico.
			tecnico(-1)
			$("#ftecnico").attr("value", 0);
			$("#ftecnico").attr("disabled", true);	
		}
		,
		uncheckAll:function(){
			
			//ninguna plaza
			$("#fplazas").attr('value','');
		
			//limpia y deshabilita el combo de tecnico.
			tecnico(-1)
			$("#ftecnico").attr("value", 0);
			$("#ftecnico").attr("disabled", false);	
		}
    });

    ftecnico();
});

function ftecnico()
{

    $("#ftecnico").multiselect({
        noneSelectedText: "Todos los técnicos",
        click: function(event, ui)
        {

            //ui.value es para obtener el valor
            //ui.checked regresa true si está seleccionado o false si no lo está
            
            valores=$("#ftecnicos").val();
            
            if(ui.checked==true)
            {
                //agrega el tecnico a la cadena.
                if(valores=='')
                    valores=ui.value;
                else
                    valores=$("#ftecnicos").val()+'_'+ui.value;
                    
                $("#ftecnicos").attr('value',valores);    
            }
            else
            {
                
                //elimina el tecnico de la cadena.
                valoresNuevo='';
                separacion=valores.split('_');
                
                for(i=0;i<separacion.length;i++)
                    if(separacion[i]!=ui.value)
                        if(valoresNuevo=='')
                            valoresNuevo=separacion[i];
                        else
                            valoresNuevo+='_'+separacion[i];
                            
                $("#ftecnicos").attr('value',valoresNuevo);
            }
        },
        checkAll:function(){
            
            //todos los tecnicos
            $("#ftecnicos").attr('value',$("#tecnicos").val());
        }
        ,
        uncheckAll:function(){
            
            //ningun tecnico
            $("#ftecnicos").attr('value','');   
        }
    });   
}

function tecnico(plaza_id)
{

	$.ajax({
        type: 'POST',
        url:"/transacciones/reportes/obtenertecnicos/id/"+plaza_id,
        success:
        function respuesta(res)
        {

            separacion = res.split('|');

            $("#ftecnico").multiselect("destroy");
            $('#ftecnico').html(separacion[0]);
            ftecnico();

            $('#tecnicos').attr('value',separacion[1]);;
            $("#ftecnicos").attr('value','');
        }
    });	
}

$(document).ready(function() 
{  	
	$("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', onSelect:function(date){
         $("#fhasta").datepicker( "option", "minDate", date );   
    }});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});
	
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/reportes/gridproductividadtecnica',
        dataType: 'xml',
        colModel : [
				{display: 'TÉCNICO',  width : 150, align: 'center'},
                {display: 'TIPO',  width : 100, align: 'center'},
        		{display: 'FECHA',  width : 100, align: 'center'},
				{display: 'CONTRATO',  width : 60, align: 'center'},
                {display: 'NODO', width : 60, align: 'center'},
				{display: 'ETIQUETA', width : 60, align: 'center'},				
                {display: 'POSTE', width : 60, align: 'center'},	
                {display: 'CABLE', width : 60, align: 'center'},				
				{display: 'CONECTOR', width : 60, align: 'center'},
				{display: 'DIVISOR 3', width : 60, align: 'center'},
				{display: 'GRAPAS', width : 60, align: 'center'},
				{display: 'ESTATUS', width : 100, align: 'center'}
                ],
        sortname: "nombre ASC, fecha",
        sortorder: "DESC",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1120,
        height: 400
    });    
    
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });   
});

function servicios()
{
    var filtro = "/transacciones/reportes/gridproductividadtecnica";
    var imprimir="/transacciones/reportes/imprimirproductividadtecnica";
    var exportar="/transacciones/reportes/exportarproductividadtecnica";  
    
    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
        imprimir += "/desde/"+$("#fdesde").val();
        exportar += "/desde/"+$("#fdesde").val();
    }
    
    if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();
    }
    
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
        imprimir += "/plaza/"+$("#fplazas").val();
        exportar += "/plaza/"+$("#fplazas").val();
    }
    
    if($("#ftecnicos").val() > 0)
    {   
        filtro += "/tecnico/"+$("#ftecnicos").val();
        imprimir += "/tecnico/"+$("#ftecnicos").val();
        exportar += "/tecnico/"+$("#ftecnicos").val();    
    }
   
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function filtrar()
{
    var filtro = "/transacciones/reportes/gridproductividadtecnica";

    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
    }

    if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
    }
    
    if($("#fplazas").val() != '')
    {
        filtro += "/plaza/"+$("#fplazas").val();
    }
    
    if($("#ftecnico").val() > 0)
    {   
        filtro += "/tecnico/"+$("#ftecnico").val();
    }
       
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{
    $("#fdesde").attr('value','');
    $("#fhasta").attr('value','');
    
    //$("#fplazas").attr('value','');   
    $("#fplaza").multiselect("widget").find(":checkbox").each(function()
    {
        if(this.checked)
            this.click();
    });

    $("#ftecnico").multiselect("widget").find(":checkbox").each(function()
    {
        if(this.checked)
            this.click();
    });
    
    $("#ftecnico").attr("disabled", false);

    var filtro = "/transacciones/reportes/gridproductividadtecnica";
    var imprimir="/transacciones/reportes/imprimirproductividadtecnica";
    var exportar="/transacciones/reportes/exportarproductividadtecnica"; 
    
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}