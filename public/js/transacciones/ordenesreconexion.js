var tipo;
function cancelar(id)
{
    if(confirm('Realmente desea cancelar esta orden de desconexión?'))
    {	
        $.ajax({
            url:"/transacciones/ordenesreconexion/cancelar/id/"+id,
            success:function result(data)
            { 
                alert("La orden  ha sido cancelada.");
		$('#flexgrid').flexReload();
            }
	});
    }
}

$(document).ready(function() 
{      
    $("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', onSelect:function(date){
        $("#fhasta").datepicker( "option", "minDate", date );   
    }});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});   
    
    
    $("#flexgrid").flexigrid(
    {        
        url: '/transacciones/ordenesreconexion/grid',
        dataType: 'xml',
        colModel : [
		{display: 'CONTRATO',  name:'Cliente.contrato',width : 138, sortable : true, align: 'center'},
                {display: 'CLIENTE',  name:'Cliente.nombre',width : 250, sortable : true, align: 'center'},
                {display: 'PLAZA',  name:'Cliente.Plaza.nombre',width : 50, sortable : true, align: 'center'},
                {display: 'COLONIA',  name:'Cliente.Colonia.nombre',width : 50, sortable : true, align: 'center'},
                {display: 'TELEFONO', name:'Cliente.telefono',width : 180, sortable : true, align: 'center'},
                {display: 'CELULAR',  name:'Cliente.celular1',width : 155, sortable : true, align: 'center'},                               
                {display: 'ESTATUS',  name:'celular', width : 110, sortable : false, align: 'center'},
                {display: 'DESCARGAR',  width :70, sortable : false, align: 'center'}
                ],
        sortname: "created_at",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });  

   $('#fplaza').change(function()
    {
        $.ajax({
                type: 'POST',                            
                url:"/transacciones/ordenesreconexion/plazas/id/"+$('#fplaza').attr('value'),
                success:
                function respuesta(res)
                {
                    $('#fsucursal').html(res);
                }
        });
    }); 

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });	
});


function descargar(id)
{
    $.ajax({
        url:"/transacciones/ordenesreconexion/descargar/id/"+id,
	method:"post",
            success:function result(data)
            {    
                $(".dialog-form").html(data);
				$('#fecha_reconexion').datepicker({maxDate:'+0',dateFormat:'yy-mm-dd'});
				
				
                $("#frmDescargar").validate(
                {
                   submitHandler: function(form)
                   {
					   
					   	 if(confirm('Realmente desea cambiar el estatus de esta orden?'))
						 {
							 $(form).ajaxSubmit(
							 {
								success:    function(response)
								{ 
									$('#flexgrid').flexReload();
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							 });
						 }
                   }
                });

                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "DESCARGAR ORDEN DE RECONEXION ",
                        position: [120,30],
                        modal: true,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            $("#frmDescargar").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
            }
    });

}

function filtrarOrdenes()
{
    var filtro = "/transacciones/ordenesreconexion/grid";
    var imprimir="/transacciones/ordenesreconexion/imprimir";

    if($("#fnombre").val() != "")
    {
        filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
    }
    
    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
	imprimir += "/desde/"+$("#fdesde").val();
    }
    
    if($("#fhasta").val() != "")
    {
     	filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();	
    }  
        
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();
    }
    
    if($("#fplaza").val() > 0)
    {
        filtro += "/plaza/"+$("#fplaza").val();
        imprimir += "/plaza/"+$("#fplaza").val();
    }

    if($("#orden_primer_nivel").val()>0)
    {
       filtro += "/orden_primer_nivel/"+$("#orden_primer_nivel").val();
       imprimir += "/orden_primer_nivel/"+$("#orden_primer_nivel").val();
    }

    if($("#orden_segundo_nivel").val()>0)
    {
       filtro += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val();
       imprimir += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val();
    }

    if($("#orden_tercer_nivel").val()>0)
    {
       filtro += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val();
       imprimir += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val();
    }
    
    if($("#agrupamiento_primer_nivel").val()>0)
    {
       filtro += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val();
       imprimir += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val();
    }
     
    $('#btnImprimir').attr("href",imprimir);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarOrdenes()
{
    $("#fnombre").attr('value','');
    $("#fdesde").attr('value','');
    $("#fhasta").attr('value','');
    $("#fsucursal").attr('value',0);
    $("#fplaza").attr('value',0);

    var filtro = "/transacciones/ordenesreconexion/grid";
    var imprimir="/transacciones/ordenesreconexion/imprimir";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}