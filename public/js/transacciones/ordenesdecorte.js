var tipo;


$(document).ready(function() 
{      
    $("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', onSelect:function(date){
         $("#fhasta").datepicker( "option", "minDate", date );   
    }});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});
    
    $("#flexgrid").flexigrid({        
        url: '/transacciones/ordenescorte/grid',
        dataType: 'xml',
        colModel : [
				{display: 'CONTRATO',  name:'Cliente.contrato',width : 100, sortable : true, align: 'center'},
                {display: 'CLIENTE',  name:'Cliente.nombre',width : 300, sortable : true, align: 'center'},
                {display: 'PLAZA',  name:'Cliente.Plaza.nombre',width : 120, sortable : true, align: 'center'},
                {display: 'COLONIA',  name:'Cliente.Colonia.nombre',width : 120, sortable : true, align: 'center'},
                {display: 'TELEFONO', name:'Cliente.telefono',width : 140, sortable : true, align: 'center'},
                {display: 'CELULAR',  name:'Cliente.celular1',width : 140, sortable : true, align: 'center'},                 
                {display: 'ESTATUS',  width : 150, sortable : false, align: 'center'},
				{display: 'DESCARGAR',  width : 98, sortable : false, align: 'center'}
                ],
        sortname: "created_at",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1120,
        height: 400
    });  
   $('#fplaza').change(function()
    {
        $.ajax({
                type: 'POST',                            
                url:"/transacciones/ordenescorte/plazas/id/"+$('#fplaza').attr('value'),
                success:
                function respuesta(res)
                {
                    $('#fsucursal').html(res);
                }
        });
    });  
                
    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
});


function descargar(id)
{
	    $.ajax({
            url:"/transacciones/ordenescorte/descargar/id/"+id,
			method:"post",
            success:function result(data)
            {    
                $(".dialog-form").html(data);
				
				$("#fecha_desconexion").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});	
				
                $("#frmDescargar").validate(
                {
                   submitHandler: function(form)
                   {
					   
					   	 if(confirm('Realmente desea cambiar el estatus de este cliente?'))
						 {
							 $(form).ajaxSubmit(
							 {
								success:    function(response)
								{ 
									$('#flexgrid').flexReload();
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							 });
						 }
                   }
                });

                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "DESCARGAR ORDEN DE CORTE ",
                        position: [120,30],
                        modal: true,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            $("#frmDescargar").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
            }
    });

}




function retiro(id)
{
	    $.ajax({
            url:"/transacciones/ordenesretiro/descargar/id/"+id,
			method:"post",
            success:function result(data)
            {    
                $(".dialog-form").html(data);
				
				$("#fecha_desconexion").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0'});	
				
                $("#frmDescargar").validate(
                {
                   submitHandler: function(form)
                   {
					   
					   	 if(confirm('Realmente desea cambiar el estatus de este cliente?'))
						 {
							 $(form).ajaxSubmit(
							 {
								success:    function(response)
								{ 
									$('#flexgrid').flexReload();
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							 });
						 }
                   }
                });

                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "DESCARGAR ORDEN DE CORTE ",
                        position: [120,30],
                        modal: true,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            $("#frmDescargar").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
            }
    });

}




function filtrarOrdenescorte()
{
    var filtro = "/transacciones/ordenescorte/grid";
    var imprimir="/transacciones/ordenescorte/imprimir";
	var exportar="/transacciones/ordenescorte/exportar";

    if($("#fcontrato").val() != "")
    {
        filtro += "/contrato/"+$("#fcontrato").val();
        imprimir += "/contrato/"+$("#fcontrato").val();
		exportar += "/contrato/"+$("#fcontrato").val();
    }
    if($("#fnombre").val() != "")
    {
        filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
		exportar += "/nombre/"+$("#fnombre").val();
    }    
  
    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
		imprimir += "/desde/"+$("#fdesde").val();
		exportar += "/desde/"+$("#fdesde").val();
    }
    
    if($("#fhasta").val() != "")
    {
     	filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();
		exportar += "/hasta/"+$("#fhasta").val();	
    }
    
    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();
		exportar += "/sucursal/"+$("#fsucursal").val();   
    }
    if($("#fplaza").val() > 0)
    {
        filtro += "/plaza/"+$("#fplaza").val();
        imprimir += "/plaza/"+$("#fplaza").val();
		exportar += "/plaza/"+$("#fplaza").val();
    }

    if($("#orden_primer_nivel").val()>0)
    {
       filtro += "/orden_primer_nivel/"+$("#orden_primer_nivel").val();
       imprimir += "/orden_primer_nivel/"+$("#orden_primer_nivel").val();
       exportar += "/orden_primer_nivel/"+$("#orden_primer_nivel").val(); 
    }

    if($("#orden_segundo_nivel").val()>0)
    {
       filtro += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val();
       imprimir += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val();
       exportar += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val(); 
    }

    if($("#orden_tercer_nivel").val()>0)
    {
       filtro += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val();
       imprimir += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val();
       exportar += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val(); 
    }
    
    if($("#agrupamiento_primer_nivel").val()>0)
    {
       filtro += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val();
       imprimir += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val();
       exportar += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val(); 
    }
	
    $('#btnImprimir').attr("href",imprimir);     
	$('#btnExportar').attr("href",exportar); 
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarOrdenes()
{
    $("#fcontrato").attr('value','');
    $("#fnombre").attr('value','');
    $("#fdesde").attr('value','');
    $("#fhasta").attr('value',''); 
    $("#fsucursal").attr('value',0);
    $("#fplaza").attr('value',0);
    
    var filtro = "/transacciones/ordenescorte/grid";
    var imprimir="/transacciones/ordenescorte/imprimir";
	var exportar="/transacciones/ordenescorte/exportar";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}