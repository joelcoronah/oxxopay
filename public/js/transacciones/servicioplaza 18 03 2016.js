var tipo;
function validarMatriz()
{	
	var bandera=false;
	$(".contenedorPlazaTarifas").each(function()
	{
		//Si la bandera está levantada no tiene caso hacer nigun tipo de evaluación
		if(bandera)
			return;
			
		var numElementoVacios=0;
		var numElementoEspecialVacios=0;
		var idDiv=this.id;
		var idTr="";
		
		// Establecemos el background del div que vamos a evaluar en color blanco por default
		$('#'+idDiv).css("background-color","#fff");
		
		// Recorremos todos los campos base del div
		$("#"+this.id+" .base").each(function()
		{	
			if(this.value!='')
			{
				// Incrementamos el contador de elementos vacios
				numElementoVacios++;
			}
		});

		
		// Si el número elementos vacios es mayor a cero, pero menor a la totalidad de cmapos base, entonces detenemos el flujo y avisamos al usuario.
		if(numElementoVacios>0 && numElementoVacios<$("#"+this.id+" .base").length)
		{
			alert("No es posble definir solo algunos de los precios que conforman la tarifa base.");
			// Hacemos que el div se vuelva color rojo
			$('#'+idDiv).css("background-color","#FFBFBF");
			
			//Levantamos la bandera para ya no hacer validaciones
			bandera=true;
			return;
		}
		else if(numElementoVacios==$("#"+this.id+" .base").length)
		{
			//Ya que recorrimos  las tarifas base; vamos a evaluar las tarifas especiales
			$("#"+this.id+" .especial1mes").each(function()
			{	
				if(bandera)
					return;
					
				numElementoEspecialVacios=0;
				idTr=this.id;
				
				cadenaSplit=idTr.split('_');
				idTarifasAdicionales='especialAdcional_'+cadenaSplit[1]+'_'+cadenaSplit[2]+'_'+cadenaSplit[3];	
				
				
				$('#'+idTr).css("background-color","#fff");
				$('#'+idTarifasAdicionales).css("background-color","#fff");
				
				
				// Recorremos los de la tarifa especial a 1 mes
				$("#"+this.id+" .especial").each(function()
				{	
					if(this.value=='')
						numElementoEspecialVacios++;
				});
				
				
				// Recorremos los de la tarifa especial a varios meses
				$("#"+idTarifasAdicionales+" .especial").each(function()
				{	
					if(this.value=='')
						numElementoEspecialVacios++;
				});
				
				
				// Si el número elementos vacios es mayor a cero, pero menor a la totalidad de cmapos base, entonces detenemos el flujo y avisamos al usuario.
				if(numElementoEspecialVacios>0 && numElementoEspecialVacios<($("#"+this.id+" .especial").length + $("#"+idTarifasAdicionales+" .especial").length))
				{
					alert("No es posble definir solo algunos de los precios que conforman la tarifa especial.");
					// Hacemos que el div se vuelva color rojo
					$('#'+idTr).css("background-color","#FFBFBF");
					$('#'+idTarifasAdicionales).css("background-color","#FFBFBF");
					
					//Levantamos la bandera para ya no hacer validaciones
					bandera=true;
					return;
				}
			});
			
			
		}
	}); 
	
	if(bandera==false)	
		$('#frmTarifas').submit();
	
}

$(document).ready(function()
{
		var pasada=0;
		/*$('.botoneraTarifas input').each(function()
		{
				if(pasada==0)
				{
					$("#"+this.id).removeClass('btn-primary').addClass('btn-success');
					objetivo=this.id.split('_');
					$('.divEstado_'+objetivo[1]).show();
				}
				pasada++;
		});*/
		
		$('.botoneraTarifas input').click(function()
		{
			$('.botoneraTarifas input').removeClass('btn-success').addClass('btn-primary');
			$('.contenedorServicioTarifas').hide();
			
			$("#"+this.id).removeClass('btn-primary').addClass('btn-success');
			objetivo=this.id.split('_');
			$('.divEstado_'+objetivo[1]).show();
			
			
			
		});
});

