var tipo;

function agregarGasto(id, tipo)
{           
    if(id!=0 && tipo==1){
        $(function() {					
            $('#dialog-message').html(
            '<p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>No es posible modificar un registro de gasto, si lo requiere debe solicitar el ajuste al <b>ADMINISTRADOR</b> del sistema.</p>');							
            $( "#dialog-message" ).dialog({
                modal: true,
                buttons: {
                    Aceptar: function() {
                        $(this).dialog("close");
                    }
            	}
            });
        });
    }
    
    else{
        $.ajax({
            url:"/transacciones/gastos/agregar/id/"+id,
            success:function result(data)
            {
                $(".dialog-form").html(data);     
		$("#fecha").datepicker({changeYear:true, dateFormat:'yy-mm-dd',maxDate:'+0', minDate:'+0'});	

                $("#frmGastos").validate(
                {                   
                   submitHandler: function(form)
                   {
                    
                    if(confirm("¿Esta seguro(a) de querer guardar este registro? Estos datos no se podrán ser eliminados ni modificados."))
                    {   
                     $("#Aplicar,#Aceptar").hide();  
                     $(form).ajaxSubmit(
                     {
                        success:  function(response) 
                        { 
                            $('#flexgrid').flexReload();
                            if(tipo==1)
                            {
                                $('#frmGastos').resetForm();
				$("#Aplicar,#Aceptar").show(); 
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							
                        }
                     });
                     
                    }
                    
                   }
                });

                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar gasto",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmGastos").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmGastos").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
        });
    }
}


$(document).ready(function() 
{
    $("#fdesde").datepicker({changeYear:true, dateFormat:'yy-mm-dd'});	
    $("#fhasta").datepicker({changeYear:true, dateFormat:'yy-mm-dd'});	
	
    $("#flexgrid").flexigrid(
    {        
        url: '/transacciones/gastos/grid',
        dataType: 'xml',
        colModel : [
		{display: 'FECHA',  name:'fecha',width : 130, sortable : true, align: 'center'},
                {display: 'CONCEPTO',  name:'concepto',width : 430, sortable : true, align: 'center'},
                {display: 'MONTO', name:'monto',width : 100, sortable : true, align: 'center'},
                {display: 'MODIFICAR',  name:'telefono',width : 80, sortable : false, align: 'center'},   
				{display: 'ELIMINAR',  name:'telefono',width : 80, sortable : false, align: 'center'},   
                ],
        sortname: "fecha",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });  
      
   $('#fplaza').change(function()
    {
        $.ajax({
                type: 'POST',                            
                url:"/transacciones/gastos/plazas/id/"+$('#fplaza').attr('value'),
                success:
                function respuesta(res)
                {
                    $('#fsucursal').html(res);
                }
        });
    });   
      
    $("#flexgrid").ready(function(){
        $.ajax({
        url:"/transacciones/gastos/calcular",
        success:function result(data)
        {    
            $("#ftotal").attr('value',data);	  
        }
 	});		
        $(".pReload").hide();
    });
		
});

function filtrarGastos()
{
    var filtro = "/transacciones/gastos/grid";
    var imprimir="/transacciones/gastos/imprimir";
    var exportar="/transacciones/gastos/exportar";
    var calcular="/transacciones/gastos/calcular";
	
    if($("#fdesde").val() != "")
    {
        filtro += "/desde/"+$("#fdesde").val();
        imprimir += "/desde/"+$("#fdesde").val();
        exportar += "/desde/"+$("#fdesde").val();
        calcular += "/desde/"+$("#fdesde").val();
    }
	
    if($("#fhasta").val() != "")
    {
        filtro += "/hasta/"+$("#fhasta").val();
        imprimir += "/hasta/"+$("#fhasta").val();
        exportar += "/hasta/"+$("#fhasta").val();
	calcular += "/hasta/"+$("#fhasta").val();
    }
	
    if($("#fplaza").val() > 0)
    {
        filtro += "/plaza/"+$("#fplaza").val();
        imprimir += "/plaza/"+$("#fplaza").val();
        exportar += "/plaza/"+$("#fplaza").val();
	calcular += "/plaza/"+$("#fplaza").val();
    }

    if($("#fsucursal").val() > 0)
    {	
    	filtro += "/sucursal/"+$("#fsucursal").val();
    	imprimir += "/sucursal/"+$("#fsucursal").val();   
        exportar += "/sucursal/"+$("#fsucursal").val();
        calcular += "/sucursal/"+$("#fsucursal").val();
    }
    
    $.ajax({
        url:calcular,
        success:function result(data)
        {    
            $("#ftotal").attr('value',data);	  
        }
    });
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarGastos()
{
    $("#fdesde").attr('value','');
    $("#fhasta").attr('value','');
    $("#fplaza").attr('value',0);
    $("#fsucursal").attr('value',0);

    var filtro = "/transacciones/gastos/grid";

    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
	
    $.ajax({
        url:"/transacciones/gastos/calcular",
        success:function result(data)
        {    
            $("#ftotal").attr('value',data);	  
       	}
    });
}


function eliminarGastos(id)
{
	var filtro = "/transacciones/gastos/grid";
    
	if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
    $.ajax({
        url:"/transacciones/gastos/eliminar/id/"+id,
        success:function result(data)
        {    
            
    		$('#flexgrid').flexOptions({url: filtro}).flexReload(); 
	
    		$.ajax({
        		url:"/transacciones/gastos/calcular",
        		success:function result(data)
        		{    
            		$("#ftotal").attr('value',data);	  
       			}
    		});	  
       	}
    });
	}
	
}