var tipo;
function descargar(id)
{
    $.ajax({
            url:"/transacciones/ordenesbajavoluntaria/descargar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
                $("#fecha_baja").datepicker({changeYear:true,dateFormat:'yy-mm-dd', maxDate:'+0'});


                $("#frmDescargar").validate(
                {
                   submitHandler: function(form) 
                   {
                     $(form).ajaxSubmit(
                     {
                        success:    function(response) 
                        { 
                            $('#flexgrid').flexReload();

                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Descargar baja",
                        position: [240,100],
                        modal: true,
                        buttons: [
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmDescargar").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
   
                    
            }
    });

}


$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/transacciones/ordenesbajavoluntaria/grid',
        dataType: 'xml',
        colModel : [
		{display: 'FECHA DE SOLICITUD',  name:'created_at',width : 160, sortable : true, align: 'center'},
		{display: 'CONTRATO',  name:'Cliente.contrato',width : 100, sortable : true, align: 'center'},
                {display: 'CLIENTE',  name:'Cliente.nombre',width : 300, sortable : true, align: 'center'},
                {display: 'TELEFONO',  name:'Cliente.telefono',width : 150, sortable : true, align: 'center'},
                {display: 'CELULAR',  name:'Cliente.celular1',width : 150, sortable : true, align: 'center'},
                {display: 'ESTATUS',  name:'Cliente.estatus',width : 120, sortable : false, align: 'center'},
		{display: 'DESCARGAR',  width : 80,  align: 'center'}
                ],
        sortname: "Cliente.fecha_contratacion",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });

    $('#fsucursal').change(function()
    {        
        $.ajax({
            type: 'POST',
            url:"/transacciones/ordenescorte/plazas/id/"+$('#fsucursal').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fplaza').html(res);
            }
        });
    });    

    $('#festado').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/municipios/id/"+$('#festado').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fmunicipio').html(res);
            }
        });
    });

    $('#fmunicipio').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/comunidades/id/"+$('#fmunicipio').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fcomunidad').html(res);
            }
        });
    });
    
    
});

function filtrarOrdenes()
{
    var filtro = "/transacciones/ordenesbajavoluntaria/grid";
    var imprimir="/transacciones/ordenesbajavoluntaria/imprimir";
    var exportar="/transacciones/ordenesbajavoluntaria/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }
    if($("#fplaza").val() > 0)
    {
            filtro += "/plaza/"+$("#fplaza").val();
            imprimir += "/plaza/"+$("#fplaza").val();
            exportar += "/plaza/"+$("#fplaza").val();
    }
    
    if($("#festado").val() > 0)
    {
            filtro += "/estado/"+$("#festado").val();
            imprimir += "/estado/"+$("#festado").val();
            exportar += "/estado/"+$("#festado").val();
    }
    
    if($("#fmunicipio").val() > 0)
    {
            filtro += "/municipio/"+$("#fmunicipio").val();
            imprimir += "/municipio/"+$("#fmunicipio").val();
            exportar += "/municipio/"+$("#fmunicipio").val();
    }
    
    if($("#fcomunidad").val() > 0)
    {
            filtro += "/comunidad/"+$("#fcomunidad").val();
            imprimir += "/comunidad/"+$("#fcomunidad").val();
            exportar += "/comunidad/"+$("#fcomunidad").val();
    }

    if($("#orden_primer_nivel").val()>0)
    {
       filtro += "/orden_primer_nivel/"+$("#orden_primer_nivel").val();
       imprimir += "/orden_primer_nivel/"+$("#orden_primer_nivel").val();
       exportar += "/orden_primer_nivel/"+$("#orden_primer_nivel").val(); 
    }

    if($("#orden_segundo_nivel").val()>0)
    {
       filtro += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val();
       imprimir += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val();
       exportar += "/orden_segundo_nivel/"+$("#orden_segundo_nivel").val(); 
    }

    if($("#orden_tercer_nivel").val()>0)
    {
       filtro += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val();
       imprimir += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val();
       exportar += "/orden_tercer_nivel/"+$("#orden_tercer_nivel").val(); 
    }
    
    if($("#agrupamiento_primer_nivel").val()>0)
    {
       filtro += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val();
       imprimir += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val();
       exportar += "/agrupamiento_primer_nivel/"+$("#agrupamiento_primer_nivel").val(); 
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarOrdenes()
{
    $("#fnombre").attr('value','');
    $("#fplaza").attr('value',0);
    $("#festado").attr('value',0)
    $("#fmunicipio").attr('value',0)
    $("#fcomunidad").attr('value',0)

    var filtro = "/transacciones/ordenesbajavoluntaria/grid";
    var imprimir="/transacciones/ordenesbajavoluntaria/imprimir";
    var exportar="/transacciones/ordenesbajavoluntaria/exportar";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);


    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

