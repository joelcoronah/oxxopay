var tipo;

$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/promocion/grid',
        dataType: 'xml',
        colModel : [
                {display: 'NOMBRE',  name:'nombre',width : 650, sortable : true, align: 'center'},
                {display: 'PLAZAS',  width : 100, sortable : false, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
});

function agregar(id)
{
	
	$.ajax({
		
		url:"/catalogos/promocion/agregar/id/"+id,
		success:function result(data){
			
			$(".dialog-form").html(data);
			$("#frm").validate({
				
				submitHandler: function(form){
					
					$("#Aplicar,#Aceptar").hide();
					
					$(form).ajaxSubmit({
					
						success:function(response){
							
							filtrar();
							
							if(tipo==1)
                            {
                                $('#frm').resetForm();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							
							if(id != 0 )
								$("#Aceptar").show();
							else
								$("#Aplicar,#Aceptar").show();
						} //success
					}) //ajaxSubmit
				} //submitHandler
			}) //validate
			
			$(".dialog-form").dialog({				
				height:'auto',
				width: 'auto',
				resizable: false,
				title: "Agregar promoción",
				position: "center",
				modal: true,
				buttons: [				
					{
						id:"Aplicar",
						text:"Aplicar",
						click:function()
						{
							tipo = 1;
							$("#frm").submit();
						}
					},
					{
						id:"Aceptar",
						text:"Aceptar",
						click:
						function()
						{
							tipo = 2;
							$("#frm").submit();
						}
					},
					{
						id:"Cancelar",
						text:"Cancelar",
						click:function()
						{
							$(".dialog-form").dialog("destroy");
							$(".dialog-form").dialog("close");
						}
					}
				] //buttons
			}) //dialog
			                  
			if(id != 0 )
			{
				$("#Aplicar").hide();
			}  
		} //success
	}) //$.ajax
}

function filtrar()
{
    var filtro = "/catalogos/promocion/grid";
    var imprimir="/catalogos/promocion/imprimir";
    var exportar="/catalogos/promocion/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function eliminar(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/promocion/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                $('#flexgrid').flexReload();
            }
        });
    }
}

function limpiar()
{
    $("#fnombre").attr('value','');

    var filtro = "/catalogos/promocion/grid";

	var imprimir="/catalogos/promocion/imprimir";
    var exportar="/catalogos/promocion/exportar";
	
	$('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);


    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function definir(id)
{
	
	$.ajax({
		
		url:"/catalogos/promocion/definir/id/"+id,
		success:function result(data){
			
			$(".dialog-form").html(data);
			
			$(function() {  $( "#tabs" ).tabs(); });
			
			$(function() {    $( ".calendario").datepicker({ dateFormat: "yy-mm-dd" });  }); 
			
			$("#frm").validate({
				
				submitHandler: function(form){
					
					$("#Aceptar").hide();
					
					$(form).ajaxSubmit({
					
						success:function(response){
							
							alert("Las promociones se han actualizado correctamente");
							
							$("#Aceptar").show();
						} //success
					}) //ajaxSubmit
				} //submitHandler
			}) //validate
			
			$(".dialog-form").dialog({				
				height:'auto',
				width: 'auto',
				resizable: false,
				title: "Editar promociones",
				position: 'center',
				modal: true,
				buttons: [				
					{
						id:"Aceptar",
						text:"Aceptar",
						click:
						function()
						{
							tipo = 2;
							$("#frm").submit();
						}
					},
					{
						id:"Cancelar",
						text:"Cancelar",
						click:function()
						{
							$(".dialog-form").dialog("destroy");
							$(".dialog-form").dialog("close");
						}
					}
				] //buttons
			}) //dialog
		} //success
	}) //$.ajax
}

function habilitar(id)
{

	if($('.definir_id_'+id).is(':checked')!=true)
	{
		$('.inicio_id_'+id).attr('value','');	
		$('.fin_id_'+id).attr('value','');
		$('.inicio_id_'+id).attr('disabled','true');	
		$('.fin_id_'+id).attr('disabled','true');
		$('.inicio_id_'+id).attr('class','calendario inicio_id_'+id);	
		$('.fin_id_'+id).attr('class','calendario inicio_id_'+id);	
	}
	else
	{
		$('.inicio_id_'+id).removeAttr('disabled');	
		$('.fin_id_'+id).removeAttr('disabled');
		$('.inicio_id_'+id).attr('class','calendario inicio_id_'+id+' required');	
		$('.fin_id_'+id).attr('class','calendario inicio_id_'+id+' required');
	}
}