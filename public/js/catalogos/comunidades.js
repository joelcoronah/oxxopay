var tipo;
function agregarComunidad(id)
{
    $.ajax({
            url:"/catalogos/comunidades/agregar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
                $('#estado_id').change(function()
                {
                    $.ajax({
                        type: 'POST',
                        url:"/catalogos/plaza/municipios/id/"+$('#estado_id').attr('value'),
                        success:
                        function respuesta(res)
                        {
                           $('#municipio_id').html(res);
                        }
                    });
                });

                $("#frmComunidad").validate(
                {
                   rules:
                   {
                     estado_id: {required:true, min:1},
                     municipio_id: {required:true, min:1}
                   },
                   messages:
                   {
                    estado_id: {min: "Debe elegir el estado en el que se encuentra la comunidad"},
                    municipio_id: {min: "Debe elegir el municipio en el que se encuentra la comunidad"}
                   }, 
                   submitHandler: function(form) 
                   {
					  $("#Aplicar,#Aceptar").hide();  
                     $(form).ajaxSubmit(
                     {
                        success:    function(response) 
                        { 
                            $('#flexgrid').flexReload();
                            if(tipo==1)
                            {
                                $('#frmComunidad').resetForm();
								$("#Aplicar,#Aceptar").show(); 
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar comunidad",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmComunidad").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmComunidad").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

function eliminarComunidad(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar esta comunidad?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/comunidades/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('La comunidad ha sido eliminada.');
                $('#flexgrid').flexReload();
            }
        });
    }
}



$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/comunidades/grid',
        dataType: 'xml',
        colModel : [
                {display: 'ESTADO',  name:'Municipio.Estado.nombre',width : 250, sortable : true, align: 'center'},
                {display: 'MUNICIPIO',  name:'Municipio.nombre',width : 250, sortable : true, align: 'center'},
                {display: 'COMUNIDAD',  name:'nombre',width : 250, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    $('#festado').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/municipios/id/"+$('#festado').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fmunicipio').html(res);
            }
        });
    });

    $('#fmunicipio').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/comunidades/id/"+$('#fmunicipio').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fcomunidad').html(res);
            }
        });
    });
    
    
});

function filtrarComunidades()
{
    var filtro = "/catalogos/comunidades/grid";
    var imprimir="/catalogos/comunidades/imprimir";
    var exportar="/catalogos/comunidades/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }
    if($("#festado").val() >0)
    {
            filtro += "/estado/"+$("#festado").val();
            imprimir += "/estado/"+$("#festado").val();
            exportar += "/estado/"+$("#festado").val();
    }
    if($("#fmunicipio").val() >0)
    {
            filtro += "/municipio/"+$("#fmunicipio").val();
            imprimir += "/municipio/"+$("#fmunicipio").val();
            exportar += "/municipio/"+$("#fmunicipio").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarComunidades()
{
    $("#fnombre").attr('value','');
    $("#festado").attr('value',0);
    $("#fmunicipio").attr('value',0);

    var filtro = "/catalogos/comunidades/grid";
	
	var imprimir="/catalogos/comunidades/imprimir";
    var exportar="/catalogos/comunidades/exportar";
	
	$('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);


    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}