var tipo;
function agregarConceptosdecontratacion(id)
{
    $.ajax({
            url:"/catalogos/conceptodecontratacion/agregar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);

                $("#frmConceptocontratacion").validate(
                {
                   submitHandler: function(form) 
                   {
                     $("#Aplicar,#Aceptar").hide(); 
                     $(form).ajaxSubmit(
                     {
						 
                        success: function(response) 
                        { 
                            $('#flexgrid').flexReload();
                            if(tipo==1)
                            {
                                $('#frmConceptocontratacion').resetForm();
                                $("#Aplicar,#Aceptar").show();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							 
                        }
                     });
                   }
                });

                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar conceptos de contratación ",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmConceptocontratacion").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmConceptocontratacion").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

function muestraPlazas(estado)
{
    $.ajax({
        url:"/catalogos/conceptodecontratacion/plazas/estado/"+estado,
            
        success:function result(data)
        {
            $(".dialog-form").html(data);            
           
           
        }
    });
}

function agregarContratacion(id)
{ 
var mensaje=0;
   $.ajax({
       url:"/catalogos/conceptodecontratacion/contratacion/id/"+id,
       success:function result(data)
       {
            $(".dialog-form").html(data);
            
            $(function() {  $( "#tabs" ).tabs(); });
            
            $("#frmContratacion").validate(
            {
                submitHandler: function(form) 
                {
							var enviar=true;
					
					        $("#tarifa input").each(function()
							{
									if(enviar==false)
										return;
									if($('#'+this.id).val() != '' ^ $('#'+$(this).attr('rel')).val()!="")//
									{
										$('#errorcontatacion').show();				  
										enviar=false;
									}  
                       	     }); 
					
					
								if(enviar==true)	
								{		
									$(form).ajaxSubmit(
									{			 
										success: function(response) 
										{ 				
											alert("Las tarifas de contratación se han actualizado correctamente");
																	
										}
									});
								}
                }
            });
            
            $(".dialog-form").dialog({
                height:'auto',
                width: 1000,
                resizable: false,
                title: "Editar tarifas",
                position: [120,100],
                modal: true,
                buttons: [
                {
                    id:"Aceptar",
                    text:"Aceptar",
                    click:
                    function()
                    {
                        tipo = 2;   
                        $("#frmContratacion").submit();		
                    }
                },
                
                {
                    id:"Cancelar",
                    text:"Cancelar",
                    click:function()
                    {
                        $(".dialog-form").dialog("destroy");
                        $(".dialog-form").dialog("close");
                    }
                }
                ]
            });        
        }
    });
}



function eliminarConceptocontratacion(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/conceptodecontratacion/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                $('#flexgrid').flexReload();
            }
        });
    }
}

$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/conceptodecontratacion/grid',
        dataType: 'xml',
        colModel : [
                {display: 'NOMBRE',  name:'nombre',width : 683, sortable : true, align: 'center'},
                {display: 'TARIFAS',   width : 100, sortable : false, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    
});

function filtrarConceptosdecontratacion()
{
    var filtro = "/catalogos/conceptodecontratacion/grid";
    var imprimir="/catalogos/conceptodecontratacion/imprimir";
    var exportar="/catalogos/conceptodecontratacion/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarConceptosdecontratacion()
{
    $("#fnombre").attr('value','');

    var filtro = "/catalogos/conceptodecontratacion/grid";	
    var imprimir="/catalogos/conceptodecontratacion/imprimir";
    var exportar="/catalogos/conceptodecontratacion/exportar";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}