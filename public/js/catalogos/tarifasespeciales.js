var tipo;
function agregarTarifa(id)
{
    $.ajax({
            url:"/catalogos/tarifasespeciales/agregar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);

                $("#frmPromotor").validate(
                {
                   rules:
                   {
                     plaza_id: {required:true, min:1}
                   },
                   messages:
                   {
                    plaza_id: {min: "Debe elegir la plaza a la que pertenece el técnico"}
                   },
                   submitHandler: function(form) 
                   {
					  $("#Aplicar,#Aceptar").hide();  
                     $(form).ajaxSubmit(
                     {
                        success:    function(response) 
                        { 
							if(response==-1)
							{
								alert('El nombre de la tarifa ya existe.');
								$("#Aplicar,#Aceptar").show();
							}
							else
							{
								$('#flexgrid').flexReload();
								if(tipo==1)
								{
									$('#frmPromotor').resetForm();
									$("#Aplicar,#Aceptar").show();
								}
								else
								{
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							}
							 
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar tarifa especial",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmPromotor").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmPromotor").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

function eliminarPromotor(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/tarifasespeciales/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                $('#flexgrid').flexReload();
            }
        });
    }
}



$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/tarifasespeciales/grid',
        dataType: 'xml',
        colModel : [
                {display: 'NOMBRE',  name:'nombre',width : 400, sortable : true, align: 'center'},
				{display: 'AUTORIZACIÓN',  name:'autorizacion',width : 350, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    
});

function filtrarTarifa()
{
    var filtro = "/catalogos/tarifasespeciales/grid";
    var imprimir="/catalogos/tarifasespeciales/imprimir";
    var exportar="/catalogos/tarifasespeciales/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }
    if($("#fautorizacion").val() != "")
    {
            filtro += "/autorizacion/"+$("#fautorizacion").val();
            imprimir += "/autorizacion/"+$("#fautorizacion").val();
            exportar += "/autorizacion/"+$("#fautorizacion").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarTarifa()
{
    $("#fnombre").attr('value','');
    $("#fautorizacion").attr('value',0);

    var filtro = "/catalogos/tarifasespeciales/grid";
	var imprimir="/catalogos/tarifasespeciales/imprimir";
    var exportar="/catalogos/tarifasespeciales/exportar";
	
	$('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);


    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}