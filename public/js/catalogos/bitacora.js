$(document).ready(function() 
{   
    $('#fdesde').datepicker({dateFormat:'yy-mm-dd'});
    $('#fhasta').datepicker({dateFormat:'yy-mm-dd'});
    
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/bitacora/grid',
        dataType: 'xml',
        colModel : [
                {display: 'ACCIÓN', name:'descripcion', width : 300, sortable : true, align: 'center'},
                {display: 'USUARIO',  name:'Usuario.nombre',width : 205, sortable : true, align: 'center'},
                {display: 'FECHA', name:'fecha', width : 150, sortable : true, align: 'center'},
                {display: 'MÓDULO O SECCIÓN', name:'fuente',  width : 300, sortable : false, align: 'center'}
                ],
        sortname: "fecha",
        sortorder: "desc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $(".flexgrid").ready(function(){
        $(".pReload").hide();
    });
});

function filtrarBitacora()
{
	var filtro = "/catalogos/bitacora/grid";
	
	if($("#fusuario").val() != "")
	{
		filtro += "/usuario/"+$("#fusuario").val();
	}
        
        if($("#faccion").val() != "")
	{
		filtro += "/accion/"+$("#faccion").val();
	}
        
        if($("#fplaza").val() != "")
	{
		filtro += "/plaza/"+$("#fplaza").val();
	}
        
        if($("#fdesde").val() != "")
	{
		filtro += "/desde/"+$("#fdesde").val();
	}
        
        if($("#fhasta").val() != "")
	{
		filtro += "/hasta/"+$("#fhasta").val();
	}


	
	$('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}