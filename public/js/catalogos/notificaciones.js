var tipo;
var contador = 1;

$(document).ready(function() 
{   
    $("#desde").datetimepicker({controlType: 'select', changeYear:true,dateFormat:'yy-mm-dd', timeFormat: 'HH:00:00'});
    $("#hasta").datetimepicker({controlType: 'select', changeYear:true,dateFormat:'yy-mm-dd', timeFormat: 'HH:00:00'});

    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/notificacion/grid',
        dataType: 'xml',
        colModel : [
                {display: 'TITULO',  name:'nombre',width : 848, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 120, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 120, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1120,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
});


function addInput(divName)
{
    var del = "div_f_"+contador;
    var delet = "fechas_"+contador;
    var cadHtml='<tr id="'+del+'"><td><input type="text" name="fechas['+contador+']"" id="fechas_'+contador+'" placeholder="DD/MM/AAA" class="required" readonly="readonly" /> </td><td><input type="checkbox" name="cfechas['+contador+']" id="cfechas_'+contador+'" value="0" onClick="$(this).val(this.checked ? 1 : 0)"/></td><td><input class="btn btn-danger" type="button" value="X" onClick="deleteInput(\''+delet+'\');"/></td></tr>';
    /*var newdiv = document.createElement('div');
    var del = "div_f_"+contador;
    newdiv.id = del;
    newdiv.innerHTML = '<input  type="text" name="fechas['+contador+']" id="fechas_'+contador+'" placeholder="DD/MM/AAA" class="required" readonly="readonly" /><input class="btn btn-danger" type="button" value="X" onClick="deleteInput(\''+del+'\');"/> <input type="checkbox" name="cfechas['+contador+']" id="cfechas_'+ contador +'" value="0" onClick="$(this).val(this.checked ? 1 : 0)"/>';
    document.getElementById(divName).appendChild(newdiv);*/
    $("#"+divName).append(cadHtml);
    $("#fechas_"+contador).datetimepicker({controlType: 'select', changeYear:true,dateFormat:'yy-mm-dd', minDate:'+0', timeFormat: 'HH:00:00'});
    contador++;
}

function deleteInput(divName)
{
    $("#"+divName).val("");
    /*var child = document.getElementById(divName);
    document.getElementById('ifechas').removeChild(child);*/
}

function seleccionado()
{
    var bandera = false;
    for(i = 0; i < 12; i++)
    {
        if($("#estatus0").prop('checked')) bandera = true;
        if($("#estatus1").prop('checked')) bandera = true;
        if($("#estatus2").prop('checked')) bandera = true;
        if($("#estatus3").prop('checked')) bandera = true;
        if($("#estatus4").prop('checked')) bandera = true;
        if($("#estatus5").prop('checked')) bandera = true;
        if($("#estatus6").prop('checked')) bandera = true;
        if($("#estatus7").prop('checked')) bandera = true;
        if($("#estatus8").prop('checked')) bandera = true;
        if($("#estatus9").prop('checked')) bandera = true;
        if($("#estatus10").prop('checked')) bandera = true;
        if($("#estatus11").prop('checked')) bandera = true;
    }
    if(bandera) $("#checks").hide();
    else{
        $("#checks").show();
    }
    return bandera;
}

function longitudSMS()
{
    var bandera = true;
    
    if($("#metodo").val()==1 || $("#metodo").val()==3)
    {
        var size = calculaSizeTXT();
        if(size > 160) bandera = false;
    }

    return bandera;
}

function agregar(id)
{
    
    $.ajax({
        
        url:"/catalogos/notificacion/agregar/id/"+id,
        success:function result(data){
            
            var res = data.split("*||*");
            var data_html = res[0]; data_html = data_html.trim();
            var data_number = res[1]; data_number = data_number.trim();

            $(".dialog-form").html(data_html);
            
            $("#fechas_0").datetimepicker({controlType: 'select', changeYear:true,dateFormat:'yy-mm-dd', minDate:'+0', timeFormat: 'HH:00:00', hour:7});
            
            //cambioTXT();

            $('#metodo').change(function(){
                if($(this).find('option:selected').val()==1 || $(this).find('option:selected').val()==3)
                {
                    //cambioTXT();
                    $("#caracteres").show();
                }
                else
                    $("#caracteres").hide();
            });

            $("#plaza_id").attr('multple','multple');
            $("#plaza_id").multiselect({
                noneSelectedText: "Elija una plaza",
                click: function(event, ui)
                {
                    if(ui.checked)
                    {
                        $("#plaza_"+ui.value).attr("value",ui.value);
                    }
                    else
                    {
                        $("#plaza_"+ui.value).attr("value","0");   
                    }
                }
            });



            contador = data_number;
        
            $("#frm").validate({
                
                submitHandler: function(form){

                    $("#Aplicar,#Aceptar").hide();
                    
                    $(form).ajaxSubmit({
                    
                        success:function(response){
                            
                            if(response.trim() > 0)
                            {
                                alert("Notificación guardada correctamente");
                                filtrar();
                            }
                            else
                            {
                                alert("Ha ocurrido un error al tratar de guardar la notificación, inténtelo nuevamente [" + response.trim() + "]");
                            }
                            
                            
                            if(tipo==1)
                            {
                                $('#frm').resetForm();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
                            
                            if(id != 0 )
                                $("#Aceptar").show();
                            else
                                $("#Aplicar,#Aceptar").show();
                        } //success
                    }) //ajaxSubmit
                } //submitHandler
            }) //validate
            
            $(".dialog-form").dialog({              
                height:'auto',
                width: 'auto',
                resizable: false,
                title: "Agregar notificación",
                position: "center",
                modal: true,
                buttons: [              
                    /*{
                        id:"Aplicar",
                        text:"Aplicar",
                        click:function()
                        {
                            tipo = 1;
                            if(seleccionado() == true)
                                $("#frm").submit();
                        }
                    },*/
                    {
                        id:"Aceptar",
                        text:"Aceptar",
                        click:
                        function()
                        {
                            tipo = 2;
                            if(seleccionado() == true)
                            {
                                if(longitudSMS() == true)
                                {
                                    if($("#plaza_id").val() == null)
                                        alert("Debe seleccionar una o varias plazas");
                                    else
                                        $("#frm").submit();
                                }
                                else
                                    alert("El método de envió incluye SMS, se ha superado el tamaño máximo del SMS en la descripción, favor de no superar los 160 caracteres.");
                            }
                        }
                    },
                    {
                        id:"Cancelar",
                        text:"Cancelar",
                        click:function()
                        {
                            $(".dialog-form").dialog("destroy");
                            $(".dialog-form").dialog("close");
                        }
                    }
                ] //buttons
            }) //dialog
                              
            if(id != 0 )
            {
                $("#Aplicar").hide();
            }  
        } //success
    }) //$.ajax
}

function filtrar()
{
    var filtro = "/catalogos/notificacion/grid";

    if($("#nombre").val() != "")
    {
            filtro += "/nombre/"+$("#nombre").val();
    }

    if($("#estatus").val() != "" && $("#estatus").val() != 0)
    {
            filtro += "/estatus/"+$("#estatus").val();
    }

    if($("#desde").val() != "")
    {
            filtro += "/desde/"+$("#desde").val();
    }

    if($("#hasta").val() != "")
    {
            filtro += "/hasta/"+$("#hasta").val();
    }

    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{
    $("#nombre").attr('value','');
    $("#estatus").attr('value','0');
    $("#desde").attr('value','');
    $("#hasta").attr('value','');

    var filtro = "/catalogos/notificacion/grid";    
    
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function eliminar(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/notificacion/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                if(res.trim() == 1)
                    alert('El registro ha sido eliminado.');
                else
                    alert('El registro NO ha sido eliminado. Inténtelo nuevamente [' + res.trim() + "]");
                filtrar();
            }
        });
    }
}

function cambioTXT()
{
    var size = calculaSizeTXT();
    document.getElementById('caracteres').innerHTML = "<label>No. CARACTERES: "+size+"</label>";
    if(size > 160) document.getElementById("caracteres").className = "error2";
    else document.getElementById("caracteres").className = 'caracteres';

}

function calculaSizeTXT()
{
    var nombre = ($("#descripcion").val().split("[nombre]").length - 1) + ($("#descripcion").val().split("[NOMBRE]").length - 1);
    var pago = ($("#descripcion").val().split("[pago]").length - 1) + ($("#descripcion").val().split("[PAGO]").length - 1);

    etiquetas = (nombre*12) + (pago*8);
    restar = (nombre*8) + (pago*6);

    var size = ($("#descripcion").val().length - restar) + etiquetas;

    return size;
}

function etiqueta(tipo)
{
    if(tipo==1) $("#descripcion").val($("#descripcion").val() + "[NOMBRE]");
    else $("#descripcion").val($("#descripcion").val() + "[PAGO]");
    cambioTXT();
}