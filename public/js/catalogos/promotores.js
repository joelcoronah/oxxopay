var tipo;
function agregarPromotor(id)
{
    $.ajax({
            url:"/catalogos/promotores/agregar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);

                    $('#plaza_id').change(function()
                    {
                        $.ajax({
                            type: 'POST',
                            
                            url:"/catalogos/index/plazas/id/"+$('#plaza_id').attr('value'),
                            success:
                            function respuesta(res)
                            {
                                $('#sucursal_id').html(res);
                            }
                        });
                    }); 

                $("#frmPromotor").validate(
                {
                   rules:
                   {
                     plaza_id: {required:true, min:1}
                   },
                   messages:
                   {
                    plaza_id: {min: "Debe elegir la plaza a la que pertenece el técnico"}
                   },
                   submitHandler: function(form) 
                   {
					  $("#Aplicar,#Aceptar").hide();  
                     $(form).ajaxSubmit(
                     {
                        success:    function(response) 
                        { 
                            $('#flexgrid').flexReload();
                            if(tipo==1)
                            {
                                $('#frmPromotor').resetForm();
								$("#Aplicar,#Aceptar").show();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							 
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar promotor",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmPromotor").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmPromotor").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

function eliminarPromotor(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/promotores/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                $('#flexgrid').flexReload();
            }
        });
    }
}

$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/promotores/grid',
        dataType: 'xml',
        colModel : [
                {display: 'PLAZA',  name:'Plaza.nombre',width : 350, sortable : true, align: 'center'},
                {display: 'NOMBRE',  name:'nombre',width : 350, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
        
    $('#fplaza').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/promotores/plazas/id/"+$('#fplaza').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });
    });   
    
});

function filtrarPromotores()
{
    var filtro = "/catalogos/promotores/grid";
    var imprimir="/catalogos/promotores/imprimir";
    var exportar="/catalogos/promotores/exportar";

    if($("#fnombre").val() != "")
    {
        filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
        exportar += "/nombre/"+$("#fnombre").val();
    }
    if($("#fplaza").val() != "")
    {
        filtro += "/plaza/"+$("#fplaza").val();
        imprimir += "/plaza/"+$("#fplaza").val();
        exportar += "/plaza/"+$("#fplaza").val();
    }
    
    if($("#fsucursal").val() != 0)
    {
        filtro += "/sucursal/"+$("#fsucursal").val();
        imprimir += "/sucursal/"+$("#fsucursal").val();
        exportar += "/sucursal/"+$("#fsucursal").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarPromotores()
{
    $("#fnombre").attr('value','');
    $("#fplaza").attr('value',0);
    $("#fsucursal").attr('value',0);

    var filtro = "/catalogos/promotores/grid";
    var imprimir="/catalogos/promotores/imprimir";
    var exportar="/catalogos/promotores/exportar";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}