var tipo;
function agregarServicio(id)
{
    $.ajax({
            url:"/catalogos/productos/agregar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);

                if($("#tipo").val() != 1)
                {
                    $("#incluye_cable").val("0");
                    $("#incluye_internet").val("0");
                    $("#incluye_internet").prop( "disabled", true );
                }

                $("#tipo").change(function(){
                    var val_tipo = $("#tipo").val();
                    if( val_tipo == 1)
                    {
                        $("#incluye_cable").val("1");
                        $("#incluye_internet").prop( "disabled", false );
                    }
                    else 
                    {
                       $("#incluye_cable").val("0");
                       $("#incluye_internet").val("0");
                       $("#incluye_internet").prop( "disabled", true ); 
                    }
                });

                $("#frmProducto").validate(
                {
                    
                   submitHandler: function(form) 
                   {
                     
					 $("#Aplicar,#Aceptar").hide();  
                     $(form).ajaxSubmit(
                     {
                        success:    function(response) 
                        { 
                            $('#flexgrid').flexReload();
                            if(tipo==1)
                            {
                                $('#frmProducto').resetForm();
                                $("#incluye_cable").val("1");
                                $("#incluye_internet").prop( "disabled", false );
								$("#Aplicar,#Aceptar").show();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							 
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar servicio",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmProducto").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmProducto").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

function eliminarServicio(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/productos/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                $('#flexgrid').flexReload();
            }
        });
    }
}


function definirTarifas(id)
{ 
  var mensaje=0;
   $.ajax({
       url:"/catalogos/productos/definirtarifas/id/"+id,
       success:function result(data)
       {
            $(".dialog-form").html(data);
            
            $(function() {  $( "#tabs" ).tabs(); });
            
            $("#frmContratacion").validate(
            {
                submitHandler: function(form) 
                {
							var enviar=true;
					
					       /* $("#tarifa input").each(function()
							{
									if(enviar==false)
										return;
									if($('#'+this.id).val() != '' ^ $('#'+$(this).attr('rel')).val()!="")//
									{
										$('#errorcontatacion').show();				  
										enviar=false;
									}  
                       	     }); */
					
					
								if(enviar==true)	
								{		
									$(form).ajaxSubmit(
									{			 
										success: function(response) 
										{ 				
											alert("Las tarifas  se han actualizado correctamente");
																	
										}
									});
								}
                }
            });
            
            $(".dialog-form").dialog({
                height:'auto',
                width: 1000,
                resizable: false,
                title: "Editar tarifas",
                position: [160,100],
                modal: true,
                buttons: [
                {
                    id:"Aceptar",
                    text:"Aceptar",
                    click:
                    function()
                    {
                        tipo = 2;   
                        $("#frmContratacion").submit();		
                    }
                },
                
                {
                    id:"Cancelar",
                    text:"Cancelar",
                    click:function()
                    {
                        $(".dialog-form").dialog("destroy");
                        $(".dialog-form").dialog("close");
                    }
                }
                ]
            });        
        }
    });
}



$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/productos/grid',
        dataType: 'xml',
        colModel : [
                {display: 'SERVICIO',  name:'nombre',width : 350, sortable : true, align: 'center'},
                {display: 'TIPO DE SERVICIO',  name:'nombre',width : 250, sortable : true, align: 'center'},
				{display: 'DEFINIR TARIFAS',  name:'nombre',width : 150, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    
});

function filtrarServicios()
{
    var filtro = "/catalogos/productos/grid";
    var imprimir="/catalogos/productos/imprimir";
    var exportar="/catalogos/productos/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }
    if($("#ftipo").val() != "")
    {
            filtro += "/tipo/"+$("#ftipo").val();
            imprimir += "/tipo/"+$("#ftipo").val();
            exportar += "/tipo/"+$("#ftipo").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarServicios()
{
    $("#fnombre").attr('value','');
    $("#ftipo").attr('value',0);

    var filtro = "/catalogos/productos/grid";

	var imprimir="/catalogos/productos/imprimir";
    var exportar="/catalogos/productos/exportar";
	
	$('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);


    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}