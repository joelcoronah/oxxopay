var tipo;
function agregarTecnico(id)
{
    $.ajax({
            url:"/catalogos/tecnicos/agregar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
                $("#fecha_ingreso").datepicker({changeYear:true, yearRange:'c-30:c', dateFormat:'yy-mm-dd'});

                $('#plaza_id').change(function()
                {
                        $.ajax({
                            type: 'POST',
                            
                            url:"/catalogos/tecnicos/plazas/id/"+$('#plaza_id').attr('value'),
                            success:
                            function respuesta(res)
                            {
                                $('#sucursal_id').html(res);
                            }
                        });
                    }); 

                $("#frmTecnico").validate(
                {
                   rules:
                   {
                     plaza_id: {required:true, min:1}
                   },
                   messages:
                   {
                    plaza_id: {min: "Debe elegir la plaza a la que pertenece el técnico"}
                   },
                   submitHandler: function(form) 
                   {
					  $("#Aplicar,#Aceptar").hide();  
                     $(form).ajaxSubmit(
                     {
                        success:    function(response) 
                        { 
                            $('#flexgrid').flexReload();
                            if(tipo==1)
                            {
                                $('#frmTecnico').resetForm();
								$("#Aplicar,#Aceptar").show();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							 
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar técnico",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmTecnico").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmTecnico").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

function eliminarTecnico(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/tecnicos/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                $('#flexgrid').flexReload();
            }
        });
    }
}

$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/tecnicos/grid',
        dataType: 'xml',
        colModel : [
                {display: 'PLAZA',  name:'Plaza.nombre',width : 350, sortable : true, align: 'center'},
                {display: 'NOMBRE',  name:'nombre',width : 350, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

        $('#fplaza').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/tecnicos/plazas/id/"+$('#fplaza').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });
    }); 

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    }); 
});

function filtrarTecnicos()
{
    var filtro = "/catalogos/tecnicos/grid";
    var imprimir="/catalogos/tecnicos/imprimir";
    var exportar="/catalogos/tecnicos/exportar";

    if($("#fnombre").val() != "")
    {
        filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
        exportar += "/nombre/"+$("#fnombre").val();
    }
    if($("#fplaza").val() != "")
    {
        filtro += "/plaza/"+$("#fplaza").val();
        imprimir += "/plaza/"+$("#fplaza").val();
        exportar += "/plaza/"+$("#fplaza").val();
    }
    
    if($("#fsucursal").val() != 0)
    {
        filtro += "/sucursal/"+$("#fsucursal").val();
        imprimir += "/sucursal/"+$("#fsucursal").val();
        exportar += "/sucursal/"+$("#fsucursal").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarTecnicos()
{
    $("#fnombre").attr('value','');
    $("#fplaza").attr('value',0);

    var filtro = "/catalogos/tecnicos/grid";
    var imprimir="/catalogos/tecnicos/imprimir";
    var exportar="/catalogos/tecnicos/exportar";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}