var tipo;

$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/colonia/grid',
        dataType: 'xml',
        colModel : [
                {display: 'ESTADO',  name:'Zona.Municipio.Estado.nombre',width : 200, sortable : true, align: 'center'},
                {display: 'MUNICIPIO',  name:'Zona.Municipio.nombre',width : 200, sortable : true, align: 'center'},
                {display: 'ZONA',  name:'Zona.nombre',width : 200, sortable : true, align: 'center'},
                {display: 'COLONIA',  name:'nombre',width : 150, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    $('#festado').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/colonia/municipios/id/"+$('#festado').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fmunicipio').html(res);
            }
        });

        $.ajax({
            type: 'POST',
            url:"/catalogos/colonia/zonas/id/0",
            success:
            function respuesta(res)
            {
               $('#fzona').html(res);
            }
        });
    });

    $('#fmunicipio').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/colonia/zonas/id/"+$('#fmunicipio').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fzona').html(res);
            }
        });
    });
});

function agregar(id)
{
    
    $.ajax({
        
        url:"/catalogos/colonia/agregar/id/"+id,
        success:function result(data){
            
            $(".dialog-form").html(data);

            $('#estado_id').change(function()
            {
                $.ajax({
                    type: 'POST',
                    url:"/catalogos/colonia/municipios/id/"+$('#estado_id').attr('value'),
                    success:
                    function respuesta(res)
                    {
                       $('#municipio_id').html(res);
                    }
                });

                
                $('#zona_id').html('<option value="0">Seleccione una zona</option>');
                
            });

            $('#municipio_id').change(function()
            {
                $.ajax({
                    type: 'POST',
                    url:"/catalogos/colonia/zonas/id/"+$('#municipio_id').attr('value'),
                    success:
                    function respuesta(res)
                    {
                       $('#zona_id').html(res);
                    }
                });
            });

            $("#frm").validate({
                
                submitHandler: function(form){
                    
                    $("#Aplicar,#Aceptar").hide();
                    
                    $(form).ajaxSubmit({
                    
                        success:function(response){
                            
                            filtrar();
                            
                            if(tipo==1)
                            {
                                $('#frm').resetForm();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
                            
                            if(id != 0 )
                                $("#Aceptar").show();
                            else
                                $("#Aplicar,#Aceptar").show();
                        } //success
                    }) //ajaxSubmit
                } //submitHandler
            }) //validate
            
            $(".dialog-form").dialog({              
                height:'auto',
                width: 'auto',
                resizable: false,
                title: "Agregar colonia",
                position: "center",
                modal: true,
                buttons: [              
                    {
                        id:"Aplicar",
                        text:"Aplicar",
                        click:function()
                        {
                            tipo = 1;
                            $("#frm").submit();
                        }
                    },
                    {
                        id:"Aceptar",
                        text:"Aceptar",
                        click:
                        function()
                        {
                            tipo = 2;
                            $("#frm").submit();
                        }
                    },
                    {
                        id:"Cancelar",
                        text:"Cancelar",
                        click:function()
                        {
                            $(".dialog-form").dialog("destroy");
                            $(".dialog-form").dialog("close");
                        }
                    }
                ] //buttons
            }) //dialog
                              
            if(id != 0 )
            {
                $("#Aplicar").hide();
            }  
        } //success
    }) //$.ajax
}

function filtrar()
{
    var filtro = "/catalogos/colonia/grid";
    var imprimir="/catalogos/colonia/imprimir";
    var exportar="/catalogos/colonia/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }

    if($("#festado").val() >0)
    {
            filtro += "/estado/"+$("#festado").val();
            imprimir += "/estado/"+$("#festado").val();
            exportar += "/estado/"+$("#festado").val();
    }

    if($("#fmunicipio").val() >0)
    {
            filtro += "/municipio/"+$("#fmunicipio").val();
            imprimir += "/municipio/"+$("#fmunicipio").val();
            exportar += "/municipio/"+$("#fmunicipio").val();
    }

    if($("#fzona").val() >0)
    {
            filtro += "/zona/"+$("#fzona").val();
            imprimir += "/zona/"+$("#fzona").val();
            exportar += "/zona/"+$("#fzona").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{

    $("#fnombre").attr('value','');
    $("#festado").attr('value',0);
    
    $.ajax({
        type: 'POST',
        url:"/catalogos/colonia/municipios/id/0",
        success:
        function respuesta(res)
        {
           $('#fmunicipio').html(res);
        }
    });

    $.ajax({
        type: 'POST',
        url:"/catalogos/colonia/zonas/id/0",
        success:
        function respuesta(res)
        {
           $('#fzona').html(res);
        }
    });

    var filtro = "/catalogos/colonia/grid";	
	var imprimir="/catalogos/colonia/imprimir";
    var exportar="/catalogos/colonia/exportar";
	
	$('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);

    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function eliminar(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/colonia/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                filtrar();
            }
        });
    }
}
