var tipo;

function validarMatriz()
{	
	$('#frmTarifas').submit();
}

$(document).ready(function()
{

		$("#estado_id").bind("change",function(){
			$.ajax({
		       url:"/catalogos/tarifaspromocionrecuperacion/obtener-plazas/estado_id/"+$(this).val(),
		       success:function result(data)
		       {
		       		//console.log(data);
		       		document.getElementById('plaza_id').innerHTML = data.trim();
		       },error:function result(data){
		       		console.log(data);
		       }
    		});
		});
		$("#plaza_id").bind("change",function(){
			if($(this).val()!=''){
					$.ajax({
				       url:"/catalogos/tarifaspromocionrecuperacion/estadoparcial/estado/"+$("#estado_id").val()+"/plaza/"+$(this).val(),
				       success:function result(data)
				       {
				       		document.getElementById('contenido_estado').innerHTML = data.trim();
				       }
		    		});
			}else{
				document.getElementById('contenido_estado').innerHTML='';
			}
			
		});





		jQuery.validator.addClassRules("fee", {
		  required: false,
		  number: true,
		  min: 0
		});

		jQuery.validator.addClassRules("fee2", {
		  required: true,
		  number: true,
		  min: 0
		});

		var pasada=0;
		$('.botoneraTarifas input').each(function()
		{
				if(pasada==0)
				{
					$("#"+this.id).removeClass('btn-primary').addClass('btn-success');
					objetivo=this.id.split('_');
					$.ajax({
				       url:"/catalogos/tarifaspromocionrecuperacion/estadoparcial/estado/"+objetivo[1],
				       success:function result(data)
				       {
				       		document.getElementById('contenido_estado').innerHTML = data.trim();
				       }
		    		});
				}
				pasada++;
		});
		
		$('.botoneraTarifas input').click(function()
		{
			$('.botoneraTarifas input').removeClass('btn-success').addClass('btn-primary');
			
			$("#"+this.id).removeClass('btn-primary').addClass('btn-success');
			objetivo=this.id.split('_');
			$.ajax({
		       url:"/catalogos/tarifaspromocionrecuperacion/estadoparcial/estado/"+objetivo[1],
		       success:function result(data)
		       {
		       		//console.log(data);
		       		document.getElementById('contenido_estado').innerHTML = data.trim();
		       }
    		});
		});
		
		$("#frmTarifas").validate();		
		
});

function noexisteregistro()
{
	alert("Debe definir primero la tarifa general para este estatus y guardarla");
}

function agregarTarifasPorMes(plaza_id, servicio_id, cliente_status)
{
	var mensaje=0;
   	$.ajax({
       url:"/catalogos/tarifaspromocionrecuperacion/definiciontarifas/plaza_id/"+plaza_id+"/servicio_id/"+servicio_id+"/cliente_status/"+cliente_status,
       success:function result(data)
       {
            $(".dialog-form").html(data);
            
            $(function() {  $( "#tabs" ).tabs(); });
            
            $("#frmTarifasMeses").validate(
            {
                submitHandler: function(form) 
                {
							var enviar=true;

							setFee();

					        $("#tarifa input").each(function()
							{
									if(enviar==false)
										return;
									if($('#'+this.id).val() != '' ^ $('#'+$(this).attr('rel')).val()!="")//
									{
										$('#errorcontatacion').show();				  
										enviar=false;
									}  
                       	     }); 
					
					
								if(enviar==true)	
								{		
									$(form).ajaxSubmit(
									{			 
										success: function(response) 
										{ 				
											alert("Las tarifas de promoción se han actualizado correctamente");
																	
										}
									});
								}
                }
            });
            
            $(".dialog-form").dialog({
                height:'auto',
                width: 1000,
                resizable: false,
                title: "Editar tarifas",
                position: [120,100],
                modal: true,
                buttons: [
                {
                    id:"Aceptar",
                    text:"Aceptar",
                    click:
                    function()
                    {
                        tipo = 2;
                        var bandera=true;
                        //Hacemos la evaluacion para determinar si se tiene que llenar todos los campos de un mes
							if($("#mes1_1_a_5").val() != "" || $("#mes1_6_a_10").val() != "" || $("#mes1_11").val() != "")
							{
								$("#mes1_1_a_5").removeClass('fee').addClass('fee2');
								$("#mes1_6_a_10").removeClass('fee').addClass('fee2');
								$("#mes1_11").removeClass('fee').addClass('fee2');
							}else
							{
								$("#mes1_1_a_5").removeClass('fee2').addClass('fee');
								$("#mes1_6_a_10").removeClass('fee2').addClass('fee');
								$("#mes1_11").removeClass('fee2').addClass('fee');
							}

							if($("#mes2_1_a_5").val() != "" || $("#mes2_6_a_10").val() != "" || $("#mes2_11").val() != "")
							{
								$("#mes2_1_a_5").removeClass('fee').addClass('fee2');
								$("#mes2_6_a_10").removeClass('fee').addClass('fee2');
								$("#mes2_11").removeClass('fee').addClass('fee2');
							}else
							{
								$("#mes2_1_a_5").removeClass('fee2').addClass('fee');
								$("#mes2_6_a_10").removeClass('fee2').addClass('fee');
								$("#mes2_11").removeClass('fee2').addClass('fee');
							}

							if($("#mes3_1_a_5").val() != "" || $("#mes3_6_a_10").val() != "" || $("#mes3_11").val() != "")
							{
								$("#mes3_1_a_5").removeClass('fee').addClass('fee2');
								$("#mes3_6_a_10").removeClass('fee').addClass('fee2');
								$("#mes3_11").removeClass('fee').addClass('fee2');
							}else
							{
								$("#mes3_1_a_5").removeClass('fee2').addClass('fee');
								$("#mes3_6_a_10").removeClass('fee2').addClass('fee');
								$("#mes3_11").removeClass('fee2').addClass('fee');
							}

							if($("#mes4_1_a_5").val() != "" || $("#mes4_6_a_10").val() != "" || $("#mes4_11").val() != "")
							{
								$("#mes4_1_a_5").removeClass('fee').addClass('fee2');
								$("#mes4_6_a_10").removeClass('fee').addClass('fee2');
								$("#mes4_11").removeClass('fee').addClass('fee2');
							}else
							{
								$("#mes4_1_a_5").removeClass('fee2').addClass('fee');
								$("#mes4_6_a_10").removeClass('fee2').addClass('fee');
								$("#mes4_11").removeClass('fee2').addClass('fee');
							}

							if($("#mes5_1_a_5").val() != "" || $("#mes5_6_a_10").val() != "" || $("#mes5_11").val() != "")
							{
								$("#mes5_1_a_5").removeClass('fee').addClass('fee2');
								$("#mes5_6_a_10").removeClass('fee').addClass('fee2');
								$("#mes5_11").removeClass('fee').addClass('fee2');
							}else
							{
								$("#mes5_1_a_5").removeClass('fee2').addClass('fee');
								$("#mes5_6_a_10").removeClass('fee2').addClass('fee');
								$("#mes5_11").removeClass('fee2').addClass('fee');
							}

							if($("#mes6_1_a_5").val() != "" || $("#mes6_6_a_10").val() != "" || $("#mes6_11").val() != "")
							{
								$("#mes6_1_a_5").removeClass('fee').addClass('fee2');
								$("#mes6_6_a_10").removeClass('fee').addClass('fee2');
								$("#mes6_11").removeClass('fee').addClass('fee2');
							}else
							{
								$("#mes6_1_a_5").removeClass('fee2').addClass('fee');
								$("#mes6_6_a_10").removeClass('fee2').addClass('fee');
								$("#mes6_11").removeClass('fee2').addClass('fee');
							}
							
							if(!bandera) $('#errorcontatacion').show();
                        $("#frmTarifasMeses").submit();		
                    }
                },
                
                {
                    id:"Cancelar",
                    text:"Cancelar",
                    click:function()
                    {
                        $(".dialog-form").dialog("destroy");
                        $(".dialog-form").dialog("close");
                    }
                }
                ]
            });        
        }
    });
}

function setFee()
{
	$("#mes1_1_a_5").removeClass('fee2').addClass('fee');
	$("#mes1_6_a_10").removeClass('fee2').addClass('fee');
	$("#mes1_11").removeClass('fee2').addClass('fee');

	$("#mes2_1_a_5").removeClass('fee2').addClass('fee');
	$("#mes2_6_a_10").removeClass('fee2').addClass('fee');
	$("#mes2_11").removeClass('fee2').addClass('fee');

	$("#mes3_1_a_5").removeClass('fee2').addClass('fee');
	$("#mes3_6_a_10").removeClass('fee2').addClass('fee');
	$("#mes3_11").removeClass('fee2').addClass('fee');

	$("#mes4_1_a_5").removeClass('fee2').addClass('fee');
	$("#mes4_6_a_10").removeClass('fee2').addClass('fee');
	$("#mes4_11").removeClass('fee2').addClass('fee');

	$("#mes5_1_a_5").removeClass('fee2').addClass('fee');
	$("#mes5_6_a_10").removeClass('fee2').addClass('fee');
	$("#mes5_11").removeClass('fee2').addClass('fee');

	$("#mes6_1_a_5").removeClass('fee2').addClass('fee');
	$("#mes6_6_a_10").removeClass('fee2').addClass('fee');
	$("#mes6_11").removeClass('fee2').addClass('fee');
}