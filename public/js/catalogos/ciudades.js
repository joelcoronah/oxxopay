var tipo;
function agregarCiudad(id)
{
    $.ajax({
            url:"/catalogos/ciudades/agregar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);

                $("#frmCiudad").validate(
                {
                   rules:
                   {
                     estado_id: {required:true, min:1}

                   },
                   messages:
                   {
                    estado_id: {min: "Debe elegir el estado en el que se encuentra la plaza"}
                   }, 
                   submitHandler: function(form) 
                   {
					 $("#Aplicar,#Aceptar").hide();
                     $(form).ajaxSubmit(
                     {
                        success:    function(response) 
                        { 
							
                            $('#flexgrid').flexReload();
                            if(tipo==1)
                            {
                                $('#frmCiudad').resetForm();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							
							$("#Aplicar,#Aceptar").show(); 
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar ciudad",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmCiudad").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmCiudad").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

function eliminarCiudad(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este municipio?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/ciudades/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('La ciudad ha sido eliminada.');
                $('#flexgrid').flexReload();
            }
        });
    }
}




$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/ciudades/grid',
        dataType: 'xml',
        colModel : [
                {display: 'ESTADO',  name:'Estado.nombre',width : 350, sortable : true, align: 'center'},
                {display: 'CIUDAD',  name:'nombre',width : 350, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    
});

function filtrarCiudades()
{
    var filtro = "/catalogos/ciudades/grid";
    var imprimir="/catalogos/ciudades/imprimir";
    var exportar="/catalogos/ciudades/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }
    if($("#festado").val() >0)
    {
            filtro += "/estado/"+$("#festado").val();
            imprimir += "/estado/"+$("#festado").val();
            exportar += "/estado/"+$("#festado").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarCiudades()
{
    $("#fnombre").attr('value','');
    $("#festado").attr('value',0);

    var filtro = "/catalogos/ciudades/grid";
	
	var imprimir="/catalogos/ciudades/imprimir";
    var exportar="/catalogos/ciudades/exportar";
	
	$('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);


    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}