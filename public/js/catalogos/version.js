var tipo;

$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/version/grid',
        dataType: 'xml',
        colModel : [
                {display: 'TIPO',  name:'tipo',width : 250, sortable : true, align: 'center'},
                {display: 'NOMBRE',  name:'nombre',width : 500, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
});

function agregar(id)
{
    
    $.ajax({
        
        url:"/catalogos/version/agregar/id/"+id,
        success:function result(data){
            
            $(".dialog-form").html(data);

            $("#frm").validate({
                
                submitHandler: function(form){
                    
                    $("#Aplicar,#Aceptar").hide();
                    
                    $(form).ajaxSubmit({
                    
                        success:function(response){
                            
                            filtrar();
                            
                            if(tipo==1)
                            {
                                $('#frm').resetForm();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
                            
                            if(id != 0 )
                                $("#Aceptar").show();
                            else
                                $("#Aplicar,#Aceptar").show();
                        } //success
                    }) //ajaxSubmit
                } //submitHandler
            }) //validate
            
            $(".dialog-form").dialog({              
                height:'auto',
                width: 'auto',
                resizable: false,
                title: "Agregar version",
                position: "center",
                modal: true,
                buttons: [              
                    {
                        id:"Aplicar",
                        text:"Aplicar",
                        click:function()
                        {
                            tipo = 1;
                            $("#frm").submit();
                        }
                    },
                    {
                        id:"Aceptar",
                        text:"Aceptar",
                        click:
                        function()
                        {
                            tipo = 2;
                            $("#frm").submit();
                        }
                    },
                    {
                        id:"Cancelar",
                        text:"Cancelar",
                        click:function()
                        {
                            $(".dialog-form").dialog("destroy");
                            $(".dialog-form").dialog("close");
                        }
                    }
                ] //buttons
            }) //dialog
                              
            if(id != 0 )
            {
                $("#Aplicar").hide();
            }  
        } //success
    }) //$.ajax
}

function filtrar()
{
    var filtro = "/catalogos/version/grid";

    if($("#fnombre").val() != "")
        filtro += "/nombre/"+$("#fnombre").val();

    if($("#ftipo").val() >0)
        filtro += "/tipo/"+$("#ftipo").val();

    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiar()
{

    $("#fnombre").attr('value','');
    $("#ftipo").attr('value',0);
    
    filtrar(); 
}

function eliminar(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/version/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                filtrar();
            }
        });
    }
}