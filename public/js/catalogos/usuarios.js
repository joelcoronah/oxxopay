var tipo;
function agregarUsuario(id)
{
    $.ajax({
            url:"/catalogos/index/agregar/id/"+id,
            success:function result(data)
            {                    
                    $(".dialog-form").html(data);

                    if($("#tipo").val()=='4' || $("#tipo").val()=='6')
                    {
                       $(".inputViaja").show();
                    }
                    else
                    {
                        $(".inputViaja").hide();
                    }
                   
                    $("#fecha_ingreso").datepicker({changeYear:true, yearRange:'c-30:c', dateFormat:'yy-mm-dd'});

                          
                    $('#plaza_id').change(function()
                    {
                        $.ajax({
                            type: 'POST',
                            
                            url:"/catalogos/index/plazas/id/"+$('#plaza_id').attr('value'),
                            success:
                            function respuesta(res)
                            {
                                $('#sucursal_id').html(res);
                            }
                        });
                    });   
                    
                    $("#tipo").change(function()
                    {
                        if($("#tipo").val()!='0')
                        {
                           $("#plaza_id").attr('disabled',false);
                           $("#sucursal_id").attr('disabled',false);
                        }
                        if($("#tipo").val()=='0')
                        {
                           $("#plaza_id").attr('disabled',true);
                           $("#sucursal_id").attr('disabled',true);
                        }

                        if($("#tipo").val()!='2')
                        {
                            $("#plaza_id").multiselect("destroy");
                        }

                        if($('#tipo').val() == 0 || $('#tipo').val() == 2){
                            $('#promotor').attr('disabled', true);
                            //alert('pelas');
                        }

                        else {
                            $('#promotor').removeAttr("disabled");
                            //alert('bien');
                        }

                        if($("#tipo").val()=='2')
                        {
                           $("#sucursal_id").attr('disabled',true);
                           $("#plaza_id").attr('multple','multple');
                           $("#plaza_id").multiselect({
                                noneSelectedText: "",
                                click: function(event, ui)
                                {
                                    if(ui.checked)
                                    {
                                        $("#plaza_"+ui.value).attr("value",ui.value);
                                    }
                                    else
                                    {
                                        $("#plaza_"+ui.value).attr("value","0");   
                                    }
                                }
                            });
                        }

                        if($("#tipo").val()=='4' || $("#tipo").val()=='6')
                        {
                           $(".inputViaja").show();
                        }
                        else
                        {
                            $(".inputViaja").hide();
                        }
                    });
                    
                    $("#frmUsuario").validate(
                    {
                       rules:
                       {
                         plaza_id: {required:function(){if($("#tipo").val()=='1')return true; else return false;},min:1}
                       },
                       messages:
                       {
                        plaza_id: {min: "Debe elegir una plaza para este tipo de usuario"}
                       },
                       submitHandler: function(form) 
                       {
                            $("#Aplicar,#Aceptar").hide(); 
                         $(form).ajaxSubmit(
                         {
                            success:    function(response) 
                            { 
                                if(response=="-1")
                                {
                                    $("#Aplicar,#Aceptar").show(); 
                                    alert("Este nombre de usuario ya existe");
                				}
                				else
                				{
                                    $('#flexgrid').flexReload();
                                    $("#Aplicar,#Aceptar").show(); 
                                    if(tipo==1)
                                    {
                                        $("#plaza_id").removeAttr("multple");
                                        $("#plaza_id").attr('disabled',true);
                                        $("#sucursal_id").attr('disabled',true);
                                        $("#plaza_id").multiselect("destroy");
                                        $('#frmUsuario').resetForm();	
                                    }
                                    else
                                    {
                                        $(".dialog-form").dialog("destroy");
					                    $(".dialog-form").dialog("close");
                                    }
				                }
                            }
                         });
                       }
                    });
                    
                    
                    $(".dialog-form").dialog({
                            height:'auto',
                            width: 'auto',
                            resizable: false,
                            title: "Agregar usuario",
                            position:"center",
                            modal: true,
                            buttons: [{
                                    id:"Aplicar",
                                    text:"Aplicar",
                                    click:function()
                                    {
                                            tipo = 1;
											
                                            $("#frmUsuario").submit();
                                    }
                                    },
                                    {
                                        id:"Aceptar",
                                        text:"Aceptar",
                                        click:
                                        function()
                                        {
                                                tipo = 2;
                                                $("#frmUsuario").submit();
                                        }
                                    },
                                    {
                                        id:"Cancelar",
                                        text:"Cancelar",
                                        click:function()
                                        {
                                            $(".dialog-form").dialog("destroy");
                                            $(".dialog-form").dialog("close");
                                        }
                                    }
                            ]
                            });                     
                    if(id != 0 )
                    {
                        $("#Aplicar").hide();
                    }    
                    
            }
    });

}

function mostrarTablaPermisos(id)
{
  $('#divPermisos'+id).toggle();  
}

function permisosUsuario(id)
{
    $.ajax({
            url:"/catalogos/index/permisos/id/"+id,
            success:function result(data)
            {
				$(".dialog-form").html(data);        
				
					$("#frmPermisos").validate(
                    {
                       submitHandler: function(form) 
                       {
                         $(form).ajaxSubmit(
                         {
                            success:    function(response) 
                            { 
									alert('Los permisos del usuario han sido actualizados');
									location.reload();
                                    $(".dialog-form").dialog("destroy");
                                    $(".dialog-form").dialog("close");
                            }
                         });
                       }
                    });				
				
				
				            
                    $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Permisos de usuario",
                        position: [240,100],
                        modal: true,
                        buttons: 
                        [
                            {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                                    tipo = 2;
                                                    $("#frmPermisos").submit();
                                    }
                            },
                            {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                            $(".dialog-form").dialog("destroy");
                                            $(".dialog-form").dialog("close");
                                    }
                            }
                        ]
                    });                     
                    
            }
    });

}


function eliminarUsuario(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/index/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                $('#flexgrid').flexReload();
            }
        });
    }
}



$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/index/grid',
        dataType: 'xml',
        colModel : [
                {display: 'PLAZA', name:'Plaza.nombre', width : 80, sortable : true, align: 'center'},
                {display: 'NOMBRE',  name:'nombre',width : 220, sortable : true, align: 'center'},
                {display: 'USUARIO', name:'usuario', width : 80, sortable : true, align: 'center'},
                {display: 'TIPO', name:'usuario', width : 180, sortable : true, align: 'center'},
                {display: 'PERMISOS',   width : 100, sortable : false, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "Plaza.nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $('#fplaza').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/index/plazas/id/"+$('#fplaza').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fsucursal').html(res);
            }
        });
    }); 

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
});

function filtrarUsuarios()
{
    var filtro = "/catalogos/index/grid";
    var imprimir="/catalogos/index/imprimir";
    var exportar="/catalogos/index/exportar";
	
    if($("#fnombre").val() != "")
    {
        filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
        exportar += "/nombre/"+$("#fnombre").val();
    }

    if($("#fsucursal").val() != 0)
    {
        filtro += "/sucursal/"+$("#fsucursal").val();
        imprimir += "/sucursal/"+$("#fsucursal").val();
        exportar += "/sucursal/"+$("#fsucursal").val();
    }

    if($("#fplaza").val() != 0)
    {
        filtro += "/plaza/"+$("#fplaza").val();
        imprimir += "/plaza/"+$("#fplaza").val();
        exportar += "/plaza/"+$("#fplaza").val();
    }	
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarUsuarios()
{
    $("#fnombre").attr('value','');
    $("#fplaza").attr('value',0);
    $("#fsucursal").attr('value',0);
    var filtro = "/catalogos/index/grid";
	
    var imprimir="/catalogos/index/imprimir";
    var exportar="/catalogos/index/exportar";
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);

    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}