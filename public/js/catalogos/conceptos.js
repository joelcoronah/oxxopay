var tipo;
function agregarPlaza(id)
{
    $.ajax({
            url:"/catalogos/conceptos/agregar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
                
                $('#estado_id').change(function()
                {
                    $.ajax({
                        type: 'POST',
                        url:"/catalogos/conceptos/municipios/id/"+$('#estado_id').attr('value'),
                        success:
                        function respuesta(res)
                        {
                           $('#municipio_id').html(res);
                        }
                    });
                });
                
                $('#municipio_id').change(function()
                {
                    $.ajax({
                        type: 'POST',
                        url:"/catalogos/conceptos/comunidades/id/"+$('#municipio_id').attr('value'),
                        success:
                        function respuesta(res)
                        {
                           $('#comunidad_id').html(res);
                        }
                    });
                });
                
                $("#frmPlaza").validate(
                {
                   rules:
                   {
                     estado_id: {required:true, min:1},
                     municipio_id: {required:true, min:1},
                     comunidad_id: {required:true, min:1}
                   },
                   messages:
                   {
                    estado_id: {min: "Debe elegir el estado en el que se encuentra la conceptos"},
                    municipio_id: {min: "Debe elegir el municipio en el que se encuentra la conceptos"},
                    comunidad_id: {min: "Debe elegir la comunidad en el que se encuentra la conceptos"}
                   },
                   submitHandler: function(form) 
                   {
					  $("#Aplicar,#Aceptar").hide();  
                     $(form).ajaxSubmit(
                     {
                        success:    function(response) 
                        { 
                            $('#flexgrid').flexReload();
                            if(tipo==1)
                            {
                                $('#frmPlaza').resetForm();
				$("#Aplicar,#Aceptar").show();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							 
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar conceptos",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmPlaza").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmPlaza").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

function eliminarPlaza(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/conceptos/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                $('#flexgrid').flexReload();
            }
        });
    }
}

$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/conceptos/grid',
        dataType: 'xml',
        colModel : [
                {display: 'NOMBRE',  name:'nombre',width : 180, sortable : true, align: 'center'},
                {display: 'DESCRIPCIÓN',  width : 180,  align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    $('#fsucursal').change(function()
    {        
        $.ajax({
            type: 'POST',
            url:"/catalogos/conceptos/conceptoss/id/"+$('#fsucursal').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fnombre').html(res);
            }
        });
    });   
    
    $('#festado').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/conceptos/municipios/id/"+$('#festado').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fmunicipio').html(res);
            }
        });
    });

    $('#fmunicipio').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/conceptos/comunidades/id/"+$('#fmunicipio').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fcomunidad').html(res);
            }
        });
    });
    
});

function filtrarPlazas()
{
    var filtro = "/catalogos/conceptos/grid";
    var imprimir="/catalogos/conceptos/imprimir";
    var exportar="/catalogos/conceptos/exportar";
    
    if($("#fconceptos").val() != 0)
    {
        filtro += "/conceptos/"+$("#fconceptos").val();
        imprimir += "/conceptos/"+$("#fconceptos").val();
        exportar += "/conceptos/"+$("#fconceptos").val();
    }
    
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarPlazas()
{
    $("#fconceptos").attr('value',0);
    
    var filtro = "/catalogos/conceptos/grid";	
    var imprimir="/catalogos/conceptos/imprimir";
    var exportar="/catalogos/conceptos/exportar";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}