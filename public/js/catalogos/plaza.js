var tipo;
function agregarPlaza(id)
{
    $.ajax({
            url:"/catalogos/plaza/agregar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);
                
                $('#estado_id').change(function()
                {
                    $.ajax({
                        type: 'POST',
                        url:"/catalogos/plaza/municipios/id/"+$('#estado_id').attr('value'),
                        success:
                        function respuesta(res)
                        {
                           $('#municipio_id').html(res);
                        }
                    });
                });
                
                $('#municipio_id').change(function()
                {
                    $.ajax({
                        type: 'POST',
                        url:"/catalogos/plaza/comunidades/id/"+$('#municipio_id').attr('value'),
                        success:
                        function respuesta(res)
                        {
                           $('#comunidad_id').html(res);
                        }
                    });
                });
                
                $("#frmPlaza").validate(
                {
                   rules:
                   {
                     estado_id: {required:true, min:1},
                     municipio_id: {required:true, min:1},
                     comunidad_id: {required:true, min:1}
                   },
                   messages:
                   {
                    estado_id: {min: "Debe elegir el estado en el que se encuentra la plaza"},
                    municipio_id: {min: "Debe elegir el municipio en el que se encuentra la plaza"},
                    comunidad_id: {min: "Debe elegir la comunidad en el que se encuentra la plaza"}
                   },
                   submitHandler: function(form) 
                   {
					  $("#Aplicar,#Aceptar").hide();  
                     $(form).ajaxSubmit(
                     {
                        success:    function(response) 
                        { 
                            $('#flexgrid').flexReload();
                            if(tipo==1)
                            {
                                $('#frmPlaza').resetForm();
				$("#Aplicar,#Aceptar").show();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							 
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar plaza",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmPlaza").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmPlaza").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

function eliminarPlaza(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                $('#flexgrid').flexReload();
            }
        });
    }
}

$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/plaza/grid',
        dataType: 'xml',
        colModel : [
                {display: 'NOMBRE',  name:'nombre',width : 180, sortable : true, align: 'center'},
                {display: 'UBICACION',  width : 180,  align: 'center'},
                {display: 'DOMICILIO',  name:'domicilio',width : 180, sortable : true, align: 'center'},
                {display: 'TELÉFONO',  name:'telefono1',width : 200, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    $('#fsucursal').change(function()
    {        
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/plazas/id/"+$('#fsucursal').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fnombre').html(res);
            }
        });
    });   
    
    $('#festado').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/municipios/id/"+$('#festado').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fmunicipio').html(res);
            }
        });
    });

    $('#fmunicipio').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/plaza/comunidades/id/"+$('#fmunicipio').attr('value'),
            success:
            function respuesta(res)
            {
               $('#fcomunidad').html(res);
            }
        });
    });
    
});

function filtrarPlazas()
{
    var filtro = "/catalogos/plaza/grid";
    var imprimir="/catalogos/plaza/imprimir";
    var exportar="/catalogos/plaza/exportar";
    
    if($("#fplaza").val() != 0)
    {
        filtro += "/plaza/"+$("#fplaza").val();
        imprimir += "/plaza/"+$("#fplaza").val();
        exportar += "/plaza/"+$("#fplaza").val();
    }
    
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarPlazas()
{
    $("#fplaza").attr('value',0);
    
    var filtro = "/catalogos/plaza/grid";	
    var imprimir="/catalogos/plaza/imprimir";
    var exportar="/catalogos/plaza/exportar";
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}