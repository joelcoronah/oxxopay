var tipo;
function agregarEstatusdefalla(id)
{
    $.ajax({
            url:"/catalogos/estatusdefallas/agregar/id/"+id,
            success:function result(data)
            {    
                $(".dialog-form").html(data);

                $("#frmEstatusfalla").validate(
                {
                   submitHandler: function(form) 
                   {
					  $("#Aplicar,#Aceptar").hide(); 
                     $(form).ajaxSubmit(
                     {
						 
                        success:    function(response) 
                        { 
                            $('#flexgrid').flexReload();
                            if(tipo==1)
                            {
                                $('#frmEstatusfalla').resetForm();
								$("#Aplicar,#Aceptar").show();
                            }
                            else
                            {
                                $(".dialog-form").dialog("destroy");
                                $(".dialog-form").dialog("close");
                            }
							 
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar estatus de falla",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmEstatusfalla").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmEstatusfalla").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

function eliminarEstatusdefalla(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar este registro?'))
    {
        $.ajax({
            type: 'POST',
            url:"/catalogos/estatusdefallas/eliminar",
            data:{id:id},
            success:
            function respuesta(res)
            {
                alert('El registro ha sido eliminado.');
                $('#flexgrid').flexReload();
            }
        });
    }
}



$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/estatusdefallas/grid',
        dataType: 'xml',
        colModel : [
                {display: 'NOMBRE',  name:'nombre',width : 550, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    
});

function filtrarEstatusdefallas()
{
    var filtro = "/catalogos/estatusdefallas/grid";
    var imprimir="/catalogos/estatusdefallas/imprimir";
    var exportar="/catalogos/estatusdefallas/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarEstatusdefallas()
{
    $("#fnombre").attr('value','');

    var filtro = "/catalogos/estatusdefallas/grid";
	
	var imprimir="/catalogos/estatusdefallas/imprimir";
    var exportar="/catalogos/estatusdefallas/exportar";
	
	$('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);


    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}