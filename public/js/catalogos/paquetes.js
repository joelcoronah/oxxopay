var tipo;

function agregarPaquete(id)
{
 $.ajax({
        url:"/catalogos/paquetes/agregar/id/"+id,
			cache: false,
            success:function result(data)
            {
                $(".dialog-form").html(data);
                
                $("#frmPaquete").validate(
                {
                   submitHandler: function(form) 
                   {
					   $("#Aplicar,#Aceptar").hide(); 
                     $(form).ajaxSubmit(
                     {
                        success: function(response) 
                        { 
							if(response==-1)
							{
								alert('El nombre del paquete ya existe.');
								$("#Aplicar,#Aceptar").show();
							}
							else
							{
								$('#flexgrid').flexReload();
								if(tipo==1)
								{
									$('#frmPaquete').resetForm();
									$("#Aplicar,#Aceptar").show(); 
								}
								else
								{
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
							}
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar paquetes",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmPaquete").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmPaquete").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}

function agregarPaqueteServicio(id)
{
 $.ajax({
        url:"/catalogos/paquetes/agregarpaqueteservicio/id/"+id,
			//cache: false,
            success:function result(data)
            {
                $(".dialog-form").html(data);
                
                $("#frmPaqueteServicio").validate(
                {
                   submitHandler: function(form) 
                   {
					   $("#Aplicar,#Aceptar").hide(); 
                     $(form).ajaxSubmit(
                     {
                        success: function(response) 
                        { 
							
								$('#flexgrid').flexReload();
								if(tipo==1)
								{
									$('#frmPaqueteServicio').resetForm();
									$("#Aplicar,#Aceptar").show(); 
								}
								else
								{
									$(".dialog-form").dialog("destroy");
									$(".dialog-form").dialog("close");
								}
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Paquetes servicios",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmPaqueteServicio").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmPaqueteServicio").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }    
                    
            }
    });

}



function eliminarPaquete(id)
{
    if(confirm('Esta acción no se podrá revertir. ¿En realidad desea eliminar el paquete?'))
    {
        $.ajax({
             url:"/catalogos/paquetes/eliminar/id/"+id,
            success:
            function respuesta(res)
            {
				var filtro = "/catalogos/paquetes/grid";	
				if($("#fnombre").val() != "")
				{
					filtro += "/nombre/"+$("#fnombre").val();
				}
				$('#flexgrid').flexOptions({url: filtro}).flexReload();     
            }
        });
        
		                    
    }
}


function habilitarArea(id)
{
    if(confirm('¿Realmente desea habilitar esta paquete?'))
    {
        $.ajax({
            url:"/catalogos/paquetes/habilitar/id/"+id,
            success:
            function respuesta(res)
            {
				if(res==1)
					alert("El registro ha sido habilitado");
				
				var filtro = "/catalogos/paquetes/grid";	
				if($("#fnombre").val() != "")
				{
					filtro += "/nombre/"+$("#fnombre").val();
				}
					
				$('#flexgrid').flexOptions({url: filtro}).flexReload();  					
            }
        });
    }
}

$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/paquetes/grid',
        dataType: 'xml',
        colModel : [
                {display: 'NOMBRE',name:'nombre',width : 590, sortable : true, align: 'center'},        
				{display: 'ASIGNAR SERVICIOS',  width : 135, sortable : false, align: 'center'},                        
                {display: 'MODIFICAR',   width : 129, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 129, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1025,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
});

function filtrarPaquete()
{
	var filtro = "/catalogos/paquetes/grid";
	
	if($("#fnombre").val() != "")
	{
		filtro += "/nombre/"+$("#fnombre").val();
	}
	
	$('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}
function limpiarPaquete()
{
	$("#fnombre").attr('value','');

    var filtro = "/catalogos/paquetes/grid";   
	var imprimir="/catalogos/paquetes/imprimir";
    var exportar="/catalogos/paquetes/exportar";     
	
	$('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);     
    
    $('#flexgrid').flexOptions({url: filtro}).flexReload();
}

function filtrarServicios()
{
    var filtro = "/catalogos/paquetes/grid";
    var imprimir="/catalogos/paquetes/imprimir";
    var exportar="/catalogos/paquetes/exportar"; 

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();         
    }
    
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}