<?php

class Catalogos_ParametrosdeproductividadController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/parametrosdeproductividad.js'));
    }

    public function indexAction()
    { 
		$parametro=Parametrosdeproductividad::obtenerParametro();   
		$this->view->registro=My_Comun::obtener("Parametrosdeproductividad", $parametro->id);

		$iparametro=Parametrosdeproductividadinternet::obtenerParametro();   
		$this->view->iregistro=My_Comun::obtener("Parametrosdeproductividadinternet", $iparametro->id);
    }
   
    public function guardarAction()
    {
    	### Deshabilitamos el layout y la vista
       	$this->_helper->layout->disableLayout();
       	$this->_helper->viewRenderer->setNoRender(TRUE);
    	
		//echo "<pre>"; print_r($_POST); exit;
		
		$meses = My_Comun::numero_de_meses1( date("Y-m-d") , $_POST['fecha'] ); 
	
		//echo $meses; exit;
		
		if($meses > 1){
			echo "insertar|";
			$_POST['fecha']=date("Y-m-d");
			echo My_Comun::guardar("Parametrosdeproductividad",$_POST['cable'],NULL, " ","id");
			echo "|".My_Comun::guardar("Parametrosdeproductividadinternet",$_POST['inter'],NULL, " ","id");
		}
		
		else{
			echo "modificar|";
			echo My_Comun::guardar("Parametrosdeproductividad",$_POST['cable'],NULL,$_POST['cable']['id'],"id");
			echo My_Comun::guardar("Parametrosdeproductividadinternet",$_POST['inter'],NULL,$_POST['inter']['id'],"id");
		}
			
    }
}