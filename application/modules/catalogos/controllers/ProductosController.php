<?php

class Catalogos_ProductosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/productos.js'));
    }

    public function indexAction()
    {

    }
    
    public function gridAction()
    {
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);              
        
        ### Establecemos el filtro por default
        $filtro = "status = 1 AND (tipo=1 OR tipo=2 OR tipo=3)";
        
        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        
        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $tipo = $this->_getParam('tipo');

        
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        
        if($tipo != '')
        {
            $filtro .= " AND tipo = '$tipo'";
        }  
		
		 ### Extraemos los registros para formar el arreglo del grid
		$registros=My_Comun::registrosGrid("Servicio",$filtro); 
		
		
		$grid=array();
		
		$i=0;
		
		if(My_Permisos::tienePermiso('EDITAR_PRODUCTOS') == 1)
			$editar=1;
		if(My_Permisos::tienePermiso('ELIMINAR_PRODUCTOS') == 1)
			$eliminar=1;	
		
		foreach($registros['registros'] as $registro)
		{
			$grid[$i]['nombre']=$registro->nombre;
			if($registro->tipo==1)
				$grid[$i]['tipo']="SERVICIO";
			if($registro->tipo==2)
				$grid[$i]['tipo']="SERVICIO ADICIONAL";	
				
			
			if($registro->tipo==1)
				$grid[$i]['tarifa']="N/A";
			if($registro->tipo==2)
				$grid[$i]['tarifa']='<span onclick="definirTarifas('.$registro->id.');" title="Definir tarifas"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png" /></span>';		
				
				
				
			if($editar==1)
				$grid[$i]['editar']='<span onclick="agregarServicio('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
			else
				$grid[$i]['editar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';

			if($eliminar==1)
				$grid[$i]['eliminar']='<span onclick="eliminarServicio('.$registro->id.');" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
			else
				$grid[$i]['eliminar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
				
			
			$i++;
		}
		
        My_Comun::grid2($registros,$grid);	
		
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Servicio", $this->_getParam('id'));
        }
    }
	
	
	public function definirtarifasAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        $this->view->EstadosPlazas=Estado::obtenerEstadosPlazas();
        
        $e=array();
        
        foreach($this->view->EstadosPlazas as $estado)
        {   
            $this->view->plazas=TarifaServicioadicional::obtenerPlazas($estado->estado_id);
            $e[$estado->estado_id] = $this->view->plazas; 
        }
        
         $this->view->e=$e;
        
        //echo "<pre>";
        //print_r($e->estado_id); exit;
        // echo $e; exit;
        
       
        //$this->view->plazas=Plaza::obtenerPlazas($e);
        $this->view->TarifasServicios=TarifaServicioadicional::obtenerTarifaAdicional($this->_getParam('id'));   
        
        $t=array();
        foreach ($this->view->TarifasServicios as $tarifa)
        {
            $t["plaza_id"][$tarifa->plaza_id]=$tarifa->id;
            $t["tarifa"][$tarifa->plaza_id]=$tarifa->tarifa;
        }
        $this->view->t=$t;    
       
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Servicio", $this->_getParam('id'));
        }
    }
	
	public function guardartarifasadicionalesAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Vamos a guarda tarifas y periodos
		Servicio::guardarTarifasAdicionales($_POST);

    }

    
    /*public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       if(!isset($_POST['incluye_internet']) || $_POST['incluye_internet'] == "") $_POST['incluye_internet'] = 0;
       if(!isset($_POST['incluye_cable']) || $_POST['incluye_cable'] == "") $_POST['incluye_cable'] = 0;

       echo My_Comun::guardar("Servicio",$_POST,NULL,$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Servicio", $_POST['id']);
    }*/

    public function guardarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
       
        $connection = Doctrine_Manager::connection(); 
        $connection->beginTransaction();
        try
        {
            if(!isset($_POST['incluye_internet']) || $_POST['incluye_internet'] == "") $_POST['incluye_internet'] = 0;
            if(!isset($_POST['incluye_cable']) || $_POST['incluye_cable'] == "") $_POST['incluye_cable'] = 0;
            if(isset($_POST['id']))
            {
                $tipo = 1;
                if($_POST['tipo'] == 2) $tipo = 0;
                $precios = Doctrine_Query::create()->from('ServicioPlaza')->where("servicio_id = ?", $_POST['id'])->execute();
                foreach ($precios as $plaza) {
                    $plaza->status = $tipo;
                    $plaza->save();
                }

                $tipo = 1;
                if($_POST['tipo'] == 1) $tipo = 0;
                $preciosadicionales = Doctrine_Query::create()->from('TarifaServicioadicional')->where("servicioadicional_id = ?", $_POST['id'])->execute();
                foreach ($preciosadicionales as $plaza) {
                    $plaza->status = $tipo;
                    $plaza->save();
                }
            }
            $id = My_Comun::guardar("Servicio",$_POST,NULL,$_POST['id']);
            ### Hacemo commit a la transacción
            $connection->commit();
            echo $id;
        }
        catch (Exception $e)
        {
            ### Rollback en caso de algún problema
            $connection->rollBack();
            echo 'ERR: '.$e->getMessage();
        }
    }
    
    public function eliminarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $connection = Doctrine_Manager::connection(); 
        $connection->beginTransaction();
        try
        {
            $precios = Doctrine_Query::create()->from('ServicioPlaza')->where("servicio_id = ?", $_POST['id'])->execute();
            foreach ($precios as $plaza) {
                $plaza->status = 0;
                $plaza->save();
            }

            $preciosadicionales = Doctrine_Query::create()->from('TarifaServicioadicional')->where("servicioadicional_id = ?", $_POST['id'])->execute();
            foreach ($preciosadicionales as $plaza) {
                $plaza->status = 0;
                $plaza->save();
            }

            $id = My_Comun::deshabilitar("Servicio", $_POST['id']);
            ### Hacemo commit a la transacción
            $connection->commit();
            echo $id;
        }
        catch (Exception $e)
        {
            ### Rollback en caso de algún problema
            $connection->rollBack();
            echo 'ERR: '.$e->getMessage();
        }
    }
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $tipo = $this->_getParam('tipo');

        
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        
        if($tipo != '')
        {
            $filtro .= " AND tipo = '$tipo'";
        }


        
        $registros=  My_Comun::obtenerFiltro("Servicio", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();;
        
        $pdf->Header("IMPRESIÓN DE SERVICIOS");

        $pdf->SetAligns(array('L','L'));
        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(90,90));
        $pdf->Row(array('SERVICIO','TIPO'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
     
           $pdf->Row
           (
                array
                (
                    $registro->nombre,
                    ($registro->tipo==1)?"SERVICIO":"SERVICIO ADICIONAL"
                ),0,1			
           );
        }
        
        
       $pdf->Output();	
       
    }
	
	
function exportarAction()
{
        ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $tipo = $this->_getParam('tipo');

        
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        
        if($tipo != '')
        {
            $filtro .= " AND tipo = '$tipo'";
        }

        
        $registros=  My_Comun::obtenerFiltro("Servicio", $filtro);


        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'SERVICIO',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'TIPO',
                        "width" => 20
                        )				
        );
        
        


        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(
				
					"A$i" =>$registro->nombre,
                    "B$i" =>($registro->tipo==1)?"SERVICIO":"SERVICIO ADICIONAL"

                );
        }
		
        $objPHPExcel->createExcel('Servicios', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'SERVICIOS'));
		
    }	

}



