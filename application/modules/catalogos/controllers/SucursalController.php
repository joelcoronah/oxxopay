<?php

class Catalogos_SucursalController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/sucursal.js'));
    }

    public function indexAction()
    {
        $this->view->plazas=Plaza::obtenerPlazas();
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
                
        if($plaza != 0)
        {
            $filtro .= " AND plaza_id = ".$plaza;
        }
		
	### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Sucursal",$filtro);
	 	   	   
        $editar=My_Permisos::tienePermiso('EDITAR_SUCURSAL');
	$eliminar=My_Permisos::tienePermiso('ELIMINAR_SUCURSAL');
		 
        $grid=array();
        $i=0;
        
        foreach($registros['registros'] as $registro)
        {
            $grid[$i]['plaza']=$registro->Plaza->nombre;
            $grid[$i]['nombre']=$registro->nombre;
            if($editar==1)
                $grid[$i]['editar']='<span onclick="agregarSucursal('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
            else
                $grid[$i]['editar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';
            if($eliminar==1)
                $grid[$i]['eliminar']='<span onclick="eliminarSucursal('.$registro->id.');" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
            else
                $grid[$i]['eliminar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
            $i++;
	}
		
        My_Comun::grid2($registros,$grid);	
	   
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
		
		$this->view->plazas=Plaza::obtenerPlazas();
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Sucursal", $this->_getParam('id'));
        }
    }
   
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       echo My_Comun::guardar("Sucursal",$_POST,NULL,$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Sucursal", $_POST['id']);
    }
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);  
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
                
        if($plaza != 0)
        {
            $filtro .= " AND plaza_id = ".$plaza;
        }
        
        $registros=  My_Comun::obtenerFiltro("Sucursal", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE SUCURSALES");

        $pdf->SetAligns(array('L'));
        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(190));
        $pdf->Row(array('NOMBRE'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
            
           $pdf->Row
           (
                array
                (
                    $registro->nombre
                ),0,1			
           );
        }
             
       $pdf->Output();	
       
    }
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
         
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
                
        if($plaza != 0)
        {
            $filtro .= " AND plaza_id = ".$plaza;
        }
        
        $registros=  My_Comun::obtenerFiltro("Sucursal", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
				
        $i=5;
        //Titulos columna
        $columns_name = array
        (
            "A$i" => array(
                "name" => 'NOMBRE',
                "width" => 50
            )					
        );
        
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
            $i++;
            $data[] = array(
		"A$i" =>$registro->nombre
            );
        }
		
        $objPHPExcel->createExcel('Sucursal', $columns_name, $data, 10,array('rango'=>'A4:A4','size'=>14,'texto'=>'SUCURSALES'));
		
    }	
}