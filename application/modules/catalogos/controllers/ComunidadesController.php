<?php

class Catalogos_ComunidadesController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/comunidades.js'));
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->estados=Estado::obtenerEstados();        
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');

        
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        if($estado > 0)
        {
            $filtro .= " AND Comunidad.Municipio.estado_id = '$estado'";
        }
        if($municipio > 0)
        {
            $filtro .= " AND municipio_id = '$municipio'";
        }

        $columnas = array
        (
            1 => "Municipio.Estado.nombre",
            2 => "Municipio.nombre",
            3 => "nombre"
        );
        
        
        
        if(My_Permisos::tienePermiso('EDITAR_COMUNIDADES') == 1)
        {
                $accion1 = '<span onclick="agregarComunidad(%s);" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
        }
        else
        {
                $accion1 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';
        }

        if(My_Permisos::tienePermiso('ELIMINAR_COMUNIDADES') == 1)
        {
                $accion2 = '<span onclick="eliminarComunidad(%s);" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
        }
        else
        {
                $accion2 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
        }

        $derecha = array
        (
            0 => array
            (
                "type" => 'other',
                "action" => $accion1,
                "params" => 'id'
            ),
            1 => array
            (
                "type" => 'other',
                "action" => $accion2,
                "params" => 'id',
            )
        );

        My_Comun::grid("Comunidad", null, $filtro, $columnas,$derecha);
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos los estados
        $this->view->estados=Estado::obtenerEstados();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {  
            
            $this->view->registro=My_Comun::obtener("Comunidad", $this->_getParam('id'));
            ### Obtenemos los estados
            $this->view->municipios=Municipio::obtenerMunicipiosEstado($this->view->registro->Municipio->estado_id);
        }
    }

    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       echo My_Comun::guardar("Comunidad",$_POST,NULL,$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Comunidad", $_POST['id']);
    }
    
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');

        if($estado > 0)
        {
            $filtro .= " AND Comunidad.Municipio.estado_id = '$estado'";
        }
        
        if($nombre !='')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        
        if($municipio > 0)
        {
            $filtro .= " AND municipio_id = '$municipio'";
        }


        
        $registros=  My_Comun::obtenerFiltro("Comunidad", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();;
        
        $pdf->Header("IMPRESIÓN DE COMUNIDADES");

        $pdf->SetAligns(array('L','L'));
        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(60,60,60));
        $pdf->Row(array('COMUNIDAD','MUNICIPIO','ESTADO'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
     
           $pdf->Row
           (
                array
                (
                    $registro->nombre,
                    $registro->Municipio->nombre,
                    $registro->Municipio->Estado->nombre
                ),0,1			
           );
        }
        
        
       $pdf->Output();	
       
    }
	
	
	
	function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');

        if($estado > 0)
        {
            $filtro .= " AND Comunidad.Municipio.estado_id = '$estado'";
        }
        
        if($nombre !='')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        
        if($municipio > 0)
        {
            $filtro .= " AND municipio_id = '$municipio'";
        }
        
        $registros=  My_Comun::obtenerFiltro("Comunidad", $filtro);


        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'COMUNIDAD',
                        "width" => 20
                        ),
				"B$i" => array(
                        "name" => 'MUNICIPIO',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'ESTADO',
                        "width" => 20
                        )		
        );
        
        


        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(
				
					"A$i" =>$registro->nombre,
                    "B$i" =>$registro->Municipio->nombre,
                    "C$i" =>$registro->Municipio->Estado->nombre
                );
        }
		
        $objPHPExcel->createExcel('Comunidades', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'COMUNIDADES'));
		
    }	
    
}



