<?php

class Catalogos_TipodefallaController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/tipofalla.js'));
    }

    public function indexAction()
    {

    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');

        
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }

        $columnas = array
        (
            1 => "nombre"
        );
        
        
        
        if(My_Permisos::tienePermiso('EDITAR_TIPODEFALLA') == 1)
        {
                $accion1 = '<span onclick="agregarTipodefalla(%s);" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
        }
        else
        {
                $accion1 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';
        }

        if(My_Permisos::tienePermiso('ELIMINAR_TIPODEFALLA') == 1)
        {
                $accion2 = '<span onclick="eliminarTipodefalla(%s);" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
        }
        else
        {
                $accion2 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
        }

        $derecha = array
        (
            0 => array
            (
                "type" => 'other',
                "action" => $accion1,
                "params" => 'id'
            ),
            1 => array
            (
                "type" => 'other',
                "action" => $accion2,
                "params" => 'id',
            )
        );

        My_Comun::grid("TipoDeFalla", null, $filtro, $columnas,$derecha);
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("TipoDeFalla", $this->_getParam('id'));
        }
    }

    
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       echo My_Comun::guardar("TipoDeFalla",$_POST,NULL,$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("TipoDeFalla", $_POST['id']);
    }
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        
        if($nombre !='')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }


        
        $registros=  My_Comun::obtenerFiltro("TipoDeFalla", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE TIPOS DE FALLA");

        $pdf->SetAligns(array('L'));
        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(190));
        $pdf->Row(array('NOMBRE'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
            
           $pdf->Row
           (
                array
                (
                    $registro->nombre
                ),0,1			
           );
        }
        
        
       $pdf->Output();	
       
    }
	
	function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        
        if($nombre !='')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }


        
        $registros=  My_Comun::obtenerFiltro("TipoDeFalla", $filtro);


        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'NOMBRE',
                        "width" => 20
                        )					
        );
        
        


        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(
				
		"A$i" =>$registro->nombre

                );
        }
		
        $objPHPExcel->createExcel('Tiposdefallas', $columns_name, $data, 10,array('rango'=>'A4:D4','size'=>14,'texto'=>'TIPOS DE FALLAS'));
		
    }	


}



