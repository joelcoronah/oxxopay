<?php
class Catalogos_ComisionesController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/comision.js'));
    }

    public function indexAction()
    {
		if(isset($_POST['comision']))
		{
			Comisiones::procesarComisiones($_POST['comision']);
			header("Location: /catalogos/comisiones");
		}
		else
		{
			$this->view->EstadosPlazas=Estado::obtenerEstadosPlazas();
			
			$e=array();
			foreach($this->view->EstadosPlazas as $estado)
			{   
				$this->view->plazas=TarifaConceptoContratacion::obtenerPlazas($estado->estado_id);
				$e[$estado->estado_id] = $this->view->plazas; 
			}
			$this->view->e=$e;  
			
			/*Muestra todos los servicios*/
			$this->view->Servicios=Servicio::obtenerServicios(' tipo=1 OR tipo=3');  	   
			
			### Vamos a armar un arreglo para poder editar tarifas no especiales
			$comisionesPlaza=ComisionPlaza::obtenerComisionPlaza(); 
			
			$arrComisionPlaza=array();
			foreach($comisionesPlaza as $comisionPlaza)
			{
				$arrComisionPlaza[$comisionPlaza->plaza_id][$comisionPlaza->servicio_id]=$comisionPlaza->tarifa;
			}
			$this->view->comisionplaza=$arrComisionPlaza;

		}
    }
 
    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("ServicioPlaza", $this->_getParam('id'));
        }
    }
	
	public function guardarAction()
	{
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
		echo "<pre>";
		print_r($_POST);
		
	}
}