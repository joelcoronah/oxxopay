<?php

class Catalogos_VersionController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/version.js'));
    }

    public function indexAction()
    {
        
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = " status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $tipo = $this->_getParam('tipo');

        
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }

        if($tipo != '')
            $filtro .= " AND tipo = '$tipo'";

        ### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::registrosGrid("Version",$filtro);        
        
        $grid=array();
        
        $i=0;
        
        foreach($registros['registros'] as $registro)
        {

            $grid[$i]['tipo']=($registro->tipo==4?'Técnico':'Vendedor');
            $grid[$i]['nombre']=$registro->nombre;
            
            $grid[$i]['editar']='<span onclick="agregar('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
            $grid[$i]['eliminar']='<span onclick="eliminar('.$registro->id.');" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
            
            $i++;
        }
        
        My_Comun::grid2($registros,$grid);
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Version", $this->_getParam('id'));
        }
    }
    
    public function guardarAction()
    {

        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->setDestination("../public/versiones");
        $files= $adapter->getFileInfo();

        $path=pathinfo($files['archivo']['name']);


        unset($_POST['archivo']);
        $_POST['archivo']='http://sistema.supertv.com.mx/versiones/'.$path['filename'].'_V_'.$_POST['nombre'].".".$path['extension'];

        if(count($files)>0)
        {
            $adapter->addFilter('Rename', array('target'=>"../public/versiones/".$files['archivo']['name'].'_V_'.$_POST['nombre'],'overwrite' => true),$files['archivo']['name']);
            $adapter->receive();
        }

        $id = My_Comun::guardar("Version",$_POST,NULL,$_POST['id']);

        //$mensaje=array("titulo"=>"Nueva versión de la aplicación","mensaje"=>"Existe una nueva versión de la aplicación","seccion"=>"nuevaversion");
        //$usuarios = My_Comun::obtenerFiltro('Usuarios', 'tipo ='.$tipo)

        //foreach($usuario as $usuario)
        //{

            //if(usuario->gcm_id != '')
            //    My_Comun::pushnotification(usuario->gcm_id,$mensaje);
        //}
        
        echo $id;
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       echo My_Comun::deshabilitar("Version", $_POST['id']);
    }
}



