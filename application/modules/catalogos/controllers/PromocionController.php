<?php

class Catalogos_PromocionController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/promocion.js'));
    }

    public function indexAction()
    {

    }

    public function gridAction()
    {
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);              
        
        ### Establecemos el filtro por default
        $filtro = "status = 1";
        
        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        
        if($nombre != '')
        	$filtro .= " AND nombre LIKE '%$nombre%'";
		
		 ### Extraemos los registros para formar el arreglo del grid
		$registros=My_Comun::registrosGrid("Promocion",$filtro); 		
		
		$grid=array();
		
		$i=0;
		### Validamos los permisos
		$editar=My_Permisos::tienePermiso('EDITAR_PROMOCION');
		$eliminar=My_Permisos::tienePermiso('ELIMINAR_PROMOCION');
		
		foreach($registros['registros'] as $registro)
		{
			$grid[$i]['nombre']=$registro->nombre;
			
			$grid[$i]['plaza']='<span onclick="definir('.$registro->id.');" title="Definir"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/catalogo.png" /></span>';		
				
			if($editar==1)
				$grid[$i]['editar']='<span onclick="agregar('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
			else
				$grid[$i]['editar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';

			if($eliminar==1)
				$grid[$i]['eliminar']='<span onclick="eliminar('.$registro->id.');" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
			else
				$grid[$i]['eliminar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
			
			$i++;
		}
		
        My_Comun::grid2($registros,$grid);			
    }
	
	public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Promocion", $this->_getParam('id'));
        }
    }
	
	public function guardarAction()
    {

		### Deshabilitamos el layout y la vista
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$adapter = new Zend_File_Transfer_Adapter_Http();
		$adapter->setDestination("../public/images/promociones");
		$files= $adapter->getFileInfo();		
		
		$id = My_Comun::guardar("Promocion",$_POST,NULL,$_POST['id']);
		
		if(count($files)>0)
		{
			
			unset($_POST['id']);
			unset($_POST['imagen']);
			
			$extension = pathinfo($files['imagen']['name'],PATHINFO_EXTENSION);
			$_POST['id']=$id;			
			$_POST['imagen']=$id.'.'.$extension;
			$adapter->addFilter('Rename', array('target'=>"../public/images/promociones/".$_POST['imagen'],'overwrite' => true),$files['imagen']['name']);
			
			$adapter->receive();
			
			My_Comun::guardar("Promocion",$_POST,NULL,$id);
		}
		
		echo $id;
    }
	
	public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Promocion", $_POST['id']);
    }
	
	public function definirAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
		
		$this->view->promocion_id = $this->_getParam('id');		
        $this->view->EstadosPlazas=Estado::obtenerEstadosPlazas();
        		
        $e=array();        
        foreach($this->view->EstadosPlazas as $estado)
        {   
            $this->view->plazas=Promocion::obtenerPlazas($estado->estado_id);
            $e[$estado->estado_id] = $this->view->plazas; 
        }
		
        $this->view->e=$e;
        $this->view->vigencias=Promocion::obtenerVigencias($this->_getParam('id'));   
        
        $vigencia=array();
        foreach ($this->view->vigencias as $vigencia_)
        {
            $vigencia["plaza_id"][$vigencia_->plaza_id]=$vigencia_->plaza_id;
            $vigencia["inicio"][$vigencia_->plaza_id]=$vigencia_->inicio;
			$vigencia["fin"][$vigencia_->plaza_id]=$vigencia_->fin;
        }
		
        $this->view->vigencia=$vigencia;
    }
	
	public function guardarpromocionAction()
    {

		### Deshabilitamos el layout y la vista
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		Promocion::eliminar($_POST['promocion_id']);
		
		$arreglo = array();		
		$arreglo['id'] = '';
		$arreglo['promocion_id'] = $_POST['promocion_id'];
		
		foreach($_POST['plaza_id'] as $plaza)
		{
		
			if($_POST['inicio'][$plaza] != '')
			{
				
				$arreglo['plaza_id'] = $plaza;
				$arreglo['inicio'] = $_POST['inicio'][$plaza];
				$arreglo['fin'] = $_POST['fin'][$plaza];
				
				My_Comun::guardar("PromocionPlaza",$arreglo,NULL,$arreglo['id'],"");
			}
		}
    }
}



