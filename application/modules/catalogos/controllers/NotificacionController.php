<?php

class Catalogos_NotificacionController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/notificaciones.js'));
    }

    public function indexAction()
    {
        
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = " o.status = 1 ";
        $inner = array();

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $estatus = $this->_getParam('estatus');

        
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND o.nombre LIKE '%$nombre%'";
        }

        if($desde != '' || $hasta != '')
        {
            $inner[] = "o.NotificacionFechas nf";
            if($desde != '') $filtro .= " AND nf.fecha >= '$desde'";
            if($hasta != '') $filtro .= " AND nf.fecha <= '$hasta'";
        }
        
        if($estatus != '' && $estatus > 0)
        {
            $inner[] = "o.NotificacionStatus ne";
            $filtro .= " AND ne.status = '$estatus'";
        }

        ### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::registrosGrid("Notificacion",$filtro, $inner);     

        //echo "<pre>"; print_r($registros); exit;   
        
        $grid=array();
        
        $i=0;
        ### Validamos los permisos
        $editar=My_Permisos::tienePermiso('EDITAR_NOTIFICACION');
        $eliminar=My_Permisos::tienePermiso('ELIMINAR_NOTIFICACION');
        
        foreach($registros['registros'] as $registro)
        {

            $grid[$i]['titulo'] = $registro->nombre;
            
            if($editar==1)
                $grid[$i]['editar']='<span onclick="agregar('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
            else
                $grid[$i]['editar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';

            if($eliminar==1)
                $grid[$i]['eliminar']='<span onclick="eliminar('.$registro->id.');" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
            else
                $grid[$i]['eliminar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
            
            $i++;
        }
        
        My_Comun::grid2($registros,$grid);
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();

        ### Obtenemos las plazas para alimentar el select de plazas
        $this->view->plazas=Plaza::obtenerPlazas();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";

        $this->view->editando = 0;
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->editando = 1;

            $this->view->registro = My_Comun::obtener("Notificacion", $this->_getParam('id'));
            $this->view->estatus = NotificacionStatus::obtenerStatus($this->_getParam('id'));
            $this->view->fechas = NotificacionFechas::obtenerFechas($this->_getParam('id'));
           
            $tmp_plazas = array();
            $mis_plazas = NotificacionPlaza::obtenerPlazas($this->_getParam('id'));
            foreach ($mis_plazas as $tmp_plaza)
            {
                $tmp_plazas[] = $tmp_plaza->plaza_id;
            }
            $this->view->mis_plazas = $tmp_plazas;

            $this->view->e1 = 0; $this->view->e2 = 0; $this->view->e3 = 0; $this->view->e4 = 0;
            $this->view->e5 = 0; $this->view->e6 = 0; $this->view->e7 = 0; $this->view->e8 = 0;
            $this->view->e9 = 0; $this->view->e10 = 0; $this->view->e11 = 0; $this->view->e12 = 0;

            foreach ($this->view->estatus as $estatus) {
                if($estatus->status == 1) $this->view->e1 = $estatus->id;
                if($estatus->status == 2) $this->view->e2 = $estatus->id;
                if($estatus->status == 3) $this->view->e3 = $estatus->id;
                if($estatus->status == 4) $this->view->e4 = $estatus->id;
                if($estatus->status == 5) $this->view->e5 = $estatus->id;
                if($estatus->status == 6) $this->view->e6 = $estatus->id;
                if($estatus->status == 7) $this->view->e7 = $estatus->id;
                if($estatus->status == 8) $this->view->e8 = $estatus->id;
                if($estatus->status == 9) $this->view->e9 = $estatus->id;
                if($estatus->status == 10) $this->view->e10 = $estatus->id;
                if($estatus->status == 11) $this->view->e11 = $estatus->id;
                if($estatus->status == 12) $this->view->e12 = $estatus->id;
            }
        }

        $number;
        if($this->view->editando == 1) $number = count($this->view->fechas)+1;
        else $number = 1;
        echo "*||*".$number;
    }

    
    public function guardarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE); 

        //$_POST['fechas'] = array_values($_POST['fechas']);
        $_POST['estatus'] = array_values($_POST['estatus']);

        $editando = false;
        if($_POST['id'] > 0) $editando = true;
        
        ### Intentamos agregar los registros
        $notificacion_id = My_Comun::guardar("Notificacion",$_POST,"nombre",$_POST['id']);
        
        if($notificacion_id > 0)
        {
            if($editando)
            {
                $e_status = NotificacionStatus::eliminarStatus($_POST['id']);
                if($e_status >= 1) 
                {
                    $e_fechas = NotificacionFechas::eliminarFechas($_POST['id']);
                    if($e_fechas >= 1)
                    {
                        $e_plazas = NotificacionPlaza::eliminarPlazas($_POST['id']);
                        if($e_plazas < 1)
                        {
                            echo -22;
                            exit;
                        }
                    }
                    else
                    {
                        echo -21;
                        exit;
                    }
                }
                else
                {
                    echo -20;
                    exit;
                }
            }

            $tmp_fechas = $_POST['fechas'];
            for ($i=0; $i <= max(array_keys($tmp_fechas)); $i++)
            { 
                if($tmp_fechas[$i] != null && $tmp_fechas[$i] != "")
                {
                    $date = new NotificacionFechas();
                    $date->notificacion_id = $notificacion_id;
                    $date->fecha = $tmp_fechas[$i];
                    if($_POST['cfechas'][$i] != null && $_POST['cfechas'][$i] != '') $date->periodico = $_POST['cfechas'][$i];
                    $date->save();
                }
            }

            foreach ($_POST['estatus'] as $estatus) {
                $status = new NotificacionStatus();
                $status->notificacion_id = $notificacion_id;
                $status->status = $estatus;
                $status->save();
            }

            foreach ($_POST['plaza_id'] as $plazas) {
                $plaza = new NotificacionPlaza();
                $plaza->notificacion_id = $notificacion_id;
                $plaza->plaza_id = $plazas;
                $plaza->save();
            }
        }
        echo $notificacion_id;
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       // Eliminamos sus relaciones de plaza, estatus y fechas
        $e_status = NotificacionStatus::eliminarStatus($_POST['id']);
        if($e_status >= 1) 
        {
            $e_fechas = NotificacionFechas::eliminarFechas($_POST['id']);
            if($e_fechas >= 1)
            {
                $e_plazas = NotificacionPlaza::eliminarPlazas($_POST['id']);
                if($e_plazas < 1)
                {
                    echo -22;
                    exit;
                }
                else
                {
                    echo My_Comun::delete("Notificacion", $_POST['id']);
                }
            }
            else
            {
                echo -21;
                exit;
            }
        }
        else
        {
            echo -20;
            exit;
        }
    }

    /*public function enviarNotificacion()
    {
        $notificaciones = Notificacion::obtenerNotificaciones(date('Y-m-d H:i:s'));
        foreach ($notificaciones as $notificacion)
        {
            echo "Notificacion: ".$notificacion->nombre."<br>";
            $estatus = NotificacionStatus::obtenerStatus($notificacion->id);
            $filtro = "";
            foreach ($estatus as $estado)
            {
                $filtro .= $estado->status.",";
            }
            $filtro = trim($filtro, ',');
            echo "Estatus: ".$filtro."<br>";
            $clientes = Cliente::obtenerCliente('status IN('.$filtro.')');
            foreach ($clientes as $cliente)
            {
                if($cliente->gcm_id != null && $cliente->gcm_id == '')
                {
                    echo "Cliente: ".$cliente->nombre."<br>";
                    //$mensaje=array("titulo"=>$notificacion->nombre,"mensaje"=>$notificacion->descripcion,"seccion"=>"notificaciones");
                    //My_Comun::pushnotification($cliente->gcm_id, $mensaje);
                }
            }
        }
    }*/
}



