<?php

class Catalogos_PlazaController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/plaza.js'));
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $plaza = $this->_getParam('plaza');
        
        if($plaza != 0)
        {
            $filtro .= " AND id =" .$plaza;
        }
        
        $columnas = array
        (
            1 => "nombre",
            2 => "Comunidad.nombre",
            3 => "domicilio",
            4 => "telefono1"
        ); 
        
        if(My_Permisos::tienePermiso('EDITAR_PLAZA') == 1)
        {
            $accion1 = '<span onclick="agregarPlaza(%s);" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
        }
        else
        {
            $accion1 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';
        }

        if(My_Permisos::tienePermiso('ELIMINAR_PLAZA') == 1)
        {
            $accion2 = '<span onclick="eliminarPlaza(%s);" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
        }
        else
        {
            $accion2 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
        }

        $derecha = array
        (
            0 => array
            (
                "type" => 'other',
                "action" => $accion1,
                "params" => 'id'
            ),
            1 => array
            (
                "type" => 'other',
                "action" => $accion2,
                "params" => 'id',
            )
        );

        My_Comun::grid("Plaza", null, $filtro, $columnas,$derecha);
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos los estados
        $this->view->estados=Estado::obtenerEstados();
        $this->view->sucursales=Sucursal::obtenerSucursal();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Plaza", $this->_getParam('id'));
            
            ### Obtenemos los estados
            $this->view->municipios=Municipio::obtenerMunicipios($this->view->registro->Comunidad->municipio_id);
            ### Obtenemos los estados
            $this->view->comunidades=Comunidad::obtenerComunidades($this->view->registro->comunidad_id);
        }
    }
    
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }
    
    public function municipiosAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboMunicipio($this->_getParam('id'));
    }
    
    public function comunidadesAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboComunidad($this->_getParam('id'));
    }
    
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       echo My_Comun::guardar("Plaza",$_POST,NULL,$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Plaza", $_POST['id']);
    }
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);      
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $plaza = $this->_getParam('plaza');
        
        if($plaza != 0)
        {
            $filtro .= " AND id =" .$plaza;
        }
        
        $registros=  My_Comun::obtenerFiltro("Plaza", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE PLAZAS");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(15, 55, 50,30,30));
        $pdf->Row(array('PLAZA','UBICACION','DOMICILIO','TELEFONO','TELEFONO2'),0,1);
         
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
           $pdf->Row
           (
                array
                (
                    $registro->nombre,
                    $registro->Comunidad->nombre.", ".$registro->Municipio->nombre.", ".$registro->Estado->nombre,
                    $registro->domicilio,
                    $registro->telefono1,
                    $registro->telefono2
                ),0,1			
           );
        }
        
       $pdf->Output();	  
    }
		
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $plaza = $this->_getParam('plaza');
        
        if($plaza != 0)
        {
            $filtro .= " AND id =" .$plaza;
        }
        
        $registros=  My_Comun::obtenerFiltro("Plaza", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=6;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'PLAZA',
                        "width" => 8
                        ),
                "B$i" => array(
                        "name" => 'UBICACION',
                        "width" => 37
                        ),
		"C$i" => array(
                        "name" => 'DOMICILIO',
                        "width" => 30
                        ),
		"D$i" => array(
                        "name" => 'TELEFONO1',
                        "width" => 16
                        ),
		"E$i" => array(
                        "name" => 'TELEFONO2',
                        "width" => 16
                        )						
        );
        
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
            $i++;
            $data[] = array(		
                "A$i" =>$registro->nombre,
                "B$i" =>$registro->Comunidad->nombre.", ".$registro->Municipio->nombre.", ".$registro->Estado->nombre,
                "C$i" =>$registro->domicilio,
                "D$i" =>$registro->telefono1,
                "E$i" =>$registro->telefono2,
            );
        }
		
        $objPHPExcel->createExcel('Plazas', $columns_name, $data, 10, array('rango'=>'A4:E4','size'=>14,'texto'=>'PLAZAS'));		
    }	
}