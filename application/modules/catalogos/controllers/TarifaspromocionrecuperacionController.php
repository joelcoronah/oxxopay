<?php
class Catalogos_TarifaspromocionrecuperacionController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/tarifaspromocionrecuperacion.js'));
    }

    public function indexAction()
    {
    	$this->view->EstadosPlazas = Estado::obtenerEstadosPlazas();
		if(isset($_POST['tarifa']))
		{
			TarifaspromocionrecuperacionPlaza::procesarTarifaspromocionrecuperacion($_POST['tarifa']);
			header("Location: /catalogos/tarifaspromocionrecuperacion/");
		}
		/*else
		{
			$this->view->EstadosPlazas = Estado::obtenerEstadosPlazas();
			$this->view->estados = Estado::obtenerEstadosPlazas()->getFirst();
		}*/
    }
    public function obtenerPlazasAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $estado = $this->_getParam("estado_id");
        $plazas = Plaza::obtenerPlazasDeUnEstado($estado);

        $opciones = '<option value="">Seleccione una plaza</option>';
        foreach($plazas as $plaza){
        	$opciones.='<option value="'.$plaza->id.'">'.$plaza->nombre.'</option>';
        }
        echo $opciones;

    }
    public function estadoparcialAction()
    {
    	### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $parametros=array();
    	$parametros['estado'] = $this->_getParam('estado');
    	//Codigo Agregado
    	$parametros['plaza_id'] = $this->_getParam('plaza');
    	//fin codigo agregado
    	
    	$parametros['servicios'] = Servicio::obtenerServicios2(' tipo=1 OR tipo=3 ');
    	//Codigo Agregado
    	$parametros['plazas'] =TarifaConceptoContratacion::obtenerPlaza($this->_getParam('plaza'));
    	$tarifasPlaza = TarifaspromocionrecuperacionPlaza::obtenerTarifaspromocionrecuperacionPlaza2($this->_getParam('plaza'));
    	//fin codigo agregado
    	### linea original $parametros['plazas'] = TarifaConceptoContratacion::obtenerPlazas($this->_getParam('estado'));
		### linea original $tarifasPlaza = TarifaspromocionrecuperacionPlaza::obtenerTarifaspromocionrecuperacionPlaza($this->_getParam('estado')); 

		$arrTarifaPlaza=array();
		foreach($tarifasPlaza as $tarifaPlaza)
		{

			//$arrTarifaPlaza[$tarifaPlaza["plaza_id"]][$tarifaPlaza["servicio_id"]][$tarifaPlaza["cliente_status"]]["tarifa"]=$tarifaPlaza["tarifa"];
			$arrTarifaPlaza[$tarifaPlaza->plaza_id][$tarifaPlaza->servicio_id][$tarifaPlaza->cliente_status]['tarifa']=$tarifaPlaza->tarifa;
			//$arrTarifaPlaza[$tarifaPlaza->Plaza->Estado->nombre][$tarifaPlaza->plaza_id][$tarifaPlaza->servicio_id][$tarifaPlaza->cliente_status]['tarifa']=$tarifaPlaza->tarifa;
		}
		$parametros['tarifaplaza'] = $arrTarifaPlaza;
    	echo $this->view->partial("tarifaspromocionrecuperacion/estado.phtml",array("parametros"=>$parametros));
    }
 
    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("ServicioPlaza", $this->_getParam('id'));
        }
    }
	
	public function guardarAction()
	{
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
		echo "<pre>";
		print_r($_POST);
		
	}

	public function definiciontarifasAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        $this->view->TarifasContratacion=TarifaspromocionrecuperacionPlaza::obtenerTarifasPorMes($this->_getParam('plaza_id'),$this->_getParam('servicio_id'),$this->_getParam('cliente_status'));   

        if(count($this->view->TarifasContratacion) > 0)
        {
        	$t=array();
	        foreach ($this->view->TarifasContratacion as $tarifa)
	        {
	            $t["mes1_1_a_5"]=$tarifa->tarifa1_1;
	            $t["mes1_6_a_10"]=$tarifa->tarifa1_2;
	            $t["mes1_11"]=$tarifa->tarifa1_3;
	            $t["mes2_1_a_5"]=$tarifa->tarifa2_1;
	            $t["mes2_6_a_10"]=$tarifa->tarifa2_2;
	            $t["mes2_11"]=$tarifa->tarifa2_3;
	            $t["mes3_1_a_5"]=$tarifa->tarifa3_1;
	            $t["mes3_6_a_10"]=$tarifa->tarifa3_2;
	            $t["mes3_11"]=$tarifa->tarifa3_3;
	            $t["mes4_1_a_5"]=$tarifa->tarifa4_1;
	            $t["mes4_6_a_10"]=$tarifa->tarifa4_2;
	            $t["mes4_11"]=$tarifa->tarifa4_3;
	            $t["mes5_1_a_5"]=$tarifa->tarifa5_1;
	            $t["mes5_6_a_10"]=$tarifa->tarifa5_2;
	            $t["mes5_11"]=$tarifa->tarifa5_3;
	            $t["mes6_1_a_5"]=$tarifa->tarifa6_1;
	            $t["mes6_6_a_10"]=$tarifa->tarifa6_2;
	            $t["mes6_11"]=$tarifa->tarifa6_3;

	        }
	        $this->view->tarifas=$t;

	        ### Cachamos los id's de los registros; esto se da solo si se está editando
	        $this->view->plaza_id=($this->_getParam('plaza_id')>0)?$this->_getParam('plaza_id'):"";
	        $this->view->servicio_id=($this->_getParam('servicio_id')>0)?$this->_getParam('servicio_id'):"";
	        $this->view->cliente_status=($this->_getParam('cliente_status')>0)?$this->_getParam('cliente_status'):"";
	        
	        ### Si recibimos los id's es porque estamos editando, asi que extraemos los datos
	        if((is_numeric($this->_getParam('plaza_id')) && $this->_getParam('plaza_id')>0) && (is_numeric($this->_getParam('servicio_id')) && $this->_getParam('servicio_id')>0) && (is_numeric($this->_getParam('cliente_status')) && $this->_getParam('cliente_status')>0))
	        {   
	            $this->view->registro=My_Comun::obtener("TarifaspromocionrecuperacionPlaza", $this->_getParam('plaza_id'), $this->_getParam('servicio_id'), $this->_getParam('cliente_status'));
	        }
        }
        else
        {
        	echo '<script type="text/javascript">alert("Debe de asignar una tarifa principal para este estatus y guardarla.");</script>';
        }
	        
    }

    public function guardartarifaspormesAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        ### Vamos a guarda tarifas y periodos
		TarifaspromocionrecuperacionPlaza::guardarTarifas($_POST);
    }


}