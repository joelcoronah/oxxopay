<?php

class Catalogos_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/usuarios.js'));
    }

    public function indexAction()
    {
        ### Obtenemos las plazas para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
        //$this->view->sucursales=Sucursal::obtenerSucursal();
        $f=My_Comun::obtenerRangoFechaDesdePeriodo(1,'2013-12-02', false); 
        print_r($f);
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = " Usuario.status = 1";
	    if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Usuario.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
                
        if($nombre != '')
        {
	    $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND (Usuario.nombre LIKE '%".$nombre."%' OR Usuario.usuario LIKE '%".$nombre."%') ";
        }
        
        if($plaza != 0)
        {
            $filtro .= " AND Usuario.plaza_id = ".$plaza;
        }
         
        if($sucursal != 0)
        {
            $filtro .= " AND Usuario.sucursal_id = ".$sucursal;
        }

        $registros=My_Comun::registrosGrid("Usuario",$filtro); 
        $grid=array();
        $i=0;

        foreach($registros['registros'] as $registro)
        {
                if($registro->tipo>0)
                    $grid[$i]['plaza']=$registro->Plaza->nombre;
                else
                    $grid[$i]['plaza']="N/A";
                $grid[$i]['nombre']=$registro->nombre;
                $grid[$i]['usuario']=$registro->usuario;
                switch($registro->tipo)
                {
                    case 0:$grid[$i]['tipo']="Administrador"; break;
                    case 1:$grid[$i]['tipo']="Usuario de plaza"; break;
                    case 2:$grid[$i]['tipo']="Socio"; break;
                    case 3:$grid[$i]['tipo']="Vendedor con acceso"; break;
                    case 4:$grid[$i]['tipo']="Técnico con acceso"; break;
                    case 5:$grid[$i]['tipo']="Vendedor"; break;
                    case 6:$grid[$i]['tipo']="Técnico"; break;
                }
                
                if(My_Permisos::tienePermiso('PERMISOS_INDEX') == 1 && ($registro->tipo==0 || $registro->tipo==1))
                    $grid[$i]['permisos']='<span onclick="permisosUsuario('.$registro->id.');" title="Permisos"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/permisos.png" /></span>';
                else
                    $grid[$i]['permisos']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/permisos-off.png" />';

                if(My_Permisos::tienePermiso('ELIMINAR_INDEX') == 1)
                    $grid[$i]['modificar']='<span onclick="agregarUsuario('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
                else
                    $grid[$i]['modificar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';

                if(My_Permisos::tienePermiso('ELIMINAR_INDEX') == 1)
                    $grid[$i]['eliminar']='<span onclick="eliminarUsuario('.$registro->id.');" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
                else
                    $grid[$i]['eliminar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
                
                $i++;
        }
        
        My_Comun::grid2($registros,$grid);  
        
 	
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos las plazas para alimentar el select de plazas
        $this->view->plazas=Plaza::obtenerPlazas();

        
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {
            $this->view->registro=My_Comun::obtener("Usuario", $this->_getParam('id'));
            $this->view->sucursales=Sucursal::obtenerSucursal(" AND plaza_id=".$this->view->registro->plaza_id);
        }
    }
		
    public function permisosAction()
    {
            ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
            $this->_helper->layout->disableLayout();
            $this->view->registro=My_Comun::obtener('Usuario',$this->_getParam('id'));
            $this->view->permisos=explode("|",$this->view->registro->permisos);
            
            ### Obtenemos las plazas para alimentar el select de plazas
            $this->view->plazas=Plaza::obtenerPlazas();
    }
		
    public function guardarpermisosAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       Usuario::guardarPermisos($_POST['permisos'],$_POST['id']);
    }	
    
    public function guardarAction()
    {

       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       if(!isset($_POST['viaja']))
        $_POST['viaja']=0;

        if(isset($_POST['promotor'])){
            $_POST['promotor'] = 1;
        }

        else {
            $_POST['promotor'] = 0;
        }
       
       echo $id=My_Comun::guardar("Usuario",$_POST,'usuario',$_POST['id']);
       Permiso::actualizaPlazas($id,$_POST['tipo'],$_POST['idsplaza']);

       if($_POST['tipo']==2)
       {
        $_POST['permisos'][]='VER_REPORTECUANTITATIVO2';
        $_POST['permisos'][]='CORTEDECAJA_REPORTES';
        Usuario::guardarPermisos($_POST['permisos'],$id);

       }
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Usuario", $_POST['id']);
    }
    
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
         
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');        
        $sucursal = $this->_getParam('sucursal');
        
        if($nombre != '')
        {
	    $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND (Usuario.nombre LIKE '%".$nombre."%' OR Usuario.usuario LIKE '%".$nombre."%') ";
        }
        
        if($plaza != 0)
        {
            $filtro .= " AND Usuario.plaza_id = ".$plaza;
        }
        
        
        if($sucursal != 0)
        {
            $filtro .= " AND Usuario.sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Usuario", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE USUARIOS");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(30,40,45,35,35));
        $pdf->Row(array('INGRESO','NOMBRE','EMAIL','USUARIO','CELULAR'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
           $pdf->Row
           (
                array
                (
                    $registro->fecha_ingreso,
                    $registro->nombre,
                    $registro->email,
                    $registro->usuario,
                    $registro->celular
                ),0,1			
           );
        }
         
       $pdf->Output();	
       
    }		
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
         ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
         
        if($nombre != '')
        {
	    $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND (Usuario.nombre LIKE '%".$nombre."%' OR Usuario.usuario LIKE '%".$nombre."%') ";
        }
        
        if($plaza != 0)
        {
            $filtro .= " AND Usuario.plaza_id = ".$plaza;
        }
         
        if($sucursal != 0)
        {
            $filtro .= " AND Usuario.sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Usuario", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA DE INGRESO',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'NOMBRE',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'EMAIL',
                        "width" => 20
                        ),
				"D$i" => array(
                        "name" => 'USUARIO',
                        "width" => 20
                        ),
				"E$i" => array(
                        "name" => 'CONTRASEÑA',
                        "width" => 20
                        ),		
				"F$i" => array(
                        "name" => 'CELULAR',
                        "width" => 20
                        )				
        );
        
        


        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(
				
						"A$i" =>$registro->fecha_ingreso,
						"B$i" =>$registro->nombre,
						"C$i" =>$registro->email,
						"D$i" =>$registro->usuario,
						"E$i" =>$registro->password,
						"F$i" =>(string)$registro->celular
                );
        }
		
		
		
        $objPHPExcel->createExcel('Usuarios', $columns_name, $data, 10,array('rango'=>'A4:D4','size'=>14,'texto'=>'USUARIOS'));
		
    }

    function pruebasAction()
    {

        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);


        $q=Doctrine_Query::create()->from("Cliente c")->innerJoin("c.Colonia p")->limit(10);

        //echo $q->getSqlQuery();
        $d=$q->execute();

        /*foreach($d as $cliente)
        {
            echo $cliente->Plaza->nombre; echo "<br>";
        }*/

       



    }
    
    



}