<?php

class Catalogos_ConceptodecontratacionController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/conceptodecontratacion.js'));
    }

    public function indexAction()
    {
        //$this->view->plazas=Plaza::obtenerPlazas();
        //echo $this->view->plazas[0]->nombre; exit;
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');

        
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        
        $columnas = array
        (
            1 => "nombre"
        );
        
        if(My_Permisos::tienePermiso('EDITAR_ESTATUSDEFALLAS') == 1)
        {
                $accion1 = '<span onclick="agregarContratacion(%s);" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png" /></span>';
        }
        else
        {
                $accion1 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar-off.png" />';
        }
   
        
        if(My_Permisos::tienePermiso('EDITAR_CONCEPTODECONTRATACION') == 1)
        {
                $accion2 = '<span onclick="agregarConceptosdecontratacion(%s);" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
        }
        else
        {
                $accion2 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';
        }

        if(My_Permisos::tienePermiso('ELIMINAR_CONCEPTODECONTRATACION') == 1)
        {
                $accion3 = '<span onclick="eliminarConceptocontratacion(%s);" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
        }
        else
        {
                $accion3 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
        }

        $derecha = array
        (
            0 => array
            (
                "type" => 'other',
                "action" => $accion1,
                "params" => 'id'
            ),
            1 => array
            (
                "type" => 'other',
                "action" => $accion2,
                "params" => 'id',
            ),
            2 => array
            (
                "type" => 'other',
                "action" => $accion3,
                "params" => 'id',
            )
        );

        My_Comun::grid("ConceptoDeContratacion", null, $filtro, $columnas,$derecha);
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("ConceptoDeContratacion", $this->_getParam('id'));
        }
    }

    public function contratacionAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        $this->view->EstadosPlazas=Estado::obtenerEstadosPlazas();
        
        $e=array();
        
        foreach($this->view->EstadosPlazas as $estado)
        {   
            $this->view->plazas=TarifaConceptoContratacion::obtenerPlazas($estado->estado_id);
            $e[$estado->estado_id] = $this->view->plazas; 
        }
        
         $this->view->e=$e;
        
        //echo "<pre>";
        //print_r($e->estado_id); exit;
        // echo $e; exit;
        
       
        //$this->view->plazas=Plaza::obtenerPlazas($e);
        $this->view->TarifasContratacion=TarifaConceptoContratacion::obtenerTarifaConcepto($this->_getParam('id'));   
        
        $t=array();
        foreach ($this->view->TarifasContratacion as $tarifa)
        {
            $t["plaza_id"][$tarifa->plaza_id]=$tarifa->id;
            $t["tarifa"][$tarifa->plaza_id]=$tarifa->tarifa;
            $t["periodo"][$tarifa->plaza_id]=$tarifa->periodo;
			$t["tv"][$tarifa->plaza_id]=$tarifa->tv;
            $t["mes1"][$tarifa->plaza_id]=$tarifa->mes1;
            $t["mes2"][$tarifa->plaza_id]=$tarifa->mes2;
            $t["mes3"][$tarifa->plaza_id]=$tarifa->mes3;
            $t["mes4"][$tarifa->plaza_id]=$tarifa->mes4;
            $t["mes5"][$tarifa->plaza_id]=$tarifa->mes5;
            $t["mes6"][$tarifa->plaza_id]=$tarifa->mes6;
            $t["mes1_1_a_5"][$tarifa->plaza_id]=$tarifa->mes1_1_a_5;
            $t["mes1_6_a_10"][$tarifa->plaza_id]=$tarifa->mes1_6_a_10;
            $t["mes1_11"][$tarifa->plaza_id]=$tarifa->mes1_11;
            $t["mes2_1_a_5"][$tarifa->plaza_id]=$tarifa->mes2_1_a_5;
            $t["mes2_6_a_10"][$tarifa->plaza_id]=$tarifa->mes2_6_a_10;
            $t["mes2_11"][$tarifa->plaza_id]=$tarifa->mes2_11;
            $t["mes3_1_a_5"][$tarifa->plaza_id]=$tarifa->mes3_1_a_5;
            $t["mes3_6_a_10"][$tarifa->plaza_id]=$tarifa->mes3_6_a_10;
            $t["mes3_11"][$tarifa->plaza_id]=$tarifa->mes3_11;
            $t["mes4_1_a_5"][$tarifa->plaza_id]=$tarifa->mes4_1_a_5;
            $t["mes4_6_a_10"][$tarifa->plaza_id]=$tarifa->mes4_6_a_10;
            $t["mes4_11"][$tarifa->plaza_id]=$tarifa->mes4_11;
            $t["mes5_1_a_5"][$tarifa->plaza_id]=$tarifa->mes5_1_a_5;
            $t["mes5_6_a_10"][$tarifa->plaza_id]=$tarifa->mes5_6_a_10;
            $t["mes5_11"][$tarifa->plaza_id]=$tarifa->mes5_11;
            $t["mes6_1_a_5"][$tarifa->plaza_id]=$tarifa->mes6_1_a_5;
            $t["mes6_6_a_10"][$tarifa->plaza_id]=$tarifa->mes6_6_a_10;
            $t["mes6_11"][$tarifa->plaza_id]=$tarifa->mes6_11;

        }
        $this->view->t=$t;    
       
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("ConceptoDeContratacion", $this->_getParam('id'));
        }
    }
    
    public function plazasAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        echo $this->view->plazas=TarifaConceptoContratacion::obtenerPlazas($this->_getParam('estado'));
        
    }
    
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       echo My_Comun::guardar("ConceptoDeContratacion",$_POST,'nombre',$_POST['id']);
    }
    
    public function guardarcnceptoAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Vamos a guarda tarifas y periodos
		ConceptoDeContratacion::guardarTarifas($_POST);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("ConceptoDeContratacion", $_POST['id']);
    }
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        
        if($nombre !='')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        
        $registros=  My_Comun::obtenerFiltro("ConceptoDeContratacion", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE CONCEPTOS DE CONTRATACIÓN");

        $pdf->SetAligns(array('L'));
        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(190));
        $pdf->Row(array('NOMBRE'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
           $pdf->Row
           (
                array
                (
                    $registro->nombre
                ),0,1			
           );
        }
        
       $pdf->Output();	
    }
	
function exportarAction()
{
        ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
              
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        
        if($nombre !='')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
 
        $registros=  My_Comun::obtenerFiltro("ConceptoDeContratacion", $filtro);


        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
				
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'NOMBRE',
                        "width" => 20
                        )
        );
        
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
            $i++;
            $data[] = array(
                "A$i" =>$registro->nombre
            );
        }
		
        $objPHPExcel->createExcel('ConceptoDeContratacion', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'CONCEPTOS DE CONTRATACIÓN'));
    }		
}