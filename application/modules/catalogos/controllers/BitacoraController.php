<?php

class Catalogos_BitacoraController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/bitacora.js'));
    }

    public function indexAction()
    {
        ### Obtenemos las plazas para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
        
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        

        ### Cachamos las variables para conformar el filtro
        $usuario = $this->_getParam('usuario');
        $accion = $this->_getParam('accion');
        $plaza = $this->_getParam('plaza');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');

        $filtro=" 1=1 ";
        if($usuario != '')
        {
            $filtro .= " AND (Bitacora.Usuario.nombre LIKE '%$usuario%' OR Bitacora.Usuario.usuario LIKE '%$usuario%')";
        }
        
        if($accion != '')
        {
            $filtro .= " AND (fuente LIKE '%$accion%' OR descripcion LIKE '%$accion%')";
        }
        
        if($plaza > 0)
        {
            $filtro .= " AND Bitacora.Usuario.plaza_id = '$plaza'";
        }
        
        if($desde != '')
        {
			$desde.=" 00:00:00";
            $filtro .= " AND created_at >= '$desde'";
        }
        
        if($hasta != '')
        {
			$hasta.=" 23:59:59";
            $filtro .= " AND created_at <= '$hasta'";
        }

        $columnas = array
        (
            1 => "descripcion",
            2 => "Usuario.nombre",
            3 => "fecha",
            4 => "fuente"
        );


        My_Comun::grid("Bitacora", null, $filtro, $columnas);	
    }


}

