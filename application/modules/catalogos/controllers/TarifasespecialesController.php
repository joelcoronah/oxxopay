<?php

class Catalogos_TarifasespecialesController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/tarifasespeciales.js'));
    }

    public function indexAction()
    {
       ### Obtenemos los estados para alimentar el filtro
       $this->view->plazas=Plaza::obtenerPlazas();
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $autorizacion = $this->_getParam('autorizacion');
		
		if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        

        if($autorizacion != '')
        {
            $filtro .= " AND autorizacion = '$autorizacion'";
        }
		
		 ### Extraemos los registros para formar el arreglo del grid
		$registros=My_Comun::registrosGrid("Tarifaespecial",$filtro); 
		
		$grid=array();
		$editar=false;
		$eliminar=false;
		
		if(My_Permisos::tienePermiso('EDITAR_TARIFASESPECIALES') == 1)
			$editar=true;
		if(My_Permisos::tienePermiso('ELIMINAR_TARIFASESPECIALES') == 1)
			$eliminar=true;	
		
		$i=0;
		
		foreach($registros['registros'] as $registro)
		{
			$grid[$i]['nombre']=$registro->nombre;
			$grid[$i]['autorizacion']=($registro->autorizacion==1)?"SI":"NO";
			
			if($editar)
				$grid[$i]['editar']='<span onclick="agregarTarifa('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
			else
				$grid[$i]['editar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';
			
			if($eliminar)	
				$grid[$i]['eliminar']='<span onclick="eliminarPromotor(%s);" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
			else
				$grid[$i]['eliminar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
			$i++;
		}
		
        My_Comun::grid2($registros,$grid);
		
		//echo $filtro; exit;
		
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos los estados
        $this->view->plazas=Plaza::obtenerPlazas();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Tarifaespecial", $this->_getParam('id'));
        }
    }

    
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   if(!isset($_POST['autorizacion']))
	   	$_POST['autorizacion']=0;
	   	
       
       echo My_Comun::guardar("Tarifaespecial",$_POST,'nombre',$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Tarifaespecial", $_POST['id']);
    }
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $autorizacion = $this->_getParam('autorizacion');
 
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
		
        if($autorizacion != '')
        {
            $filtro .= " AND autorizacion = '$autorizacion'";
        }
        
        $registros=  My_Comun::obtenerFiltro("Tarifaespecial", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE TARIFAS ESPECIELES");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(100,90));
        $pdf->Row(array('NOMBRE','AUTORIZACIÓN'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
            
           $pdf->Row
           (
                array
                (
                    $registro->nombre,
                    $registro->autorizacion
                ),0,1			
           );
        }
        
        
       $pdf->Output();	
       
    }
	
	
	function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $autorizacion = $this->_getParam('autorizacion');

        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
		
        if($autorizacion != '')
        {
            $filtro .= " AND autorizacion = '$autorizacion'";
        }
		        
        $registros=  My_Comun::obtenerFiltro("Tarifaespecial", $filtro);


        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'NOMBRE',
                        "width" => 60
                        ),
				"B$i" => array(
                        "name" => 'AUTORIZACIÓN',
                        "width" => 60
                        )
        );
        
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(
				
					"A$i" =>$registro->nombre,
                    "B$i" =>$registro->autorizacion
                );
        }
		
        $objPHPExcel->createExcel('Tarifaespecial', $columns_name, $data, 10, array('rango'=>'A4:B4','size'=>14,'texto'=>'TARIFAS ESPECIALES'));
		
    }
}