<?php

class Catalogos_PromotoresController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/promotores.js'));
    }

    public function indexAction()
    {
       ### Obtenemos los estados para alimentar el filtro
       $this->view->plazas=Plaza::obtenerPlazas();
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
        
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        
        if($plaza>0)
        {
            $filtro .= " AND plaza_id = '$plaza'";
        }
        
        if($sucursal>0)
        {
            $filtro .= " AND sucursal_id = '$sucursal'";
        }
        
        $columnas = array
        (
            1 => "Plaza.nombre",
            2 => "nombre"
        ); 
        
        if(My_Permisos::tienePermiso('EDITAR_PROMOTORES') == 1)
        {
            $accion1 = '<span onclick="agregarPromotor(%s);" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
        }
        else
        {
            $accion1 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';
        }

        if(My_Permisos::tienePermiso('ELIMINAR_PROMOTORES') == 1)
        {
            $accion2 = '<span onclick="eliminarPromotor(%s);" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
        }
        else
        {
            $accion2 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
        }

        $derecha = array
        (
            0 => array
            (
                "type" => 'other',
                "action" => $accion1,
                "params" => 'id'
            ),
            1 => array
            (
                "type" => 'other',
                "action" => $accion2,
                "params" => 'id',
            )
        );

        My_Comun::grid("Promotor", null, $filtro, $columnas,$derecha);
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos los estados
        $this->view->plazas=Plaza::obtenerPlazas();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Promotor", $this->_getParam('id'));
            $this->view->sucursales=Sucursal::obtenerSucursal(" AND id=".$this->view->registro->sucursal_id);
        }
    }

    
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       echo My_Comun::guardar("Promotor",$_POST,NULL,$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Promotor", $_POST['id']);
    }
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
        
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        
        if($plaza>0)
        {
            $filtro .= " AND plaza_id = '$plaza'";
        }
        
        if($sucursal>0)
        {
            $filtro .= " AND sucursal_id = '$sucursal'";
        }
        
        $registros=  My_Comun::obtenerFiltro("Promotor", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE PROMOTORES");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(20,75,60,35));
        $pdf->Row(array('PLAZA','NOMBRE','EMAIL','CELULAR'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
            
           $pdf->Row
           (
                array
                (
                    $registro->Plaza->nombre,
                    $registro->nombre,
                    $registro->email,
                    $registro->celular
                ),0,1			
           );
        }
             
       $pdf->Output();	
       
    }	
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
        
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        
        if($plaza>0)
        {
            $filtro .= " AND plaza_id = '$plaza'";
        }
        
        if($sucursal>0)
        {
            $filtro .= " AND sucursal_id = '$sucursal'";
        }
        
        $registros=  My_Comun::obtenerFiltro("Promotor", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'PLAZA',
                        "width" => 20
                        ),
				"B$i" => array(
                        "name" => 'NOMBRE',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'EAMAIL',
                        "width" => 20
                        ),
				"D$i" => array(
                        "name" => 'CELULAR',
                        "width" => 20
                        )			
        );
        
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(
				
					"A$i" =>$registro->Plaza->nombre,
                    "B$i" =>$registro->nombre,
                    "C$i" =>$registro->email,
                    "D$i" =>$registro->celular
                );
        }
		
        $objPHPExcel->createExcel('Promotores', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'PROMOTORES'));
		
    }

    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }

}