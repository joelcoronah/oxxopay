<?php

class Catalogos_TecnicosController extends Zend_Controller_Action
{

    public function init()
    {
        header("Location: /transacciones/clientes");
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/tecnico.js'));
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
       
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        
        if($sucursal != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal;
        }

        $columnas = array
        (
            1 => "Plaza.nombre",
            2 => "nombre"
        );  
        
        if(My_Permisos::tienePermiso('EDITAR_TECNICOS') == 1)
        {
            $accion1 = '<span onclick="agregarTecnico(%s);" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
        }
        else
        {
            $accion1 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';
        }

        if(My_Permisos::tienePermiso('ELIMINAR_TECNICOS') == 1)
        {
            $accion2 = '<span onclick="eliminarTecnico(%s);" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
        }
        else
        {
            $accion2 = '<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
        }

        $derecha = array
        (
            0 => array
            (
                "type" => 'other',
                "action" => $accion1,
                "params" => 'id'
            ),
            1 => array
            (
                "type" => 'other',
                "action" => $accion2,
                "params" => 'id',
            )
        );

        My_Comun::grid("Tecnico", null, $filtro, $columnas,$derecha);
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos los estados
        $this->view->plazas=Plaza::obtenerPlazas();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Tecnico", $this->_getParam('id'));
            $this->view->sucursales=Sucursal::obtenerSucursal(" AND id=".$this->view->registro->sucursal_id);
        }
    }

    
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       echo My_Comun::guardar("Tecnico",$_POST,NULL,$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Tecnico", $_POST['id']);
    }
    
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
       
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        
        if($sucursal != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Tecnico", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE TÉCNICOS");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(30,30,35,35,30,30));
        $pdf->Row(array('PLAZA','INGRESO','NOMBRE','EMAIL','CELULAR','VIAJA'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
            
           $pdf->Row
           (
                array
                (
                    $registro->Plaza->nombre,
                    $registro->fecha_ingreso,
                    $registro->nombre,
                    $registro->email,
                    $registro->celular,
                    ($registro->viaja==1)?"SI":"NO",
                ),0,1			
           );
        }
          
       $pdf->Output();	
       
    }	
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
          
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
       
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        
        if($sucursal != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Tecnico", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'PLAZA',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'INGRESO',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'NOMBRE',
                        "width" => 20
                        ),
				"D$i" => array(
                        "name" => 'EAMAIL',
                        "width" => 20
                        ),
				"E$i" => array(
                        "name" => 'CELULAR',
                        "width" => 20
                        ),
				"F$i" => array(
                        "name" => 'VIAJA',
                        "width" => 20
                        )						
        );
        
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(
				
		    "A$i" =>$registro->Plaza->nombre,
                    "B$i" =>$registro->fecha_ingreso,
                    "C$i" =>$registro->nombre,
                    "D$i" =>$registro->email,
                    "E$i" =>$registro->celular,
                    "F$i" =>($registro->viaja==1)?"SI":"NO"

                );
        }
		
        $objPHPExcel->createExcel('Tecnicos', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'TÉCNICOS'));
		
    }
    
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }

}