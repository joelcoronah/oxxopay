<?php

class Catalogos_ZonaController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/zona.js'));
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->estados=Estado::obtenerEstados();
    }

    public function municipiosAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboMunicipio($this->_getParam('id'));
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = " status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $municipio = $this->_getParam('municipio');
        $estado = $this->_getParam('estado');

        
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }

        if($municipio != '')
        {
            $filtro .= " AND municipio_id = '$municipio'";
        }
        elseif($estado != '')
        {
            $filtro .= " AND Municipio.estado_id = '$estado'";
        }

        ### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::registrosGrid("Zona",$filtro);        
        
        $grid=array();
        
        $i=0;
        ### Validamos los permisos
        $editar=My_Permisos::tienePermiso('AGREGAR_ZONA');
        $eliminar=My_Permisos::tienePermiso('AGREGAR_ZONA');
        
        foreach($registros['registros'] as $registro)
        {

            $grid[$i]['estado']=$registro->Municipio->Estado->nombre;
            $grid[$i]['municipio']=$registro->Municipio->nombre;
            $grid[$i]['nombre']=$registro->nombre;
            
            if($editar==1)
                $grid[$i]['editar']='<span onclick="agregar('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
            else
                $grid[$i]['editar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';

            if($eliminar==1)
                $grid[$i]['eliminar']='<span onclick="eliminar('.$registro->id.');" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
            else
                $grid[$i]['eliminar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar-off.png" />';
            
            $i++;
        }
        
        My_Comun::grid2($registros,$grid);
    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos los estados
        $this->view->estados=Estado::obtenerEstados();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Zona", $this->_getParam('id'));
            $this->view->municipios=Municipio::obtenerMunicipiosEstado($this->view->registro->Municipio->estado_id);
        }
    }

    
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       echo My_Comun::guardar("Zona",$_POST,NULL,$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Zona", $_POST['id']);
    }
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $municipio = $this->_getParam('municipio');
        $estado = $this->_getParam('estado');

        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }

        if($municipio != '')
        {
            $filtro .= " AND municipio_id = '$municipio'";
        }
        elseif($estado != '')
        {
            $filtro .= " AND Municipio.estado_id = '$estado'";
        }
        
        $registros=  My_Comun::obtenerFiltro("Zona", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();;
        
        $pdf->Header("IMPRESIÓN DE ZONAS");

        $pdf->SetAligns(array('L','L','L'));
        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(60,60,60));
        $pdf->Row(array('ZONA','MUNICIPIO','ESTADO'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
     
           $pdf->Row
           (
                array
                (
                    $registro->nombre,
                    $registro->Municipio->nombre,
                    $registro->Municipio->Estado->nombre
                ),0,1			
           );
        }
        
     ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);
       $pdf->Output();	
       
    }
    
    
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
       
       
        ### Establecemos el filtro por default
        $filtro = "status = 1 ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $municipio = $this->_getParam('municipio');
        $estado = $this->_getParam('estado');

        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }

        if($municipio != '')
        {
            $filtro .= " AND municipio_id = '$municipio'";
        }
        elseif($estado != '')
        {
            $filtro .= " AND Municipio.estado_id = '$estado'";
        }
		
		
		$registros=  My_Comun::obtenerFiltro("Zona", $filtro);


        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => utf8_encode('ZONA'),
                        "width" => 30
                        ),
                "B$i" => array(
                        "name" => utf8_encode('MUNICIPIO'),
                        "width" => 30
                        ),
                "C$i" => array(
                        "name" => utf8_encode('ESTADO'),
                        "width" => 30
                        )
        );
        
        


        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(
                        "A$i" => utf8_decode($registro->nombre),
                        "B$i" => utf8_decode($registro->Municipio->nombre),
                        "C$i" => utf8_decode($registro->Municipio->Estado->nombre)
                );
        }
		
		
		
        $objPHPExcel->createExcel(utf8_encode('Zonas'), $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'ZONAS'));
		
    }
    
    


}



