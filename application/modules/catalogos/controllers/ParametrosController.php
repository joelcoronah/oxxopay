<?php

class Catalogos_ParametrosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/catalogos/parametros.js'));
    }

    public function indexAction()
    {
		   
		$parametro=Parametros::obtenerParametro();   
		$this->view->registro=My_Comun::obtener("Parametros", $parametro->id);
	
    }
   
	public function guardarAction()
    {
    	### Deshabilitamos el layout y la vista
       	$this->_helper->layout->disableLayout();
       	$this->_helper->viewRenderer->setNoRender(TRUE);
    
	   echo My_Comun::guardar("Parametros",$_POST,NULL,$_POST['id'],'extensiones',false);
    }

}

