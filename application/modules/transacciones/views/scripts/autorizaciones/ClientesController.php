<?php

class Transacciones_ClientesController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/clientes.js'));
	$this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/cobranza.js'));
		
	/*$fecha='2013-02-28';
	$periodos=6;
	$suma=true;
	$f=My_Comun::obtenerRangoFechaDesdePeriodo($periodos,$fecha,$suma);
	print_r($f);
	exit;*/
		
	/*$periodos=12;
	$f=My_Comun::obtenerRangoFechaDesdePeriodo($periodos);
	print_r($f);
	exit;*/
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
        ### Obtenemos los estados para alimentar el filtro
        $this->view->estados=Estado::obtenerEstados();
        $this->view->sucursales=Sucursal::obtenerSucursal();		
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);    
		
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
		$filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');
        $comunidad = $this->_getParam('comunidad');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
        
        if($nombre != '')
        {
           $filtro .= " AND (nombre LIKE '%$nombre%' OR telefono LIKE '%$nombre%'  OR calle LIKE '%$nombre%' OR contrato LIKE '%$nombre%') ";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        if($estado >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.Estado.id =" .$estado;
        }
        if($municipio >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.id =" .$municipio;
        }
        if($comunidad >0)
        {
            $filtro .= " AND comunidad_id =" .$comunidad;
        }
        
        if($sucursal != 0)
        {
            $filtro .= " AND (Cliente.Plaza.sucursal_id = ".$sucursal." ) ";
        }        
	
	### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Cliente",$filtro); 
		
	$grid=array();
		
	$i=0;
	$editar=false;
	if(My_Permisos::tienePermiso('EDITAR_CLIENTES') == 1)
            $editar=true;
						
	foreach($registros['registros'] as $registro)
	{
            $domicilio=$registro->calle." No.".$registro->no_exterior;
            if($registro->no_interior!='')
                $domicilio.=', Int. '.$registro->no_interior;
            $grid[$i]['contrato']=$registro->contrato;
            $grid[$i]['nombre']=$registro->nombre;
            $grid[$i]['domicilio']=$domicilio;
            $grid[$i]['telefono']=$registro->telefono;
            $grid[$i]['celular']=$registro->celular1;
            if(count($registro->Autorizacion)==1 && $registro->Autorizacion[0]->status!=1)
                $grid[$i]['estatus']=($registro->Autorizacion[0]->status==0)?"POR AUTORIZAR":"NO AUTORIZADO";
            else
                $grid[$i]['estatus']=$registro->estatus;
				
            $grid[$i]['editar']='<span onclick="agregarCliente('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png"/></span>';
            
            if(Cliente::tieneAdeudoAdicional($registro->id))
                $grid[$i]['cobrar']='<span onclick="cobrarAdicional('.$registro->id.');" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png" /></span>';
            else	
            {
                if($registro->estatus=="POR INSTALAR")
                $grid[$i]['cobrar']='<span onclick="cobrar(-1);" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png" /></span>';

                else
                    $grid[$i]['cobrar']='<span onclick="cobrar('.$registro->id.');" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png" /></span>';
            }	
				
            if(Ordencambiodomicilio::tieneOrdenPendiente($registro->id))	
            {
                $grid[$i]['cambiardomicilio']='<span onclick="cambiarDomicilio(-3);" title="Cambio de domicilio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/map.png" /></span>';
            }
            else
            {
                if($registro->estatus=='ACTIVO')	
		$grid[$i]['cambiardomicilio']='<span onclick="cambiarDomicilio('.$registro->id.');" title="Cambio de domicilio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/map.png" /></span>';
		elseif($registro->estatus=='POR INSTALAR')	
                    $grid[$i]['cambiardomicilio']='<span onclick="cambiarDomicilio(-2);" title="Cambio de domicilio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/map.png" /></span>';
		else
                    $grid[$i]['cambiardomicilio']='<span onclick="cambiarDomicilio(-1);" title="Cambio de domicilio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/map.png" /></span>';
            }
			
			
            if(Ordenextensionadicional::tieneOrdenPendiente($registro->id))	
            {
                $grid[$i]['tv']='<span onclick="anadirTv(-3);" title="Añadir Televisión"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/television_add.png" /></span>';	
            }
            
            else
            {
                if($registro->estatus=='ACTIVO')	
                    $grid[$i]['tv']='<span onclick="anadirTv('.$registro->id.');" title="Añadir Televisión"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/television_add.png" /></span>';
                elseif($registro->estatus=='POR INSTALAR')	
                    $grid[$i]['tv']='<span onclick="anadirTv(-2);" title="Añadir Televisión"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/television_add.png" /></span>';	
		else
                    $grid[$i]['tv']='<span onclick="anadirTv(-1);" title="Añadir Televisión"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/television_add.png" /></span>';	
            }
			
            if($registro->estatus=='ACTIVO')	
                $grid[$i]['baja']='<span onclick="solicitarBaja('.$registro->id.');" title="Solicitar baja"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';
            else if($registro->estatus=='POR INSTALAR')	
                $grid[$i]['baja']='<span onclick="solicitarBaja(-2);" title="Solicitar baja"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';	
            else
                $grid[$i]['baja']='<span onclick="solicitarBaja(-1);" title="Solicitar baja"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';	
            $i++;
	}
		
        My_Comun::grid2($registros,$grid);	

    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos las plazas
        $this->view->plazas=Plaza::obtenerPlazas();
		
        ### Obtenemos las comunidades de la plaza
        $this->view->comunidades=Comunidad::obtenerComunidadesDeMunicipio(Zend_Auth::getInstance()->getIdentity()->Plaza->municipio_id);
        
        $this->view->promotores=My_Comun::obtenerPromotores();
		
	$this->view->comisiones=Comisiones::obtenerComisiones();
        
        $this->view->conceptoContratacion=ConceptoDeContratacion::obtenerConceptoParaContratacion();
        $this->view->servicioAContratar=Servicio::obtenerServicios("status=1 AND tipo=1");  
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Cliente", $this->_getParam('id'));
        }
    }

    
    
	
	public function establecerconceptocontratacionAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   $conceptoContratacion=ConceptoDeContratacion::obtenerConceptoContratacion($this->_getParam('id'));
	   
	   echo $conceptoContratacion->ConceptoDeContratacion->nombre."_".$conceptoContratacion->tarifa;
		
	}
	
	public function establecertvsconceptocontratacionAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   $conceptoContratacion=ConceptoDeContratacion::obtenerConceptoContratacion($this->_getParam('id'));
	   
	   echo $conceptoContratacion->tv;
		
	}
	
	public function establecerserviciocontratacionAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	    $servicio=Servicio::obtenerServicios("id='".$this->_getParam('id')."'");
		$comision=Servicio::obtenerComision($this->_getParam('id'));
		

		
		### Primero verificamos si es un servicio o un servicio adicional
		
		### Si es un servicio
		if($servicio[0]->tipo!=2)
		{
			$tarifa=Servicio::obtenerTarifa($this->_getParam('id'));
			###Buscamos las tarifas especiales asociadas a este servicio
			$tarifas=Servicio::obtenerTarifasEspecialesServicio($tarifa->servicio_id);
			
			### Si encontramos tarifas especiales asociadas al servicio en la plaza actual, entonces armamos un select de tarifas
			if(count($tarifas)>0)
			{
				$opt.='<option value="0">Tarifa base</option>';
				foreach($tarifas as $t)
				{
					$opt.='<option value="'.$t->id.'">'.$t->Tarifaespecial->nombre.'</option>';
				}
				
				$optPeriodo='<option value="0">Indefinido</option>';
				for($i=1;$i<=12;$i++)
				{
					$optPeriodo.='<option value="'.$i.'">'.$i.' periodo(s)</option>';
				}

				
				echo '<div class="cuerpo" id="divCuerpo'.$tarifa->id.'"><div>1</div><div>'.$servicio[0]->nombre.'</div> <div class="divComision">$'.sprintf("%0.2f",$comision).'</div><div><select name="tipotarifa[]" id="select_'.$tarifa->id.'" onchange="estableceTipoTarifa(this.id,'.$tarifa->precio_1a5.');" class="span10">'.$opt.'</select></div><div id="precio'.$tarifa->id.'" class="divMontoServicio">$'.sprintf("%0.2f",$tarifa->precio_1a5).'</div><div class="eliminar"><img src="/images/png/cancelar.png" width="20px" onclick="eliminarServicio('.$tarifa->id.');" /></div><input type="hidden" name="tarifas[]" value="'.$tarifa->id.'" class="hdnTarifa" /><input type="hidden" name="servicios[]" value="'.$tarifa->servicio_id.'" class="hdnServicio" /><input type="hidden" name="autorizacion[]" value="0" class="hdnServicio" id="hdnautorizacion'.$tarifa->id.'" /></div>';
			}
			else
			{
				echo '<div class="cuerpo" id="divCuerpo'.$tarifa->id.'"><div>1</div><div>'.$servicio[0]->nombre.'</div><div class="divComision">$'.sprintf("%0.2f",$comision).'</div><div>Base</div><div class="divMontoServicio">$'.sprintf("%0.2f",$tarifa->precio_1a5).'</div><div class="eliminar"><img src="/images/png/cancelar.png" width="20px" onclick="eliminarServicio('.$tarifa->id.');" /></div><input type="hidden" name="tarifas[]" value="'.$tarifa->id.'" class="hdnTarifa" /><input type="hidden" name="servicios[]" value="'.$tarifa->servicio_id.'" class="hdnServicio" /><input type="hidden" name="autorizacion[]" value="0" class="hdnServicio" id="hdnautorizacion'.$tarifa->id.'" /></div>';
			}
			
		}
		else
		{
			$tarifa=Servicio::obtenerTarifaAdicional($this->_getParam('id'));
			echo '<div class="cuerpo" id="divCuerpo'.$tarifa->id.'"><div>1</div><div>Servicio adicional '.$servicio[0]->nombre.'</div> <div class="divComision">$'.sprintf("%0.2f",$comision).'</div><div>Base</div><div class="divMonto">$'.sprintf("%0.2f",$tarifa->tarifa).'</div><div class="eliminar"><img src="/images/png/cancelar.png" width="20px" onclick="eliminarServicio('.$tarifa->id.');" /></div><input type="hidden" name="tarifas[]" value="'.$tarifa->id.'"  /><input type="hidden" name="serviciosAdicionales[]" value="'.$tarifa->servicioadicional_id.'" class="hdnServicio" /></div>';
		}
		
		
		
		
	}
	
	public function obtenermontotarifaespecialAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   $q=Doctrine_Query::create()->from('ServicioPlazaTarifaespecial')->where('id=?',$this->_getParam('id'))->execute()->getFirst();
	   
	   echo "$".sprintf("%.2f",$q->precio_1a5)."_".$q->Tarifaespecial->autorizacion;
		
	}
	
	public function agregarservicioAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   
	   $this->view->servicioAContratar=Servicio::obtenerServiciosAContratar();
		
	}
	
	public function tiposervicioAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   ### Obtenemos servicios
	   if($this->_getParam('id')==1)
	   {
		   	$servicios=Servicio::obtenerServiciosAContratar();
			foreach($servicios as $servicio)
			{
				echo '<option value="'.$servicio->servicio_id.'">'.$servicio->Servicio->nombre.'</option>';
			}
	   }
	   else
	   {
		   ### Obtenemos servicios adicionales
		   $servicios=Servicio::obtenerServicios(" tipo=2 AND status=1 AND Servicio.id=Servicio.TarifaServicioadicional.servicioadicional_id AND Servicio.TarifaServicioadicional.plaza_id=".Usuario::plaza()."");
			foreach($servicios as $servicio)
			{
				echo '<option value="'.$servicio->id.'">'.$servicio->nombre.'</option>';
			}
	   }
	   
	   $this->view->servicioAContratar=Servicio::obtenerServiciosAContratar();
		
	}

    
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);

       
       echo Cliente::guardar("Cliente",$_POST,NULL,$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Cliente", $_POST['id']);
    }
	
	
	public function cambiodomicilioAction()
	{
		### Deshabilitamos el layout
       $this->_helper->layout->disableLayout();
	   $this->view->registro=My_Comun::obtener("Cliente",$this->_getParam('id'));
	}
	
	public function guardarcambiodomicilioAction()
	{
		 ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   echo Cliente::cambiarDomicilio($_POST['id'],$_POST);
	}
	
	public function anadirtvAction()
	{
		### Deshabilitamos el layout
       $this->_helper->layout->disableLayout();
	   $this->view->registro=My_Comun::obtener("Cliente",$this->_getParam('id'));
	}
	
	public function guardaranadirtvAction()
	{
		 ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   echo Cliente::anadirTv($_POST['id'],$_POST);
	}
	
	public function costotvadicionalAction()
	{
	   ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   echo Parametros::obtenerParametros('extensiones');
	}
	
	public function bajaAction()
	{
		### Deshabilitamos el layout
       $this->_helper->layout->disableLayout();
	   $this->view->registro=My_Comun::obtener("Cliente",$this->_getParam('id'));
	}
	
	public function guardarbajaAction()
	{
		 ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
		Cliente::baja($_POST['id'],$_POST);
	}
    
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
		$filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');
        $comunidad = $this->_getParam('comunidad');
        $plaza = $this->_getParam('plaza');
         $sucursal = $this->_getParam('sucursal');
        
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        if($estado >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.Estado.id =" .$estado;
        }
        if($municipio >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.id =" .$municipio;
        }
        if($comunidad >0)
        {
            $filtro .= " AND comunidad_id =" .$comunidad;
        }
        
        if($sucursal != 0)
        {
            $filtro .= " AND (Cliente.Plaza.sucursal_id = ".$sucursal." ) ";
        }
        
        $registros=  My_Comun::obtenerFiltro("Cliente", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE CLIENTES");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(90,30,35,35));
        $pdf->Row(array('CLIENTE','TELEFONO','CELULAR','ESTATUS'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
            
           $pdf->Row
           (
                array
                (
                    $registro->nombre,
                    $registro->telefono,
                    $registro->celular1,
                    $registro->estatus
                ),0,1			
           );
        }   
       $pdf->Output();	
       
    }
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
		$filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $cliente = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');
        $comunidad = $this->_getParam('comunidad');
        $plaza = $this->_getParam('plaza');
         $sucursal = $this->_getParam('sucursal');
        
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        if($estado >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.Estado.id =" .$estado;
        }
        if($municipio >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.id =" .$municipio;
        }
        if($comunidad >0)
        {
            $filtro .= " AND comunidad_id =" .$comunidad;
        }
        
        if($sucursal != 0)
        {
            $filtro .= " AND Cliente.Plaza.sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Cliente", $filtro);


        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'NOMBRE',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'TELEFONO',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'CELUAR',
                        "width" => 20
                        ),
				"D$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 20
                        )					
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(
				
		    "A$i" =>$registro->nombre,
                    "B$i" =>$registro->telefono,
                    "C$i" =>$registro->celular1,
                    "D$i" =>$registro->estatus

                );
        }
		
        $objPHPExcel->createExcel('Clientes', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'CLIENTES'));
    }
	
	public function  cronAction()
	{
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
		### Verificamos en que día estamos
		$d=date('d');
		
		$clientes=My_Comun::obtenerFiltro('Cliente');
		
		### Generamos un nuevo registro de ejecución del cronjob
		$cron= new Cronjob();
			$cron->fecha=date('Y-m-d h:i:s');
		$cron->save();	
		
		foreach($clientes as $cliente)
		{
			
			### Nos aseguramos de no cambiar el estatus de un cliente al que aun no se le ha instalado
			if($cliente->status!='POR INSTALAR' && $cliente->status!='POR REINSTALAR')
			{
				### Si no se ha ejecutado el cambio de estatus para este cliente entonces lo ejecutamos
				if(!Cronjob::statusgenerado($cliente->id))
				{
					### 1. Obtenemos el último mes cubierto
					$qUltimoMesCubierto=Doctrine_Query::create()->select('MAX(fecha_fin) as fecha')->from('Cobranza')->where('cliente_id=?',$cliente->id);
					$ultimoMesCubierto=$qUltimoMesCubierto->execute()->getFirst();
					
					### Si la fecha de hoy es menor al último mes cubierto entonces el cliente es ACTIVO
					$fecha=date('Y-m-d');
					if($ultimoMesCubierto->fecha>=$fecha)
					{
						$cliente->status=1;
					}
					else
					{
						### Obtengo el número de meses transcurridos entre la fecha actual y la fecha del ultimo pago.
						$meses=My_Comun::numero_de_meses($fecha,$ultimoMesCubierto->fecha);
						
						if($meses<=0)
							$cliente->status=1;
						else if($meses==1)	
							$cliente->status=2;
						else if($meses==2)	
							echo $cliente->status=3;
						else if($meses==3)	
							$cliente->status=4;
						else
							$cliente->status=5;	
					}
					$cliente->save();
					
					### Insteramos el detalle de la ejecución en el historial de cambios de estatus
					$historialstatus= new Historialstatuscliente();
						$historialstatus->cronjob_id=$cron->id;
						$historialstatus->cliente_id=$cliente->id;
						$historialstatus->status=$cliente->status;
						$historialstatus->fecha=date('Y-m-d');
					$historialstatus->save();	
					
				}
				
				### Si el dia actual es igual o mayor al 11 intentamos generar la orden  correspondiente.
				else if($d>=11)
				{	
					echo $cliente->estatus;			
					### Si el dia es 11, entonces ya están actualizados los estatus y habrá que generar órdenes
					 if($cliente->estatus=="DESCONECTAR")	
						Ordendesconexion::generar($cliente->id);
				}
			}//if por instalar	
			

			
	   }//foreach	
    }// end function
	
    public function importarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        
        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objReader  = new My_PHPExcel_Importar();
		
	//$objPHPExcel = $objReader->loadalterno("excel/cuidado/SMO1.xlsx",6,136,86684,631);
		
	//$objPHPExcel = $objReader->load("excel/SNB.xlsx",13,143,62617,767);
	//($archivo, $plaza, $usuario, $comunidad,$cantidad)
		
	### Deshabilitamos el layout y la vista
        /*  $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        
        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objReader  = new My_PHPExcel_Importar();
		
	$objPHPExcel = $objReader->loadalterno("excel/SMO1.xlsx");		*/
    }
	
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboPlazas($this->_getParam('id'));
    }
}