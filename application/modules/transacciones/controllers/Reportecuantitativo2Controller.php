<?php
class Transacciones_Reportecuantitativo2Controller extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/reportecuantitativo.js'));
    }

    public function indexAction()
    {

        $this->view->plazas=Plaza::obtenerPlazas();
        $this->view->sucursales=Sucursal::obtenerSucursal();
        $this->view->permisos = Permiso::obtienePlazas(Usuario::id());
             
        $dia[1]= "L";
        $dia[2]= "M";
        $dia[3]= "M";
        $dia[4]= "J";
        $dia[5]= "V";
        $dia[6]= "S";
        $dia[7]= "D";      
        $this->view->dia=$dia;
       
        $mes[1]= "Enero";
        $mes[2]= "Febrero";
        $mes[3]= "Marzo";
        $mes[4]= "Abril";
        $mes[5]= "Mayo";
        $mes[6]= "Junio";
        $mes[7]= "Julio";
        $mes[8]= "Agosto";
        $mes[9]= "Septiembre";
        $mes[10]= "Octubre";
        $mes[11]= "Noviembre";
        $mes[12]= "Diciembre";
        $this->view->mes =$mes;

        //echo "<pre>"; print_r($_POST); exit;
    
        $qp=Doctrine_Query::create()->from('Parametrosdeproductividad');
        $productividades=$qp->execute();                       
        foreach($productividades as $productividad)
        {
            $puntos[1]= $productividad->instalaciones;
            $puntos[2]= $productividad->reconexiones;
            $puntos[3]= $productividad->cambio_domicilio;
            $puntos[4]= $productividad->atencion_a_fallas;
            $puntos[5]= $productividad->extensiones_adicionales;
            $puntos[6]= $productividad->recuperaciones;
            $puntos[7]= $productividad->auditorias_de_acometidas;
            $puntos[8]= $productividad->reinstalaciones;  
            $puntos[9]= $productividad->retiros; 
        }
        
        $this->view->puntos = $puntos;


        $iqp=Doctrine_Query::create()->from('Parametrosdeproductividadinternet');
        $iproductividades=$iqp->execute();                       
        foreach($iproductividades as $productividad)
        {
            $ipuntos[1]= $productividad->instalaciones;
            $ipuntos[2]= $productividad->reconexiones;
            $ipuntos[3]= $productividad->cambio_domicilio;
            $ipuntos[4]= $productividad->atencion_a_fallas;
            $ipuntos[5]= $productividad->extensiones_adicionales;
            $ipuntos[6]= $productividad->recuperaciones;
            $ipuntos[7]= $productividad->auditorias_de_acometidas;
            $ipuntos[8]= $productividad->reinstalaciones;  
            $ipuntos[9]= $productividad->retiros; 
        }
        
        $this->view->puntos_internet = $ipuntos;
        
        ### Cachamos las variables
        $desde = date("Y-m-d",mktime(0,0,0,date("m"),1,date("Y")));
          
        $diaF = date("d",mktime(0,0,0,date("m")+1,0,date("Y")));  
        $hasta= date("Y-m-d",mktime(0,0,0,date("m"),$diaF,date("Y")));

        
          
        if($_POST['fdesde'] != ''){
            $mesI= date("m", strtotime($_POST['fdesde']));
            $anoI= date("Y", strtotime($_POST['fdesde']));
            $desde = $anoI."-".$mesI."-01";
            //echo "Desde: ".$desde;
        }
        
        if($_POST['fhasta'] != ''){
            $mesF= date("m", strtotime($_POST['fhasta']));
            $anoF= date("Y", strtotime($_POST['fhasta']));
            $diaF = date("d",mktime(0,0,0,$mesF+1,0,$anoF));        
            $hasta = $anoF."-".$mesF."-".$diaF;
            //echo "Hasta: ".$hasta;
        }
        

        $filtro =" 1=1 ";
        $plaza = "";

        if($_POST['fplaza'] !=0)
        {
            if($_POST['fsucursal'] == 0) $filtro.= " AND plaza_id =". $_POST['fplaza'];
        }
        else
        {
            $tmp_plaza = "";
            $tmp_plaza_uno = Permiso::obtienePlazas(Usuario::id());
            foreach ($tmp_plaza_uno as $permiso)
            {
                $tmp_plaza .= $permiso->plaza_id.",";
            }

            if($tmp_plaza != "" && Usuario::tipo() != 0)
            {
                $plaza = trim($tmp_plaza, ',');
                $filtro .= " AND plaza_id in (".$plaza.")";
            }
        }

        if($_POST['fsucursal'] != 0) $filtro.= " AND sucursal_id =". $_POST['fsucursal'];
        
        $this->view->desde =$desde;
        $this->view->hasta =$hasta;
        $this->view->meses = My_Comun::numero_de_meses1( $hasta, $desde );

        
        for($i=1; $i<=$this->view->meses; $i++)
        {
            $fechas=My_Comun::obtenerRangoFechaDesdePeriodo2($i, $desde, false);
            //echo "Mes(".$i."|".$desde."): "; echo "<pre>"; print_r($fechas);
            $fecha_fin=explode("-",$fechas['fin']);            
            $desde2=$fecha_fin[0]."-".$fecha_fin[1]."-01";
            $hasta2=date('Y-m-t',strtotime($desde2));
           
            $clientes_pagados[$hasta2]=Cliente::cuantitativoClientesPagados($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $mensualidades_cobradas[$hasta2]=Cliente::cuantitativoMensualidadesCobradas($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $recuperaciones[$hasta2]=Cliente::cuantitativoRecuperaciones($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $ventas_nuevas[$hasta2]=Cliente::cuantitativoVentasNuevas($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $instalaciones[$hasta2]=Cliente::cuantitativoInstalaciones($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $desconexiones[$hasta2]=Cliente::cuantitativoDesconexiones($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $reconexiones[$hasta2]=Cliente::cuantitativoReconexiones($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $cambio_domicilio[$hasta2]=Cliente::cuantitativoDomicilio($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $extensiones[$hasta2]=Cliente::cuantitativoExtensiones($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $retiros[$hasta2]=Cliente::cuantitativoRetiros($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);

            $clientes_pagados_internet[$hasta2]=Cliente::cuantitativoClientesPagadosInternet($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $mensualidades_cobradas_internet[$hasta2]=Cliente::cuantitativoMensualidadesCobradasInternet($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $recuperaciones_internet[$hasta2]=Cliente::cuantitativoRecuperacionesInternet($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $ventas_nuevas_internet[$hasta2]=Cliente::cuantitativoVentasNuevasInternet($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $instalaciones_internet[$hasta2]=Cliente::cuantitativoInstalacionesInternet($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $desconexiones_internet[$hasta2]=Cliente::cuantitativoDesconexionesInternet($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $reconexiones_internet[$hasta2]=Cliente::cuantitativoReconexionesInternet($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $cambio_domicilio_internet[$hasta2]=Cliente::cuantitativoDomicilioInternet($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $retiros_internet[$hasta2]=Cliente::cuantitativoRetirosInternet($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);

            $clientes_pagados_general[$hasta2]=Cliente::cuantitativoClientesPagadosGeneral($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $mensualidades_cobradas_general[$hasta2]=Cliente::cuantitativoMensualidadesCobradasGeneral($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $recuperaciones_general[$hasta2]=Cliente::cuantitativoRecuperacionesGeneral($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $ventas_nuevas_general[$hasta2]=Cliente::cuantitativoVentasNuevasGeneral($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $instalaciones_general[$hasta2]=Cliente::cuantitativoInstalacionesGeneral($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $desconexiones_general[$hasta2]=Cliente::cuantitativoDesconexionesGeneral($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $reconexiones_general[$hasta2]=Cliente::cuantitativoReconexionesGeneral($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $cambio_domicilio_general[$hasta2]=Cliente::cuantitativoDomicilioGeneral($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
            $retiros_general[$hasta2]=Cliente::cuantitativoRetirosGeneral($desde2, $hasta2, $_POST['fplaza'], $_POST['fsucursal'], $plaza);
        }

        //echo "<pre>"; print_r($clientes_pagados); exit;

        //echo "Reporte: "; echo "<pre>"; print_r($reconexiones);
        $this->view->clientes_pagados=$clientes_pagados;
        $this->view->mensualidades_cobradas=$mensualidades_cobradas;
        //echo "<pre>"; print_r($this->view->mensualidades_cobradas); exit;
        $this->view->recuperaciones=$recuperaciones;
        $this->view->reconexiones=$reconexiones;
        $this->view->ventas_nuevas=$ventas_nuevas;
        $this->view->instalaciones=$instalaciones;
        $this->view->desconexiones=$desconexiones;
        $this->view->cambio_domicilio=$cambio_domicilio;
        $this->view->extensiones=$extensiones;
        $this->view->retiros=$retiros;

        $this->view->clientes_pagados_internet=$clientes_pagados_internet;
        $this->view->mensualidades_cobradas_internet=$mensualidades_cobradas_internet;
        $this->view->recuperaciones_internet=$recuperaciones_internet;
        $this->view->reconexiones_internet=$reconexiones_internet;
        $this->view->ventas_nuevas_internet=$ventas_nuevas_internet;
        $this->view->instalaciones_internet=$instalaciones_internet;
        $this->view->desconexiones_internet=$desconexiones_internet;
        $this->view->cambio_domicilio_internet=$cambio_domicilio_internet;
        $this->view->retiros_internet=$retiros_internet;

        $this->view->clientes_pagados_general=$clientes_pagados_general;
        $this->view->mensualidades_cobradas_general=$mensualidades_cobradas_general;
        $this->view->recuperaciones_general=$recuperaciones_general;
        $this->view->reconexiones_general=$reconexiones_general;
        $this->view->ventas_nuevas_general=$ventas_nuevas_general;
        $this->view->instalaciones_general=$instalaciones_general;
        $this->view->desconexiones_general=$desconexiones_general;
        $this->view->cambio_domicilio_general=$cambio_domicilio_general;
        $this->view->retiros_general=$retiros_general;
        //echo "Fin";
        
        
        /**************Tablita****************/
        if($this->view->meses==1)
        {
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =1 AND ".$filtro);   
            $this->view->activos=$p->execute()->getFirst();                       

            
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status = 2 AND ".$filtro);   
            $this->view->desconectar=$p->execute()->getFirst();  
              
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status = 8 AND ".$filtro);   
            $this->view->desconectados=$p->execute()->getFirst();                     


            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =3 AND ".$filtro);   
            $this->view->cortar=$p->execute()->getFirst();                                
                
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =5 AND ".$filtro);   
            $this->view->bajas=$p->execute()->getFirst();                       
                 
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =4 AND ".$filtro);   
            $this->view->retirar=$p->execute()->getFirst();                       
                           
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =9 AND ".$filtro);   
            $this->view->retirado=$p->execute()->getFirst();

            
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =6 AND ".$filtro);   
            $this->view->instalar=$p->execute()->getFirst(); 

            
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =10 AND ".$filtro);   
            $this->view->reinstalar=$p->execute()->getFirst();  
            
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =11 AND ".$filtro);   
            $this->view->reconectar=$p->execute()->getFirst();

            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =12 AND ".$filtro);   
            $this->view->baja_voluntaria=$p->execute()->getFirst();
        }                   

    }        
    
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        $this->view->sucursales=Sucursal::obtenerSucursal();
             
        $dia[1]= "L";
        $dia[2]= "M";
        $dia[3]= "M";
        $dia[4]= "J";
        $dia[5]= "V";
        $dia[6]= "S";
        $dia[7]= "D";      
       
        $nombre_mes[1]= "Enero";
        $nombre_mes[2]= "Febrero";
        $nombre_mes[3]= "Marzo";
        $nombre_mes[4]= "Abril";
        $nombre_mes[5]= "Mayo";
        $nombre_mes[6]= "Junio";
        $nombre_mes[7]= "Julio";
        $nombre_mes[8]= "Agosto";
        $nombre_mes[9]= "Septiembre";
        $nombre_mes[10]= "Octubre";
        $nombre_mes[11]= "Noviembre";
        $nombre_mes[12]= "Diciembre";
    
        $qp=Doctrine_Query::create()->from('Parametrosdeproductividad');
        $productividades=$qp->execute();                       
        foreach($productividades as $productividad)
        {
            $puntos[1]= $productividad->instalaciones;
            $puntos[2]= $productividad->reconexiones;
            $puntos[3]= $productividad->cambio_domicilio;
            $puntos[4]= $productividad->atencion_a_fallas;
            $puntos[5]= $productividad->extensiones_adicionales;
            $puntos[6]= $productividad->recuperaciones;
            $puntos[7]= $productividad->auditorias_de_acometidas;
            $puntos[8]= $productividad->reinstalaciones;  
            $puntos[9]= $productividad->retiros;  
        }
        
        ### Cachamos las variables  
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
        
        if( $desde != ''){
            $mesI = date("m", strtotime($desde));
            $anoI = date("Y", strtotime($desde));
            $desde = $anoI."-".$mesI."-01";
        }
        
        else{
            $desde = date("Y-m-d",mktime(0,0,0,date("m"),1,date("Y")));
        }
       
        if($hasta != ''){
            $mesF= date("m", strtotime($this->_getParam('hasta')));
            $anoF= date("Y", strtotime($this->_getParam('hasta')));
            $diaF = date("d",mktime(0,0,0,$mesF+1,0,$anoF));        
            $hasta = $anoF."-".$mesF."-".$diaF;
        } 
        
        else{
            $diaF = date("d",mktime(0,0,0,date("m")+1,0,date("Y")));  
            $hasta= date("Y-m-d",mktime(0,0,0,date("m"),$diaF,date("Y")));
        }
        
        $filtro =" 1 = 1 ";
        $clientes_pagados =" 1=1 ";
        $recuperaciones =" 1=1 ";
        $ventas_nuevas =" 1=1 ";
        $instalaciones =" 1=1 ";
        $desconexiones =" 1=1 ";
        $reconexiones =" 1=1 ";
        $cambio_domicilio =" 1=1 ";
        $extencion_adic =" 1=1 ";
        $ordenes_retiros=" 1=1 ";
         
        if( $plaza !=0){
            $filtro.= " AND plaza_id =". $plaza;
            $clientes_pagados.= " AND Cobranza.plaza_id =". $plaza;
            $recuperaciones.= " AND CobranzaDetalle.Cobranza.Cliente.Plaza.id =". $plaza;
            $ventas_nuevas.= " AND plaza_id =". $plaza;
            $instalaciones.= " AND Ordeninstalacion.Cliente.plaza_id =". $plaza;
            $desconexiones.= " AND Ordendesconexion.Cliente.Plaza.id =". $plaza;
            $reconexiones.= " AND Ordendesreconexion.Cliente.Plaza.id =". $plaza;
            $cambio_domicilio.= " AND Ordencambiodomicilio.Cliente.Plaza.id =". $plaza;
            $extencion_adic.= " AND Ordenextensionadicional.Cliente.Plaza.id =". $plaza;   
            $ordenes_retiros.= " AND Ordendesretiro.Cliente.Plaza.id =". $plaza;   
        }
        else
        {
            $tmp_plaza = "";
            $tmp_plaza_uno = Permiso::obtienePlazas(Usuario::id());
            foreach ($tmp_plaza_uno as $permiso)
            {
                $tmp_plaza .= $permiso->plaza_id.",";
            }
            if($tmp_plaza != ""  && Usuario::tipo() != 0)
            {
                $plaza = trim($tmp_plaza, ',');
                $filtro.= " AND plaza_id IN (". $plaza.")";
                $clientes_pagados.= " AND Cobranza.plaza_id IN (". $plaza.")";
                $recuperaciones.= " AND CobranzaDetalle.Cobranza.Cliente.Plaza.id IN (". $plaza.")";
                $ventas_nuevas.= " AND plaza_id IN (". $plaza.")";
                $instalaciones.= " AND Ordeninstalacion.Cliente.plaza_id IN (". $plaza.")";
                $desconexiones.= " AND Ordendesconexion.Cliente.Plaza.id IN (". $plaza.")";
                $reconexiones.= " AND Ordendesreconexion.Cliente.Plaza.id IN (". $plaza.")";
                $cambio_domicilio.= " AND Ordencambiodomicilio.Cliente.Plaza.id IN (". $plaza.")";
                $extencion_adic.= " AND Ordenextensionadicional.Cliente.Plaza.id IN (". $plaza.")";
                $ordenes_retiros.= " AND Ordendesretiro.Cliente.Plaza.id IN (". $plaza.")";
            }
        }       
        
        if($sucursal != 0){
            $filtro.= " AND sucursal_id =". $sucursal;
            $clientes_pagados.= " AND Cobranza.Plaza.sucursal_id =". $sucursal;
            $recuperaciones.= " AND CobranzaDetalle.Cobranza.Cliente.Plaza.sucursal_id =". $sucursal;
            $ventas_nuevas.= " AND Cliente.Plaza.sucursal_id =". $sucursal;
            $instalaciones.= " AND Cliente.Plaza.sucursal_id =". $sucursal;
            $desconexiones.= " AND Ordendesconexion.Cliente.Plaza.sucursal_id =". $sucursal;
            $reconexiones.= " AND Ordendesreconexion.Cliente.Plaza.sucursal_id =". $sucursal;
            $cambio_domicilio.= " AND Ordencambiodomicilio.Cliente.Plaza.sucursal_id =". $sucursal;
            $extencion_adic.= " AND Ordenextensionadicional.Cliente.Plaza.sucursal_id =". $sucursal;
            $ordenes_retiros.= " AND Ordendesretiro.Cliente.Plaza.sucrusal_id =". $sucursal; 
        }
                
        $meses = My_Comun::numero_de_meses1( $hasta, $desde );  
        $data=array();
        $data=0;
        
        $clientes_pagados=$clientes_pagados." AND Cobranza.fecha_inicio != 'null' AND Cobranza.fecha_fin != 'null' AND Cobranza.CobranzaDetalle.otro_concepto='' AND Cobranza.CobranzaDetalle.concepto_contratacion_id IS NULL AND CAST(Cobranza.created_at AS DATE) ";
        $cliente_pagado = Cliente::reporteCuantitativo($desde, $hasta,'Cobranza',$clientes_pagados); 
        
        $men_cobrada = Cliente::mensualidaCobrada($desde, $hasta, $clientes_pagados);        
        
        $recuperaciones.= " AND otro_concepto = 'Recuperación' AND CAST(CobranzaDetalle.Cobranza.created_at AS DATE) ";
        $recuperacion = Cliente::reporteCuantitativo($desde, $hasta,'CobranzaDetalle', $recuperaciones, 2);
        
        $ventas_nuevas.=" AND CAST(Cliente.created_at AS DATE) ";
        $venta_nueva= Cliente::reporteCuantitativo($desde, $hasta, 'Cliente',$ventas_nuevas);
         
        $instalaciones.=" AND CAST(fecha_instalacion AS DATE) ";
        $instalacion = Cliente::reporteCuantitativo($desde, $hasta, 'Ordeninstalacion',$instalaciones);
        
        $desconexiones.=" AND CAST(Ordendesconexion.fecha_desconexion AS DATE) ";
        $desconexion = Cliente::reporteCuantitativo($desde, $hasta,'Ordendesconexion' ,$desconexiones);
        
        $reconexiones.=" AND CAST(fecha_reconexion AS DATE) ";
        $reconexion = Cliente::reporteCuantitativo($desde, $hasta,'Ordendesreconexion', $reconexiones);
        
        $auditoria_acom = $data;
                
        $cambio_domicilio.=" AND CAST(fecha_instalacion AS DATE) ";
        $domicilio = Cliente::reporteCuantitativo($desde, $hasta,'Ordencambiodomicilio', $cambio_domicilio);

        $ordenes_retiros.=" AND CAST(Ordendesretiro.fecha_retiro AS DATE) ";
        $retiros = Cliente::reporteCuantitativo($desde, $hasta,'Ordendesretiro' ,$ordenes_retiros);
        
        $extencion_adic.=" AND CAST(fecha_instalacion AS DATE)  ";
        $extencion_adi = Cliente::reporteCuantitativo($desde, $hasta,'Ordenextensionadicional',$extencion_adic); 
        
        
       $qp=Doctrine_Query::create()->from('Metas')->where($filtro);
        $metas=$qp->execute();                       
        foreach($metas as $meta)
        {
            $MetaRecupe += $meta->recuperaciones;
            $MetaVentas += $meta->ventas;
        }
        
        $notifi_cobranza = $data; 

        //exit;
        
        //ini_set("memory_limit", "130M");
        ini_set("memory_limit", "2048M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
        $objPHPExcel = new PHPExcel();
         
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
        $objPHPExcel->getActiveSheet()->getStyle('A2:AA2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2:AA2')->getFont()->setSize(16);
        $objPHPExcel->getActiveSheet()->getStyle('A2:AA2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->mergeCells('A2:AA2');    
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2','REPORTE CUANTITATIVO DE OPERACIÓN');
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);  //0.33

        $letras="B";
        /********TAMAÑO DE LAS CELDAS*******************/
        while($letras != "AP" )
        {  
            $objPHPExcel->getActiveSheet()->getColumnDimension($letras)->setWidth(4);  //5.14
            $letras++;            
        }
              
        $numeros=5;
        $letras="b";
        $mes=0;   
        while($meses > $mes )
        {  
            /*********Nombre del mes ************/
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$numeros.':AA'.$numeros);    
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':AA'.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':AA'.$numeros)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':AA'.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$numeros, $nombre_mes[date("n", strtotime($hasta))]);
            
            $numeros++;
            $dias = date("d", mktime( 0, 0, 0,    date("m", strtotime($hasta))+1  , 0 ,  date("Y", strtotime($hasta))  ));            
            $i=1;               
            $rangoCategorias="Worksheet!$"."B$".$numeros;                
            while($dias >= $i ){
                if(date("N",mktime(0,0,0,  date("m", strtotime($hasta)) ,$i,  date("Y", strtotime($hasta))  )) != 8){       
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $i );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(10);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);                    
                     $letras++;  
                }
                $i++;
            }
            $rangoCategorias.=":$".strtoupper($letras)."$".$numeros; 
            $objPHPExcel->getActiveSheet()->mergeCells($letras.$numeros.':'.$letras.($numeros+1));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, 'TOTAL' );
            $objPHPExcel->getActiveSheet()->getColumnDimension($letras)->setWidth(6);  //5.14  
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $letras++;
            $objPHPExcel->getActiveSheet()->mergeCells($letras.$numeros.':'.$letras.($numeros+1));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, 'METAS' );
            $objPHPExcel->getActiveSheet()->getColumnDimension($letras)->setWidth(6);  //5.14  
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $letras++;
            $objPHPExcel->getActiveSheet()->mergeCells($letras.$numeros.':'.$letras.($numeros+1));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, 'DIFERENCIA' );
            $objPHPExcel->getActiveSheet()->getColumnDimension($letras)->setWidth(8);  //5.14  
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $letras++;
            $objPHPExcel->getActiveSheet()->mergeCells($letras.$numeros.':'.$letras.($numeros+1));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, 'PORCENTAJE' );
            $objPHPExcel->getActiveSheet()->getColumnDimension($letras)->setWidth(9);  //5.14  
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);                        
                        
            /*****************TABLA OBJ*****************/
            
            $letras++; $letras++;
            $letrasA=$letras;
            $letras++;
            
            $objPHPExcel->getActiveSheet()->mergeCells($letrasA.$numeros.':'.$letras.$numeros);  
            $objPHPExcel->getActiveSheet()->setCellValue($letrasA.$numeros, 'OBJ');
            $objPHPExcel->getActiveSheet()->getStyle($letrasA.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letrasA.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letrasA.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '1-5');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '6-10');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '11-15');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '16-20');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '21-25');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '26-'.$dias);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            
            $objPHPExcel->getActiveSheet()->getStyle($letrasA.$numeros.':'.$letras.$numeros)->applyFromArray(
                array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'cccccc')
                    )
                )
            );
            
            /*****************FIN TABLA OBJ*****************/
              
            $numeros++;
            $letras="b";
            $i=1;                                                                  
            while($dias >= $i ){
                if(date("N",mktime(0,0,0,  date("m", strtotime($hasta)) ,$i,  date("Y", strtotime($hasta))  )) != 8){   
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $dia[date("N",mktime(0,0,0, date("m", strtotime($hasta))  ,$i,  date("Y", strtotime($hasta))   ))] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                }
                $i++;
            }
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "CLIENTES PAGADOS" );
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);
            
            $rangoValues="Worksheet!$"."B$".$numeros;
            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $cliente_pagado[date("n",strtotime($hasta))][$i]);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }           
            $rangoValues.=":$".strtoupper($letrasA)."$".$numeros;           
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true); 
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "MENS. COBRADA" );               
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);         
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $men_cobrada[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++; 
                    $letrasA++;
                }
                $i++;
            }            
            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);      
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "RECUPERACIONES" );
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);
            $r1_5=0; 
            $r6_10=0;
            $r11_15=0;
            $r16_20=0;
            $r21_25=0;
            $r26_31=0;
            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $recuperacion[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;    
                    $letrasA++;
                  
                    if( $i <= 5 && $i>0 ){                 
                        $r1_5+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 10  && $i > 5 ){                    
                        $r6_10+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 15  && $i >10 ){                    
                        $r11_15+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 20  && $i >15 ){                    
                        $r16_20+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    } 
                    
                    if( $i <= 25  && $i >20 ){                    
                        $r21_25+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 31  && $i >25 ){                    
                        $r26_31+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                }
                $i++;
            }
            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            
            $letras++;                        
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $MetaRecupe);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);  
            
            $letras++;      $letrasA++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.($letrasA).$numeros.'-'.$MetaRecupe);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);                          
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.($letrasA.$numeros.'/'.$MetaRecupe).'*100');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);   
            
            $letras++; $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $MetaRecupe);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);   
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.$letrasA.$numeros.'/'.$MetaRecupe);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);   
             
            $letras++;            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r1_5);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
           
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r6_10);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);            
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r11_15);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r16_20);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
                        
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r21_25);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
                               
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r26_31);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
                        
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $v1_5=0; 
            $v6_10=0;
            $v11_15=0;
            $v16_20=0;
            $v21_25=0;
            $v26_31=0;
                        
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "VENTAS NUEVAS" );
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $venta_nueva[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;     
                    $letrasA++;
                    
                    if( $i <= 5 && $i>0 ){                 
                        $v1_5+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 10  && $i > 5 ){                    
                        $v6_10+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 15  && $i >10 ){                    
                        $v11_15+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 20  && $i >15 ){                    
                        $v16_20+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    } 
                    
                    if( $i <= 25  && $i >20 ){                    
                        $v21_25+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 31  && $i >25 ){                    
                        $v26_31+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    }
                }
                
                $i++;
            }           
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            
            $letras++;            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $MetaVentas);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            
            $letras++;$letrasA++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.($letrasA).$numeros.'-'.$MetaVentas);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true); 
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.($letrasA.$numeros.'/'.$MetaVentas).'*100');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true); 
            
            $letras++; $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $MetaVentas);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);   
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.$letrasA.$numeros.'/'.$MetaVentas);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);                          
            
            $letras++;            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v1_5);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
           
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v6_10);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);            
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v11_15);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v16_20);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
                        
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v21_25);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
                               
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v26_31);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "INSTALACIONES");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $instalacion[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;                
                    $letrasA++;                 
                    
                    if( $i <= 5 && $i>0 ){                 
                        $r1_5+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 10  && $i > 5 ){                    
                        $r6_10+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 15  && $i >10 ){                    
                        $r11_15+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 20  && $i >15 ){                    
                        $r16_20+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    } 
                    
                    if( $i <= 25  && $i >20 ){                    
                        $r21_25+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 31  && $i >25 ){                    
                        $r26_31+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);            
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;          
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "DESCONEXIONES");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);  
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $desconexion[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;        
                    $letrasA++;
                }
                $i++;
            }            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
                        
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "RECONEXIONES");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $reconexion[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;       
                    $letrasA++;
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);       
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "AUDITORIA ACOM");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $auditoria_acom[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
                        
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "CAMBIO DOMICILIO");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $domicilio[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;      
                    $letrasA++;
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);            
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "EXTENCION ADIC");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $extencion_adi[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);

            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "RETIROS");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $retiros[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);            
            
            /*$numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "NOTIFI COBRANZA");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);           
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $notifi_cobranza[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);*/
                        
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;                        
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->applyFromArray(
                array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FF0000')
                    )
                )
            );
            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "PRODUCT TECNICA");           
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);
            
            while($dias >= $i ){ 
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    
                    $resultado =  $instalacion[date("n",strtotime($hasta))][$i] * $puntos[1] ;
                    $resultado += ( $desconexion[date("n",strtotime($hasta))][$i] );
                    $resultado += ( $reconexion[date("n",strtotime($hasta))][$i] * $puntos[2]);
                    $resultado += ( $auditoria_acom[date("n",strtotime($hasta))][$i] * $puntos[7]);
                    $resultado +=( $domicilio[date("n",strtotime($hasta))][$i] * $puntos[3]);
                    $resultado +=( $extencion_adi[date("n",strtotime($hasta))][$i] * $puntos[5]);
                    $resultado +=( $notifi_cobranza[date("n",strtotime($hasta))][$i]);                             
                    
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $resultado );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }      
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);    
            
            $numeros+=2;            
           /********************************/
             $sheet = $objPHPExcel->getActiveSheet();
             $sheet->getPageMargins()->setTop(0.6);
             $sheet->getPageMargins()->setBottom(0.6);
             $sheet->getPageMargins()->setHeader(0.4);
             $sheet->getPageMargins()->setFooter(0.4);
             $sheet->getPageMargins()->setLeft(0.4);
             $sheet->getPageMargins()->setRight(0.4);
             $objPHPExcel->getProperties()->setTitle("Demo");
             $objPHPExcel->getProperties()->setCreator("Demo");
             $objPHPExcel->getProperties()->setLastModifiedBy("Demo");
             $objPHPExcel->getProperties()->setCompany("Demo");
            
             $values = new PHPExcel_Chart_DataSeriesValues('Number', $rangoValues);//Worksheet!$B$8:$AA$8                                 
             $categories = new PHPExcel_Chart_DataSeriesValues('String', $rangoCategorias);//Worksheet!$B$6:$AA$6

             $series = new PHPExcel_Chart_DataSeries(
                PHPExcel_Chart_DataSeries::TYPE_LINECHART,       // plotType
                PHPExcel_Chart_DataSeries::GROUPING_STANDARD,  // plotGrouping
                array(0),                                       // plotOrder
                array(),                                        // plotLabel
                array($categories),                             // plotCategory
                array($values)                                  // plotValues
            );
            $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

            $layout = new PHPExcel_Chart_Layout();
            $plotarea = new PHPExcel_Chart_PlotArea($layout, array($series));

            $title = new PHPExcel_Chart_Title('CLIENTES PAGADOS');
            $chart = new PHPExcel_Chart('sample', $title, null, $plotarea, true);          
            
            $chart->setTopLeftPosition('A'.$numeros);   
            $numeros+=10;
            $chart->setBottomRightPosition('AC'.$numeros);

            $sheet->addChart($chart);       
           /***********fin de la grafica*********************/ 
            
           /*********Inicio de la tablita ************/ 
           if( strtotime($hasta) == strtotime(date("Y-m-".$dias)) ){       
            $numeros+=1; 
                  
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$numeros.':C'.$numeros);    
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$numeros, $nombre_mes[date("n", strtotime($hasta))]);

            $numeros++;    
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "ACTIVOS");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);            
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =1 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);   
           
            /*****************************************************************/
            
            $numeros++;  
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "DESCONECTAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);           
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status = 2 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);   
            
            $numeros++;  
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "DESCONECTADOS");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);           
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status = 8 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);            
            
            $numeros++;  
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "CORTAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);   
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =3 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);
               
            $numeros++;  
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "BAJAS");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);                     
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =5 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);
              
            $numeros++; 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "RETIRAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros); 
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =4 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);
                       
            $numeros++; 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "RETIRADO");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =9 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total); 
            
            $numeros++; 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "POR INSTALAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =6 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);
            
            $numeros++; 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "POR REINSTALAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =10 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);
            
            $numeros++; 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "POR RECONECTAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =11 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);     
            
                   
            }
                         
            $hasta=date("Y/m/d", strtotime($hasta." -1 month"));
            $letras="b";
            $numeros+=5;
            $mes++;               
        }
        /***********Fin de la tablita*************/
        
        // Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Reporte.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->setIncludeCharts(TRUE);
        $objWriter->save('php://output');
        exit;   
    }
     
    public function sucursalesAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $sucursales=Sucursal::obtenerSucursal("AND plaza_id=".$this->_getParam('id'));

        echo '<option value="0">TODAS LAS SUCURSALES</option>';
        foreach($sucursales as $sucursal)
        {
            echo '<option value="'.$sucursal->id.'">'.$sucursal->nombre.'</option>';
        }
       
    }


    public function detalleAction()
    {
        $this->_helper->layout->disableLayout();

        $plaza=$this->_getParam('plaza');
        $fecha=$this->_getParam('fecha');
        $this->view->reporte=$this->_getParam('reporte');

        switch($this->view->reporte)
        {
            case "clientesPagados": 
                $q=Doctrine_Query::create()->from('Cobranza')->where("fecha_inicio != 'null' AND fecha_fin != 'null' AND Cobranza.CobranzaDetalle.otro_concepto='' AND Cobranza.CobranzaDetalle.concepto_contratacion_id IS NULL AND created_at>='".$fecha." 00:00:00' AND created_at<='".$fecha." 23:59:59'");
                if($plaza>0)
                    $q->andWhere('plaza_id="'.$plaza.'"');
                $this->view->resultado=$q->execute();
            break;

            case "mensualidadesCobradas": 
                $q=Doctrine_Query::create()->from('Cobranza')->where("fecha_inicio != 'null' AND fecha_fin != 'null' AND Cobranza.CobranzaDetalle.otro_concepto='' AND Cobranza.CobranzaDetalle.concepto_contratacion_id IS NULL AND created_at>='".$fecha." 00:00:00' AND created_at<='".$fecha." 23:59:59'");
                if($plaza>0)
                    $q->andWhere('plaza_id="'.$plaza.'"');
                $this->view->resultado=$q->execute();
            break;

            case "recuperaciones": 
                $q=Doctrine_Query::create()->from('Cobranza')->where("fecha_inicio != 'null' AND fecha_fin != 'null' AND Cobranza.CobranzaDetalle.otro_concepto='Recuperación' AND Cobranza.CobranzaDetalle.concepto_contratacion_id IS NULL AND created_at>='".$fecha." 00:00:00' AND created_at<='".$fecha." 23:59:59'");
                if($plaza>0)
                    $q->andWhere('plaza_id="'.$plaza.'"');
                $this->view->resultado=$q->execute();
            break;

            case "ventas": 
                $q=Doctrine_Query::create()->from('Cobranza')->where("fecha_inicio != 'null' AND fecha_fin != 'null' AND Cobranza.CobranzaDetalle.otro_concepto='' AND Cobranza.CobranzaDetalle.concepto_contratacion_id IS NOT NULL AND created_at>='".$fecha." 00:00:00' AND created_at<='".$fecha." 23:59:59'");
                if($plaza>0)
                    $q->andWhere('plaza_id="'.$plaza.'"');
                $this->view->resultado=$q->execute();
            break;

            case "instalaciones": 
                $q=Doctrine_Query::create()->from('Ordeninstalacion')->where("fecha_instalacion>='".$fecha." 00:00:00' AND fecha_instalacion<='".$fecha." 23:59:59'");
                if($plaza>0)
                    $q->andWhere('Ordeninstalacion.Cliente.plaza_id="'.$plaza.'"');
                $this->view->resultado=$q->execute();
            break;

            case "desconexiones": 
                $q=Doctrine_Query::create()->from('Ordendesconexion')->where("fecha_desconexion>='".$fecha." 00:00:00' AND fecha_desconexion<='".$fecha." 23:59:59'");
                if($plaza>0)
                    $q->andWhere('Ordendesconexion.Cliente.plaza_id="'.$plaza.'"');
                $this->view->resultado=$q->execute();
            break;

            case "reconexiones": 
                $q=Doctrine_Query::create()->from('Ordendesreconexion')->where("fecha_reconexion>='".$fecha." 00:00:00' AND fecha_reconexion<='".$fecha." 23:59:59'");
                if($plaza>0)
                    $q->andWhere('Ordendesreconexion.Cliente.plaza_id="'.$plaza.'"');
                $this->view->resultado=$q->execute();
            break;

            case "cambioDomicilio": 
                $q=Doctrine_Query::create()->from('Ordencambiodomicilio')->where("fecha_instalacion>='".$fecha." 00:00:00' AND fecha_instalacion<='".$fecha." 23:59:59'");
                if($plaza>0)
                    $q->andWhere('Ordencambiodomicilio.Cliente.plaza_id="'.$plaza.'"');
                $this->view->resultado=$q->execute();
            break;

            case "extensionesAdicionales": 
                $q=Doctrine_Query::create()->from('Ordenextensionadicional')->where("fecha_instalacion>='".$fecha." 00:00:00' AND fecha_instalacion<='".$fecha." 23:59:59'");
                if($plaza>0)
                    $q->andWhere('Ordenextensionadicional.Cliente.plaza_id="'.$plaza.'"');
                $this->view->resultado=$q->execute();
            break;

            case "retiros": 
                $q=Doctrine_Query::create()->from('Ordendesretiro')->where("fecha_retiro>='".$fecha." 00:00:00' AND fecha_retiro<='".$fecha." 23:59:59'");
                if($plaza>0)
                    $q->andWhere('Ordendesretiro.Cliente.plaza_id="'.$plaza.'"');
                $this->view->resultado=$q->execute();
            break;

            
        }
    }
}