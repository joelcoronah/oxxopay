<?php

class Transacciones_AutorizacionesController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/autorizaciones.js'));
    }

    public function indexAction()
    {
        $this->view->plazas=Plaza::obtenerPlazas();
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);              
        
        ### Establecemos el filtro por default
        $filtro=" Autorizacion.status=0 ";
        
        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal'); 
                 
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);  
            $filtro .= " AND nombre LIKE '%".$nombre."%'";
        }   
        if($sucursal != 0)
        {
            $filtro .= " AND Autorizacion.Cliente.sucursal_id = ".$sucursal."  ";
        }    
        else if($plaza >0)
        {
            $filtro .= " AND Autorizacion.Cliente.plaza_id =" .$plaza;
        }
		
        ### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Autorizacion",$filtro); 
				
	$grid=array();	
	$i=0;
		
	foreach($registros['registros'] as $registro)
	{
            $grid[$i]['id']=$registro->id;
            $grid[$i]['fecha_solicitud']=$registro->fecha_solicitud;
			
            if(!is_null($registro->cliente_id) && $registro->cliente_id>0)
                $grid[$i]['cliente']=$registro->Cliente->nombre;
            if(!is_null($registro->cobranza_id) && $registro->cobranza_id>0)
		$grid[$i]['cliente']=$registro->Cobranza->Cliente->nombre;	
				
			$grid[$i]['usuario']=$registro->Solicito->nombre;
				
			$grid[$i]['plaza']=$registro->Solicito->Plaza->nombre;
			
			switch($registro->status)
			{
				case 0: $grid[$i]['estatus']="Pendiente"; break;
				case 1: $grid[$i]['estatus']="Autorizada"; break;
				case 2: $grid[$i]['estatus']="Rechazada"; break;
			}
			$grid[$i]['ver']='<span onclick="verautorizacion('.$registro->id.');" title="Ver"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/lupa.png" /></span>';
				
			$i++;
		}
		
        My_Comun::grid2($registros,$grid);	
    }


	
	public function verAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   
	   ### Obtenemos el detalle de la solicitud de autorización.
	   $this->view->autorizacion=My_Comun::obtener("Autorizacion",$this->_getParam('id'));
	   
	   ### Obtenemos el detalle del cliente
	   if($this->view->autorizacion->cliente_id>0)
	   {
			### Caemos en este caso cuando se trata de una contratación  
			$this->view->cliente=My_Comun::obtener("Cliente",$this->view->autorizacion->cliente_id);
			$this->view->tipo="contratacion";
			$this->view->preciosServicio=array();
			foreach($this->view->cliente->ClienteServicio as $servicio)
			{
				$this->view->preciosServicio[$servicio->servicio_id]['especial']=Servicio::obtenerTarifaAdministrador($servicio->servicio_id,$this->view->cliente->plaza_id,$servicio->tarifa_especial_id);
				$this->view->preciosServicio[$servicio->servicio_id]['normal']=Servicio::obtenerTarifaAdministrador($servicio->servicio_id,$this->view->cliente->plaza_id);
			}
	   }
	else	
	{
		### Caemos en este caso cuando se trata de un cobro
	   	$this->view->cliente=My_Comun::obtener("Cliente",$this->view->autorizacion->Cobranza->cliente_id);
		$this->view->tipo="cobro";         
        }

    }	
	
    public function dictaminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	  
        ### Actualizamos el contrato del cliente y la orden de instalación
       Autorizacion::dictaminar($_POST);	   	   
    }
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro=" Autorizacion.status=0 ";
        
        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal'); 
                 
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);  
            $filtro .= " AND nombre LIKE '%".$nombre."%'";
        }   
        if($sucursal != 0)
        {
            $filtro .= " AND Autorizacion.Cliente.sucursal_id = ".$sucursal."  ";
        }    
        else if($plaza >0)
        {
            $filtro .= " AND Autorizacion.Cliente.plaza_id =" .$plaza;
        }
        
        $registros=  My_Comun::obtenerFiltro("Ordeninstalacion", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE ÓRDENES DE AUTORIZACIONES");

        
        $pdf->SetWidths(array(45,45,45,45));
        
        
        foreach($registros as $registro)
        {
			$domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
			
			$pdf->SetWidths(array(45,45,45,45));
			$pdf->SetFillColor(220,220,220);
			$pdf->SetFont('Arial','B',10);
			$pdf->Row(array('FECHA DE CONTRATACIÓN','CONTRATO','CLIENTE','ESTATUS'),1,1);
			$pdf->SetFont('Arial','',10);
			$pdf->Row(array($registro->Cliente->fecha_contratacion,$registro->Cliente->contrato,$registro->Cliente->nombre,$registro->Cliente->estatus),1,1);
			
			$pdf->SetWidths(array(45,135));
			$pdf->SetFont('Arial','B',10);
			$pdf->Row(array('DOMICILIO',$domicilio),1,1);
			
			$pdf->SetWidths(array(45,45,45,45));
			$pdf->SetFont('Arial','B',10);
			$pdf->Row(array('NO. DE TELEVISIONES','CABLE','CONECTOR','DIVISOR 2'),0,1);
			$pdf->SetFont('Arial','',10);
			$pdf->Row(array($registro->Cliente->televisores,$registro->cable,$registro->conector,$registro->divisor2),0,1);
			
			$pdf->SetFont('Arial','B',10);
			$pdf->Row(array('DIVISOR 3','DIVISOR 4','GRAPAS','ETIQUETA'),0,1);
			$pdf->SetFont('Arial','',10);
			$pdf->Row(array($registro->divisor3,$registro->divisor4,$registro->grapas,$registro->Cliente->etiqueta),0,1);
			
			$pdf->SetFont('Arial','B',10);
			$pdf->Row(array('NODO','FECHA DE INSTALACIÓN','HORA INICIO','HORA DE TÉRMINO'),0,1);
			$pdf->SetFont('Arial','',10);
			$pdf->Row(array($registro->Cliente->nodo,$registro->fecha_instalacion,'',''),0,1);
			
			$pdf->SetWidths(array(90,90));
			$pdf->SetFont('Arial','B',11);
			$pdf->Row(array('NOMBRE DEL TÉCNICO INSTALADOR:',''),0,1);			
			$pdf->Ln(6);
        }
           
       $pdf->Output();	
       
    }
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro=" Autorizacion.status=0 ";
        
        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal'); 
                 
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);  
            $filtro .= " AND nombre LIKE '%".$nombre."%'";
        }   
        if($sucursal != 0)
        {
            $filtro .= " AND Autorizacion.Cliente.sucursal_id = ".$sucursal."  ";
        }    
        else if($plaza >0)
        {
            $filtro .= " AND Autorizacion.Cliente.plaza_id =" .$plaza;
        }
        
        $registros=  My_Comun::obtenerFiltro("Ordeninstalacion", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 20
                        ),
		"C$i" => array(
                        "name" => 'DOMICILIO',
                        "width" => 20
                        ),
		"D$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 20
                        )					
        );
        
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
		$domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
                $i++;
                $data[] = array(
				
		    "A$i" =>$registro->Cliente->contrato,
                    "B$i" =>$registro->Cliente->nombre,
                    "C$i" =>$domicilio,
                    "D$i" =>$registro->Cliente->estatus
                );
        }
		
        $objPHPExcel->createExcel('Ordenes_de_instalacion', $columns_name, $data, 10);
		
    }
    
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }
}