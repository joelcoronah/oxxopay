<?php

class Transacciones_ComisionesporpagarController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/comisionesporpagar.js'));
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
	
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);              
        
        ### Establecemos el filtro por default
        $filtro=" Cliente.comision_pagada=0 AND Cliente.promotor > 0 AND Cliente.comision>0 AND Cliente.created_at>='".date('Y-m-d 00:00:00')."' AND Cliente.created_at<='".date('Y-m-d 23:59:59')."'";
        
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Cliente.plaza_id=".Usuario::plaza();
        
        ### Cachamos las variables para conformar el filtro
        $tipo = $this->_getParam('tipo');
        $nombre = $this->_getParam('nombre');
	    $sucursal = $this->_getParam('sucursal'); 
        $plaza = $this->_getParam('plaza');	
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        
        
        
        if($desde != '')
        {    
            $filtro .= " AND Cliente.created_at >=  '$desde'";
        }    
        if($hasta != '')
        {    
            $filtro .= " AND Cliente.created_at <= '$hasta'";
        } 
        if($nombre != '')
        {    
            $filtro .= " AND Cliente.Promotor.nombre LIKE '%$nombre%'";
        }    


        if($sucursal != 0)
        {
            $filtro .= " AND Cliente.sucursal_id = ".$sucursal."  ";
        }        
	    else if($plaza >0)
        {
            $filtro .= " AND Cliente.plaza_id =" .$plaza;
        }
       
        ### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::registrosGrid("Cliente",$filtro); 
		
	$grid=array();
	$i=0;
		
	foreach($registros['registros'] as $registro)
	{      
        $grid[$i]['nombre']=$registro->Promotor->nombre;     
        $grid[$i]['concepto']="Comisión por el contrato ".$registro->contrato;
        $grid[$i]['fecha']=$registro->fecha_contratacion;
        $grid[$i]['comision']=$registro->comision;
        $grid[$i]['cobrar']='<span onclick="cobrar('.$registro->id.');" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png"/></span>';
        $i++;
	}
		
        My_Comun::grid2($registros,$grid);	
    }
    
    public function  cantidadAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE); 

        $filtro=" id= ".$this->_getParam('id');
        $this->view->cliente=Cliente::obtenerCliente($filtro);
	
        echo "$".number_format($this->view->cliente->comision,2,".",",");
    }
    
    public function  pagarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE); 
        
        $q = Doctrine_Query::create()->update('Cliente'.' c')->set('comision_pagada','1')->where('c.id='.$this->_getParam('id'));
        $q->execute();
		
    	$cliente = Cliente::obtenerCliente("id=".$this->_getParam('id'));
    		
        $_POST['plaza_id']= $cliente->plaza_id;
    	$_POST['fecha']= date("Y-m-d H:i:s");
    	$_POST['concepto']="Pago de comisiones del contrato ".$cliente->contrato;
    	$_POST['monto']=$cliente->comision;
    		
    	echo My_Comun::guardar("Gasto",$_POST,NULL,'','concepto');
    }
	
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
                $filtro=" Cliente.comision_pagada=0 AND Cliente.promotor > 0 AND Cliente.comision>0 AND Cliente.created_at>='".date('Y-m-d 00:00:00')."' AND Cliente.created_at<='".date('Y-m-d 23:59:59')."'";
        
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Cliente.plaza_id=".Usuario::plaza();
        
        ### Cachamos las variables para conformar el filtro
        $tipo = $this->_getParam('tipo');
        $nombre = $this->_getParam('nombre');
	$sucursal = $this->_getParam('sucursal'); 
        $plaza = $this->_getParam('plaza');	
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        
        if($tipo != '')
        {
            $filtro .= " AND Cliente.tipo_promotor ='" .$tipo."'";
        } 
        
        /*if($tipo=='vendedor' && $nombre != ''){ 
            
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);        
            $q=Doctrine_Query::create()->from('Tecnico')->where(" nombre LIKE '%".$nombre."%'");
            $tecnicos=$q->execute();         
            
            $contador = 0;
            
            foreach($tecnicos as $tecnico)
            {
                if($contador==0)
                     $filtro .= " AND ( promotor = ".$tecnico->id;
                
                else
                    $filtro .= " OR promotor = ".$tecnico->id;
                
                $contador++;
                
            }
            
            if($contador > 0)
                $filtro .= "  )  ";
            
            
           // echo $filtro; exit;
        }
        
        if($tipo=='promotor' && $nombre != ''){
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);   
            $filtro .= " AND Cliente.Promotor.nombre LIKE '%".$nombre."%'";      
        }*/ 

        if($desde != '')
        {    
            $filtro .= " AND Cliente.created_at >=  '$desde'";
        }    
        if($hasta != '')
        {    
            $filtro .= " AND Cliente.created_at <= '$hasta'";
        }                    
        if($sucursal != 0)
        {
            $filtro .= " AND Cliente.sucursal_id = ".$sucursal."  ";
        }        
	else if($plaza >0)
        {
            $filtro .= " AND Cliente.plaza_id =" .$plaza;
        }       	   
        
        $registros=  My_Comun::obtenerFiltro("Cliente", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE COMISIONES POR PAGAR");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(90,50,40));
        $pdf->Row(array('NOMBRE DEL TÉCNICO O PROMOTOR','FECHA DE GENERACIÓN','MONTO A PAGAR'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
            
            if($registro->tipo_promotor=="vendedor"){
               $tecnico=  Tecnico::obtenerTecnicosId($registro->promotor);
               $nombre=$tecnico->nombre;                 
            }        
            
            else if($registro->tipo_promotor=="promotor"){
                $promotor=Promotor::obtenerPromotorId($registro->promotor);
                $nombre=$promotor->nombre;     
            }      			

           $pdf->Row
           (
                array
                (
                    $nombre,
                    $registro->fecha_contratacion,
                    "$".number_format($registro->comision,2,".",",")
                ),0,1			
           );
        }
        
        
       $pdf->Output();	
       
    }
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
       ### Establecemos el filtro por default
       $filtro=" Cliente.comision_pagada=0 AND Cliente.promotor > 0 AND Cliente.comision>0 AND Cliente.created_at>='".date('Y-m-d 00:00:00')."' AND Cliente.created_at<='".date('Y-m-d 23:59:59')."'";
        
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Cliente.plaza_id=".Usuario::plaza();
        
        ### Cachamos las variables para conformar el filtro
        $tipo = $this->_getParam('tipo');
        $nombre = $this->_getParam('nombre');
	$sucursal = $this->_getParam('sucursal'); 
        $plaza = $this->_getParam('plaza');	
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        
        if($tipo != '')
        {
            $filtro .= " AND Cliente.tipo_promotor ='" .$tipo."'";
        } 
        
        /*if($tipo=='vendedor' && $nombre != ''){ 
            
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);        
            $q=Doctrine_Query::create()->from('Tecnico')->where(" nombre LIKE '%".$nombre."%'");
            $tecnicos=$q->execute();         
            
            $contador = 0;
            
            foreach($tecnicos as $tecnico)
            {
                if($contador==0)
                     $filtro .= " AND ( promotor = ".$tecnico->id;
                
                else
                    $filtro .= " OR promotor = ".$tecnico->id;
                
                $contador++;
                
            }
            
            if($contador > 0)
                $filtro .= "  )  ";
            
            
           // echo $filtro; exit;
        }
        
        if($tipo=='promotor' && $nombre != ''){
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);   
            $filtro .= " AND Cliente.Promotor.nombre LIKE '%".$nombre."%'";      
        }*/ 

        if($desde != '')
        {    
            $filtro .= " AND Cliente.created_at >=  '$desde'";
        }    
        if($hasta != '')
        {    
            $filtro .= " AND Cliente.created_at <= '$hasta'";
        }                    
        if($sucursal != 0)
        {
            $filtro .= " AND Cliente.sucursal_id = ".$sucursal."  ";
        }        
	else if($plaza >0)
        {
            $filtro .= " AND Cliente.plaza_id =" .$plaza;
        }       	

        $registros=  My_Comun::obtenerFiltro("Cliente", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'NOMBRE DEL TÉCNICO O PROMOTOR',
                        "width" => 40
                        ),
                "B$i" => array(
                        "name" => 'FECHA DE GENERACIÓN',
                        "width" => 26
                        ),
                "C$i" => array(
                        "name" => 'MONTO A PAGAR',
                        "width" => 20
                        )			
        );	
		
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
            if($registro->tipo_promotor=="vendedor"){
               $tecnico=  Tecnico::obtenerTecnicosId($registro->promotor);
               $nombre=$tecnico->nombre;                 
            }        
            
            else if($registro->tipo_promotor=="promotor"){
                $promotor=Promotor::obtenerPromotorId($registro->promotor);
                $nombre=$promotor->nombre;     
            }      
            
                $i++;
                $data[] = array(		
		    "A$i" =>$nombre,
                    "B$i" =>$registro->fecha_contratacion,
                    "C$i" =>"$".number_format($registro->comision,2,".",","),
                );
        }
        
        $objPHPExcel->createExcel('ComisionesPorPagar', $columns_name, $data, 10,array('rango'=>'A4:C4','size'=>14,'texto'=>'COMISIONES POR PAGAR'));
    }
    
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }
}