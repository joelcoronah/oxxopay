<?php

class Transacciones_ReportesController extends Zend_Controller_Action
{
	
	public function llenarAction()
    {
		
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);  
		
		
		
		echo My_Comun::llenar();
	}
	
    public function cobranzaAction()
    {	
        ini_set("memory_limit", "500M");
        ini_set('max_execution_time', 0);    
        $this->view->headScript()->appendFile('/js/transacciones/reportes/cobranza.js');
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();

    }
	
    public function gridcobranzaAction()
    {
        ini_set("memory_limit", "500M");
        ini_set('max_execution_time', 0);    
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 =1 AND Cobranza.pagada=1";
 
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Cobranza.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
		$contrato = $this->_getParam('contrato');
        $fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
		$estatus = $this->_getParam('estatus');
        $sucursal = $this->_getParam('sucursal');		
		$concepto = $this->_getParam('concepto');
                     
        if($nombre != '')
        {
           $filtro .= " AND (Cobranza.Cliente.nombre LIKE '%$nombre%' OR Cobranza.Cliente.telefono LIKE '%$nombre%' OR Cobranza.Cliente.calle LIKE '%$nombre%')";		   
        }
			            
        if($contrato != '')
        {		   
		   $filtro .= " AND Cobranza.Cliente.contrato = ".$contrato;
        }		
        
        if($fecha != '')
        {
           $filtro .= " AND (Cobranza.created_at >= '$fecha 00:00:00' ) ";
        }
						
        if($hasta != '')
        {
           $filtro .= " AND ( Cobranza.created_at <= '$hasta 23:59:59' ) ";
        }
		
        if($estatus != '')
        {
			$estatus = str_replace('_', ',', $estatus);
            $filtro .= " AND Cobranza.Cliente.status in (".$estatus.")";
        }
        
        if($plaza != '' && $plaza != 'undefined')
        {
            $plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Cobranza.plaza_id in (".$plaza.")";
        }	
        
        if($sucursal != 0)
        {
            $filtro .= " AND Cobranza.Cliente.sucursal_id = ".$sucursal;
        }
			
        if($concepto == 1)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%Cambio de domicilio%'";
        }
					
        if($concepto == 2)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.servicio_adicional_id != 'null'";
        }
		
        if($concepto == 3)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%Recuperaci%'";
        }
				
        if($concepto == 4)
        {
            $filtro .= " AND Cobranza.fecha_inicio != 'null' AND Cobranza.fecha_fin != 'null'";
        }
				
        if($concepto == 5)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%extensiones adicionales%'";
        }	
		
		//echo $filtro; exit;
		//### Extraemos los registros para formar el arreglo del grid
		$registros=My_Comun::registrosGrid("Cobranza",$filtro);      
        
		$grid=array();
		$i=0;
		
		$cancelarPago=(My_Permisos::tienePermiso('CANCELAR_PAGOS') == 1)?true:false;
		
		foreach($registros['registros'] as $registro)
		{
            $grid[$i]['folio']=$registro->id;
	    	$grid[$i]['fecha']=$registro->created_at;  
            $grid[$i]['contrato']=$registro->Cliente->contrato; 
            $grid[$i]['nombre']=$registro->Cliente->nombre;
			$grid[$i]['plaza']=$registro->Plaza->nombre; 
            $grid[$i]['cajero']=$registro->Usuario->nombre; 
            $grid[$i]['total']="$".number_format($registro->total,2,".",",");
						
			$concepto ="";	
        		
			if($registro->fecha_inicio != null && $registro->fecha_fin != null)
				$concepto .="Pago de mensualidad ";
			
			
			if(0 == strcmp($registro->CobranzaDetalle[0]->otro_concepto,"Cambio de domicilio"))       					
				$concepto .=", Cambio de domicilio";
        	
			
			if($registro->CobranzaDetalle[0]->servicio_adicional_id != null )       	
				$concepto .=", Televisión adicionales";
        	
									
        	if(0 == strcmp($registro->CobranzaDetalle[0]->otro_concepto,"Recuperación"))        	
				$concepto .=", Recuperación";
			
			$grid[$i]['concepto']=$concepto;		
						
			
			$grid[$i]['mes']= $registro->fecha_fin;
			
			if($registro->fecha_fin==NULL)
			$grid[$i]['mes']= "N/A";
			
			
            $grid[$i]['status']=($registro->cancelado==0)?"ACTIVO":"CANCELADO"; 
            $grid[$i]['canceladopor']=$registro->Cancelo->nombre;
			
            $grid[$i]['imprimir']='<a  href="/transacciones/recibo/imprimir/id/'.$registro->id.'" target="_blank"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/filtrar.png"/ title="Imprimir recibo"></a>';
            if($registro->cancelado==0 && $cancelarPago==true)
                $grid[$i]['cancelar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" title="Cancelar pago" onclick="cancelarPago('.$registro->id.')"/>';
            else	
                $grid[$i]['cancelar']='';
            
            $i++;
        }
	
        My_Comun::grid2($registros,$grid);
    }
	
    public function imprimircobranzaAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";        
		
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Cobranza.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
		$contrato = $this->_getParam('contrato');
        $fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
		$estatus = $this->_getParam('estatus');
        $sucursal = $this->_getParam('sucursal');
        $concepto = $this->_getParam('concepto');
		
		             
        if($nombre != '')
        {
           $filtro .= " AND (Cobranza.Cliente.nombre LIKE '%$nombre%' OR Cobranza.Cliente.telefono LIKE '%$nombre%' OR Cobranza.Cliente.calle LIKE '%$nombre%')";		   
        }
			            
        if($contrato != '')
        {		   
		   $filtro .= " AND Cobranza.Cliente.contrato = ".$contrato;
        }
        
        if($fecha != '')
        {
           $filtro .= " AND (Cobranza.created_at >= '$fecha 00:00:00' ) ";
        }
						
        if($hasta != '')
        {
           $filtro .= " AND ( Cobranza.created_at <= '$hasta 23:59:59' ) ";
        }		
		        
        if($estatus != 0)
        {
            $filtro .= " AND Cobranza.Cliente.status = ".$estatus;
        }
        
        if($plaza != '' && $plaza != 'undefined')
        {
            $plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Cobranza.plaza_id in (".$plaza.")";
        }
        
        if($sucursal != 0)
        {
            $filtro .= " AND Cobranza.Cliente.sucursal_id = ".$sucursal;
        }
        
		if($concepto == 1)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%Cambio de domicilio%'";
        }
					
        if($concepto == 2)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.servicio_adicional_id != 'null'";
        }
		
        if($concepto == 3)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%Recuperaci%'";
        }
				
        if($concepto == 4)
        {
            $filtro .= " AND Cobranza.fecha_inicio != 'null' AND Cobranza.fecha_fin != 'null'";
        }
				
        if($concepto == 5)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%extensiones adicionales%'";
        }
		
        $registros=  My_Comun::obtenerFiltro("Cobranza", $filtro);
       
        $pdf= new My_Fpdf_Pdf('L');
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE COBRANZA");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(15,40,25,65,52,18,40,22));
        $pdf->Row(array('FOLIO','FECHA DE COBRO','CONTRATO','CLIENTE','CAJERO','MONTO', 'CONCEPTO', 'FECHA'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {    
		
			$concepto =" ";	
			
			if($registro->fecha_inicio != null && $registro->fecha_fin  != null)
				$concepto.="Pago de mensualidad ";
						
			if(0 == strcmp($registro->CobranzaDetalle[0]->otro_concepto,"Cambio de domicilio"))       					
				$concepto.=", Cambio de domicilio";
        				
			if($registro->CobranzaDetalle[0]->servicio_adicional_id != null )       	
				$concepto.=", Televisión adicionales";        	
									
        	if(0 == strcmp($registro->CobranzaDetalle[0]->otro_concepto,"Recuperación"))        	
				$concepto.=", Recuperación";
			
			$fin=$registro->fecha_fin;
			if($registro->fecha_fin==NULL)
				$fin= "N/A";
			
           $pdf->Row
           (
                array
                (
                    str_pad((int)$registro->id,0,"0",STR_PAD_LEFT),
                    $registro->created_at,
                    str_pad((int)$registro->Cliente->contrato,0,"0",STR_PAD_LEFT),
                    $registro->Cliente->nombre,
                    $registro->Usuario->nombre,
                    "$".number_format($registro->total,2,".",","),
					$concepto,
					$fin
                ),0,1			
           );
        }	
		
       $pdf->Output();	 
    }
	
    function exportarcobranzaAction()
    {
        ini_set("memory_limit", "500M");
        ini_set('max_execution_time', 0);
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Cobranza.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
		$contrato = $this->_getParam('contrato');
        $fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
		$estatus = $this->_getParam('estatus');
        $sucursal = $this->_getParam('sucursal');
        $concepto = $this->_getParam('concepto');
		             
        if($nombre != '')
        {
           $filtro .= " AND (Cobranza.Cliente.nombre LIKE '%$nombre%' OR Cobranza.Cliente.telefono LIKE '%$nombre%' OR Cobranza.Cliente.calle LIKE '%$nombre%')";		   
        }
			            
        if($contrato != '')
        {		   
		   $filtro .= " AND Cobranza.Cliente.contrato = ".$contrato;
        }
		
        if($fecha != '')
        {
           $filtro .= " AND (Cobranza.created_at >= '$fecha 00:00:00' ) ";
        }
						
        if($hasta != '')
        {
           $filtro .= " AND ( Cobranza.created_at <= '$hasta 23:59:59' ) ";
        }		
        
        if($estatus != 0)
        {
            $filtro .= " AND Cobranza.Cliente.status = ".$estatus;
        }
        
        if($plaza != '' && $plaza != 'undefined')
        {
            $plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Cobranza.plaza_id in (".$plaza.")";
        }
        
        if($sucursal != 0)
        {
            $filtro .= " AND Cobranza.Cliente.sucursal_id = ".$sucursal;
        }
		
		if($concepto == 1)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%Cambio de domicilio%'";
        }
					
        if($concepto == 2)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.servicio_adicional_id != 'null'";
        }
		
        if($concepto == 3)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%Recuperaci%'";
        }
				
        if($concepto == 4)
        {
            $filtro .= " AND Cobranza.fecha_inicio != 'null' AND Cobranza.fecha_fin != 'null'";
        }
				
        if($concepto == 5)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%extensiones adicionales%'";
        }
		
        $registros=  My_Comun::obtenerFiltro("Cobranza", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FOLIO',
                        "width" => 11
                        ),
                "B$i" => array(
                        "name" => 'FECHA DE COBRO',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 11
                        ),
				"D$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 30
                        ),
				"E$i" => array(
                        "name" => 'CAJERO',
                        "width" => 30
                        ),
				"F$i" => array(
                        "name" => 'MONTO',
                        "width" => 11
                        ),
				"G$i" => array(
                        "name" => 'CONCEPTO',
                        "width" => 20
                        ),
                "H$i" => array(
                        "name" => 'ESTATUS DEL PAGO',
                        "width" => 20
                        ),
				"I$i" => array(
                        "name" => 'MES CUBIERTO',
                        "width" => 15
                        )										
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
			
			$concepto =" ";	
			
			if($registro->fecha_inicio != null && $registro->fecha_fin  != null)
				$concepto.="Pago de mensualidad ";
						
			if(0 == strcmp($registro->CobranzaDetalle[0]->otro_concepto,"Cambio de domicilio"))       					
				$concepto.=", Cambio de domicilio";
        				
			if($registro->CobranzaDetalle[0]->servicio_adicional_id != null )       	
				$concepto.=", Televisión adicionales";        	
									
        	if(0 == strcmp($registro->CobranzaDetalle[0]->otro_concepto,"Recuperación"))        	
				$concepto.=", Recuperación";
			
			
			$fin=$registro->fecha_fin;
			if($registro->fecha_fin==NULL)
				$fin= "N/A";

            if($registro->cancelado==1)
                $estatus_pago="CANCELADO";

			
						
            $i++;
            $data[] = array(				
                "A$i" =>str_pad((int)$registro->id,0,"0",STR_PAD_LEFT),
                "B$i" =>$registro->created_at,
                "C$i" =>str_pad((int)$registro->Cliente->contrato,0,"0",STR_PAD_LEFT),
                "D$i" =>$registro->Cliente->nombre,
                "E$i" =>$registro->Usuario->nombre,
                "F$i" =>"$".number_format($registro->total,2,".",","),
				"G$i" =>$concepto,
                "H$i" =>$estatus_pago,
				"I$i" =>$fin
            );
        }
				
        $objPHPExcel->createExcel('Cobranza', $columns_name, $data, 10, array('rango'=>'A4:F4','size'=>14,'texto'=>'COBRANZA'));
    } 	
	
    /**********LLAMAS DE COBRANZA*****************/
    public function gridllamadasdecobranzaAction()
    {
	### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 =1 ";        
 
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');           
        $sucursal = $this->_getParam('sucursal');
           
        if($nombre != '')
        {
           $filtro .= " AND (nombre LIKE '%$nombre%' OR telefono LIKE '%$nombre%' OR calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            $filtro .= " AND status =" .$estatus;
        }	
        if($estatus == 0)
        {
            $filtro .= " AND (status = 2 OR status = 8) ";
        }        
        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal;
        }    
		
	//### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Cliente",$filtro);      
        
	$grid=array();
	$i=0;
		
	foreach($registros['registros'] as $registro)
	{
	        $grid[$i]['fecha']=$registro->updated_at;  
            $grid[$i]['contrato']=$registro->contrato; 
            $grid[$i]['nombre']=$registro->nombre; 
            $grid[$i]['plaza']=$registro->Plaza->nombre; 
            $grid[$i]['telefono']=$registro->telefono; 
            $grid[$i]['celular']=$registro->celular1; 
            $grid[$i]['estatus']=$registro->estatus;
            $i++;
        }
	
        My_Comun::grid2($registros,$grid);
    }
	
    public function imprimirllamadasdecobranzaAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');           
        $sucursal = $this->_getParam('sucursal');
           
        if($nombre != '')
        {
           $filtro .= " AND (nombre LIKE '%$nombre%' OR telefono LIKE '%$nombre%' OR calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            $filtro .= " AND status =" .$estatus;
        }	
        if($estatus == 0)
        {
            $filtro .= " AND (status = 2 OR status = 8) ";
        }        
        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Cliente", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE LLAMADAS DE COBRANZA");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(35,60,30,30,25));
        $pdf->Row(array('FECHA','CLIENTE','TELEFONO','CELULAR','ESTATUS'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {    
           $pdf->Row
           (
                array
                (
                    $registro->updated_at,
                    $registro->nombre,
                    $registro->telefono,
                    $registro->celular1,
                    $registro->estatus
                ),0,1			
           );
        }
		
       $pdf->Output();	 
    }
	
    function exportarllamadasdecobranzaAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');           
        $sucursal = $this->_getParam('sucursal');
           
        if($nombre != '')
        {
           $filtro .= " AND (nombre LIKE '%$nombre%' OR telefono LIKE '%$nombre%' OR calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            $filtro .= " AND status =" .$estatus;
        }	
        if($estatus == 0)
        {
            $filtro .= " AND (status = 2 OR status = 8) ";
        }        
        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Cliente", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 30
                        ),
				"C$i" => array(
                        "name" => 'TELEFONO',
                        "width" => 20
                        ),
				"D$i" => array(
                        "name" => 'CELULAR',
                        "width" => 20
                        ),
				"E$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 20
                        )										
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(				
		    		"A$i" =>$registro->updated_at,
                    "B$i" =>$registro->nombre,
                    "C$i" =>$registro->telefono,
                    "D$i" =>$registro->celular1,
					"E$i" =>$registro->estatus
                );
        }
				
        $objPHPExcel->createExcel('Cliente', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'LLAMADAS DE COBRANZA'));
    } 	
	
	/*******************CAMBIO DE DOMICILIO*************************/
    public function ordencambiodomicilioAction()
    {		
        $this->view->headScript()->appendFile('/js/transacciones/reportes/ordencambiodomicilio.js');
        $this->view->plazas=Plaza::obtenerPlazas();
    }
	
    public function gridordencambiodomicilioAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        
 
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordencambiodomicilio.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
         
        if($nombre != '')
        {
           $filtro .= " AND (Ordencambiodomicilio.Cliente.nombre LIKE '%$nombre%' OR Ordeninstalacion.Cliente.telefono LIKE '%$nombre%' OR Ordeninstalacion.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordencambiodomicilio.Cliente.contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            if($estatus==3){ $estatus=0; }
            $filtro .= " AND Ordencambiodomicilio.status =" .$estatus;
        }        
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordencambiodomicilio.Cliente.plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND Ordencambiodomicilio.Cliente.sucursal_id = ".$sucursal;
        }
        
	//### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Ordencambiodomicilio",$filtro);      
        
	$grid=array();
	$i=0;
		
	foreach($registros['registros'] as $registro)
	{
            $grid[$i]['fecha']=$registro->created_at; 
            $grid[$i]['nombre']=$registro->Cliente->nombre; 
			$grid[$i]['contrato']=$registro->Cliente->contrato;			
			$grid[$i]['domicilio']=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
			$grid[$i]['domicilionuevo']=$registro->calle." #".$registro->no_exterior.", Col.:".$registro->colonia;
			$grid[$i]['fecharetiro']=$registro->fecha_retiro;			
			$grid[$i]['tecnicoR']= $registro->Tecnico_retiro->nombre;
			$grid[$i]['fecha_instalacion']=$registro->fecha_instalacion;
			$grid[$i]['tecnicoI']= $registro->Tecnico_instalo->nombre;
			
			if($registro->status==0){ $grid[$i]['status']="CANCELADO"; }			
			if($registro->status==1){ $grid[$i]['status']="PENDIENTE"; }
			if($registro->status==2){ $grid[$i]['status']="TERMINADO"; }

            $i++;
        }
	
        My_Comun::grid2($registros,$grid);
    }

    public function imprimirordencambiodomicilioAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordencambiodomicilio.Cliente.plaza_id=".Usuario::plaza()." ";

                ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
         
        if($nombre != '')
        {
           $filtro .= " AND (Ordencambiodomicilio.Cliente.nombre LIKE '%$nombre%' OR Ordeninstalacion.Cliente.telefono LIKE '%$nombre%' OR Ordeninstalacion.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordencambiodomicilio.Cliente.contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            if($estatus==3){ $estatus=0; }
            $filtro .= " AND Ordencambiodomicilio.status =" .$estatus;
        }        
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordencambiodomicilio.Cliente.plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND Ordencambiodomicilio.Cliente.sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Ordencambiodomicilio", $filtro);
       
        $pdf= new My_Fpdf_Pdf("L");
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE ÓRDENES DE CAMBIO DE DOMICILIO");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(35,43,26,26,25,25,25,25,25,25));
        $pdf->Row(array('FECHA DE SOLICITUD','CLIENTE','CONTRATO','DOMICILIO ANTERIOR','DOMICILIO NUEVO','FECHA DE RETIRO','TÉCNICO QUE RETIRÓ',
		'FECHA DE INSTALACIÓN','TÉCNICO QUE INSTALÓ','ESTATUS DE LA ORDÉN'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {  
			if($registro->status==0){ $status="CANCELADO"; }
			if($registro->status==1){ $status="PENDIENTE"; }
			if($registro->status==2){ $status="TERMINADO"; }		
			
           $pdf->Row
           (
                array
                (
                    $registro->created_at,
                    $registro->Cliente->nombre,
                    $registro->Cliente->contrato,
                    $registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id),
					$registro->calle." #".$registro->no_exterior.", Col.:".$registro->colonia,
					$registro->fecha_retiro,
					"",
					$registro->fecha_instalacion,
					"",
					$status
                ),0,1			
           );
        }
		
       $pdf->Output();	 
    }
	
    function exportarordencambiodomicilioAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordencambiodomicilio.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
         
        if($nombre != '')
        {
           $filtro .= " AND (Ordencambiodomicilio.Cliente.nombre LIKE '%$nombre%' OR Ordeninstalacion.Cliente.telefono LIKE '%$nombre%' OR Ordeninstalacion.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordencambiodomicilio.Cliente.contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            if($estatus==3){ $estatus=0; }
            $filtro .= " AND Ordencambiodomicilio.status =" .$estatus;
        }        
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordencambiodomicilio.Cliente.plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND Ordencambiodomicilio.Cliente.sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Ordencambiodomicilio", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA DE SOLICITUD',
                        "width" => 25
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 20
                        ),
				"D$i" => array(
                        "name" => 'DOMICILIO ANTERIOR',
                        "width" => 20
                        ),
				"E$i" => array(
                        "name" => 'DOMICILIO NUEVO',
                        "width" => 20
                        ),
				"F$i" => array(
                        "name" => 'FECHA DE RETIRO',
                        "width" => 20
                        ),
				"G$i" => array(
                        "name" => 'TÉCNICO QUE RETIRÓ',
                        "width" => 20
                        ),
				"H$i" => array(
                        "name" => 'FECHA DE INSTALACIÓN',
                        "width" => 20
                        ),
				"I$i" => array(
                        "name" => 'TÉCNICO QUE INSTALÓ',
                        "width" => 20
                        ),
				"J$i" => array(
                        "name" => 'ESTATUS DE LA ORDÉN',
                        "width" => 20
                        )																			
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
		if($registro->status==0){ $status="CANCELADO"; }
		if($registro->status==1){ $status="PENDIENTE"; }
		if($registro->status==2){ $status="TERMINADO"; }
			
         	$i++;
           	$data[] = array(				
		    		"A$i" =>$registro->created_at,
                    "B$i" =>$registro->Cliente->nombre,
                    "C$i" =>$registro->Cliente->contrato,
                    "D$i" =>$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id),
					"E$i" =>$registro->calle." #".$registro->no_exterior.", Col.:".$registro->colonia,
					"F$i" =>$registro->fecha_retiro,
					"G$i" =>"",
					"H$i" =>$registro->fecha_instalacion,
					"I$i" =>"",
					"J$i" =>$status
        	);	
        }
				
$objPHPExcel->createExcel('Ordeninstalacion', $columns_name, $data, 10, array('rango'=>'A4:J4','size'=>14,'texto'=>'ÓRDENES DE CAMBIO DE DOMICILIO'));
    } 
	
    public function ordenesdeinstalacionAction()
    {
        $this->view->headScript()->appendFile('/js/transacciones/reportes/ordenesdeinstalacion.js');
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
	
    public function gridordenesdeinstalacionAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        
 
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordeninstalacion.Cliente.plaza_id=".Usuario::plaza()." ";	

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
          
        if($nombre != '')
        {
           $filtro .= " AND (Ordeninstalacion.Cliente.nombre LIKE '%$nombre%' OR Cliente.telefono LIKE '%$nombre%' OR Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordeninstalacion.Cliente.contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            if($estatus==2){ $estatus=0; }
            $filtro .= " AND Ordeninstalacion.estatus =" .$estatus;
        }
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordeninstalacion.Cliente.plaza_id in (".$plaza.")";
        }		
        if($sucursal != 0)
        {
            $filtro .= " AND Ordeninstalacion.Cliente.sucursal_id = ".$sucursal;
        }
		
	//### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Ordeninstalacion",$filtro);      
        
		$grid=array();
		$i=0;
		
		foreach($registros['registros'] as $registro)
		{							
                    $grid[$i]['fecha']=$registro->created_at;  
                    $grid[$i]['nombre']=$registro->Cliente->nombre; 
                    $grid[$i]['contrato']=$registro->Cliente->contrato;	
                    $grid[$i]['domicilio']=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
                    $grid[$i]['televisores']=$registro->Cliente->televisores;
                    $grid[$i]['tecnico']=$registro->Tecnico->nombre; 
                    $grid[$i]['fecha_instalacion']=$registro->fecha_instalacion;		
                $grid[$i]['estatus']=$registro->estatus; 
            $i++; 			
        }
	
        My_Comun::grid2($registros,$grid);
    }
	
	public function imprimirordenesdeinstalacionAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordeninstalacion.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
          
        if($nombre != '')
        {
           $filtro .= " AND (Ordeninstalacion.Cliente.nombre LIKE '%$nombre%' OR Cliente.telefono LIKE '%$nombre%' OR Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordeninstalacion.Cliente.contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            if($estatus==2){ $estatus=0; }
            $filtro .= " AND Ordeninstalacion.estatus =" .$estatus;
        }
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordeninstalacion.Cliente.plaza_id in (".$plaza.")";
        }		
        if($sucursal != 0)
        {
            $filtro .= " AND Ordeninstalacion.Cliente.sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Ordeninstalacion", $filtro);
       
        $pdf= new My_Fpdf_Pdf("L");
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE ÓRDENES DE INSTALACIÓN");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(36,50,24,38,26,37,35,31));
        $pdf->Row(array('FECHA DE SOLICITUD','CLIENTE','CONTRATO','DOMICILIO','TELEVISIONES','TÉCNICO QUE INSTALÓ','FECHA DE INSTALACIÓN','ESTATUS DE LA ORDEN'),0,1);
				
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {    
           $pdf->Row
           (
                array
                (
                    $registro->created_at,
                    $registro->Cliente->nombre,
                    $registro->Cliente->contrato,
                    $registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id),
					$registro->Cliente->televisores,
					$registro->Tecnico->nombre,
					$registro->fecha_instalacion,
					$registro->estatus
                ),0,1			
           );
        }
		
       $pdf->Output();	 
    }
	
    function exportarordenesdeinstalacionAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordeninstalacion.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
          
        if($nombre != '')
        {
           $filtro .= " AND (Ordeninstalacion.Cliente.nombre LIKE '%$nombre%' OR Cliente.telefono LIKE '%$nombre%' OR Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordeninstalacion.Cliente.contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            if($estatus==2){ $estatus=0; }
            $filtro .= " AND Ordeninstalacion.estatus =" .$estatus;
        }
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordeninstalacion.Cliente.plaza_id in (".$plaza.")";
        }		
        if($sucursal != 0)
        {
            $filtro .= " AND Ordeninstalacion.Cliente.sucursal_id = ".$sucursal;
        }		
		
        $registros=  My_Comun::obtenerFiltro("Ordeninstalacion", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA DE SOLICITUD',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 12
                        ),
				"D$i" => array(
                        "name" => 'DOMICILIO',
                        "width" => 20
                        ),
				"E$i" => array(
                        "name" => 'TELEVISIONES',
                        "width" => 17
                        ),
				"F$i" => array(
                        "name" => 'TÉCNICO QUE INSTALÓ',
                        "width" => 20
                        ),
				"G$i" => array(
                        "name" => 'FECHA DE INSTALACIÓN',
                        "width" => 20
                        ),
				"H$i" => array(
                        "name" => 'ESTATUS DE LA ORDEN',
                        "width" => 20
                        )			
					
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(				
                            "A$i" =>$registro->created_at,
                            "B$i" =>$registro->Cliente->nombre,
                            "C$i" =>$registro->Cliente->contrato,
                            "D$i" =>$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id),
                            "E$i" =>$registro->Cliente->televisores,
                            "F$i" =>$registro->Tecnico->nombre,
                            "G$i" =>$registro->fecha_instalacion,
                            "H$i" =>$registro->estatus
                );
        }
				
        $objPHPExcel->createExcel('Ordeninstalacion', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'ÓRDENES DE INSTALACIÓN'));
    } 
    
    public function ordenesdedesconexionAction()
    {
        $this->view->headScript()->appendFile('/js/transacciones/reportes/ordenesdedesconexion.js');
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
	
    public function gridordenesdedesconexionAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        
 
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordendesconexion.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus'); 
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
         
        if($nombre != '')
        {
           $filtro .= " AND (Ordendesconexion.Cliente.nombre LIKE '%$nombre%' OR Ordendesconexion.Cliente.telefono LIKE '%$nombre%' OR Ordendesconexion.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordendesconexion.Cliente.contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            if($estatus==2){ $estatus=0; }
            $filtro .= " AND Ordendesconexion.status =" .$estatus;
        } 
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordendesconexion.Cliente.plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND Ordendesconexion.Cliente.sucursal_id = ".$sucursal;
        }
        
	//### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Ordendesconexion",$filtro);      
        		
    $grid=array();
	$i=0;
		
	foreach($registros['registros'] as $registro)
	{
	    $grid[$i]['fecha']=$registro->fecha_desconexion;  
            $grid[$i]['nombre']=$registro->Cliente->nombre; 
            $grid[$i]['telefono']=$registro->Cliente->telefono; 
            $grid[$i]['celular']=$registro->Cliente->celular1; 
            $grid[$i]['status']=$registro->Cliente->estatus; 
            $i++;
        }
	
        My_Comun::grid2($registros,$grid);
    }
	
    public function imprimirordenesdedesconexionAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordeninstalacion.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus'); 
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
         
        if($nombre != '')
        {
           $filtro .= " AND (Ordendesconexion.Cliente.nombre LIKE '%$nombre%' OR Ordendesconexion.Cliente.telefono LIKE '%$nombre%' OR Ordendesconexion.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordendesconexion.Cliente.contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            if($estatus==2){ $estatus=0; }
            $filtro .= " AND Ordendesconexion.status =" .$estatus;
        } 
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordendesconexion.Cliente.plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND Ordendesconexion.Cliente.sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Ordendesconexion", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE ÓRDENES DE DESCONEXIÓN");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(35,60,30,30,25));
        $pdf->Row(array('FECHA','CLIENTE','TELEFONO','CELULAR','ESTATUS'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {    
           $pdf->Row
           (
                array
                (
                    $registro->fecha_desconexion,
                    $registro->Cliente->nombre,
                    $registro->Cliente->telefono,
                    $registro->Cliente->celular1,
					$registro->Cliente->estatus
                ),0,1			
           );
        }
		
       $pdf->Output();	 
    }
	
    function exportarordenesdedesconexionAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordeninstalacion.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
                $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus'); 
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
         
        if($nombre != '')
        {
           $filtro .= " AND (Ordendesconexion.Cliente.nombre LIKE '%$nombre%' OR Ordendesconexion.Cliente.telefono LIKE '%$nombre%' OR Ordendesconexion.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordendesconexion.Cliente.contrato = '$contrato') ";
        }
        if($estatus > 0)
        {
            if($estatus==2){ $estatus=0; }
            $filtro .= " AND Ordendesconexion.status =" .$estatus;
        } 
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordendesconexion.Cliente.plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND Ordendesconexion.Cliente.sucursal_id = ".$sucursal;
        }		
		
        $registros=  My_Comun::obtenerFiltro("Ordendesconexion", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'TELEFONO',
                        "width" => 20
                        ),

				"D$i" => array(
                        "name" => 'CELULAR',
                        "width" => 20
                        ),
				"E$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 20
                        )										
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(				
		    		"A$i" =>$registro->fecha_desconexion,
                    "B$i" =>$registro->Cliente->nombre,
                    "C$i" =>$registro->Cliente->telefono,
                    "D$i" =>$registro->Cliente->celular1,
					"E$i" =>$registro->Cliente->estatus
                );
        }
				
        $objPHPExcel->createExcel('Ordendesconexion', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'ÓRDENES DE DESCONEXIÓN'));
    }	
	
    public function ordenesderetiroAction()
    {
        $this->view->headScript()->appendFile('/js/transacciones/reportes/ordenesderetiro.js');
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
    
    public function gridordenesderetiroAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        
 
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordendesretiro.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
                       
        if($nombre != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.nombre LIKE '%$nombre%' OR Ordendesretiro.Cliente.telefono LIKE '%$nombre%' OR Ordendesretiro.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.contrato = '$contrato') ";
        }        
        if($estatus >0)
        {
            if($estatus==2){ $estatus=0; }
            $filtro .= " AND Ordendesretiro.estatus =" .$estatus;
        }
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordendesretiro.Cliente.plaza_id in (".$plaza.")";
        }		
        if($sucursal != 0)
        {
            $filtro .= " AND Ordendesretiro.Cliente.sucursal_id = ".$sucursal;
        }
        
	//### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Ordendesretiro",$filtro);      
        
	$grid=array();
	$i=0;
		
	foreach($registros['registros'] as $registro)
	{
	    $grid[$i]['fecha']=$registro->fecha_retiro;  
            $grid[$i]['nombre']=$registro->Cliente->nombre; 
            $grid[$i]['telefono']=$registro->Cliente->telefono; 
            $grid[$i]['celular']=$registro->Cliente->celular1; 
            $grid[$i]['router']=$registro->mac_router; 
            $grid[$i]['switch']=$registro->mac_switch; 
            $grid[$i]['roku']=$registro->mac_roku; 
            $grid[$i]['salida']=$registro->salida_switch; 
            $grid[$i]['status']=$registro->Cliente->estatus; 
            $i++;
        }
	
        My_Comun::grid2($registros,$grid);
    }

    public function imprimirordenesderetiroAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
                       
        if($nombre != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.nombre LIKE '%$nombre%' OR Ordendesretiro.Cliente.telefono LIKE '%$nombre%' OR Ordendesretiro.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.contrato = '$contrato') ";
        }        
        if($estatus >0)
        {
            if($estatus==2){ $estatus=0; }
            $filtro .= " AND Ordendesretiro.estatus =" .$estatus;
        }
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordendesretiro.Cliente.plaza_id in (".$plaza.")";
        }		
        if($sucursal != 0)
        {
            $filtro .= " AND Ordendesretiro.Cliente.sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Ordendesretiro", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE ÓRDENES DE RETIRO");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(35,60,30,30,25,25,25,25,25));
        $pdf->Row(array('FECHA','CLIENTE','TELEFONO','CELULAR','MAC ROUTER','MAC SWITCH','MAC ROKU','SALIDA SWITCH','ESTATUS'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {    
           $pdf->Row
           (
                array
                (
                    $registro->fecha_retiro,
                    $registro->Cliente->nombre,
                    $registro->Cliente->telefono,
                    $registro->Cliente->celular1,
                    $registro->mac_router,
                    $registro->mac_switch,
                    $registro->mac_roku,
                    $registro->salida_switch,
					$registro->Cliente->estatus
                ),0,1			
           );
        }
		
       $pdf->Output();	 
    }
	
    function exportarordenesderetiroAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordeninstalacion.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus');
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
                       
        if($nombre != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.nombre LIKE '%$nombre%' OR Ordendesretiro.Cliente.telefono LIKE '%$nombre%' OR Ordendesretiro.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.contrato = '$contrato') ";
        }        
        if($estatus >0)
        {
            if($estatus==2){ $estatus=0; }
            $filtro .= " AND Ordendesretiro.estatus =" .$estatus;
        }
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordendesretiro.Cliente.plaza_id in (".$plaza.")";
        }		
        if($sucursal != 0)
        {
            $filtro .= " AND Ordendesretiro.Cliente.sucursal_id = ".$sucursal;
        }		
		
        $registros=  My_Comun::obtenerFiltro("Ordendesretiro", $filtro);

        ini_set("memory_limit", "1024M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
				 "A$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 12
                        ),		
                "B$i" => array(
                        "name" => 'FECHA',
                        "width" => 19
                        ),
                "C$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 35
                        ),
				"D$i" => array(
                        "name" => 'DIRECCIÓN',
                        "width" => 35
                        ),
						
				"E$i" => array(
                        "name" => 'TELEFONO',
                        "width" => 20
                        ),
				"F$i" => array(
                        "name" => 'CELULAR',
                        "width" => 15
                        ),
				"G$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 19
                        ),
				"H$i" => array(
                        "name" => 'ETIQUETA',
                        "width" => 11
                        ),
				"I$i" => array(
                        "name" => 'NODO',
                        "width" => 8
                        ),
				"J$i" => array(
                        "name" => 'POSTE',
                        "width" => 9
                        ),
                "K$i" => array(
                        "name" => 'MAC ROUTER',
                        "width" => 25
                        ),
                "L$i" => array(
                        "name" => 'MAC SWITCH',
                        "width" => 25
                        ),
                "M$i" => array(
                        "name" => 'MAC ROKU',
                        "width" => 25
                        ),
                "N$i" => array(
                        "name" => 'SALIDA SWITCH',
                        "width" => 25
                        )											
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(				
		    		"A$i" =>$registro->Cliente->contrato,
					"B$i" =>$registro->fecha_retiro,
                    "C$i" =>$registro->Cliente->nombre,
					"D$i" =>$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id),
                    "E$i" =>$registro->Cliente->telefono,
                    "F$i" =>$registro->Cliente->celular1,
                    "G$i" =>$registro->Cliente->estatus,
					"H$i" =>$registro->Cliente->etiqueta,
					"I$i" =>$registro->Cliente->nodo,
					"J$i" =>$registro->Cliente->poste,
                    "K$i" =>$registro->mac_router,
                    "L$i" =>$registro->mac_switch,
                    "M$i" =>$registro->mac_roku,
                    "N$i" =>$registro->salida_switch
                );
        }
				
        $objPHPExcel->createExcel('Ordendesretiro', $columns_name, $data, 10, array('rango'=>'A4:N4','size'=>14,'texto'=>'ÓRDENES DE RETIRO'));
    } 
   
    public function recuperacionesAction()
    {
	$this->view->headScript()->appendFile('/js/transacciones/reportes/recuperaciones.js');
        ### Obtenemos las plazas para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
	
    public function gridrecuperacionesAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        
 
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordendesretiro.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus'); 
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
         
        if($nombre != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.nombre LIKE '%$nombre%' OR Ordendesretiro.Cliente.telefono LIKE '%$nombre%' OR Ordendesretiro.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.contrato = '$contrato') ";
        }        
        if($estatus != 0)
        {
            if($estatus ==2){ $estatus=0; }
            $filtro .= " AND Ordendesretiro.estatus =" .$estatus;
        }
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordendesretiro.Cliente.plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND Ordendesretiro.Cliente.sucursal_id = ".$sucursal;
        }   
	
	//### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Ordendesretiro",$filtro);      
        
	$grid=array();
	$i=0;
		
	foreach($registros['registros'] as $registro)
	{
	    $grid[$i]['fecha']=$registro->fecha_retiro;  
            $grid[$i]['nombre']=$registro->Cliente->nombre; 
            $grid[$i]['telefono']=$registro->Cliente->telefono; 
            $grid[$i]['celular']=$registro->Cliente->celular1; 
            $grid[$i]['status']=$registro->Cliente->estatus; 
            $i++;
        }
	
        My_Comun::grid2($registros,$grid);
    }

    public function imprimirrecuperacionesAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        
 
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordendesretiro.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus'); 
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
         
        if($nombre != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.nombre LIKE '%$nombre%' OR Ordendesretiro.Cliente.telefono LIKE '%$nombre%' OR Ordendesretiro.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.contrato = '$contrato') ";
        }        
        if($estatus != 0)
        {
            if($estatus ==2){ $estatus=0; }
            $filtro .= " AND Ordendesretiro.estatus =" .$estatus;
        }
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordendesretiro.Cliente.plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND Ordendesretiro.Cliente.sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Ordendesretiro", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE RECUPERACIONES");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(35,60,30,30,25));
        $pdf->Row(array('FECHA','CLIENTE','TELEFONO','CELULAR','ESTATUS'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {    
           $pdf->Row
           (
                array
                (
                    $registro->fecha_retiro,
                    $registro->Cliente->nombre,
                    $registro->Cliente->telefono,
                    $registro->Cliente->celular1,
                    $registro->Cliente->estatus
                ),0,1			
           );
        }
		
       $pdf->Output();	 
    }
	
    function exportarrecuperacionesAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        
 
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" Ordendesretiro.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $contrato = $this->_getParam('contrato');
        $estatus = $this->_getParam('estatus'); 
        $plaza = $this->_getParam('plaza');	
        $sucursal = $this->_getParam('sucursal');
         
        if($nombre != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.nombre LIKE '%$nombre%' OR Ordendesretiro.Cliente.telefono LIKE '%$nombre%' OR Ordendesretiro.Cliente.calle LIKE '%$nombre%')";
        }
        if($contrato != '')
        {
           $filtro .= " AND (Ordendesretiro.Cliente.contrato = '$contrato') ";
        }        
        if($estatus != 0)
        {
            if($estatus ==2){ $estatus=0; }
            $filtro .= " AND Ordendesretiro.estatus =" .$estatus;
        }
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND Ordendesretiro.Cliente.plaza_id in (".$plaza.")";
        }
        if($sucursal != 0)
        {
            $filtro .= " AND Ordendesretiro.Cliente.sucursal_id = ".$sucursal;
        }
        
        $registros=  My_Comun::obtenerFiltro("Ordendesretiro", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'TELEFONO',
                        "width" => 20
                        ),
				"D$i" => array(
                        "name" => 'CELULAR',
                        "width" => 20
                        ),
				"E$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 20
                        )										
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(				
		    		"A$i" =>$registro->fecha_retiro,
                    "B$i" =>$registro->Cliente->nombre,
                    "C$i" =>$registro->Cliente->telefono,
                    "D$i" =>$registro->Cliente->celular1,
                    "E$i" =>$registro->Cliente->estatus
                );
        }
				
        $objPHPExcel->createExcel('Ordendesretiro', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'RECUPERACIONES'));
    } 

    public function llamadasdecobranzaAction()
    {
        $this->view->headScript()->appendFile('/js/transacciones/reportes/llamadasdecobranza.js');
        $this->view->plazas=Plaza::obtenerPlazas();
    }


    /*************** SMS **************/
    public function enviosmsAction()
    {
        $this->view->headScript()->appendFile('/js/transacciones/reportes/enviosms.js');
        $this->view->plazas = Plaza::obtenerPlazas();
        $this->view->notificaciones = Notificacion::obtenerNotificaciones();
        $this->view->actualizaciones = Sms::tieneActualizaciones();
    }

    public function actualizaenviosmsAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE); 

        Sms::actualiza();

        header('Location: /transacciones/reportes/enviosms');
    }
    
    public function gridenviosmsAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estatus = $this->_getParam('estatus'); 
        $plaza = $this->_getParam('plaza');
        $notificacion = $this->_getParam('notificacion');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');

        if($estatus==null || $estatus==NULL || $estatus == '') $estatus=0;
 
        if(Usuario::tipo()!=0 && !is_null(Usuario::plaza()))
            $plaza = Usuario::plaza();
         
        if($nombre != '')
        {
           $filtro .= " AND (Sms.Cliente.nombre LIKE '%$nombre%' OR Sms.celular LIKE '%$nombre%')";
        }
        if($plaza > 0)
        {
            $filtro .= " AND Sms.Cliente.plaza_id in (".$plaza.")";
        }
        if($notificacion > 0)
        {
            $filtro .= " AND Sms.notificacion_id=".$notificacion;
        }
        if($desde != '')
        {
            $filtro .= " AND Sms.created_at >= '".$desde."'";
        }
        if($hasta != '')
        {
            $filtro .= " AND Sms.created_at <= '".$hasta."'";
        }
        if($estatus > 0)
        {
            switch ($estatus) 
            {
                case 1:
                    $filtro .= " AND Sms.estatus_envio='nuevo'"; break;
                case 2:
                    $filtro .= " AND Sms.estatus_envio='enviando'"; break;
                case 3:
                    $filtro .= " AND Sms.estatus_envio='enviado'"; break;
                case 4:
                    $filtro .= " AND Sms.estatus_envio='noEnviado'"; break;
                case 5:
                    $filtro .= " AND Sms.estatus_envio='blackList'"; break;
                case 6:
                    $filtro .= " AND Sms.estatus_envio='CI'"; break;
                case 7:
                    $filtro .= " AND Sms.estatus_envio='errorEnRed'"; break;
                case 8:
                    $filtro .= " AND Sms.estatus_envio='WS'"; break;
                case 9:
                    $filtro .= " AND Sms.estatus_envio='FD'"; break;
                default:
                    
                    break;
            }
        }
        
        //### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::registrosGrid("Sms",$filtro);    
                    
        $grid=array();
        $i=0;

        foreach($registros['registros'] as $registro)
        {
            $grid[$i]['fecha'] =  $registro->created_at;
            $grid[$i]['nombre'] = $registro->Cliente->nombre;
            $grid[$i]['celular'] = $registro->celular;

            switch ($registro->estatus_envio) 
            {
                case 'nuevo':
                    $grid[$i]['status'] = 'Pendiente'; break;
                case 'enviando':
                    $grid[$i]['status'] = 'Enviando'; break;
                case 'enviado':
                    $grid[$i]['status'] = 'Enviado'; break;
                case 'noEnviado':
                    $grid[$i]['status'] = 'Destinatario invalido'; break;
                case 'blackList':
                    $grid[$i]['status'] = 'Lista negra'; break;
                case 'CI':
                    $grid[$i]['status'] = 'Créditos insuficientes'; break;
                case 'errorEnRed':
                    $grid[$i]['status'] = 'Error en red'; break;
                case 'WS':
                    $grid[$i]['status'] = 'Error en servicio'; break;
                case 'FD':
                    $grid[$i]['status'] = 'Falta de datos para el envió'; break;
                default:
                    
                    break;
            }
            
            $i++;
        }
    
        My_Comun::grid2($registros,$grid);
    }


    public function imprimirenviosmsAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estatus = $this->_getParam('estatus'); 
        $plaza = $this->_getParam('plaza');
        $notificacion = $this->_getParam('notificacion');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');

        if($estatus==null || $estatus==NULL || $estatus == '') $estatus=0;
 
        if(Usuario::tipo()!=0 && !is_null(Usuario::plaza()))
            $plaza = Usuario::plaza();
         
        if($nombre != '')
        {
           $filtro .= " AND (Sms.Cliente.nombre LIKE '%$nombre%' OR Sms.celular LIKE '%$nombre%')";
        }
        if($plaza > 0)
        {
            $filtro .= " AND Sms.Cliente.plaza_id in (".$plaza.")";
        }
        if($notificacion > 0)
        {
            $filtro .= " AND Sms.notificacion_id=".$notificacion;
        }
        if($desde != '')
        {
            $filtro .= " AND Sms.created_at >= '".$desde."'";
        }
        if($hasta != '')
        {
            $filtro .= " AND Sms.created_at <= '".$hasta."'";
        }
        if($estatus > 0)
        {
            switch ($estatus) 
            {
                case 1:
                    $filtro .= " AND Sms.estatus_envio='nuevo'"; break;
                case 2:
                    $filtro .= " AND Sms.estatus_envio='enviando'"; break;
                case 3:
                    $filtro .= " AND Sms.estatus_envio='enviado'"; break;
                case 4:
                    $filtro .= " AND Sms.estatus_envio='noEnviado'"; break;
                case 5:
                    $filtro .= " AND Sms.estatus_envio='blackList'"; break;
                case 6:
                    $filtro .= " AND Sms.estatus_envio='CI'"; break;
                case 7:
                    $filtro .= " AND Sms.estatus_envio='errorEnRed'"; break;
                case 8:
                    $filtro .= " AND Sms.estatus_envio='WS'"; break;
                case 9:
                    $filtro .= " AND Sms.estatus_envio='FD'"; break;
                default:
                    
                    break;
            }
        }

        //### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::obtenerFiltro("Sms",$filtro);    
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE ENVIOS DE SMS");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(50, 60, 30, 40));
        $pdf->Row(array('FECHA','CLIENTE','CELULAR','ESTATUS DE ENVIO'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {   
            $envio;
            switch ($registro->estatus_envio) 
            {
                case 'nuevo':
                    $envio = 'Pendiente'; break;
                case 'enviando':
                    $envio = 'Enviando'; break;
                case 'enviado':
                    $envio = 'Enviado'; break;
                case 'noEnviado':
                    $envio = 'Destinatario invalido'; break;
                case 'blackList':
                    $envio = 'Lista negra'; break;
                case 'CI':
                    $envio = 'Créditos insuficientes'; break;
                case 'errorEnRed':
                    $envio = 'Error en red'; break;
                case 'WS':
                    $envio = 'Error en servicio'; break;
                case 'FD':
                    $envio = 'Falta de datos para el envió'; break;
                default:
                    $envio = "N/D";
                    break;
            }
            $pdf->Row
            (
                array
                (
                    $registro->created_at,
                    $registro->Cliente->nombre,
                    $registro->celular,
                    $envio,
                ),0,1           
            );
        }
        
       $pdf->Output();   
    }

    function exportarenviosmsAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estatus = $this->_getParam('estatus'); 
        $plaza = $this->_getParam('plaza');
        $notificacion = $this->_getParam('notificacion');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');

        if($estatus==null || $estatus==NULL || $estatus == '') $estatus=0;
 
        if(Usuario::tipo()!=0 && !is_null(Usuario::plaza()))
            $plaza = Usuario::plaza();
         
        if($nombre != '')
        {
           $filtro .= " AND (Sms.Cliente.nombre LIKE '%$nombre%' OR Sms.celular LIKE '%$nombre%')";
        }
        if($plaza > 0)
        {
            $filtro .= " AND Sms.Cliente.plaza_id in (".$plaza.")";
        }
        if($notificacion > 0)
        {
            $filtro .= " AND Sms.notificacion_id=".$notificacion;
        }
        if($desde != '')
        {
            $filtro .= " AND Sms.created_at >= '".$desde."'";
        }
        if($hasta != '')
        {
            $filtro .= " AND Sms.created_at <= '".$hasta."'";
        }
        if($estatus > 0)
        {
            switch ($estatus) 
            {
                case 1:
                    $filtro .= " AND Sms.estatus_envio='nuevo'"; break;
                case 2:
                    $filtro .= " AND Sms.estatus_envio='enviando'"; break;
                case 3:
                    $filtro .= " AND Sms.estatus_envio='enviado'"; break;
                case 4:
                    $filtro .= " AND Sms.estatus_envio='noEnviado'"; break;
                case 5:
                    $filtro .= " AND Sms.estatus_envio='blackList'"; break;
                case 6:
                    $filtro .= " AND Sms.estatus_envio='CI'"; break;
                case 7:
                    $filtro .= " AND Sms.estatus_envio='errorEnRed'"; break;
                case 8:
                    $filtro .= " AND Sms.estatus_envio='WS'"; break;
                case 9:
                    $filtro .= " AND Sms.estatus_envio='FD'"; break;
                default:
                    
                    break;
            }
        }

        //### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::obtenerFiltro("Sms",$filtro); 

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();     
        
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA',
                        "width" => 30
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 30
                        ),
                "C$i" => array(
                        "name" => 'CELULAR',
                        "width" => 20
                        ),
                "D$i" => array(
                        "name" => 'ESTATUS ENVIO SMS',
                        "width" => 30
                        )                                     
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {   $envio;
            switch ($registro->estatus_envio) 
            {
                case 'nuevo':
                    $envio = 'Pendiente'; break;
                case 'enviando':
                    $envio = 'Enviando'; break;
                case 'enviado':
                    $envio = 'Enviado'; break;
                case 'noEnviado':
                    $envio = 'Destinatario invalido'; break;
                case 'blackList':
                    $envio = 'Lista negra'; break;
                case 'CI':
                    $envio = 'Créditos insuficientes'; break;
                case 'errorEnRed':
                    $envio = 'Error en red'; break;
                case 'WS':
                    $envio = 'Error en servicio'; break;
                case 'FD':
                    $envio = 'Falta de datos para el envió'; break;
                default:
                    $envio = "N/D";
                    break;
            }
            
            $i++;
            $data[] = array(                
                "A$i" =>$registro->created_at,
                "B$i" =>$registro->Cliente->nombre,
                "C$i" =>$registro->celular,
                "D$i" =>$envio
            );
        }
                
        $objPHPExcel->createExcel('Sms', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'ENVIO DE SMS'));
    }

    /*************** SMS INVALIDOS **************/
    public function smsinvalidoAction()
    {
        $this->view->headScript()->appendFile('/js/transacciones/reportes/smsinvalido.js');
        $this->view->plazas = Plaza::obtenerPlazas();
        //$this->view->notificaciones = Notificacion::obtenerNotificaciones();
        $this->view->actualizaciones = Sms::tieneActualizaciones();
    }

    public function actualizaenviosms2Action()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE); 

        Sms::actualiza();

        header('Location: /transacciones/reportes/smsinvalido');
    }
    
    public function gridsmsinvalidoAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre'); 
        $plaza = $this->_getParam('plaza');
        /*$notificacion = $this->_getParam('notificacion');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');*/
 
        if(Usuario::tipo()!=0 && !is_null(Usuario::plaza()))
            $plaza = Usuario::plaza();
        
        //$filtro .= " AND s.estatus_envio='enviado'";

        if($nombre != '')
        {
           $filtro .= " AND (c.nombre LIKE '%$nombre%' OR s.celular LIKE '%$nombre%')";
        }
        if($plaza > 0)
        {
            $filtro .= " AND c.plaza_id in (".$plaza.")";
        }
        /*if($notificacion > 0)
        {
            $filtro .= " AND o.notificacion_id=".$notificacion;
        }
        if($desde != '')
        {
            $filtro .= " AND o.created_at >= '".$desde."'";
        }
        if($hasta != '')
        {
            $filtro .= " AND o.created_at <= '".$hasta."'";
        }*/
        
        //### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::registrosGrid3("Sms", $filtro, array("s.Cliente c"), "c.id", null, "s.celular, c.nombre, s.created_at");
                    
        $grid=array();
        $i=0;

        foreach($registros['registros'] as $registro)
        {
            $grid[$i]['fecha'] =  $registro->created_at;
            $grid[$i]['nombre'] = $registro->Cliente->nombre;
            $grid[$i]['celular'] = $registro->celular;
            $grid[$i]['status'] = "Celular invalido";
            $i++;
        }
    
        My_Comun::grid2($registros,$grid);
    }


    public function imprimirsmsinvalidoAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        /*$notificacion = $this->_getParam('notificacion');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');*/
 
        if(Usuario::tipo()!=0 && !is_null(Usuario::plaza()))
            $plaza = Usuario::plaza();
         
        if($nombre != '')
        {
           $filtro .= " AND (c.nombre LIKE '%$nombre%' OR s.celular LIKE '%$nombre%')";
        }
        if($plaza > 0)
        {
            $filtro .= " AND c.plaza_id in (".$plaza.")";
        }
        /*if($notificacion > 0)
        {
            $filtro .= " AND Sms.notificacion_id=".$notificacion;
        }
        if($desde != '')
        {
            $filtro .= " AND Sms.created_at >= '".$desde."'";
        }
        if($hasta != '')
        {
            $filtro .= " AND Sms.created_at <= '".$hasta."'";
        }*/

        //### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::obtenerFiltro("Sms", $filtro, "o.id DESC", array("o.Cliente c"), "c.id", "o.celular, c.nombre, o.created_at");    
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE ENVIOS DE SMS");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(50, 60, 30, 40));
        $pdf->Row(array('FECHA','CLIENTE','CELULAR','ESTATUS DE ENVIO'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {   
            $pdf->Row
            (
                array
                (
                    $registro->created_at,
                    $registro->Cliente->nombre,
                    $registro->celular,
                    "Celular invalido",
                ),0,1           
            );
        }
        
       $pdf->Output();   
    }

    function exportarsmsinvalidoAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1";        

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
        /*$notificacion = $this->_getParam('notificacion');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');*/
 
        if(Usuario::tipo()!=0 && !is_null(Usuario::plaza()))
            $plaza = Usuario::plaza();
         
        if($nombre != '')
        {
           $filtro .= " AND (c.nombre LIKE '%$nombre%' OR s.celular LIKE '%$nombre%')";
        }
        if($plaza > 0)
        {
            $filtro .= " AND c.plaza_id in (".$plaza.")";
        }
        /*if($notificacion > 0)
        {
            $filtro .= " AND Sms.notificacion_id=".$notificacion;
        }
        if($desde != '')
        {
            $filtro .= " AND Sms.created_at >= '".$desde."'";
        }
        if($hasta != '')
        {
            $filtro .= " AND Sms.created_at <= '".$hasta."'";
        }*/

        //### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::obtenerFiltro("Sms", $filtro, "o.id DESC", array("o.Cliente c"), "c.id", "o.celular, c.nombre, o.created_at");     

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();     
        
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA',
                        "width" => 30
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 30
                        ),
                "C$i" => array(
                        "name" => 'CELULAR',
                        "width" => 20
                        ),
                "D$i" => array(
                        "name" => 'ESTATUS ENVIO SMS',
                        "width" => 30
                        )                                     
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {   
            $i++;
            $data[] = array(                
                "A$i" =>$registro->created_at,
                "B$i" =>$registro->Cliente->nombre,
                "C$i" =>$registro->celular,
                "D$i" =>"Celular invalido"
            );
        }
                
        $objPHPExcel->createExcel('Celulares', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'ENVIO DE SMS'));
    }
		
    /*****************POR TERMINAR*******************/
    public function calcularAction()
    {	
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);    
		
     	$fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
		$plaza = $this->_getParam('plaza');
		$sucursal = $this->_getParam('sucursal');
        $tipo = $this->_getParam('tipo');
        $proviene = $this->_getParam('proviene');
        
       ### Establecemos el filtro por default	 
       $filtroG = " gastos.cancelado =0 ";
       $filtroC = " cobranza.cancelado =0 and cobranza.pagada=1";

       if($proviene != 0 && $proviene != null)
        {
            if($proviene == 2)
            {
                $filtroC .= " AND 1=2";
            }
            else
            {
                $filtroG .= " AND 1=2";
                $filtroC .= " AND cobranza.origen_pago = ". $proviene;
            }
        }
        	
	if($fecha=='' || $hasta==''){
            $filtroG .= " AND gastos.created_at>='".date('Y-m-d 00:00:00')."' AND gastos.created_at<='".date('Y-m-d 23:59:59')."'"; 
            $filtroC .= " AND cobranza.created_at>='".date('Y-m-d 00:00:00')."' AND cobranza.created_at<='".date('Y-m-d 23:59:59')."'"; 
        }
                
	if($fecha != '')
        {
           $filtroG .= " AND gastos.created_at >= '$fecha 00:00:00'";
           $filtroC .= " AND cobranza.created_at >= '$fecha 00:00:00'";
        }
		
	if($hasta != '')
        {
           $filtroG .= " AND gastos.created_at <= '$hasta 23:59:59'";
           $filtroC .= " AND cobranza.created_at <= '$hasta 23:59:59'";
        }
        
        if(Usuario::tipo()==1)
        {
            if(!is_null(Usuario::plaza()))
            {
                $filtroG.=" AND plaza_id=".Usuario::plaza()." AND usuario_id=".Usuario::id();
                $filtroC.=" AND plaza_id=".Usuario::plaza()." AND usuario_id=".Usuario::id();
            }
            else
            {
                $filtroG.=" AND plaza_id=".Usuario::plaza();
                $filtroC.=" AND plaza_id=".Usuario::plaza();
            }
        }
        else
        {
            $tmp_plaza = "";
            $tmp_plaza_uno = Permiso::obtienePlazas(Usuario::id());
            foreach ($tmp_plaza_uno as $permiso)
            {
                $tmp_plaza .= $permiso->plaza_id.",";
            }

            if($plaza != '' && $plaza!='undefined')
            {
                $plaza = str_replace('_', ',', $plaza);
                //$plaza = $tmp_plaza.$plaza;
                $filtroG .= " AND plaza_id in (".$plaza.")";
                $filtroC .= " AND plaza_id in (".$plaza.")";
            }
            else
            {
                if($tmp_plaza != "" && Usuario::tipo() != 0)
                {
                    $plaza = trim($tmp_plaza, ',');
                    $filtroG .= " AND plaza_id in (".$plaza.")";
                    $filtroC .= " AND plaza_id in (".$plaza.")";
                }
            }
        }
        
        if($sucursal != 0)
        {
            $gastos = " INNER JOIN plaza ON plaza.id=gastos.plaza_id AND sucursal_id = ".$sucursal;
            $cobranza = " INNER JOIN plaza ON plaza.id=cobranza.plaza_id AND sucursal_id = ".$sucursal;
        }
				
	$query = "SELECT fecha, monto * -1 as monto, plaza_id, gastos.id FROM gastos ".$gastos."  WHERE(".$filtroG.") UNION SELECT cobranza.created_at, total, plaza_id, cobranza.id FROM cobranza ".$cobranza." WHERE (".$filtroC.") ORDER BY fecha DESC";
	 	
	//recuperamos el singleton de la conexión
	$con = Doctrine_Manager::getInstance()->connection();	 
	//ejecutamos la consulta
	$st = $con->execute($query);	 	
	//recuperamos los resultados en un arreglo
	$registros = $st->fetchAll();			
	//print_r($registros); exit;		
		
	### INICIO Hacemos la separacion para el filtro por tipo fuera de la consulta para evitar modificar lo que ya se tiene
        if($tipo != 0)
        {
            $contador_array = 0;
            $eliminar = array();
            foreach ($registros as $registro)
            {
                $internet = ClienteServicio::obtenerPrimerServicioPorFecha($registro['id'])->Servicio->incluye_internet;
                $cambio_en_internet = false;
                $servicio_en_internet = false;
                $bandera_es_cambio_servicio = false;
                $bandera_es_servicio_con_internet = false;

                $cobros=  My_Comun::obtenerFiltro("CobranzaDetalle", " cobranza_id =" .$registro['id']);
                foreach($cobros as $cobro)
                {
                    if($cobro->servicio_id != NULL && $cobro->servicio_id != '')
                    {
                        $bandera_es_servicio_con_internet = true;
                        $query = Doctrine_Query::create()->from('Cobranza')->where('id = '.$registro['id'])->execute()->getFirst();
                        $qServicio = Servicio::obtenerServicios("id=".$cobro->servicio_id)->getFirst();
                        if($qServicio->incluye_internet == 1) $servicio_en_internet = true;
                    }
                    
                    if($cobro->otro_concepto == "Cambio de servicio")
                    {
                        $bandera_es_cambio_servicio = true;
                        $query = Doctrine_Query::create()->from('Cobranza')->where('id = '.$registro['id'])->execute()->getFirst();
                        $cambio = Doctrine_Query::create()->from('Ordencambioservicio')->where("created_at >= '".$query->created_at."'")->orderBy("id ASC")->execute()->getFirst();
                        $servicio_nuevo = Servicio::obtenerServicios("id=".$cambio->servicio_nuevo_id)->getFirst();
                        if($servicio_nuevo->incluye_internet == 1) $cambio_en_internet = true;
                    }
                }
                
                if($tipo == 1)
                {
                    if($bandera_es_cambio_servicio == true)
                    {
                        if($cambio_en_internet == true) $eliminar[] = $contador_array;
                    }
                    else if($bandera_es_servicio_con_internet == true)
                    {
                        if($servicio_en_internet == true) $eliminar[] = $contador_array;
                    }
                    else if($internet == 1)
                        $eliminar[] = $contador_array;
                }
                else if($tipo == 2)
                {   
                    if($bandera_es_cambio_servicio == true)
                    {
                        if($cambio_en_internet == false) $eliminar[] = $contador_array;
                    }
                    else if($bandera_es_servicio_con_internet == true)
                    {
                        if($servicio_en_internet == false) $eliminar[] = $contador_array;
                    }
                    else if($internet == NULL || $internet == 0)
                        $eliminar[] = $contador_array;
                }

                $contador_array++;
            }

            foreach ($eliminar as $registro)
                unset($registros[$registro]);
        }
        ### FIN De separacion

    		
    	$total =0;
		
		
	   foreach($registros as $registro)
	   {		
            $total += $registro['monto'];
        }
	
         echo "$".number_format($total,2,".",",");
    }	
				
    public function cortedecajaAction()
    {
	   $this->view->headScript()->appendFile('/js/transacciones/reportes/cortedecaja.js');
        ### Obtenemos las plazas para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
        $this->view->plaza=Usuario::plaza();
    }
	
    public function gridcortedecajaAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE); 
    
        $fecha = $this->_getParam('fecha');
        $hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');          
		$tipo = $this->_getParam('tipo');
        $proviene = $this->_getParam('proviene'); 
        
        ### Establecemos el filtro por default    
        $filtroG = " gastos.cancelado =0 ";
        $filtroC = " cobranza.cancelado =0 AND cobranza.pagada =1 ";

        /*$filtroG = " gastos.cancelado =0 AND p.id!=25 and p.id!=19";
        $filtroC = " cobranza.cancelado =0  AND p.id!=25 and p.id!=19";*/

       if($proviene != 0 && $proviene != null)
        {
            if($proviene == 2)
            {
                $filtroC .= " AND 1=2";
            }
            else
            {
                $filtroG .= " AND 1=2";
                $filtroC .= " AND cobranza.origen_pago = ". $proviene;
            }
        }
            
        if($fecha=='' || $hasta==''){
            $filtroG .= " AND gastos.created_at>='".date('Y-m-d 00:00:00')."' AND gastos.created_at<='".date('Y-m-d 23:59:59')."'"; 
            $filtroC .= " AND cobranza.created_at>='".date('Y-m-d 00:00:00')."' AND cobranza.created_at<='".date('Y-m-d 23:59:59')."'"; 
        }
                
       if($fecha != '')
        {
           $filtroG .= " AND gastos.created_at >= '$fecha 00:00:00'";
           $filtroC .= " AND cobranza.created_at >= '$fecha 00:00:00'";
        }
        
       if($hasta != '')
        {
           $filtroG .= " AND gastos.created_at <= '$hasta 23:59:59'";
           $filtroC .= " AND cobranza.created_at <= '$hasta 23:59:59'";
        }
        
       if(Usuario::tipo()==1)
        {
            if(!is_null(Usuario::plaza()))
            {
                $filtroG.=" AND p.id=".Usuario::plaza()." AND usuario_id=".Usuario::id();
                $filtroC.=" AND p.id=".Usuario::plaza()." AND usuario_id=".Usuario::id();
            }
            else
            {
                $filtroG.=" AND p.id=".Usuario::plaza();
                $filtroC.=" AND p.id=".Usuario::plaza();
            }
        }
        else
        {
            $tmp_plaza = "";
            $tmp_plaza_uno = Permiso::obtienePlazas(Usuario::id());
            foreach ($tmp_plaza_uno as $permiso)
            {
                $tmp_plaza .= $permiso->plaza_id.",";
            }

            if($plaza >0)
            {
                $plaza = str_replace('_', ',', $plaza);
                //$plaza = $tmp_plaza.$plaza;
                $filtroG .= " AND p.id in (".$plaza.")";
                $filtroC .= " AND p.id in (".$plaza.")";
            }
            else
            {
                if($tmp_plaza != "" && Usuario::tipo() != 0)
                {
                    $plaza = trim($tmp_plaza, ',');
                    $filtroG .= " AND p.id in (".$plaza.")";
                    $filtroC .= " AND p.id in (".$plaza.")";
                }
            }
        }
        
        if($sucursal != 0)
        {
            $gastos = " AND sucursal_id = ".$sucursal;
            $cobranza = "  AND u.sucursal_id = ".$sucursal;
        }
        
    	$query = "
                    SELECT fecha as fecha, 'N/A' as contrato, 'N/A' AS cliente_id, monto * -1 as monto, plaza_id, gastos.id, p.nombre, 'N/A' AS origen, gastos.concepto as concepto
                    FROM gastos INNER JOIN plaza p ON p.id=gastos.plaza_id ".$gastos."  
                    WHERE(".$filtroG.") 
                    UNION 
                        SELECT cobranza.created_at as fecha, cliente.contrato as contrato, cliente.id as cliente_id, total, cobranza.plaza_id, cobranza.id, p.nombre, cobranza.origen_pago AS origen, 'N/A' as concepto
                        FROM cobranza 
                        INNER JOIN cliente ON cliente.id=cobranza.cliente_id
                        INNER JOIN plaza p ON p.id = cobranza.plaza_id";

        if($sucursal != 0)
        {
            $query.=" INNER JOIN usuario u ON cobranza.usuario_id = u.id";
        }
                        
        $query.=" WHERE (".$filtroC.") ORDER BY fecha DESC";
        
        //echo $query; exit;

        //recuperamos el singleton de la conexión
        $con = Doctrine_Manager::getInstance()->connection();    
        //ejecutamos la consulta
        $st = $con->execute($query);        
        //recuperamos los resultados en un arreglo
        $registros = $st->fetchAll();           
        //print_r($registros); exit;        
            
        $grid=array();
        $i=0;       
        $total =0;

        ### INICIO Hacemos la separacion para el filtro por tipo fuera de la consulta para evitar modificar lo que ya se tiene
        if($tipo != 0)
        {
            $contador_array = 0;
            $eliminar = array();
            foreach ($registros as $registro)
            {
                $internet = ClienteServicio::obtenerPrimerServicioPorFecha($registro['id'])->Servicio->incluye_internet;
                $cambio_en_internet = false;
                $servicio_en_internet = false;
                $bandera_es_cambio_servicio = false;
                $bandera_es_servicio_con_internet = false;

                $cobros=  My_Comun::obtenerFiltro("CobranzaDetalle", " cobranza_id =" .$registro['id']);
                foreach($cobros as $cobro)
                {
                    if($cobro->servicio_id != NULL && $cobro->servicio_id != '')
                    {
                        $bandera_es_servicio_con_internet = true;
                        $query = Doctrine_Query::create()->from('Cobranza')->where('id = '.$registro['id'])->execute()->getFirst();
                        $qServicio = Servicio::obtenerServicios("id=".$cobro->servicio_id)->getFirst();
                        if($qServicio->incluye_internet == 1) $servicio_en_internet = true;
                    }
                    
                    if($cobro->otro_concepto == "Cambio de servicio")
                    {
                        $bandera_es_cambio_servicio = true;
                        $query = Doctrine_Query::create()->from('Cobranza')->where('id = '.$registro['id'])->execute()->getFirst();
                        $cambio = Doctrine_Query::create()->from('Ordencambioservicio')->where("created_at >= '".$query->created_at."'")->orderBy("id ASC")->execute()->getFirst();
                        $servicio_nuevo = Servicio::obtenerServicios("id=".$cambio->servicio_nuevo_id)->getFirst();
                        if($servicio_nuevo->incluye_internet == 1) $cambio_en_internet = true;
                    }
                }
                
                if($tipo == 1)
                {
                    if($bandera_es_cambio_servicio == true)
                    {
                        if($cambio_en_internet == true) $eliminar[] = $contador_array;
                    }
                    else if($bandera_es_servicio_con_internet == true)
                    {
                        if($servicio_en_internet == true) $eliminar[] = $contador_array;
                    }
                    else if($internet == 1)
                        $eliminar[] = $contador_array;
                }
                else if($tipo == 2)
                {   
                    if($bandera_es_cambio_servicio == true)
                    {
                        if($cambio_en_internet == false) $eliminar[] = $contador_array;
                    }
                    else if($bandera_es_servicio_con_internet == true)
                    {
                        if($servicio_en_internet == false) $eliminar[] = $contador_array;
                    }
                    else if($internet == NULL || $internet == 0)
                        $eliminar[] = $contador_array;
                }

                $contador_array++;
            }

            foreach ($eliminar as $registro)
                unset($registros[$registro]);
        }
        ### FIN De separacion
        
        ### Seguimos con el proceso que ya se tenia antes:
	    foreach($registros as $registro)
	    {       
            $grid[$i]['fecha']=$registro['fecha'];    
            $total += $registro['monto'];
			$descripcion = "";
            $des_servicio = "";
            
            if($registro['monto'] >= 0)
            {
                ////////////////////
                $filtro1 = "  cobranza_id =" .$registro[id];
                $cobros=  My_Comun::obtenerFiltro("CobranzaDetalle", $filtro1);
                
                foreach($cobros as $cobro)
                {   
                    $saltar = false;       
                    if($cobro->servicio_id != null){
                        $descripcion .= "* Servicio: ".$cobro->Servicio->nombre."<br>";
                    }
                    
                    if($cobro->servicio_adicional_id != null){
                        $descripcion .= "* Servicio adicional : ".$cobro->ServicioAdicional->nombre."<br>";
                    }
                                                            
                    if($cobro->concepto_contratacion_id != null){
                        $descripcion .= "* Concepto de contratación : ".$cobro->ConceptoDeContratacion->nombre."<br>";
                    } 
                    
                    if($cobro->otro_concepto != ''){
                        if($cobro->otro_concepto == "Cambio de servicio")
                        {
                            //$query = Doctrine_Query::create()->from('Cobranza')->where('id = '.$cobro->cobranza_id)->execute()->getFirst();
                            $cambio = Doctrine_Query::create()->from('Ordencambioservicio')->where("created_at >= '".$registro['fecha']."' and cliente_id='".$registro['cliente_id']."'")->orderBy("id ASC")->execute()->getFirst();
                            $servicio_nuevo = Servicio::obtenerServicios("id=".$cambio->servicio_nuevo_id)->getFirst();
                            $saltar = true;
                            $des_servicio .= "* ".$servicio_nuevo->nombre."<br>";
                        }
                        
                        $descripcion .= "* Otro concepto : ".$cobro->otro_concepto."<br>";
                    }

                    if($cobro->servicio_id != NULL && $cobro->Servicio == 1)
                        $des_servicio .= "* ".$cobro->Servicio->nombre."<br>";
                    /*else if($cobro->concepto_contratacion_id != NULL)
                        $des_servicio .= "* ".ClienteServicio::obtenerPrimerServicio($cobro->Cobranza->Cliente->id)->Servicio->nombre."<br>";*/
                    else if($cobro->concepto_contratacion_id != NULL)
                        $des_servicio .= "* ".ClienteServicio::obtenerPrimerServicio($registro['cliente_id'])->id."<br>";
                    else if(!$saltar)
                        $des_servicio .= "* N/A"."<br>";

                    //$contrato = $cobro->Cobranza->Cliente->contrato;
                } 
            
                $grid[$i]['contrato'] = $registro['contrato'];
                $grid[$i]['plaza'] = $registro['nombre'];
                $grid[$i]['descripcion'] = $descripcion;
                $grid[$i]['servicio'] = $des_servicio;
                $grid[$i]['entrada'] = "<font color='#0000ff'>$".number_format($registro[monto],2,".",",")."</font>";
                $grid[$i]['salida'] = "<font color='#0000ff'>$0.00</font>";
            }
        
            if($registro['monto'] < 0)
			{         
        		$filtro1 = "  id =" .$registro[id];
                /*$cobros=  My_Comun::obtenerFiltro("Gasto", $filtro1);
                
                foreach($cobros as $cobro)
                {               
                    $descripcion = "* ".$cobro->concepto."<br>";  
                }*/
                
		        $grid[$i]['contrato']="N/A";
		        $grid[$i]['plaza']=$registro['nombre'];
		        $grid[$i]['descripcion']=$registro['concepto'];  
                $grid[$i]['servicio'] = $des_servicio;                    
		        $grid[$i]['entrada']="<font color='#ff0000'>$0.00</font>";              
                $grid[$i]['salida']="<font color='#ff0000'>$".number_format($registro[monto],2,".",",")."</font>";                  
            }
            
            if($total < 0){
                $grid[$i]['total']="<font color='#ff0000'>$".number_format($total,2,".",",")."</font>";
            }   
                        
            if($total >= 0){
                $grid[$i]['total']="<font color='#0000ff'>$".number_format($total,2,".",",")."</font>";
            }   

            if($registro['origen'] == 1)
                $grid[$i]['origen'] = 'Pago en caja';
            else if($registro['origen'] == 2)
                $grid[$i]['origen'] = 'Pago en linea';
            else
                $grid[$i]['origen'] = 'N/A';
                      
            $i++;
        }
        $total=count($registros);
        $registros=array('pagina'=>1,"total"=>$total);
        My_Comun::grid2($registros,$grid);
    }
	
    public function imprimircortedecajaAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
	    $fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
		$plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');          
        $tipo = $this->_getParam('tipo');
        $proviene = $this->_getParam('proviene');

	    ### Establecemos el filtro por default	 
	    $filtroG = " gastos.cancelado =0 ";
        $filtroC = " cobranza.cancelado =0 AND cobranza.pagada =1 ";

        if($proviene != 0 && $proviene != null)
        {
            if($proviene == 2)
            {
                $filtroC .= " AND 1=2";
            }
            else
            {
                $filtroG .= " AND 1=2";
                $filtroC .= " AND cobranza.origen_pago = ". $proviene;
            }
        }
        	
	if($fecha=='' || $hasta==''){
            $filtroG .= " AND gastos.created_at>='".date('Y-m-d 00:00:00')."' AND gastos.created_at<='".date('Y-m-d 23:59:59')."'"; 
            $filtroC .= " AND cobranza.created_at>='".date('Y-m-d 00:00:00')."' AND cobranza.created_at<='".date('Y-m-d 23:59:59')."'"; 
        }
                
	if($fecha != '')
        {
           $filtroG .= " AND gastos.created_at >= '$fecha 00:00:00'";
           $filtroC .= " AND cobranza.created_at >= '$fecha 00:00:00'";
        }
		
	if($hasta != '')
        {
           $filtroG .= " AND gastos.created_at <= '$hasta 23:59:59'";
           $filtroC .= " AND cobranza.created_at <= '$hasta 23:59:59'";
        }
        
        if(Usuario::tipo()==1)
        {
            if(!is_null(Usuario::plaza()))
            {
                $filtroG.=" AND plaza_id=".Usuario::plaza()." AND usuario_id=".Usuario::id();
                $filtroC.=" AND plaza_id=".Usuario::plaza()." AND usuario_id=".Usuario::id();
            }
            else
            {
                $filtroG.=" AND plaza_id=".Usuario::plaza();
                $filtroC.=" AND plaza_id=".Usuario::plaza();
            }
        }
        else
        {
            $tmp_plaza = "";
            $tmp_plaza_uno = Permiso::obtienePlazas(Usuario::id());
            foreach ($tmp_plaza_uno as $permiso)
            {
                $tmp_plaza .= $permiso->plaza_id.",";
            }

            if($plaza != '' && $plaza!='undefined')
            {
                $plaza = str_replace('_', ',', $plaza);
                //$plaza = $tmp_plaza.$plaza;
                $filtroG .= " AND plaza_id in (".$plaza.")";
                $filtroC .= " AND plaza_id in (".$plaza.")";
            }
            else
            {
                if($tmp_plaza != "" && Usuario::tipo() != 0)
                {
                    $plaza = trim($tmp_plaza, ',');
                    $filtroG .= " AND plaza_id in (".$plaza.")";
                    $filtroC .= " AND plaza_id in (".$plaza.")";
                }
            }
        }
        
        if($sucursal != 0)
        {
            $gastos = " AND sucursal_id = ".$sucursal;
            $cobranza = "  AND sucursal_id = ".$sucursal;
        }
        
        //echo  $filtroG; exit;     
        $query = "SELECT fecha, monto * -1 as monto, plaza_id, gastos.id, plaza.nombre, 'N/A' AS origen FROM gastos INNER JOIN plaza ON plaza.id=gastos.plaza_id ".$gastos."  WHERE(".$filtroG.") UNION SELECT cobranza.created_at, total, plaza_id, cobranza.id, plaza.nombre, cobranza.origen_pago AS origen FROM cobranza INNER JOIN plaza ON plaza.id=cobranza.plaza_id ".$cobranza." WHERE (".$filtroC.") ORDER BY fecha DESC";
	 		
		//recuperamos el singleton de la conexión
    	$con = Doctrine_Manager::getInstance()->connection();	 
    	//ejecutamos la consulta
    	$st = $con->execute($query);	 	
    	//recuperamos los resultados en un arreglo
    	$registros = $st->fetchAll();			
    	//print_r($registros); exit;

        ### INICIO Hacemos la separacion para el filtro por tipo fuera de la consulta para evitar modificar lo que ya se tiene
        if($tipo != 0)
        {
            $contador_array = 0;
            $eliminar = array();
            foreach ($registros as $registro)
            {
                $internet = ClienteServicio::obtenerPrimerServicioPorFecha($registro['id'])->Servicio->incluye_internet;
                $cambio_en_internet = false;
                $servicio_en_internet = false;
                $bandera_es_cambio_servicio = false;
                $bandera_es_servicio_con_internet = false;

                $cobros=  My_Comun::obtenerFiltro("CobranzaDetalle", " cobranza_id =" .$registro['id']);
                foreach($cobros as $cobro)
                {
                    if($cobro->servicio_id != NULL && $cobro->servicio_id != '')
                    {
                        $bandera_es_servicio_con_internet = true;
                        $query = Doctrine_Query::create()->from('Cobranza')->where('id = '.$registro['id'])->execute()->getFirst();
                        $qServicio = Servicio::obtenerServicios("id=".$cobro->servicio_id)->getFirst();
                        if($qServicio->incluye_internet == 1) $servicio_en_internet = true;
                    }
                    
                    if($cobro->otro_concepto == "Cambio de servicio")
                    {
                        $bandera_es_cambio_servicio = true;
                        $query = Doctrine_Query::create()->from('Cobranza')->where('id = '.$registro['id'])->execute()->getFirst();
                        $cambio = Doctrine_Query::create()->from('Ordencambioservicio')->where("created_at >= '".$query->created_at."'")->orderBy("id ASC")->execute()->getFirst();
                        $servicio_nuevo = Servicio::obtenerServicios("id=".$cambio->servicio_nuevo_id)->getFirst();
                        if($servicio_nuevo->incluye_internet == 1) $cambio_en_internet = true;
                    }
                }
                
                if($tipo == 1)
                {
                    if($bandera_es_cambio_servicio == true)
                    {
                        if($cambio_en_internet == true) $eliminar[] = $contador_array;
                    }
                    else if($bandera_es_servicio_con_internet == true)
                    {
                        if($servicio_en_internet == true) $eliminar[] = $contador_array;
                    }
                    else if($internet == 1)
                        $eliminar[] = $contador_array;
                }
                else if($tipo == 2)
                {   
                    if($bandera_es_cambio_servicio == true)
                    {
                        if($cambio_en_internet == false) $eliminar[] = $contador_array;
                    }
                    else if($bandera_es_servicio_con_internet == true)
                    {
                        if($servicio_en_internet == false) $eliminar[] = $contador_array;
                    }
                    else if($internet == NULL || $internet == 0)
                        $eliminar[] = $contador_array;
                }

                $contador_array++;
            }

            foreach ($eliminar as $registro)
                unset($registros[$registro]);
        }
        ### FIN De separacion		
    		
    	$total =0;						       
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE CORTE DE CAJA");

        $pdf->SetFont('Arial','B',10);
        $pdf->SetWidths(array(23,20,20,40,16,16,16,16,19));
        $pdf->Row(array('FECHA Y HORA','CONTRATO','PLAZA','CONCEPTO','SERVICIO','ENTRADA','SALIDA','SALDO','ORIGEN'),0,1);
        
        $pdf->SetFont('Arial','',8);        
        $total =0;
        
        foreach($registros as $registro)
        {    
            $contrato="N/A";
            $total += $registro[monto];
            $salida="$0.00";
            $entrada="$0.00";
			$descripcion = "";
            $des_servicio = "";
            
            if($registro['monto'] >= 0){
                $filtro1 = "  cobranza_id =" .$registro[id];
                $cobros=  My_Comun::obtenerFiltro("CobranzaDetalle", $filtro1);
				
                foreach($cobros as $cobro)
                {	
                    $saltar = false;		
                    if($cobro->servicio_id != null){
                        $descripcion .= "* Servicio: ".$cobro->Servicio->nombre."\n";
                    }
					
                    if($cobro->servicio_adicional_id != null){
                        $descripcion .= "* Servicio adicional : ".$cobro->ServicioAdicional->nombre."\n";
                    }
															
                    if($cobro->concepto_contratacion_id != null){
                        $descripcion .= "* Concepto de contratación : ".$cobro->ConceptoDeContratacion->nombre."\n";
                    } 
					
                    if($cobro->otro_concepto != ''){
                        if($cobro->otro_concepto == "Cambio de servicio")
                        {
                            $query = Doctrine_Query::create()->from('Cobranza')->where('id = '.$cobro->cobranza_id)->execute()->getFirst();
                            $cambio = Doctrine_Query::create()->from('Ordencambioservicio')->where("created_at >= '".$query->created_at."'")->orderBy("id ASC")->execute()->getFirst();
                            $servicio_nuevo = Servicio::obtenerServicios("id=".$cambio->servicio_nuevo_id)->getFirst();
                            $saltar = true;
                            $des_servicio .= "* ".$servicio_nuevo->nombre."\n";
                        }
                    
                        $descripcion .= "* Otro concepto : ".$cobro->otro_concepto."\n";
                    } 

                    if($cobro->servicio_id != NULL && $cobro->Servicio == 1)
                        $des_servicio .= "* ".$cobro->Servicio->nombre."\n";
                    else if($cobro->concepto_contratacion_id != NULL)
                        $des_servicio .= "* ".ClienteServicio::obtenerPrimerServicio($cobro->Cobranza->cliente_id)->Servicio->nombre."\n";
                    else if(!$saltar)
                        $des_servicio .= "* N/A"."\n";
                   
                    $contrato =  str_pad((int)$cobro->Cobranza->Cliente->contrato,4,"0",STR_PAD_LEFT);
                }	
                $entrada="$".number_format($registro[monto],2,".",",");
            }
               			
            if($registro['monto'] < 0){			
                $filtro1 = "  id =" .$registro[id];
                $cobros=  My_Comun::obtenerFiltro("Gasto", $filtro1);
				
                foreach($cobros as $cobro)
                {				
                    $descripcion = "* ".$cobro->concepto."\n";  
                }	
               								
                $salida="$".number_format($registro[monto],2,".",",");					
            }
						
           $subtotal="$".number_format($total,2,".",",");
            
            $org;
            if($registro['origen'] == 1)
                $org = 'Pago en caja';
            else if($registro['origen'] == 2)
                $org = 'Pago en linea';
            else
                $org = 'N/A';
			    	     
           $pdf->Row
           (
                array
                (
                    $registro[fecha],
                    $contrato,
                    $registro[nombre],
                    $descripcion,
					$des_servicio,
                    $entrada,
                    $salida,
                    $subtotal,
                    $org
                ),0,1			
           );
        }	     
        
       $pdf->Output();	 
    }
	
    function exportarcortedecajaAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
	    $fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
		$plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');          
        $tipo = $this->_getParam('tipo');
        $proviene = $this->_getParam('proviene');

       ### Establecemos el filtro por default	 
       $filtroG = " gastos.cancelado =0 ";
        $filtroC = " cobranza.cancelado =0 AND cobranza.pagada =1 ";

       if($proviene != 0 && $proviene != null)
        {
            if($proviene == 2)
            {
                $filtroC .= " AND 1=2";
            }
            else
            {
                $filtroG .= " AND 1=2";
                $filtroC .= " AND cobranza.origen_pago = ". $proviene;
            }
        }
        	
	if($fecha=='' || $hasta==''){
            $filtroG .= " AND gastos.created_at>='".date('Y-m-d 00:00:00')."' AND gastos.created_at<='".date('Y-m-d 23:59:59')."'"; 
            $filtroC .= " AND cobranza.created_at>='".date('Y-m-d 00:00:00')."' AND cobranza.created_at<='".date('Y-m-d 23:59:59')."'"; 
        }
                
	if($fecha != '')
        {
           $filtroG .= " AND gastos.created_at >= '$fecha 00:00:00'";
           $filtroC .= " AND cobranza.created_at >= '$fecha 00:00:00'";
        }
		
	if($hasta != '')
        {
           $filtroG .= " AND gastos.created_at <= '$hasta 23:59:59'";
           $filtroC .= " AND cobranza.created_at <= '$hasta 23:59:59'";
        }
        
        if(Usuario::tipo()==1)
        {
            if(!is_null(Usuario::plaza()))
            {
                $filtroG.=" AND plaza_id=".Usuario::plaza()." AND usuario_id=".Usuario::id();
                $filtroC.=" AND plaza_id=".Usuario::plaza()." AND usuario_id=".Usuario::id();
            }
            else
            {
                $filtroG.=" AND plaza_id=".Usuario::plaza();
                $filtroC.=" AND plaza_id=".Usuario::plaza();
            }
        }
        else
        {
            $tmp_plaza = "";
            $tmp_plaza_uno = Permiso::obtienePlazas(Usuario::id());
            foreach ($tmp_plaza_uno as $permiso)
            {
                $tmp_plaza .= $permiso->plaza_id.",";
            }

            if($plaza != '' && $plaza!='undefined')
            {
                $plaza = str_replace('_', ',', $plaza);
                //$plaza = $tmp_plaza.$plaza;
                $filtroG .= " AND plaza_id in (".$plaza.")";
                $filtroC .= " AND plaza_id in (".$plaza.")";
            }
            else
            {
                if($tmp_plaza != "" && Usuario::tipo() != 0)
                {
                    $plaza = trim($tmp_plaza, ',');
                    $filtroG .= " AND plaza_id in (".$plaza.")";
                    $filtroC .= " AND plaza_id in (".$plaza.")";
                }
            }
        }
        
        if($sucursal != 0)
        {
            $gastos = " AND sucursal_id = ".$sucursal;
            $cobranza = "  AND sucursal_id = ".$sucursal;
        }
        
        //echo  $filtroG; exit;     
        $query = "SELECT fecha, monto * -1 as monto, plaza_id, gastos.id, plaza.nombre, 'N/A' AS origen FROM gastos INNER JOIN plaza ON plaza.id=gastos.plaza_id ".$gastos."  WHERE(".$filtroG.") UNION SELECT cobranza.created_at, total, plaza_id, cobranza.id, plaza.nombre, cobranza.origen_pago AS origen FROM cobranza INNER JOIN plaza ON plaza.id=cobranza.plaza_id ".$cobranza." WHERE (".$filtroC.") ORDER BY fecha DESC";
	 		
		//recuperamos el singleton de la conexión
    	$con = Doctrine_Manager::getInstance()->connection();	 
    	//ejecutamos la consulta
    	$st = $con->execute($query);	 	
    	//recuperamos los resultados en un arreglo
    	$registros = $st->fetchAll();			
    	//print_r($registros); exit;

        ### INICIO Hacemos la separacion para el filtro por tipo fuera de la consulta para evitar modificar lo que ya se tiene
        if($tipo != 0)
        {
            $contador_array = 0;
            $eliminar = array();
            foreach ($registros as $registro)
            {
                $internet = ClienteServicio::obtenerPrimerServicioPorFecha($registro['id'])->Servicio->incluye_internet;
                $cambio_en_internet = false;
                $servicio_en_internet = false;
                $bandera_es_cambio_servicio = false;
                $bandera_es_servicio_con_internet = false;

                $cobros=  My_Comun::obtenerFiltro("CobranzaDetalle", " cobranza_id =" .$registro['id']);
                foreach($cobros as $cobro)
                {
                    if($cobro->servicio_id != NULL && $cobro->servicio_id != '')
                    {
                        $bandera_es_servicio_con_internet = true;
                        $query = Doctrine_Query::create()->from('Cobranza')->where('id = '.$registro['id'])->execute()->getFirst();
                        $qServicio = Servicio::obtenerServicios("id=".$cobro->servicio_id)->getFirst();
                        if($qServicio->incluye_internet == 1) $servicio_en_internet = true;
                    }
                    
                    if($cobro->otro_concepto == "Cambio de servicio")
                    {
                        $bandera_es_cambio_servicio = true;
                        $query = Doctrine_Query::create()->from('Cobranza')->where('id = '.$registro['id'])->execute()->getFirst();
                        $cambio = Doctrine_Query::create()->from('Ordencambioservicio')->where("created_at >= '".$query->created_at."'")->orderBy("id ASC")->execute()->getFirst();
                        $servicio_nuevo = Servicio::obtenerServicios("id=".$cambio->servicio_nuevo_id)->getFirst();
                        if($servicio_nuevo->incluye_internet == 1) $cambio_en_internet = true;
                    }
                }
                
                if($tipo == 1)
                {
                    if($bandera_es_cambio_servicio == true)
                    {
                        if($cambio_en_internet == true) $eliminar[] = $contador_array;
                    }
                    else if($bandera_es_servicio_con_internet == true)
                    {
                        if($servicio_en_internet == true) $eliminar[] = $contador_array;
                    }
                    else if($internet == 1)
                        $eliminar[] = $contador_array;
                }
                else if($tipo == 2)
                {   
                    if($bandera_es_cambio_servicio == true)
                    {
                        if($cambio_en_internet == false) $eliminar[] = $contador_array;
                    }
                    else if($bandera_es_servicio_con_internet == true)
                    {
                        if($servicio_en_internet == false) $eliminar[] = $contador_array;
                    }
                    else if($internet == NULL || $internet == 0)
                        $eliminar[] = $contador_array;
                }

                $contador_array++;
            }

            foreach ($eliminar as $registro)
                unset($registros[$registro]);
        }
        ### FIN De separacion		
		
        $total =0;						       

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA Y HORA',
                        "width" => 19
                        ),
                "B$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 13
                        ),
                "C$i" => array(
                        "name" => 'PLAZA',
                        "width" => 13
                        ),
				"D$i" => array(
                        "name" => 'CONCEPTO',
                        "width" => 40
                        ),
				"E$i" => array(
                        "name" => 'SERVICIO',
                        "width" => 40
                        ),
				"F$i" => array(
                        "name" => 'ENTRADA',
                        "width" => 11
                        ),
                "G$i" => array(
                        "name" => 'SALIDA',
                        "width" => 11
                        ),
                "H$i" => array(
                        "name" => 'SALDO',
                        "width" => 11
                        ),
                "I$i" => array(
                        "name" => 'ORIGEN',
                        "width" => 11
                        )									
        );
        
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
            $contrato="N/A";
            $total += $registro[monto];
            $salida="$0.00";
            $entrada="$0.00";
			$descripcion = "";
            $des_servicio = "";

            $org;
            if($registro['origen'] == 1)
                $org = 'Pago en caja';
            else if($registro['origen'] == 2)
                $org = 'Pago en linea';
            else
                $org = 'N/A';

            if($registro['monto'] >= 0){
                $filtro1 = "  cobranza_id =" .$registro[id];
                $cobros=  My_Comun::obtenerFiltro("CobranzaDetalle", $filtro1);
				
                foreach($cobros as $cobro)
                {				
                    if($cobro->servicio_id != null){
                        $descripcion = "* Servicio: ".$cobro->Servicio->nombre."\n";
                    }
					
                    if($cobro->servicio_adicional_id != null){
                        $descripcion = "* Servicio adicional : ".$cobro->ServicioAdicional->nombre."\n";
                    }
															
                    if($cobro->concepto_contratacion_id != null){
                        $descripcion = "* Concepto de contratación : ".$cobro->ConceptoDeContratacion->nombre."\n";
                    } 
					
                    if($cobro->otro_concepto != ''){
                        if($cobro->otro_concepto == "Cambio de servicio")
                        {
                            $query = Doctrine_Query::create()->from('Cobranza')->where('id = '.$cobro->cobranza_id)->execute()->getFirst();
                            $cambio = Doctrine_Query::create()->from('Ordencambioservicio')->where("created_at >= '".$query->created_at."'")->orderBy("id ASC")->execute()->getFirst();
                            $servicio_nuevo = Servicio::obtenerServicios("id=".$cambio->servicio_nuevo_id)->getFirst();
                            $saltar = true;
                            $des_servicio .= "* ".$servicio_nuevo->nombre."\n";
                        }
                        $descripcion = "* Otro concepto : ".$cobro->otro_concepto."\n";
                    }

                    if($cobro->servicio_id != NULL && $cobro->Servicio == 1)
                        $des_servicio .= "* ".$cobro->Servicio->nombre."\n";
                    else if($cobro->concepto_contratacion_id != NULL)
                        $des_servicio .= "* ".ClienteServicio::obtenerPrimerServicio($cobro->Cobranza->cliente_id)->Servicio->nombre."\n";
                    else if(!$saltar)
                        $des_servicio .= "* N/A"."\n";
                   
                    $contrato =  str_pad((int)$cobro->Cobranza->Cliente->contrato,4,"0",STR_PAD_LEFT);
                }	
                $entrada="$".number_format($registro[monto],2,".",",");
            }
               			
            if($registro['monto'] < 0){			
                $filtro1 = "  id =" .$registro[id];
                $cobros=  My_Comun::obtenerFiltro("Gasto", $filtro1);
				
                foreach($cobros as $cobro)
                {				
                    $descripcion = "* ".$cobro->concepto."\n";  
                }	
               								
                $salida="$".number_format($registro[monto],2,".",",");					
            }
						
            $subtotal="$".number_format($total,2,".",",");
          
            $i++;
            $data[] = array(				
		            "A$i" =>$registro[fecha],
                    "B$i" =>str_pad((int)$contrato,4,"0",STR_PAD_LEFT),
                    "C$i" =>$registro[nombre],
                    "D$i" =>$descripcion,
                    "E$i" =>$des_servicio,
                    "F$i" =>$entrada,
                    "G$i" =>$salida,
                    "H$i" =>$subtotal,
                    "I$i" =>$org
            );
        }
				
        $objPHPExcel->createExcel('cortedecaja', $columns_name, $data, 10, array('rango'=>'A4:F4','size'=>14,'texto'=>'CORTE DE CAJA'));
    }
    
    ///////////////////////////////////////////////////////////////////////////////// 
    public function productividadAction()
    {
        $this->view->headScript()->appendFile('/js/transacciones/reportes/productividad.js');
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
	
    public function gridproductividadAction()
    {
	### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default        
        $filtro = " 1= 1 AND (tipo=4 OR tipo=6) AND status=1 "; 
        
        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
	    $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
        $tecnico = $this->_getParam('tecnico');    
        $tipo = $this->_getParam('es');         
           
        if($desde != ''){  $inicio = $desde; }        
        else{  $inicio = date("Y-m-d");   }
        
        if($hasta != ''){ $fin= $hasta;  } 
        else{ $fin = date("Y-m-d"); }  
        
        if($tecnico != 0)
        {
            $filtro .= " AND id =".$tecnico;
        }     
		else
		{
			if($sucursal != 0)
			{
				$filtro .= " AND sucursal_id = ".$sucursal;
			}	
			else
			{
			
				if($plaza != '')
				{
					$plaza = str_replace('_', ',', $plaza);
					$filtro .= " AND plaza_id in (".$plaza.")";
				}	
			}
		}
          
    	$grid=array();
        $product=array();
        $i_product=array();
    	$i=0;	
        $cueta=0;
        
        $qp=Doctrine_Query::create()->from('Parametrosdeproductividad');
        $productividades=$qp->execute();         
              
        foreach($productividades as $productividad)
        {
            $product[1]= $productividad->instalaciones;
            $product[2]= $productividad->reconexiones;
            $product[3]= $productividad->cambio_domicilio;
            $product[4]= $productividad->atencion_a_fallas;
            $product[5]= $productividad->extensiones_adicionales;
            $product[6]= $productividad->recuperaciones;
            $product[7]= $productividad->auditorias_de_acometidas;
            $product[8]= $productividad->reinstalaciones;  
            $product[9]= $productividad->retiros; 
        }  

        $qp=Doctrine_Query::create()->from('Parametrosdeproductividadinternet');
        $productividades=$qp->execute();         
              
        foreach($productividades as $productividad)
        {
            $i_product[1]= $productividad->instalaciones;
            $i_product[2]= $productividad->reconexiones;
            $i_product[3]= $productividad->cambio_domicilio;
            $i_product[4]= $productividad->atencion_a_fallas;
            $i_product[5]= $productividad->extensiones_adicionales;
            $i_product[6]= $productividad->recuperaciones;
            $i_product[7]= $productividad->auditorias_de_acometidas;
            $i_product[8]= $productividad->reinstalaciones;  
            $i_product[9]= $productividad->retiros; 
        }  
        
        while(strtotime($inicio) <= strtotime($fin)){                    
            $q=Doctrine_Query::create()->from('Usuario')->where($filtro);
            $tecnicos=$q->execute();         
              
            foreach($tecnicos as $tecnico)
            {        
                $grid[$i]['fecha']=$inicio;
                $grid[$i]['tecnico']=$tecnico->nombre;
                $grid[$i]['plaza']=$tecnico->Plaza->nombre;    
                
                //INTERNET

                    /********************* Orden instalacion**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordeninstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $i_instalaciones = $c["puntos"];
                    $i_instalacionest = $c["puntos"] * $i_product[1];
                    $i_total = $c["puntos"] * $i_product[1];
                    
                    /********************* Ordendes reconexion**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_reconexion o_fi 
                                 FROM ordendesreconexion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_reconexion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_reconexion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $i_reconexiones = $c["puntos"];
                    $i_reconexionest = $c["puntos"] * $i_product[2];
                    $i_total += $c["puntos"] * $i_product[2];
                    
                    /********************* Ordenes cambio domicilio**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos 
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenescambiodomicilio o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'  
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_instalo = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $i_cambiodomicilio = $c["puntos"];
                    $i_cambiodomiciliot = $c["puntos"] * $i_product[3];
                    $i_total += $c["puntos"] * $i_product[3]; 

                    /********************* Atención a fallas **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(o.id) AS puntos
                            FROM cliente_servicio o
                            INNER JOIN (
                                SELECT MAX(cs.id) m_id, hf.fecha o_fi  
                                FROM historial_de_fallas hf 
                                INNER JOIN reportar_fallas rf ON hf.reportar_falla_id = rf.id
                                INNER JOIN cliente_servicio cs ON cs.cliente_id = rf.cliente_id 
                                WHERE hf.tecnico_id = ".$tecnico->id." 
                                AND hf.fecha = '".$inicio."'
                                AND hf.status_falla = 2
                                AND CAST( cs.created_at AS DATE) <= rf.fecha_falla
                                GROUP BY hf.id
                            ) qr ON qr.m_id = o.id
                            INNER JOIN servicio s ON o.servicio_id = s.id 
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $i_atencionfallas = $c["puntos"]; 
                    $i_atencionfallast = $c["puntos"] * $i_product[4];
                    $i_total += $c["puntos"] * $i_product[4];
                     
                    /********************* Extensiones adicionales **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenextensionadicional o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $i_extensionesadicionales = $c["puntos"]; 
                    $i_extensionesadicionalest = $c["puntos"] * $i_product[5];
                    $i_total += $c["puntos"] * $i_product[5];
                    
                    /********************* Recuperaciones **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenextensionadicional o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $i_recuperaciones = 0;
                    $i_recuperacionest = 0 * $i_product[6];
                    $i_total += 0;

                    /********************* Auditorías de acometidas  **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenreinstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $i_auditoria = 0;
                    $i_auditoriat = 0 * $i_product[7];
                    $i_total += 0;  
                    
                    /********************* Reinstalaciones  **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenreinstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $i_reinstalacion = $c["puntos"];
                    $i_reinstalaciont =  $c["puntos"] * $i_product[8];
                    $i_total += $c["puntos"] * $i_product[8];

                    /********************* Ordendes de retiro **********************/            
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_retiro o_fi 
                                 FROM ordendesretiro o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_retiro AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_retiro 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $i_retiros = $c["puntos"];  
                    $i_retirost = $c["puntos"] * $i_product[9];
                    $i_total += $c["puntos"] * $i_product[9]; 

                //CABLE

                    /********************* Orden instalacion**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordeninstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $instalaciones = $c["puntos"];
                    $instalacionest = $c["puntos"] * $product[1];
                    $total = $c["puntos"] * $product[1];
                    
                    /********************* Ordendes reconexion**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_reconexion o_fi 
                                 FROM ordendesreconexion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_reconexion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_reconexion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $reconexiones = $c["puntos"];
                    $reconexionest = $c["puntos"] * $product[2];
                    $total += $c["puntos"] * $product[2];
                    
                    /********************* Ordenes cambio domicilio**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos 
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenescambiodomicilio o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'  
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_instalo = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $cambiodomicilio = $c["puntos"]; 
                    $cambiodomiciliot = $c["puntos"] * $product[3];
                    $total += $c["puntos"] * $product[3]; 

                    /********************* Atención a fallas **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(o.id) AS puntos
                            FROM cliente_servicio o
                            INNER JOIN (
                                SELECT MAX(cs.id) m_id, hf.fecha o_fi  
                                FROM historial_de_fallas hf 
                                INNER JOIN reportar_fallas rf ON hf.reportar_falla_id = rf.id
                                INNER JOIN cliente_servicio cs ON cs.cliente_id = rf.cliente_id 
                                WHERE hf.tecnico_id = ".$tecnico->id." 
                                AND hf.fecha = '".$inicio."'
                                AND hf.status_falla = 2
                                AND CAST( cs.created_at AS DATE) <= rf.fecha_falla
                                GROUP BY hf.id
                            ) qr ON qr.m_id = o.id
                            INNER JOIN servicio s ON o.servicio_id = s.id 
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $atencionfallas = $c["puntos"]; 
                    $atencionfallast = $c["puntos"] * $product[4];
                    $total += $c["puntos"] * $product[4];
                     
                    /********************* Extensiones adicionales **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenextensionadicional o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $extensionesadicionales = $c["puntos"]; 
                    $extensionesadicionalest =  $c["puntos"] * $product[5];
                    $total += $c["puntos"] * $product[5];
                    
                    /********************* Recuperaciones **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenextensionadicional o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $recuperaciones = 0;
                    $recuperacionest = 0 * $product[6];
                    $total += 0;

                    /********************* Auditorías de acometidas  **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenreinstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $auditoria = 0;
                    $auditoriat = 0 * $product[7];
                    $total += 0;  
                    
                    /********************* Reinstalaciones  **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenreinstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $reinstalacion = $c["puntos"];  
                    $reinstalaciont = $c["puntos"] * $product[8];
                    $total += $c["puntos"] * $product[8];

                    /********************* Ordendes de retiro **********************/            
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_retiro o_fi 
                                 FROM ordendesretiro o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_retiro AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_retiro 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $retiros = $c["puntos"];     
                    $retirost = $c["puntos"] * $product[9];
                    $total += $c["puntos"] * $product[9];


                $grid[$i]['i_total'] = $i_total;
                $grid[$i]['total'] = $total;
                
                $grid[$i]['i_instalaciones'] = $i_instalaciones;
                $grid[$i]['i_instalacionest'] = $i_instalacionest;
                $grid[$i]['instalaciones'] = $instalaciones;
                $grid[$i]['instalacionest'] = $instalacionest;

                $grid[$i]['i_reconexiones'] = $i_reconexiones;     
                $grid[$i]['i_reconexionest'] = $i_reconexionest;
                $grid[$i]['reconexiones'] = $reconexiones;     
                $grid[$i]['reconexionest'] = $reconexionest;

                $grid[$i]['i_cambiodomicilio'] = $i_cambiodomicilio;     
                $grid[$i]['i_cambiodomiciliot'] = $i_cambiodomiciliot;
                $grid[$i]['cambiodomicilio'] = $cambiodomicilio;     
                $grid[$i]['cambiodomiciliot'] = $cambiodomiciliot;

                $grid[$i]['i_atencionfallas'] = $i_atencionfallas;    
                $grid[$i]['i_atencionfallast'] = $i_atencionfallast;
                $grid[$i]['atencionfallas'] = $atencionfallas;    
                $grid[$i]['atencionfallast'] = $atencionfallast;

                $grid[$i]['i_extensionesadicionales'] = $i_extensionesadicionales;       
                $grid[$i]['i_extensionesadicionalest'] = $i_extensionesadicionalest;
                $grid[$i]['extensionesadicionales'] = $extensionesadicionales;       
                $grid[$i]['extensionesadicionalest'] = $extensionesadicionalest;

                $grid[$i]['i_recuperaciones'] = $i_recuperaciones;       
                $grid[$i]['i_recuperacionest'] = $i_recuperacionest;
                $grid[$i]['recuperaciones'] = $recuperaciones;       
                $grid[$i]['recuperacionest'] = $recuperacionest;

                $grid[$i]['i_auditoria'] = $i_auditoria;       
                $grid[$i]['i_auditoriat'] = $i_auditoriat;
                $grid[$i]['auditoria'] = $auditoria;       
                $grid[$i]['auditoriat'] = $auditoriat;

                $grid[$i]['i_reinstalacion'] = $i_reinstalacion;  
                $grid[$i]['i_reinstalaciont'] = $i_reinstalaciont;
                $grid[$i]['reinstalacion'] = $reinstalacion;  
                $grid[$i]['reinstalaciont'] = $reinstalaciont;

                $grid[$i]['i_retiros'] = $i_retiros;     
                $grid[$i]['i_retirost'] = $i_retirost;
                $grid[$i]['retiros'] = $retiros;     
                $grid[$i]['retirost'] = $retirost;

                $i++;

            } 
                       
            $inicio = date("Y/m/d", strtotime("$inicio +1 day"));       
        }					       

        //$total=count($registros);
	   $registros=array('pagina'=>1,"total"=>$i);
        
        My_Comun::grid2($registros,$grid);
    }
	
	
    function exportarproductividadAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
        ### Establecemos el filtro por default        
        $filtro = " 1= 1 AND (tipo=4 OR tipo=6) AND status=1"; 
        
        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
	    $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
        $tecnico = $this->_getParam('tecnico');           
           
        if($desde != ''){  $inicio = $desde; }        
        else{  $inicio = date("Y-m-d");   }
        
        if($hasta != ''){ $fin= $hasta;  } 
        else{ $fin = date("Y-m-d"); }  
        
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND plaza_id in (".$plaza.")";
        } 
        if($sucursal != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal;
        }  
        if($tecnico >0)
        {
            $filtro .= " AND id =".$tecnico;
        }     
            
	    $qp=Doctrine_Query::create()->from('Parametrosdeproductividad');
        $productividades=$qp->execute();  

        $product=array();
        $i_product=array();
         
              
        foreach($productividades as $productividad)
        {
            $product[1]= $productividad->instalaciones;
            $product[2]= $productividad->reconexiones;
            $product[3]= $productividad->cambio_domicilio;
            $product[4]= $productividad->atencion_a_fallas;
            $product[5]= $productividad->extensiones_adicionales;
            $product[6]= $productividad->recuperaciones;
            $product[7]= $productividad->auditorias_de_acometidas;
            $product[8]= $productividad->reinstalaciones;  
            $product[9]= $productividad->retiros; 
        }

        $qp=Doctrine_Query::create()->from('Parametrosdeproductividadinternet');
        $productividades=$qp->execute();         
              
        foreach($productividades as $productividad)
        {
            $i_product[1]= $productividad->instalaciones;
            $i_product[2]= $productividad->reconexiones;
            $i_product[3]= $productividad->cambio_domicilio;
            $i_product[4]= $productividad->atencion_a_fallas;
            $i_product[5]= $productividad->extensiones_adicionales;
            $i_product[6]= $productividad->recuperaciones;
            $i_product[7]= $productividad->auditorias_de_acometidas;
            $i_product[8]= $productividad->reinstalaciones;  
            $i_product[9]= $productividad->retiros; 
        }       
        
        ini_set("memory_limit", "260M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA',
                        "width" => 11
                        ),
                "B$i" => array(
                        "name" => 'TÉCNICO',
                        "width" => 30
                        ),
		        "C$i" => array(
                        "name" => 'PLAZA',
                        "width" => 11
                        ),

                "D$i" => array(
                        "name" => 'NÚM. DE PTS. TOTALES INTER.',
                        "width" => 35
                        ),
                "E$i" => array(
                        "name" => 'NÚM. DE PTS. TOTALES CABLE',
                        "width" => 35
                        ),

		        "F$i" => array(
                        "name" => 'NÚM. DE INST. INTER. - PTS. GANADOS',
                        "width" => 55
                        ),
                "G$i" => array(
                        "name" => 'NÚM. DE INST. CABLE - PTS. GANADOS',
                        "width" => 55
                        ),

                "H$i" => array(
                        "name" => 'NÚM. DE RECON. INTER. - PTS. GANADOS',
                        "width" => 55
                        ),
                "I$i" => array(
                        "name" => 'NÚM. DE RECON. CABLE - PTS. GANADOS',
                        "width" => 55
                        ),

                "J$i" => array(
                        "name" => 'NÚM. DE CAMB. DE DOM. INTER. - PTS. GANADOS',
                        "width" => 55
                        ),
                "K$i" => array(
                        "name" => 'NÚM. DE CAMB. DE DOM. CABLE - PTS. GANADOS',
                        "width" => 55
                        ),

                "L$i" => array(
                        "name" => 'NÚM. DE ATEN. A FALLAS INTER. - PTS. GANADOS',
                        "width" => 55
                        ),
                "M$i" => array(
                        "name" => 'NÚM. DE ATEN. A FALLAS CABLE - PTS. GANADOS',
                        "width" => 55
                        ),

                "N$i" => array(
                        "name" => 'NÚM. DE EXT. ADI. INTER. - PTS. GANADOS',
                        "width" => 55
                        ),
                "O$i" => array(
                        "name" => 'NÚM. DE EXT. ADI. CABLE - PTS. GANADOS',
                        "width" => 55
                        ),

                "P$i" => array(
                        "name" => 'NÚM. DE RECUP. INTER. - PTS. GANADOS',
                        "width" => 55
                        ),
                "Q$i" => array(
                        "name" => 'NÚM. DE RECUP. CABLE - PTS. GANADOS',
                        "width" => 55
                        ),

                "R$i" => array(
                        "name" => 'NÚM. DE AUDI. ACOMET. INTER. - PTS. GANADOS',
                        "width" => 55
                        ),
                "S$i" => array(
                        "name" => 'NÚM. DE AUDI. ACOMET. CABLE - PTS. GANADOS',
                        "width" => 55
                        ),

                "T$i" => array(
                        "name" => 'NÚM. DE REINS. INTER. - PTS. GANADOS',
                        "width" => 55
                        ),
                "U$i" => array(
                        "name" => 'NÚM. DE REINS. CABLE - PTS. GANADOS',
                        "width" => 55
                        ),

                "V$i" => array(
                        "name" => 'NÚM. DE RETIROS INTER. - PTS. GANADOS',
                        "width" => 55
                        ),
                "W$i" => array(
                        "name" => 'NÚM. DE RETIROS CABLE - PTS. GANADOS',
                        "width" => 55
                        )
        );
        
        //Datos tabla
        $data = array();
        
        while(strtotime($inicio) <= strtotime($fin)){              
            
            $q=Doctrine_Query::create()->from('Usuario')->where($filtro);
            $tecnicos=$q->execute();         
              
            foreach($tecnicos as $tecnico)
            {        
               //INTERNET

                    /********************* Orden instalacion**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordeninstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $i_instalaciones = $c["puntos"];
                    $i_instalacionest = $c["puntos"] * $i_product[1];
                    $i_total = $c["puntos"] * $i_product[1];
                    
                    /********************* Ordendes reconexion**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_reconexion o_fi 
                                 FROM ordendesreconexion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_reconexion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_reconexion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $i_reconexiones = $c["puntos"];
                    $i_reconexionest = $c["puntos"] * $i_product[2];
                    $i_total += $c["puntos"] * $i_product[2];
                    
                    /********************* Ordenes cambio domicilio**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos 
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenescambiodomicilio o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'  
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_instalo = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $i_cambiodomicilio = $c["puntos"];
                    $i_cambiodomiciliot = $c["puntos"] * $i_product[3];
                    $i_total += $c["puntos"] * $i_product[3]; 

                    /********************* Atención a fallas **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(o.id) AS puntos
                            FROM cliente_servicio o
                            INNER JOIN (
                                SELECT MAX(cs.id) m_id, hf.fecha o_fi  
                                FROM historial_de_fallas hf 
                                INNER JOIN reportar_fallas rf ON hf.reportar_falla_id = rf.id
                                INNER JOIN cliente_servicio cs ON cs.cliente_id = rf.cliente_id 
                                WHERE hf.tecnico_id = ".$tecnico->id." 
                                AND hf.fecha = '".$inicio."'
                                AND hf.status_falla = 2
                                AND CAST( cs.created_at AS DATE) <= rf.fecha_falla
                                GROUP BY hf.id
                            ) qr ON qr.m_id = o.id
                            INNER JOIN servicio s ON o.servicio_id = s.id 
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $i_atencionfallas = $c["puntos"]; 
                    $i_atencionfallast = $c["puntos"] * $i_product[4];
                    $i_total += $c["puntos"] * $i_product[4];
                     
                    /********************* Extensiones adicionales **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenextensionadicional o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $i_extensionesadicionales = $c["puntos"]; 
                    $i_extensionesadicionalest = $c["puntos"] * $i_product[5];
                    $i_total += $c["puntos"] * $i_product[5];
                    
                    /********************* Recuperaciones **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenextensionadicional o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $i_recuperaciones = 0;
                    $i_recuperacionest = 0 * $i_product[6];
                    $i_total += 0;

                    /********************* Auditorías de acometidas  **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenreinstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $i_auditoria = 0;
                    $i_auditoriat = 0 * $i_product[7];
                    $i_total += 0;  
                    
                    /********************* Reinstalaciones  **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenreinstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $i_reinstalacion = $c["puntos"];
                    $i_reinstalaciont =  $c["puntos"] * $i_product[8];
                    $i_total += $c["puntos"] * $i_product[8];

                    /********************* Ordendes de retiro **********************/            
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_retiro o_fi 
                                 FROM ordendesretiro o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_retiro AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_retiro 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE s.incluye_internet = 1 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $i_retiros = $c["puntos"];  
                    $i_retirost = $c["puntos"] * $i_product[9];
                    $i_total += $c["puntos"] * $i_product[9]; 

                //CABLE

                    /********************* Orden instalacion**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordeninstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $instalaciones = $c["puntos"];
                    $instalacionest = $c["puntos"] * $product[1];
                    $total = $c["puntos"] * $product[1];
                    
                    /********************* Ordendes reconexion**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_reconexion o_fi 
                                 FROM ordendesreconexion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_reconexion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_reconexion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $reconexiones = $c["puntos"];
                    $reconexionest = $c["puntos"] * $product[2];
                    $total += $c["puntos"] * $product[2];
                    
                    /********************* Ordenes cambio domicilio**********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos 
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenescambiodomicilio o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'  
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_instalo = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $cambiodomicilio = $c["puntos"]; 
                    $cambiodomiciliot = $c["puntos"] * $product[3];
                    $total += $c["puntos"] * $product[3]; 

                    /********************* Atención a fallas **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(o.id) AS puntos
                            FROM cliente_servicio o
                            INNER JOIN (
                                SELECT MAX(cs.id) m_id, hf.fecha o_fi  
                                FROM historial_de_fallas hf 
                                INNER JOIN reportar_fallas rf ON hf.reportar_falla_id = rf.id
                                INNER JOIN cliente_servicio cs ON cs.cliente_id = rf.cliente_id 
                                WHERE hf.tecnico_id = ".$tecnico->id." 
                                AND hf.fecha = '".$inicio."'
                                AND hf.status_falla = 2
                                AND CAST( cs.created_at AS DATE) <= rf.fecha_falla
                                GROUP BY hf.id
                            ) qr ON qr.m_id = o.id
                            INNER JOIN servicio s ON o.servicio_id = s.id 
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $atencionfallas = $c["puntos"]; 
                    $atencionfallast = $c["puntos"] * $product[4];
                    $total += $c["puntos"] * $product[4];
                     
                    /********************* Extensiones adicionales **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenextensionadicional o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $extensionesadicionales = $c["puntos"]; 
                    $extensionesadicionalest =  $c["puntos"] * $product[5];
                    $total += $c["puntos"] * $product[5];
                    
                    /********************* Recuperaciones **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenextensionadicional o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."' 
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id 
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }

                    $recuperaciones = 0;
                    $recuperacionest = 0 * $product[6];
                    $total += 0;

                    /********************* Auditorías de acometidas  **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenreinstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    }
                    
                    $auditoria = 0;
                    $auditoriat = 0 * $product[7];
                    $total += 0;  
                    
                    /********************* Reinstalaciones  **********************/
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi 
                                 FROM ordenreinstalacion o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_instalacion AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $reinstalacion = $c["puntos"];  
                    $reinstalaciont = $c["puntos"] * $product[8];
                    $total += $c["puntos"] * $product[8];

                    /********************* Ordendes de retiro **********************/            
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT COUNT(orden.id) AS puntos
                            FROM cliente_servicio orden 
                            INNER JOIN ( 
                                 SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_retiro o_fi 
                                 FROM ordendesretiro o 
                                 INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                 WHERE CAST( o.fecha_retiro AS DATE ) = '".$inicio."'
                                 AND CAST( cs.created_at AS DATE ) <= o.fecha_retiro 
                                 AND o.tecnico_id = ".$tecnico->id."
                            GROUP BY o.id 
                            ) qr ON qr.m_id = orden.id
                            INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                            INNER JOIN servicio s ON orden.servicio_id = s.id
                            WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0) 
                            GROUP BY CAST(qr.o_fi AS DATE)";
                    $q = $con->execute($query)->fetchAll(); 

                    $c=array();
                    $c["puntos"]=0;
                    foreach($q as $r)
                    {
                        $c["puntos"]=$r[0]; break;
                    } 

                    $retiros = $c["puntos"];     
                    $retirost = $c["puntos"] * $product[9];
                    $total += $c["puntos"] * $product[9];

                       
                $i++;
                $data[] = array(				
                    "A$i" =>$inicio,
                    "B$i" =>$tecnico->nombre,
                    "C$i" =>$tecnico->Plaza->nombre,
		            "D$i" =>$i_total,
                    "E$i" =>$total,
                    "F$i" =>$i_instalaciones."                 ".$i_instalacionest,
                    "G$i" =>$instalaciones."                 ".$instalacionest,
                    "H$i" =>$i_reconexiones."                 ".$i_reconexionest,
                    "I$i" =>$reconexiones."                 ".$reconexionest,
                    "J$i" =>$i_cambiodomicilio."                 ".$i_cambiodomiciliot,
                    "K$i" =>$cambiodomicilio."                 ".$cambiodomiciliot,
                    "L$i" =>$i_atencionfallas."                 ".$i_atencionfallast,
                    "M$i" =>$atencionfallas."                 ".$atencionfallast,
                    "N$i" =>$i_extensionesadicionales."                 ".$i_extensionesadicionalest,
                    "O$i" =>$extensionesadicionales."                 ".$extensionesadicionalest,
                    "P$i" =>$i_recuperaciones."                 ".$i_recuperacionest,
                    "Q$i" =>$recuperaciones."                 ".$recuperacionest,
                    "R$i" =>$i_auditoria."                 ".$i_auditoriat,
                    "S$i" =>$auditoria."                 ".$auditoriat,
                    "T$i" =>$i_reinstalacion."                 ".$i_reinstalaciont,
                    "U$i" =>$reinstalacion."                 ".$reinstalaciont,
                    "V$i" =>$i_retiros."                 ".$i_retirost,
                    "W$i" =>$retiros."                 ".$retirost
                );
           
            } 
                       
            $inicio = date("Y/m/d", strtotime("$inicio +1 day"));       
        }
        $objPHPExcel->createExcel('reporteproductividad', $columns_name, $data, 10, array('rango'=>'A4:F4','size'=>14,'texto'=>'REPORTE DE PRODUCTIVIDAD'));
    }
     
    public function tecnicosAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboTecnico($this->_getParam('id'),0);
    }
    
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }   
	
	//EXISTENCIAS
	public function existenciasAction()
    {	
        $this->view->headScript()->appendFile('/js/transacciones/reportes/existencias.js');
        ### Obtenemos las plazas para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();

    }
	
	public function gridexistenciasAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
		
		$query = "select sum(ordendesretiro.cable) cable, sum(ordendesretiro.conector) conector, sum(ordendesretiro.divisor2) divisor2, sum(ordendesretiro.divisor3) divisor3, sum(ordendesretiro.divisor4) divisor4, sum(ordendesretiro.grapas) grapas from ordendesretiro inner join cliente on ordendesretiro.cliente_id = cliente.id ";
        
        ### Establecemos el filtro por default
        $query .= " where 1 = 1 ";
 
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $fecha = $this->_getParam('desde');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
		$sucursal = $this->_getParam('sucursal');	
		
		$reporte = "/transacciones/reportes/exportarexistencias";	
		             
        if($fecha != '')
        {
			$reporte .= "/desde/".$fecha;
           $query .= " AND (ordendesretiro.created_at >= '$fecha 00:00:00' ) ";
        }
						
        if($hasta != '')
        {
			$reporte .= "/hasta/".$hasta;
           $query .= " AND (ordendesretiro.created_at <= '$hasta 23:59:59' ) ";
        }
        
        if($plaza != '')
        {
			$reporte .= "/plaza/".$plaza;
            $plaza = str_replace('_', ',', $plaza);
            $query .= " AND cliente.plaza_id in (".$plaza.")";
        }	
        
        if($sucursal != 0)
        {
			$reporte .= "/sucursal/".$sucursal;
            $query .= " AND cliente.sucursal_id = ".$sucursal;
        }
		
		//echo $query; exit;
		//### Extraemos los registros para formar el arreglo del grid
		$registros=My_Comun::createQuery("ordendesretiro",$query);      
        
		$grid=array();
		$i=0;
		
		foreach($registros as $registro)
		{
            $grid[$i]['cable']=$registro['cable']==null?0:$registro['cable'];
	    	$grid[$i]['conector']=$registro['conector']==null?0:$registro['conector'];  
            $grid[$i]['divisor2']=$registro['divisor2']==null?0:$registro['divisor2']; 
            $grid[$i]['divisor3']=$registro['divisor3']==null?0:$registro['divisor3'];
			$grid[$i]['divisor4']=$registro['divisor4']==null?0:$registro['divisor4']; 
            $grid[$i]['grapas']=$registro['grapas']==null?0:$registro['grapas'];
			$grid[$i]['detalle']="<a target='_blank' href='".$reporte."'>VER</a>";
            
            $i++;
        }
		
		$total=count($registros);
		$registros=array('pagina'=>1,"total"=>$total);
	
        My_Comun::grid2($registros,$grid);
    }
	
    function exportarexistenciasAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = " 1 = 1 ";
 
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND cliente.plaza_id=".Usuario::plaza()." AND estatus=1";

        ### Cachamos las variables para conformar el filtro
        $fecha = $this->_getParam('desde');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
		$sucursal = $this->_getParam('sucursal');		
		             
        if($fecha != '')
        	$filtro .= " AND (ordendesretiro.created_at >= '$fecha 00:00:00' ) ";
						
        if($hasta != '')
        	$filtro .= " AND (ordendesretiro.created_at <= '$hasta 23:59:59' ) ";
        
        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND cliente.plaza_id in (".$plaza.")";
        }	
        
        if($sucursal != 0)
        	$filtro .= " AND cliente.sucursal_id = ".$sucursal;
		
		//echo $query; exit;
		//### Extraemos los registros para formar el arreglo del grid
		$registros=  My_Comun::obtenerFiltro("Ordendesretiro", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
				"A$i" => array(
                        "name" => 'FECHA RETIRO',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 10
                        ),
				"C$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 50
                        ),
				"D$i" => array(
                        "name" => 'PLAZA',
                        "width" => 10
                        ),
				"E$i" => array(
                        "name" => 'ETIQUETA',
                        "width" => 20
                        ),
				"F$i" => array(
                        "name" => 'NODO',
                        "width" => 20
                        ),
				"G$i" => array(
                        "name" => 'POSTE',
                        "width" => 20
                        ),
                "H$i" => array(
                        "name" => 'CABLE',
                        "width" => 10
                        ),
                "I$i" => array(
                        "name" => 'CONECTOR',
                        "width" => 10
                        ),
				"J$i" => array(
                        "name" => 'DIVISOR2',
                        "width" => 10
                        ),
				"K$i" => array(
                        "name" => 'DIVISOR3',
                        "width" => 10
                        ),
				"L$i" => array(
                        "name" => 'DIVISOR4',
                        "width" => 10
                        ),
				"M$i" => array(
                        "name" => 'GRAPAS',
                        "width" => 10
                        )									
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
			
			$i++;
            $data[] = array(				
				"A$i" =>$registro->fecha_retiro,
                "B$i" =>$registro->Cliente->contrato,
                "C$i" =>$registro->Cliente->nombre,
                "D$i" =>$registro->Cliente->Plaza->nombre,
                "E$i" =>$registro->Cliente->etiqueta,
                "F$i" =>$registro->Cliente->nodo,
				"G$i" =>$registro->Cliente->poste,
				"H$i" =>$registro->cable,
                "I$i" =>$registro->conector,
                "J$i" =>$registro->divisor2,
                "K$i" =>$registro->divisor3,
                "L$i" =>$registro->divisor4,
                "M$i" =>$registro->grapas
            );
        }
				
        $objPHPExcel->createExcel('Existencia', $columns_name, $data, 10, array('rango'=>'A4:F4','size'=>14,'texto'=>'EXISTENCIA'));
    }
	//FIN EXISTENCIAS
	
	//UBICACION
	public function ubicacionAction()
    {	
        
		$this->view->headScript()->appendFile("https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false");
        $this->view->headScript()->appendFile('/js/transacciones/reportes/ubicacion.js');
        ### Obtenemos las plazas para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
	
	public function usuariosAction()
    {
		### Deshabilitamos el layout y la vista
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$plaza_id = $this->_getParam('plaza_id');
		$sucursal_id = $this->_getParam('sucursal_id');
		$tipo_id = $this->_getParam('tipo_id');

        //echo "tipo: ".$tipo_id; exit;
		
		$filtro .= " status = 1 ";
		
		if($sucursal_id != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal_id;
        }
		else
		{
			if($plaza_id != '')
			{
				$filtro .= " AND plaza_id = ".$plaza_id;
			}	
		}
		
		if($tipo_id != '')
		{
			$filtro .= " AND (tipo = ".$tipo_id." OR tipo = 6)";
		}
		
        //echo "FILTRO: ".$filtro; exit;

		$usuarios=My_Comun::obtenerFiltro("Usuario",$filtro); 
			 
		if($tipo_id == 4)
		{
			$opciones='<option value="0">Todos los tecnicos</option>';	
		}else if ($tipo_id == 3)
		{
			$opciones='<option value="0">Todos los promotores</option>';	
		}

		foreach($usuarios as $usuario)
			$opciones.='<option value="'.$usuario['id'].'">'.$usuario['nombre'].'</option>';
		
		echo $opciones;
    }
	
	public function mapaAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ### Establecemos el filtro por default
        $filtro = " 1=1 ";
 
        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
		$sucursal = $this->_getParam('sucursal');		
		$tecnico = $this->_getParam('tecnico');
        $promotor = $this->_getParam('promotor');


        if($desde != '')
        {
           $filtro .= " AND (tablet_fecha >= '".$desde.":00' ) ";
        }
						
        if($hasta != '')
        {
           $filtro .= " AND (tablet_fecha <= '".$hasta.":00') ";
        }


        // Obtenemos las variantes de la selecciones de tecnicos y promotores

        if(($tecnico != null && $tecnico != "") || ($promotor != null && $promotor != ""))
        {
            if(($tecnico != null && $tecnico != "") && ($promotor != null && $promotor != ""))
            {
                if($tecnico == 0 && $promotor == 0)
                {
                    if($sucursal != 0)
                    {
                        $filtro .= " AND usuario.sucursal_id = ".$sucursal;
                    }
                    else
                    {
                        if($plaza != '')
                        {
                            $plaza = str_replace('_', ',', $plaza);
                            $filtro .= " AND usuario.plaza_id in (".$plaza.")";
                        }                   
                    }
                }
                else if($tecnico == 0 && $promotor != 0)
                {
                    $promotor = str_replace('_', ',', $promotor);
                    $filtro .= " AND (usuario_id in (".$promotor.") OR usuario.tipo = 4)";
                }
                else if($tecnico != 0 && $promotor == 0)
                {
                    $tecnico = str_replace('_', ',', $tecnico);
                    $filtro .= " AND (usuario_id in (".$tecnico.") OR usuario.tipo = 3)";
                }
                else
                {
                    $promotor = str_replace('_', ',', $promotor);
                    $tecnico = str_replace('_', ',', $tecnico);
                    $filtro .= " AND (usuario_id in (".$promotor.",".$tecnico."))";
                }
            }
            else if($tecnico == null && ($promotor != null && $promotor != ""))
            {
                if($promotor == 0)
                {
                    $filtro .= " AND usuario.tipo = 3";
                }
                else
                {
                    $promotor = str_replace('_', ',', $promotor);
                    $filtro .= " AND (usuario_id in (".$promotor."))";
                }
            }
            else if(($tecnico != null && $tecnico != "") && $promotor == null)
            {
                if($tecnico == 0)
                {
                    $filtro .= " AND usuario.tipo = 4";
                }
                else
                {
                    $tecnico = str_replace('_', ',', $tecnico);
                    $filtro .= " AND (usuario_id in (".$tecnico."))";
                }
            }
        }
        //else
        //{
        if($sucursal != 0)
        {
            $filtro .= " AND usuario.sucursal_id = ".$sucursal;
        }
        else
        {
            if($plaza != '')
            {
                $plaza = str_replace('_', ',', $plaza);
                $filtro .= " AND usuario.plaza_id in (".$plaza.")";
            }                   
        }
        //}

        
        /*if($tecnico!=0 && $promotor!=0)
        {
            $filtro .= " AND (usuario_id =".$tecnico."  OR usuario_id=".$promotor.")";
        }
        else if($tecnico!=0 && $promotor==0)
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $filtro .= " AND (usuario_id in (".$tecnico."))";
        }
        else if($tecnico==0 && $promotor!=0)
        {
            $promotor = str_replace('_', ',', $promotor);
            $filtro .= " AND (usuario_id in (".$promotor."))";
        }
		else
		{
			if($sucursal != 0)
			{
				$filtro .= " AND usuario.sucursal_id = ".$sucursal;
			}
			else
			{
				if($plaza != '')
				{
					$plaza = str_replace('_', ',', $plaza);
					$filtro .= " AND usuario.plaza_id in (".$plaza.")";
				}					
			}
		}*/
		
		//echo $filtro; exit;
		//$registros=My_Comun::obtenerFiltro("Ubicacion",$filtro);
		$registros=My_Comun::createQuery("Ubicacion","select usuario_id 'usuario_id', tablet_fecha 'fecha', latitud 'latitud', longitud 'longitud', usuario.nombre 'nombre' from ubicacion inner join usuario on ubicacion.usuario_id = usuario.id where ".$filtro." GROUP BY tablet_fecha order by usuario_id, tablet_id ");   
        
		$this->view->datos='';
		$i=0;
		
		foreach($registros as $registro)
		{

            $gl=explode(":",$registro['latitud']);
            $latitud=$gl[0]+($gl[1]/60);

            $gl=explode(":",$registro['longitud']);
            $longitud=$gl[0]+($gl[1]/60*-1);

	    	$this->view->datos.=$registro['usuario_id'].'|'.$registro['fecha'].'|'.$latitud.'|'.$longitud.'|'.$registro['nombre'].'|'.$registro['tipo'].'|'.$registro['sesion'].'¬';
            $i++;
        }
	
        //echo $this->view->datos; exit;
    }
	//FIN UBICACION

    //PRODUCTIVIDAD TÉCNICA
    public function productividadtecnicaAction()
    {   
        $this->view->headScript()->appendFile('/js/transacciones/reportes/productividadtecnica.js');
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }

    public function gridproductividadtecnicaAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ini_set("memory_limit", "512M");
        ini_set('max_execution_time', 0);

        ### Recibimos los parámetros de paginación y ordenamiento.
        if (isset($_POST['page']) != ""){$page = $_POST['page'];}
        if (isset($_POST['sortname']) != ""){$sortname =$_POST['sortname'];}
        if (isset($_POST['sortorder']) != ""){$sortorder = $_POST['sortorder'];}
        if (isset($_POST['qtype']) != ""){$qtype = $_POST['qtype'];}
        if (isset($_POST['query']) != ""){$query = $_POST['query'];}
        if (isset($_POST['rp']) != ""){$rp = $_POST['rp'];}

        $con = Doctrine_Manager::getInstance()->connection();
 
        //if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
        //    $filtro.=" AND Cobranza.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
        $tecnico = $this->_getParam('tecnico');

        $consulta = "
                    select
                        usuario_id
                        ,nombre
                        ,tipo
                        ,fecha
                        ,contrato
                        ,nodo
                        ,etiqueta
                        ,poste
                        ,cable
                        ,conector
                        ,divisor3
                        ,grapas
                        ,mac_router
                        ,mac_switch
                        ,mac_roku
                        ,salida_switch
                        ,status
                    from
                    (";

        $consulta .= "
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de instalacion'                 as 'tipo'
                        ,ordeninstalacion.fecha_instalacion     as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,ordeninstalacion.cable                 as 'cable'
                        ,ordeninstalacion.conector              as 'conector'
                        ,ordeninstalacion.divisor3              as 'divisor3'
                        ,ordeninstalacion.grapas                as 'grapas'
                        ,ordeninstalacion.mac_router            as 'mac_router'     
                        ,ordeninstalacion.mac_switch            as 'mac_switch'
                        ,ordeninstalacion.mac_roku              as 'mac_roku'
                        ,ordeninstalacion.salida_switch         as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordeninstalacion
                        inner join usuario
                            on ordeninstalacion.tecnico_id = usuario.id
                        inner join cliente
                            on ordeninstalacion.cliente_id = cliente.id
                    where
                        ordeninstalacion.estatus = 1 ";

        if($desde != '')
            $consulta .= " and (ordeninstalacion.fecha_instalacion >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordeninstalacion.fecha_instalacion <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordeninstalacion.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de desconexion'                 as 'tipo'
                        ,ordendesconexion.fecha_desconexion     as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,'N/A'                                  as 'cable'
                        ,'N/A'                                  as 'conector'
                        ,'N/A'                                  as 'divisor3'
                        ,'N/A'                                  as 'grapas'
                        ,'N/A'                                  as 'mac_router'
                        ,'N/A'                                  as 'mac_switch'
                        ,'N/A'                                  as 'mac_roku'
                        ,'N/A'                                  as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordendesconexion
                        inner join usuario
                            on ordendesconexion.tecnico_id = usuario.id
                        inner join cliente
                            on ordendesconexion.cliente_id = cliente.id
                    where
                        ordendesconexion.status = 1 ";

        if($desde != '')
            $consulta .= " and (ordendesconexion.fecha_desconexion >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordendesconexion.fecha_desconexion <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordendesconexion.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de retiro'                      as 'tipo'
                        ,ordendesretiro.fecha_retiro            as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,ordendesretiro.cable                   as 'cable'
                        ,ordendesretiro.conector                as 'conector'
                        ,ordendesretiro.divisor3                as 'divisor3'
                        ,ordendesretiro.grapas                  as 'grapas'
                        ,ordendesretiro.mac_router              as 'mac_router'     
                        ,ordendesretiro.mac_switch              as 'mac_switch'
                        ,ordendesretiro.mac_roku                as 'mac_roku'
                        ,ordendesretiro.salida_switch           as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordendesretiro
                        inner join usuario
                            on ordendesretiro.tecnico_id = usuario.id
                        inner join cliente
                            on ordendesretiro.cliente_id = cliente.id
                    where
                        ordendesretiro.estatus = 1 ";

        if($desde != '')
            $consulta .= " and (ordendesretiro.fecha_retiro >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordendesretiro.fecha_retiro <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordendesretiro.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de baja'                        as 'tipo'
                        ,ordenbaja.fecha_baja                   as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,'N/A'                                  as 'cable'
                        ,'N/A'                                  as 'conector'
                        ,'N/A'                                  as 'divisor3'
                        ,'N/A'                                  as 'grapas'
                        ,'N/A'                                  as 'mac_router'
                        ,'N/A'                                  as 'mac_switch'
                        ,'N/A'                                  as 'mac_roku'
                        ,'N/A'                                  as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordenbaja
                        inner join usuario
                            on ordenbaja.tecnico_id = usuario.id
                        inner join cliente
                            on ordenbaja.cliente_id = cliente.id
                    where
                        ordenbaja.estatus = 1 ";

        if($desde != '')
            $consulta .= " and (ordenbaja.fecha_baja >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordenbaja.fecha_baja <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordenbaja.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de retiro'                      as 'tipo'
                        ,ordenreinstalacion.fecha_instalacion   as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,ordenreinstalacion.cable               as 'cable'
                        ,ordenreinstalacion.conector            as 'conector'
                        ,ordenreinstalacion.divisor3            as 'divisor3'
                        ,ordenreinstalacion.grapas              as 'grapas'
                        ,ordenreinstalacion.mac_router          as 'mac_router'     
                        ,ordenreinstalacion.mac_switch          as 'mac_switch'
                        ,ordenreinstalacion.mac_roku            as 'mac_roku'
                        ,ordenreinstalacion.salida_switch       as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordenreinstalacion
                        inner join usuario
                            on ordenreinstalacion.tecnico_id = usuario.id
                        inner join cliente
                            on ordenreinstalacion.cliente_id = cliente.id
                    where
                        ordenreinstalacion.estatus = 1 ";

        if($desde != '')
            $consulta .= " and (ordenreinstalacion.fecha_instalacion >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordenreinstalacion.fecha_instalacion <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordenreinstalacion.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " ) datos ";
        
        ### Extraemos los registros para formar el arreglo del grid
        $q = $con->execute($consulta)->fetchAll();

        ### Incializamos el arreglo de registros
        $registros = array();

        $registros['total']=count($q);
        $paginas=ceil($registros['total']/$rp);
        if($page>$paginas)
            $page=1;

        $consulta .= "
                    order by
                        $sortname $sortorder
                    limit ".(($page-1)*$rp).", $rp";

        ### Extraemos los registros para formar el arreglo del grid
        $registros['registros']=$con->execute($consulta)->fetchAll();  
        $registros['pagina']=$page;     

        $grid=array();
        $i=0;

        $tecnico_id = 0;
        
        foreach($registros['registros'] as $registro)
        {
            
            if($tecnico_id != $registro['usuario_id'])
            {
                $grid[$i]['tecnico']=$registro['nombre'];
                $tecnico_id = $registro['usuario_id'];
            }
            else
            {
                $grid[$i]['tecnico']="";
            }

            $fecha = explode(" ", $registro['fecha']);

            $grid[$i]['tipo']=$registro['tipo'];
            $grid[$i]['fecha']=($registro['fecha']!="")?$fecha[0]:"";  
            $grid[$i]['contrato']=$registro['contrato']; 
            $grid[$i]['nodo']=$registro['nodo'];
            $grid[$i]['etiqueta']=$registro['etiqueta']; 
            $grid[$i]['poste']=$registro['poste']; 
            $grid[$i]['cable']=$registro['cable'];
            $grid[$i]['conector']=$registro['conector'];
            $grid[$i]['divisor3']=$registro['divisor3'];  
            $grid[$i]['grapas']=$registro['grapas']; 
            $grid[$i]['router']=$registro["mac_router"]; 
            $grid[$i]['switch']=$registro['mac_switch']; 
            $grid[$i]['roku']=$registro['mac_roku']; 
            $grid[$i]['salida']=$registro['salida_switch']; 
            $grid[$i]['estatus']=Cliente::obtenerEstatus($registro['status']);
            
            $i++;
        }
    
        My_Comun::grid2($registros,$grid);
    }

    public function imprimirproductividadtecnicaAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        $con = Doctrine_Manager::getInstance()->connection();
 
        //if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
        //    $filtro.=" AND Cobranza.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
        $tecnico = $this->_getParam('tecnico');

        $consulta = "
                    select
                        usuario_id
                        ,nombre
                        ,tipo
                        ,fecha
                        ,contrato
                        ,nodo
                        ,etiqueta
                        ,poste
                        ,cable
                        ,conector
                        ,divisor3
                        ,grapas
                        ,mac_router
                        ,mac_switch
                        ,mac_roku
                        ,salida_switch
                        ,status
                    from
                    (";

        $consulta .= "
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de instalacion'                 as 'tipo'
                        ,ordeninstalacion.fecha_instalacion     as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,ordeninstalacion.cable                 as 'cable'
                        ,ordeninstalacion.conector              as 'conector'
                        ,ordeninstalacion.divisor3              as 'divisor3'
                        ,ordeninstalacion.grapas                as 'grapas'
                        ,ordeninstalacion.mac_router            as 'mac_router'     
                        ,ordeninstalacion.mac_switch            as 'mac_switch'
                        ,ordeninstalacion.mac_roku              as 'mac_roku'
                        ,ordeninstalacion.salida_switch         as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordeninstalacion
                        inner join usuario
                            on ordeninstalacion.tecnico_id = usuario.id
                        inner join cliente
                            on ordeninstalacion.cliente_id = cliente.id
                    where
                        ordeninstalacion.estatus = 1 ";

        if($desde != '')
            $consulta .= " and (ordeninstalacion.fecha_instalacion >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordeninstalacion.fecha_instalacion <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordeninstalacion.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de desconexion'                 as 'tipo'
                        ,ordendesconexion.fecha_desconexion     as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,'N/A'                                  as 'cable'
                        ,'N/A'                                  as 'conector'
                        ,'N/A'                                  as 'divisor3'
                        ,'N/A'                                  as 'grapas'
                        ,'N/A'                                  as 'mac_router'
                        ,'N/A'                                  as 'mac_switch'
                        ,'N/A'                                  as 'mac_roku'
                        ,'N/A'                                  as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordendesconexion
                        inner join usuario
                            on ordendesconexion.tecnico_id = usuario.id
                        inner join cliente
                            on ordendesconexion.cliente_id = cliente.id
                    where
                        ordendesconexion.status = 1 ";

        if($desde != '')
            $consulta .= " and (ordendesconexion.fecha_desconexion >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordendesconexion.fecha_desconexion <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordendesconexion.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de retiro'                      as 'tipo'
                        ,ordendesretiro.fecha_retiro            as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,ordendesretiro.cable                   as 'cable'
                        ,ordendesretiro.conector                as 'conector'
                        ,ordendesretiro.divisor3                as 'divisor3'
                        ,ordendesretiro.grapas                  as 'grapas'
                        ,ordendesretiro.mac_router              as 'mac_router'     
                        ,ordendesretiro.mac_switch              as 'mac_switch'
                        ,ordendesretiro.mac_roku                as 'mac_roku'
                        ,ordendesretiro.salida_switch           as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordendesretiro
                        inner join usuario
                            on ordendesretiro.tecnico_id = usuario.id
                        inner join cliente
                            on ordendesretiro.cliente_id = cliente.id
                    where
                        ordendesretiro.estatus = 1 ";

        if($desde != '')
            $consulta .= " and (ordendesretiro.fecha_retiro >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordendesretiro.fecha_retiro <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordendesretiro.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de baja'                        as 'tipo'
                        ,ordenbaja.fecha_baja                   as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,'N/A'                                  as 'cable'
                        ,'N/A'                                  as 'conector'
                        ,'N/A'                                  as 'divisor3'
                        ,'N/A'                                  as 'grapas'
                        ,'N/A'                                  as 'mac_router'
                        ,'N/A'                                  as 'mac_switch'
                        ,'N/A'                                  as 'mac_roku'
                        ,'N/A'                                  as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordenbaja
                        inner join usuario
                            on ordenbaja.tecnico_id = usuario.id
                        inner join cliente
                            on ordenbaja.cliente_id = cliente.id
                    where
                        ordenbaja.estatus = 1 ";

        if($desde != '')
            $consulta .= " and (ordenbaja.fecha_baja >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordenbaja.fecha_baja <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordenbaja.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de retiro'                      as 'tipo'
                        ,ordenreinstalacion.fecha_instalacion   as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,ordenreinstalacion.cable               as 'cable'
                        ,ordenreinstalacion.conector            as 'conector'
                        ,ordenreinstalacion.divisor3            as 'divisor3'
                        ,ordenreinstalacion.grapas              as 'grapas'
                        ,ordenreinstalacion.mac_router          as 'mac_router'     
                        ,ordenreinstalacion.mac_switch          as 'mac_switch'
                        ,ordenreinstalacion.mac_roku            as 'mac_roku'
                        ,ordenreinstalacion.salida_switch       as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordenreinstalacion
                        inner join usuario
                            on ordenreinstalacion.tecnico_id = usuario.id
                        inner join cliente
                            on ordenreinstalacion.cliente_id = cliente.id
                    where
                        ordenreinstalacion.estatus = 1 ";

        if($desde != '')
            $consulta .= " and (ordenreinstalacion.fecha_instalacion >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordenreinstalacion.fecha_instalacion <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordenreinstalacion.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " ) datos
                    order by
                        nombre ASC, fecha DESC ";

        ### Extraemos los registros para formar el arreglo del grid
        $registros=$con->execute($consulta)->fetchAll();

        $pdf= new My_Fpdf_Pdf('L');
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE DETALLE DE PRODUCTIVIDAD TÉCNICA");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(23,23,23,23,23,23,23,23,23,23,23,23,23,23,23,23));
                                    
        $pdf->Row(array('TECNICO','TIPO','FECHA','CONTRATO','NODO','ETIQUETA','POSTE', 'CABLE', 'CONECTOR', 'DIVISOR 3', 'GRAPAS','MAC ROUTER','MAC SWITCH','MAC ROKU','SALIDA SWITCH','ESTATUS'),0,1);
        
        $pdf->SetFont('Arial','',10);

        $tecnico = "";
        $tecnico_id = 0;
        $i=0;
        
        foreach($registros as $registro)
        {

            if($tecnico_id != $registro['usuario_id'])
            {
                $tecnico=$registro['nombre'];
                $tecnico_id = $registro['usuario_id'];
            }
            else
            {
                $tecnico="";
            }

            $fecha = explode(" ", $registro['fecha']);

            $pdf->Row
            (
                array
                (
                    $tecnico,
                    $registro['tipo'],
                    ($registro['fecha']!="")?$fecha[0]:"",
                    $registro['contrato'],
                    $registro['nodo'],
                    $registro['etiqueta'],
                    $registro['poste'],
                    $registro['cable'],
                    $registro['conector'],
                    $registro['divisor3'],  
                    $registro['grapas'],
                    $registro['mac_router'],
                    $registro['mac_switch'],
                    $registro['mac_roku'],
                    $registro['salida_switch'],
                    
                    Cliente::obtenerEstatus($registro['status'])
                ),0,1           
            );
            
            $i++;
        }
    
        ini_set("memory_limit", "2048M");
        ini_set('max_execution_time', 0);
        $pdf->Output();
    }

    public function exportarproductividadtecnicaAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        ini_set("memory_limit", "2048M");
        ini_set('max_execution_time', 0);

        $con = Doctrine_Manager::getInstance()->connection();
 
        //if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
        //    $filtro.=" AND Cobranza.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
        $tecnico = $this->_getParam('tecnico');

        $consulta = "
                    select
                        usuario_id
                        ,nombre
                        ,tipo
                        ,fecha
                        ,contrato
                        ,nodo
                        ,etiqueta
                        ,poste
                        ,cable
                        ,conector
                        ,divisor3
                        ,grapas
                        ,mac_router
                        ,mac_switch
                        ,mac_roku
                        ,salida_switch
                        ,status
                    from
                    (";

        $consulta .= "
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de instalacion'                 as 'tipo'
                        ,ordeninstalacion.fecha_instalacion     as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,ordeninstalacion.cable                 as 'cable'
                        ,ordeninstalacion.conector              as 'conector'
                        ,ordeninstalacion.divisor3              as 'divisor3'
                        ,ordeninstalacion.grapas                as 'grapas'
                        ,ordeninstalacion.mac_router            as 'mac_router'     
                        ,ordeninstalacion.mac_switch            as 'mac_switch'
                        ,ordeninstalacion.mac_roku              as 'mac_roku'
                        ,ordeninstalacion.salida_switch         as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordeninstalacion
                        inner join usuario
                            on ordeninstalacion.tecnico_id = usuario.id
                        inner join cliente
                            on ordeninstalacion.cliente_id = cliente.id
                    where
                        ordeninstalacion.estatus = 1 ";

        if($desde != '')
            $consulta .= " and (ordeninstalacion.fecha_instalacion >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordeninstalacion.fecha_instalacion <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordeninstalacion.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de desconexion'                 as 'tipo'
                        ,ordendesconexion.fecha_desconexion     as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,'N/A'                                  as 'cable'
                        ,'N/A'                                  as 'conector'
                        ,'N/A'                                  as 'divisor3'
                        ,'N/A'                                  as 'grapas'
                        ,'N/A'                                  as 'mac_router'
                        ,'N/A'                                  as 'mac_switch'
                        ,'N/A'                                  as 'mac_roku'
                        ,'N/A'                                  as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordendesconexion
                        inner join usuario
                            on ordendesconexion.tecnico_id = usuario.id
                        inner join cliente
                            on ordendesconexion.cliente_id = cliente.id
                    where
                        ordendesconexion.status = 1 ";

        if($desde != '')
            $consulta .= " and (ordendesconexion.fecha_desconexion >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordendesconexion.fecha_desconexion <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordendesconexion.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de retiro'                      as 'tipo'
                        ,ordendesretiro.fecha_retiro            as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,ordendesretiro.cable                   as 'cable'
                        ,ordendesretiro.conector                as 'conector'
                        ,ordendesretiro.divisor3                as 'divisor3'
                        ,ordendesretiro.grapas                  as 'grapas'
                        ,ordendesretiro.mac_router              as 'mac_router'     
                        ,ordendesretiro.mac_switch              as 'mac_switch'
                        ,ordendesretiro.mac_roku                as 'mac_roku'
                        ,ordendesretiro.salida_switch           as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordendesretiro
                        inner join usuario
                            on ordendesretiro.tecnico_id = usuario.id
                        inner join cliente
                            on ordendesretiro.cliente_id = cliente.id
                    where
                        ordendesretiro.estatus = 1 ";

        if($desde != '')
            $consulta .= " and (ordendesretiro.fecha_retiro >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordendesretiro.fecha_retiro <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordendesretiro.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de baja'                        as 'tipo'
                        ,ordenbaja.fecha_baja                   as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,'N/A'                                  as 'cable'
                        ,'N/A'                                  as 'conector'
                        ,'N/A'                                  as 'divisor3'
                        ,'N/A'                                  as 'grapas'
                        ,'N/A'                                  as 'mac_router'
                        ,'N/A'                                  as 'mac_switch'
                        ,'N/A'                                  as 'mac_roku'
                        ,'N/A'                                  as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordenbaja
                        inner join usuario
                            on ordenbaja.tecnico_id = usuario.id
                        inner join cliente
                            on ordenbaja.cliente_id = cliente.id
                    where
                        ordenbaja.estatus = 1 ";

        if($desde != '')
            $consulta .= " and (ordenbaja.fecha_baja >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            $consulta .= " and (ordenbaja.fecha_baja <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordenbaja.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " union
                    select
                        usuario.id                              as 'usuario_id'
                        ,usuario.nombre                         as 'nombre'
                        ,'Orden de retiro'                      as 'tipo'
                        ,ordenreinstalacion.fecha_instalacion   as 'fecha'
                        ,cliente.contrato                       as 'contrato'
                        ,cliente.nodo                           as 'nodo'
                        ,cliente.etiqueta                       as 'etiqueta'
                        ,cliente.poste                          as 'poste'
                        ,ordenreinstalacion.cable               as 'cable'
                        ,ordenreinstalacion.conector            as 'conector'
                        ,ordenreinstalacion.divisor3            as 'divisor3'
                        ,ordenreinstalacion.grapas              as 'grapas'
                        ,ordenreinstalacion.mac_router            as 'mac_router'     
                        ,ordenreinstalacion.mac_switch            as 'mac_switch'
                        ,ordenreinstalacion.mac_roku              as 'mac_roku'
                        ,ordenreinstalacion.salida_switch         as 'salida_switch'
                        ,cliente.status                         as 'status'
                    from
                        ordenreinstalacion
                        inner join usuario
                            on ordenreinstalacion.tecnico_id = usuario.id
                        inner join cliente
                            on ordenreinstalacion.cliente_id = cliente.id
                    where
                        ordenreinstalacion.estatus = 1 ";

        if($desde != '')
            //$consulta .= " and (ordenreinstalacion.fecha_retiro >= '$desde 00:00:00') ";
            $consulta .= " and (ordenreinstalacion.fecha_instalacion >= '$desde 00:00:00') ";
                        
        if($hasta != '')
            //$consulta .= " and (ordenreinstalacion.fecha_retiro <= '$hasta 23:59:59' ) ";
            $consulta .= " and (ordenreinstalacion.fecha_instalacion <= '$hasta 23:59:59' ) ";

        if($plaza != '')
        {
            $plaza = str_replace('_', ',', $plaza);
            $consulta .= " and cliente.plaza_id in (".$plaza.")";
        }

        if($tecnico != '')
        {
            $tecnico = str_replace('_', ',', $tecnico);
            $consulta .= " and ordenreinstalacion.tecnico_id in (".$tecnico.")";
        }

        $consulta .= " ) datos
                    order by
                        nombre ASC, fecha DESC ";

        ### Extraemos los registros para formar el arreglo del grid
        $registros=$con->execute($consulta)->fetchAll();

        //ini_set("memory_limit", "130M");
        //ini_set('max_execution_time', 0);
        $objPHPExcel = new My_PHPExcel_Excel();     
        
        $i=5;
        //Titulos columna
        $columns_name = array
        (
            "A$i" => array(
                    "name" => 'TECNICO',
                    "width" => 40
                    ),
            "B$i" => array(
                    "name" => 'TIPO',
                    "width" => 20
                    ),
            "C$i" => array(
                    "name" => 'FECHA',
                    "width" => 20
                    ),
            "D$i" => array(
                    "name" => 'CONTRATO',
                    "width" => 14
                    ),
            "E$i" => array(
                    "name" => 'NODO',
                    "width" => 10
                    ),
            "F$i" => array(
                    "name" => 'ETIQUETA',
                    "width" => 10
                    ),
            "G$i" => array(
                    "name" => 'POSTE',
                    "width" => 10
                    ),
            "H$i" => array(
                    "name" => 'CABLE',
                    "width" => 10
                    ),
            "I$i" => array(
                    "name" => 'CONECTOR',
                    "width" => 14
                    ),
            "J$i" => array(
                    "name" => 'DIVISOR 3',
                    "width" => 10
                    ),
            "K$i" => array(
                    "name" => 'GRAPAS',
                    "width" => 10
                    ),
            "L$i" => array(
                    "name" => 'MAC ROUTER',
                    "width" => 25
                    ),    
            "M$i" => array(
                    "name" => 'MAC SWITCH',
                    "width" => 25
                    ),   
            "N$i" => array(
                    "name" => 'MAC ROKU',
                    "width" => 25
                    ),   
            "O$i" => array(
                    "name" => 'SALIDA SWITCH',
                    "width" => 25
                    ),    
            "P$i" => array(
                    "name" => 'ESTATUS',
                    "width" => 20
                    )                                       
        );

        $tecnico = "";
        $tecnico_id = 0;
        //Datos tabla
        $data = array();

        foreach($registros as $registro)
        {

            if($tecnico_id != $registro['usuario_id'])
            {
                $tecnico=$registro['nombre'];
                $tecnico_id = $registro['usuario_id'];
            }
            else
            {
                $tecnico="";
            }

            $fecha = explode(" ", $registro['fecha']);

            $i++;
            $data[] = array(                
                "A$i" =>$tecnico,
                "B$i" =>$registro['tipo'],
                "C$i" =>($registro['fecha']!="")?$fecha[0]:"",
                "D$i" =>$registro['contrato'],
                "E$i" =>$registro['nodo'],
                "F$i" =>$registro['etiqueta'],
                "G$i" =>$registro['poste'],
                "H$i" =>$registro['cable'],
                "I$i" =>$registro['conector'],
                "J$i" =>$registro['divisor3'],  
                "K$i" =>$registro['grapas'],
                "L$i" =>$registro['mac_router'],
                "M$i" =>$registro['mac_switch'],
                "N$i" =>$registro['mac_roku'],
                "O$i" =>$registro['salida_switch'],
                "P$i" =>Cliente::obtenerEstatus($registro['status'])

            );
        }
    
        $objPHPExcel->createExcel('ProductividadTecnica', $columns_name, $data, 10, array('rango'=>'A4:F4','size'=>14,'texto'=>'DETALLE DE PRODUCTIVIDAD TÉCNICA'));
    }

    public function obtenertecnicosAction()
    {   
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $resultado = '';
        $cadena = '';

        if(Usuario::tipo()==0){ 

            $q=Doctrine_Query::create()->from('Usuario')->where('plaza_id = '.$this->_getParam('id'))->orderBy('nombre asc')->andWhere('tipo=4 OR tipo=6');
            $tecnicos = $q->execute();            
            
            foreach($tecnicos as $tecnico){

                $resultado .= '<option value="'.$tecnico->id.'">'.$tecnico->nombre.'</option>';
            
                if($cadena==''){
                    $cadena=$tecnico->id;}
                else{
                    $cadena.='_'.$tecnico->id;}
            }
       }

       echo $resultado."|".$cadena;
    }
    //FIN PRODUCTIVIDAD TÉCNICA
}