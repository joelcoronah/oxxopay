<?php

class Transacciones_ServiciostestController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        //$_POST['token']=md5(162);
        $this->usuario=Usuario::obtenerPorToken($_POST['token']);
    }

    public function indexAction()
    {
        //echo "index";
    }

    public function loginAction()
    {
        $usuario=Usuario::autenticaApp($_POST['usuario'], $_POST['clave'], $_POST['tipo']);
        if($usuario==false)
            echo "ERROR";
        else
        {
            $version=Version::obtenerVersion($_POST['tipo']);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
                //ci, codigo identificador, ID del usuario
                $xml .= '<usuario ci="'.$usuario->id.'" fecha="'.date('Y-m-d H:i:s').'" plaza="'.$usuario->plaza_id.'" nombreplaza="'.Plaza::obtenerNombrePlaza($usuario->plaza_id).'" version="'.$version['nombre'].'" url="'.$version['archivo'].'" nombre="'.$this->reformarString($usuario->nombre).'" token="'.md5($usuario->id).'"></usuario>';
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    public function logoutAction()
    {
       if(is_object($this->usuario))
       {
         $this->usuario->gcm_id="";
         $this->usuario->save();
         if(isset($_POST['ubicacion']))
         {
            $this->registrarubicacionAction();
         }
       }
    }

    public function registrationidusuarioAction()
    {

        if(isset($_POST['registrationid']))
        {
            
            if(is_object($this->usuario))
            {
                $this->usuario->gcm_id=$_POST['registrationid'];
                $this->usuario->save();
            }


            $this->cliente->gcm_id=$_POST['registrationid'];
            $this->cliente->save();            
        }

        
    }

    public function obtenerclientesAction()
    {
        if(is_object($this->usuario))
        {
            $filtro=" plaza_id='".$this->usuario->plaza_id."' AND vendedor_id='".$this->usuario->id."'";
            if(!empty($_POST['filtro']))
            {
                $filtro.=" AND (contrato LIKE '%".$_POST['filtro']."%' OR nombre LIKE '%".$_POST['filtro']."%')";
            }
            $clientes=Cliente::obtenerClientes($filtro);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';

            foreach($clientes as $cliente){
                //$xml .= '<cliente nombre="'.$cliente->nombre.'" id="'.$cliente->id.'" contrato="'.$cliente->contrato.'"></cliente>';
                $xml .= '<cliente id="' . $cliente->id . '" nombre="' . $this->reformarString($cliente->nombre) . ' Asignado a: '. $this->reformarString($cliente->Ordeninstalacion[0]->Tecnico->nombre) .'" celular2="' . $this->reformarString($cliente->celular2) . '" celular="' . $this->reformarString($cliente->celular1) . '" calle="' . $this->reformarString($cliente->calle) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($cliente->colonia_id)) . '" comentarios="' . $this->reformarString($cliente->comentarios) . '" comunidad="' . $this->reformarString($cliente->Comunidad->nombre) . '" conceptoContratacion="" contrato="' . $this->reformarString($cliente->contrato) . '" email="' . $this->reformarString($cliente->email) . '" fechaAccion="' . $this->reformarString($cliente->created_at) . '" fechaContratacion="' . $this->reformarString($cliente->fecha_contratacion) . '" idUsuarioToken="' . md5($this->usuario->id) . '" limiteTvs="" numeroExterior="' . $this->reformarString($cliente->no_exterior) . '" numeroInterior="' . $this->reformarString($cliente->no_interior) . '" numeroTvs="" promotor="' . $this->reformarString($cliente->Usuario->nombre) . '" referenciaDomicilio="' . $this->reformarString($cliente->referencia) . '" servicios="" serviciosAdicionales="" telefono="' . $this->reformarString($cliente->telefono) . '"></cliente>';
            }

            $xml .= '</respuesta>';

            echo $xml; 
        }
    }

    public function obtenerclienteAction()
    {

        if(is_object($this->usuario))
        {
            //$_POST['idCliente']=1577;
            $filtro=" plaza_id='".$this->usuario->plaza_id."' AND vendedor_id='".$this->usuario->id."' AND id = '".$_POST['idCliente']."'";
            $cliente=Cliente::obtenerCliente($filtro);

            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            $xml .= '<cliente nombre="'. $this->reformarString($cliente->nombre) .'" id="'. $cliente->id .'" contrato="' .$cliente->contrato. '" calle="'. $this->reformarString($cliente->calle) .'" celular2="'. $this->reformarString($cliente->celular2) .'" celular="'. $this->reformarString($cliente->celular1) .'" colonia="'. $this->reformarString(Colonia::obtenerColonia($cliente->colonia_id)) .'" comentarios="'. $this->reformarString($cliente->comentarios) .'" comunidad="'. $this->reformarString($cliente->Comunidad->nombre) .'" email="'. $this->reformarString($cliente->email) .'" fecha_accion="'. $this->reformarString($cliente->created_at) .'" fecha_contratacion="'. $this->reformarString($cliente->fecha_contratacion). '" numero_exterior="'. $this->reformarString($cliente->no_exterior) .'" numero_interior="'. $this->reformarString($cliente->no_interior) .'" promotor="'. $this->reformarString(Cliente::obtenerPromotor($cliente->promotor)) .'" referencia_domicilio="'. $this->reformarString($cliente->referencia) .'" telefono="'. $this->reformarString($cliente->telefono) .'"></cliente>';
            $xml .= '</respuesta>';

            echo $xml; 
        }

    }

    public function obtenerspinnersAction()
    {
        if(is_object($this->usuario))
        {
            $comunidades=Comunidad::obtenerComunidadesDeMunicipio($this->usuario->Plaza->municipio_id);
            $promotores=My_Comun::obtenerPromotoresApp($this->usuario->plaza_id);
            $parametros=Parametros::obtenerParametro();
            $conceptos=ConceptoDeContratacion::obtenerConceptoParaContratacion($this->usuario->plaza_id);
            $servicios=Servicio::obtenerServiciosAContratar($this->usuario->plaza_id);
            $serviciosAdicionales=Servicio::obtenerServiciosAdicionalesContratar($this->usuario->plaza_id);
            $colonias = Colonia::obtenerColoniasMunicipio($this->usuario->Plaza->municipio_id);

            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            foreach($comunidades as $comunidad)
                $xml .= '<comunidad id="'.$comunidad->id.'" nombre="'. $this->reformarString($comunidad->nombre) .'"></comunidad>';
            /*foreach($promotores as $promotor)
                $xml .= '<promotor id="'.$promotor->id.'" nombre="'.$promotor->nombre.'"></promotor>';*/
            $xml.='<parametros numero_tvs_maximo="5" costo_tv_extra="'.$parametros->extensiones.'" ></parametros>';
            foreach($conceptos as $concepto)
                $xml.='<concepto_contratacion id_tarifa="'.$concepto->id.'"  id="'.$concepto->concepto_de_contratacion_id.'" nombre="'.$this->reformarString($concepto->ConceptoDeContratacion->nombre).'" numero_tvs="'.$concepto->tv.'" precio="'.$concepto->tarifa.'"></concepto_contratacion>';
            foreach($servicios as $servicio)
            {
                $comision = Servicio::obtenerComisionTablet($servicio->servicio_id, $this->usuario->plaza_id);
                $xml.='<servicio id="'.$servicio->servicio_id.'" nombre="'.$this->reformarString($servicio->Servicio->nombre).'" id_tarifa="'.$servicio->id.'" precio="'.$servicio->precio_1a5.'" comision="'.$comision.'"></servicio>';
            }
            foreach($serviciosAdicionales as $servicioAdicional)
                $xml.='<servicio_adicional id="'.$servicioAdicional->servicioadicional_id.'" nombre="'.$this->reformarString($servicioAdicional->Servicio->nombre).'" id_tarifa="'.$servicioAdicional->id.'" precio="'.$servicioAdicional->tarifa.'"></servicio_adicional>';
            foreach($colonias as $colonia)
                $xml.='<colonia id="'.$colonia->id.'" nombre="'. $this->reformarString($colonia->nombre) .'"></colonia>';
            $xml .= '</respuesta>';

            echo $xml;
        }
        
    }

    public function registrarclienteAction()
    {
        echo $cliente= Cliente::guardar("Cliente",$_POST,NULL);

        // A partir del recibo obtenemos el id del cliente
        $qCliente=Doctrine_Query::create()->from("Cobranza")->where('id='.$cliente);
        $dCliente=$qCliente->execute()->getFirst();
        if($cliente>0)
        {
            //echo $dCliente->cliente_id;
            Ordeninstalacion::asignarTecnico($dCliente->cliente_id, $_POST['plaza_id'], true);
        }
    }
    
    public function verificacontratoAction()
    {
        if(is_object($this->usuario))
        {
            // Recorremos las ventas
            $ventas = $_POST['venta'];
    
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';

            if(Cliente::verificarContrato($venta['contrato'], $venta['plaza_id']) != NULL)
            {
                $xml .= '<status valor="existe"></status>';
            }else
            {
                $xml .= '<status valor="ok"></status>';
            }

            $xml .= '</respuesta>';
            echo $xml;
        }   
    }
    
    public function registrarclientetestAction()
    {
        // Verificamos que el usuario sea correcto
        if(is_object($this->usuario))
        {
            // Recorremos las ventas
            $ventas = $_POST['venta'];
    
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            
            $ok = "";
            
            if($ventas != NULL)
            {
                foreach($ventas AS $venta)
                {
                    if(Cliente::verificarContrato($venta['contrato'], $venta['plaza_id']) != NULL)
                    {
                        $xml .= '<cliente id="' .$venta['contrato']. '" valor="error" status="-1"></cliente>';
                    }else
                    {
    
                        $servicios = explode(" ", $venta['servicios']);
                        $serviciosAdicionales = explode(" ", $venta['serviciosAdicionales']);
                        $tarifas = explode(" ", $venta['tarifas']);
                        $comisiones = explode(" ", $venta['comisiones']);
                        
                        if($venta['comisiones'] != NULL){
                            $total_comision = 0;
                            foreach($comisiones as $comision)
                                $total_comision += $comision;
                            $venta['comision'] = $total_comision;
                        }

                        //$venta['fecha_contratacion'] = date('Y-m-d');
    
                        if($venta['serviciosAdicionales'] != NULL){
                            $venta['servicios'] = $servicios;
                            $venta['serviciosAdicionales'] = $serviciosAdicionales;
                            $venta['tarifas'] = $tarifas;
                        }else{
                            $venta['servicios'] = $servicios;
                            $venta['tarifas'] = $tarifas;
                        }

                        $cliente = Cliente::guardar("Cliente", $venta, NULL);
                        
                        // A partir del recibo obtenemos el id del cliente
                        $qCliente = Doctrine_Query::create()->from("Cobranza")->where('id='.$cliente);
                        $dCliente = $qCliente->execute()->getFirst();
                        if($cliente > 0)
                        {
                            Ordeninstalacion::asignarTecnico($dCliente->cliente_id, $venta['plaza_id'], true);
                            $ok .= '<cliente id="' .$venta['contrato']. '" valor="ok" status="' .$cliente. '"></cliente>';
                        }else
                        {
                            $xml .= '<cliente id="' .$venta['contrato']. '" valor="error" status="' .$cliente. '"></cliente>';
                        }

                        /*if($venta['plaza_id'] == 9)
                        {
                            $mail = new Zend_Mail();
                            $mail->setBodyText($venta['latitud'].' : '.$venta['longitud']);
                            $mail->setFrom('info@supertv.com.mx', 'Ultracable');
                            $mail->addTo('ricardo_sonora@avansys.com.mx', 'Richard');
                            $mail->setSubject('Log Venta');
                            $mail->send();
                        }*/
                        
                    }
                }
            }
            $xml .= $ok;
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    

    

    ##################################
    ####Ordenes de instalación########
    ##################################

    public function oiobtenerclientesAction()
    {
        if(is_object($this->usuario))
        {
            $ordenes=Ordeninstalacion::obtenerOrdenesDeTecnico($this->usuario->id);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            foreach($ordenes as $cliente)
            {
                 $xml .= '<orden cliente="'.$cliente->Cliente->nombre.'" cliente_id="'.$cliente->Cliente->id.'" id="'.$cliente->id.'" contrato="'.$cliente->Cliente->contrato.'" calle="'.$cliente->Cliente->calle.'" celular2="'.$cliente->Cliente->celular2.'" celular="'.$cliente->Cliente->celular1.'" colonia="'.Colonia::obtenerColonia($cliente->Cliente->colonia_id).'" comentarios="'.$cliente->Cliente->comentarios.'" comunidad="'.$cliente->Cliente->Comunidad->nombre.'" email="'.$cliente->Cliente->email.'" fecha_accion="'.$cliente->Cliente->created_at.'" fecha_contratacion="'.$cliente->Cliente->fecha_contratacion.'" numero_exterior="'.$cliente->Cliente->no_exterior.'" numero_interior="'.$cliente->Cliente->no_interior.'" promotor="'.$cliente->Cliente->Usuario->nombre.'" referencia_domicilio="'.$cliente->Cliente->referencia.'" telefono="'.$cliente->Cliente->telefono.'" tecnico="'.$cliente->Tecnico->nombre.'" estatus="0"></orden>';
            }
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    public function oiobtenerclienteAction()
    {
        if(is_object($this->usuario))
        {
            $cliente=Ordeninstalacion::obtenerDetalleOrden($_POST['idOrdenInstalacion']);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            $xml .= '<orden_detalle nombre="'.$cliente->nombre.'" cliente_id="'.$cliente->Cliente->id.'" id="'.$cliente->id.'" contrato="'.$cliente->contrato.'" calle="'.$cliente->calle.'" celular2="'.$cliente->celular2.'" celular="'.$cliente->celular1.'" colonia="'.Colonia::obtenerColonia($cliente->colonia_id).'" comentarios="'.$cliente->comentarios.'" comunidad="'.$cliente->Comunidad->nombre.'" email="'.$cliente->email.'" fecha_accion="'.$cliente->created_at.'" fecha_contratacion="'.$cliente->fecha_contratacion.'" numero_exterior="'.$cliente->no_exterior.'" numero_interior="'.$cliente->no_interior.'" promotor="'.$cliente->Usuario->nombre.'" referencia_domicilio="'.$cliente->referencia.'" telefono="'.$cliente->telefono.'" tecnico="'.$cliente->Tecnico->nombre.'"></orden_detalle>';
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    public function descargarordeninstalacionAction()
    {
        if(is_object($this->usuario))
        {
            $_POST['tecnico_id']=$this->usuario->id;
            Ordeninstalacion::actualiza($_POST);
        }
    }


    ##################################
    ####Ordenes de reconexion########
    ##################################

    public function reconexionobtenerclientesAction()
    {
        if(is_object($this->usuario))
        {  
            $_POST['token']=$this->usuario->id;
            $ordenes=Ordendesreconexion::obtenerOrdenesDeTecnico($_POST);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            foreach($ordenes as $cliente)
            {
                 $xml .= '<orden cliente="'.$cliente->Cliente->nombre.'" cliente_id="'.$cliente->Cliente->id.'" id="'.$cliente->id.'" contrato="'.$cliente->Cliente->contrato.'" calle="'.$cliente->Cliente->calle.'" colonia="'.Colonia::obtenerColonia($cliente->Cliente->colonia_id).'" numero_exterior="'.$cliente->Cliente->no_exterior.'" numero_interior="'.$cliente->Cliente->no_interior.'" ultimo_mes_cubierto="' . $orden->Cliente->Ultimomescubierto . '" estatus="0"></orden>';
            }
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    public function reconexionobtenerclienteAction()
    {
        if(is_object($this->usuario))
        {
            $cliente=Ordendesreconexion::obtenerDetalleOrden($_POST['idOrdenReconexion']);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            $xml .= '<orden cliente="'.$cliente->Cliente->nombre.'" cliente_id="'.$cliente->Cliente->id.'" id="'.$cliente->id.'" contrato="'.$cliente->Cliente->contrato.'" calle="'.$cliente->Cliente->calle.'" colonia="'.Colonia::obtenerColonia($cliente->Cliente->colonia_id).'" numero_exterior="'.$cliente->Cliente->no_exterior.'" numero_interior="'.$cliente->Cliente->no_interior.'" ultimo_mes_cubierto="' . $cliente->Cliente->ultimomescubierto . '" tecnico="' . Ordendesreconexion::obtenerTecnico($this->usuario->id) . '"></orden>';
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    public function descargarordenreconexionAction()
    {
        if(is_object($this->usuario))
        {
            $_POST['tecnico_id']=$this->usuario->id;
            $_POST['fecha_reconexion'] = date('Y-m-d');
            Ordendesreconexion::actualiza($_POST);
        }
    }


    ##################################
    #### Ordenes de desconexión  #####
    ##################################
    public function odobtenerclientesAction()
    {
        if(is_object($this->usuario))
        {
            $ordenes=Ordendesconexion::obtenerOrdenesDeTecnico($this->usuario->id);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            foreach($ordenes as $cliente)
            {
                $xml .= '<orden nombre="'.$cliente->Cliente->nombre.'" cliente_id="'.$cliente->Cliente->id.'" id="'.$cliente->id.'" contrato="'.$cliente->Cliente->contrato.', Etiqueta:'.$cliente->Cliente->etiqueta.', Nodo:'.$cliente->Cliente->nodo.', Poste:'.$cliente->Cliente->poste.'" calle="'.$cliente->Cliente->calle.'" celular2="'.$cliente->Cliente->celular2.'" celular="'.$cliente->Cliente->celular1.'" colonia="'.Colonia::obtenerColonia($cliente->Cliente->colonia_id).'" comentarios="'.$cliente->Cliente->comentarios.'" comunidad="'.$cliente->Cliente->Comunidad->nombre.'" email="'.$cliente->Cliente->email.'" fecha_accion="'.$cliente->Cliente->created_at.'" fecha_contratacion="'.$cliente->Cliente->fecha_contratacion.'" numero_exterior="'.$cliente->Cliente->no_exterior.'" numero_interior="'.$cliente->Cliente->no_interior.'" promotor="'.$cliente->Cliente->Usuario->nombre.'" referencia_domicilio="'.$cliente->Cliente->referencia.'" telefono="'.$cliente->Cliente->telefono.'" tecnico="'.$cliente->Tecnico->nombre.'"></orden>';
            }
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    public function odobtenerclienteAction()
    {
        if(is_object($this->usuario))
        {
            $cliente=Ordendesconexion::obtenerDetalleOrden($_POST['idOrdenDesconexion']);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            $xml .= '<orden_detalle nombre="'.$cliente->nombre.'" id="'.$cliente->id.'" contrato="'.$cliente->Cliente->contrato.', Etiqueta:'.$cliente->Cliente->etiqueta.', Nodo:'.$cliente->Cliente->nodo.', Poste:'.$cliente->Cliente->poste.'" calle="'.$cliente->calle.'" celular2="'.$cliente->celular2.'" celular="'.$cliente->celular1.'" colonia="'.Colonia::obtenerColonia($cliente->colonia_id).'" comentarios="'.$cliente->comentarios.'" comunidad="'.$cliente->Comunidad->nombre.'" email="'.$cliente->email.'" fecha_accion="'.$cliente->created_at.'" fecha_contratacion="'.$cliente->fecha_contratacion.'" numero_exterior="'.$cliente->no_exterior.'" numero_interior="'.$cliente->no_interior.'" promotor="'.$cliente->Usuario->nombre.'" referencia_domicilio="'.$cliente->referencia.'" telefono="'.$cliente->telefono.'" tecnico="'.$cliente->Tecnico->nombre.'"></cliente>';
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    public function descargarordendesconexionAction()
    {
        if(is_object($this->usuario))
        {
            Ordendesconexion::actualiza($_POST);
        }
    }


    ##################################
    ####   Ordenes de retiro  ########
    ##################################
    public function orobtenerclientesAction()
    {
        if(is_object($this->usuario))
        {
            $ordenes=Ordendesretiro::obtenerOrdenesDeTecnico($this->usuario->id);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            foreach($ordenes as $cliente)
            {
                $xml .= '<orden nombre="'.$this->reformarString($cliente->Cliente->nombre).'" id="'.$cliente->id.'" contrato="'.$cliente->Cliente->contrato.'" calle="'.$this->reformarString($cliente->Cliente->calle).'" celular2="'.$cliente->Cliente->celular2.'" celular="'.$cliente->Cliente->celular1.'" colonia="'.$this->reformarString(Colonia::obtenerColonia($cliente->Cliente->colonia_id)).'" comentarios="'.$this->reformarString($cliente->Cliente->comentarios).'" comunidad="'.$this->reformarString($cliente->Cliente->Comunidad->nombre).'" email="'.$cliente->Cliente->email.'" fecha_accion="'.$cliente->Cliente->created_at.'" fecha_contratacion="'.$cliente->Cliente->fecha_contratacion.'" numero_exterior="'.$cliente->Cliente->no_exterior.'" numero_interior="'.$cliente->Cliente->no_interior.'" promotor="'.$this->reformarString($cliente->Cliente->Usuario->nombre).'" referencia_domicilio="'.$this->reformarString($cliente->Cliente->referencia).'" telefono="'.$cliente->Cliente->telefono.'" tecnico="'.$this->reformarString($cliente->Tecnico->nombre).'"></orden>';
            }
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    public function orobtenerclienteAction()
    {
        if(is_object($this->usuario))
        {
            $cliente=Ordendesretiro::obtenerDetalleOrden($_POST['idOrdenRetiro']);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            $xml .= '<orden_detalle nombre="'.$cliente->nombre.'" id="'.$cliente->id.'" contrato="'.$cliente->contrato.'" calle="'.$cliente->calle.'" celular2="'.$cliente->celular2.'" celular="'.$cliente->celular1.'" colonia="'.Colonia::obtenerColonia($cliente->colonia_id).'" comentarios="'.$cliente->comentarios.'" comunidad="'.$cliente->Comunidad->nombre.'" email="'.$cliente->email.'" fecha_accion="'.$cliente->created_at.'" fecha_contratacion="'.$cliente->fecha_contratacion.'" numero_exterior="'.$cliente->no_exterior.'" numero_interior="'.$cliente->no_interior.'" promotor="'.$cliente->Usuario->nombre.'" referencia_domicilio="'.$cliente->referencia.'" telefono="'.$cliente->telefono.'" tecnico="'.$cliente->Tecnico->nombre.'"></cliente>';
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    public function descargarordenretiroAction()
    {
        if(is_object($this->usuario))
        {
            Ordendesretiro::actualiza($_POST);
        }
    }


    ##################################
    ###  Ordenes de reinstalación  ###
    ##################################
    public function oriobtenerclientesAction()
    {
        if(is_object($this->usuario))
        {
            $ordenes=Ordenreinstalacion::obtenerOrdenesDeTecnico($this->usuario->id);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            foreach($ordenes as $cliente)
            {
                $xml .= '<orden nombre="'.$this->reformarString($cliente->Cliente->nombre).'" id="'.$cliente->id.'" contrato="'.$cliente->Cliente->contrato.'" calle="'.$this->reformarString($cliente->Cliente->calle).'" celular2="'.$cliente->Cliente->celular2.'" celular="'.$cliente->Cliente->celular1.'" colonia="'.$this->reformarString(Colonia::obtenerColonia($cliente->Cliente->colonia_id)).'" comentarios="'.$this->reformarString($cliente->Cliente->comentarios).'" comunidad="'.$this->reformarString($cliente->Cliente->Comunidad->nombre).'" email="'.$cliente->Cliente->email.'" fecha_accion="'.$cliente->Cliente->created_at.'" fecha_contratacion="'.$cliente->Cliente->fecha_contratacion.'" numero_exterior="'.$cliente->Cliente->no_exterior.'" numero_interior="'.$cliente->Cliente->no_interior.'" promotor="'.$this->reformarString($cliente->Cliente->Usuario->nombre).'" referencia_domicilio="'.$this->reformarString($cliente->Cliente->referencia).'" telefono="'.$this->reformarString($cliente->Cliente->telefono).'" tecnico="'.$this->reformarString($cliente->Tecnico->nombre).'"></orden>';
            }
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    public function oriobtenerclienteAction()
    {
        if(is_object($this->usuario))
        {
            $cliente=Ordenreinstalacion::obtenerDetalleOrden($_POST['idOrdenReinstalacion']);
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            $xml .= '<orden_detalle nombre="'.$this->reformarString($cliente->nombre).'" id="'.$cliente->id.'" contrato="'.$cliente->contrato.'" calle="'.$this->reformarString($cliente->calle).'" celular2="'.$cliente->celular2.'" celular="'.$cliente->celular1.'" colonia="'.$this->reformarString(Colonia::obtenerColonia($cliente->colonia_id)).'" comentarios="'.$this->reformarString($cliente->comentarios).'" comunidad="'.$this->reformarString($cliente->Comunidad->nombre).'" email="'.$cliente->email.'" fecha_accion="'.$cliente->created_at.'" fecha_contratacion="'.$cliente->fecha_contratacion.'" numero_exterior="'.$cliente->no_exterior.'" numero_interior="'.$cliente->no_interior.'" promotor="'.$this->reformarString($cliente->Usuario->nombre).'" referencia_domicilio="'.$this->reformarString($cliente->referencia).'" telefono="'.$cliente->telefono.'" tecnico="'.$this->reformarString($cliente->Tecnico->nombre).'"></cliente>';
            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    public function descargarordenreinstalacionAction()
    {
        if(is_object($this->usuario))
        {
            Ordenreinstalacion::actualiza($_POST);
        }
    }

    public function obtenerfallastecnicasAction()
    {
        if(is_object($this->usuario))
        {
            $fallas = ReportarFallas::obtenerFallasDeUnTecnico(162);

            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';

            foreach($fallas AS $falla){
                $xml .= '<falla id="' . $falla->id . '" cliente="' . $this->reformarString($falla->Cliente->nombre) . '" tipoFalla="' . $this->reformarString($falla->TipoDeFalla->nombre) . '" estatus="' . $this->reformarString($falla->EstatusDeFalla->nombre) . '" fechaReporte="' . $falla->fecha_reporte . '" fechaSolucion="' . $falla->fecha_solucion . '" tecnico="' . $this->reformarString($falla->Tecnico->nombre) . '" comentarios="' . $this->reformarString($falla->comentarios) . '"></falla>';

                $fallaHistorial = HistorialDeFallas::obtenerHistorial($falla->id);

                foreach($fallaHistorial AS $registro){
                    $xml .= '<falla_detalle id="' . $registro->id . '" tecnico="' . $this->reformarString($registro->Tecnico->nombre) . '" fecha="' . $registro->fecha . '" estatus="' . $this->reformarString($registro->EstatusDeFalla->nombre) . '" comentarios="' . $this->reformarString($registro->comentarios) . '" idFalla="' . $registro->reportar_falla_id . '"></falla_detalle>';
                }
            }

            $xml .= '</respuesta>';
            echo $xml;
        }
    }

    ########################################################

    public function obtenerordenesAction()
    {
        // Verificamos que el usuario sea correcto
        if(is_object($this->usuario))
        {
            // El usuario es correcto
                $_POST['tecnico_id'] = $this->usuario->id;

            // Formamos el XML de respuesta
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';

            // Obtenemos las órdenes
            switch($_POST['tipo']){
                case 0:
                    // Ordenes de instalación
                    $ordenes = Ordeninstalacion::obtenerOrdenesDeTecnico($_POST['tecnico_id']);
                break;

                case 1:
                    // Ordenes de desconexión
                    $ordenes = Ordendesconexion::obtenerOrdenesDeTecnico($_POST['tecnico_id']);
                break;

                case 2:
                    // Ordenes de retiro
                    $ordenes = Ordendesretiro::obtenerOrdenesDeTecnico($_POST['tecnico_id']);
                break;

                case 3:
                    // Ordenes de reinstalación
                    $ordenes = Ordenreinstalacion::obtenerOrdenesDeTecnico($_POST['tecnico_id']);
                break;
                
                case 4:
                    // Ordenes de reconexion
                    $ordenes = Ordendesreconexion::obtenerOrdenesDeTecnico($_POST['tecnico_id']);
                break;
                
                case 5:
                    // Ordenes de cambio de domicilio retirar
                    $ordenes = Ordencambiodomicilio::obtenerOrdenesDeTecnicoRetirar($_POST['tecnico_id']);
                break;

                case 6:
                    // Ordenes de cambio de domicilio instalar
                    $ordenes = Ordencambiodomicilio::obtenerOrdenesDeTecnicoInstalar($_POST['tecnico_id']);
                break;

                case 7:
                    // Ordenes de extension adicional
                    $ordenes = Ordenextensionadicional::obtenerOrdenesDeTecnico($_POST['tecnico_id']);
                break;

                case 8:
                    // Ordenes de baja voluntaria
                    $ordenes = Bajavoluntaria::obtenerOrdenesDeTecnico($_POST['tecnico_id']);
                break;
            }

           
            if($_POST['filtro'] != "" && $_POST['filtro'] != NULL)
            {
                // Recorremos las órdenes
                if($_POST['tipo'] == 5 || $_POST['tipo'] == 6)
                {
                    foreach($ordenes as $orden)
                    {
                        if(strpos($orden->Cliente->nombre, $_POST['filtro']) !== false || strpos($orden->Cliente->contrato, $_POST['filtro']) !== false)
                        {
                            $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString(Ordencambiodomicilio::obtenerTecnico($this->usuario->id)) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '"></orden>';
                        }
                    }
                }else if($_POST['tipo'] == 1){
                    foreach($ordenes as $orden)
                    {
                        if(strpos($orden->Cliente->nombre, $_POST['filtro']) !== false || strpos($orden->Cliente->contrato, $_POST['filtro']) !== false)
                        {
                            $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . " Col: " . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . " Et: " . $this->reformarString($orden->Cliente->etiqueta) . " Nod: " . $this->reformarString($orden->Cliente->nodo) . " Pos: " . $this->reformarString($orden->Cliente->poste) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString($orden->Tecnico->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '"></orden>';
                        }
                    }
                }
                else
                {
                    foreach($ordenes as $orden)
                    {
                        if(strpos($orden->Cliente->nombre, $_POST['filtro']) !== false || strpos($orden->Cliente->contrato, $_POST['filtro']) !== false)
                        {
                            $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString($orden->Tecnico->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '"></orden>';
                        }
                    }
                }
            }else{
                // Recorremos las órdenes
                if($_POST['tipo'] == 5 || $_POST['tipo'] == 6)
                {
                    foreach($ordenes as $orden)
                    {
                        $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString(Ordencambiodomicilio::obtenerTecnico($this->usuario->id)) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '"></orden>'; 
                    }
                }else if($_POST['tipo'] == 1){
                    foreach($ordenes as $orden)
                    {
                        $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . " Col: " . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . " Et: " . $this->reformarString($orden->Cliente->etiqueta) . " Nod: " . $this->reformarString($orden->Cliente->nodo) . " Pos: " . $this->reformarString($orden->Cliente->poste) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString($orden->Tecnico->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '"></orden>';
                    }
                }
                else
                {
                    foreach($ordenes as $orden)
                    {
                        $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString($orden->Tecnico->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '"></orden>';
                    }
                }
            }

            // Enviamos la respuesta
            $xml .= '</respuesta>';
            echo $xml;
        }

        else {
            // El usuario no es correcto
        }
    }

    ########################################################


    public function obtenerordenesporagrupamientoAction()
    {
        // Verificamos que el usuario sea correcto
        /*$_POST['tecnico_id']=233;
        $_POST['tipo']=0;
        $_POST['agrupar_por']=1;*/
        if(is_object($this->usuario))
        {
            // El usuario es correcto
            $_POST['tecnico_id'] = $this->usuario->id;

            // Formamos el XML de respuesta
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';

            // Obtenemos las órdenes
            switch($_POST['tipo'])
            {
                case 0:
                    // Ordenes de instalación
                    $ordenes = Ordeninstalacion::obtenerOrdenesDeTecnicoPorAgrupamiento($_POST['tecnico_id']);
                    $agrupamiento=My_Comun::prepararQuery("Ordeninstalacion o");
                    
                break;

                case 1:
                    // Ordenes de desconexión
                    $ordenes = Ordendesconexion::obtenerOrdenesDeTecnicoPorAgrupamiento($_POST['tecnico_id']);
                    $agrupamiento=My_Comun::prepararQuery("Ordendesconexion o");
                break;

                case 2:
                    // Ordenes de retiro
                    $ordenes = Ordendesretiro::obtenerOrdenesDeTecnicoPorAgrupamiento($_POST['tecnico_id']);
                    $agrupamiento=My_Comun::prepararQuery("Ordendesretiro o");
                break;

                case 3:
                    // Ordenes de reinstalación
                    $ordenes = Ordenreinstalacion::obtenerOrdenesDeTecnicoPorAgrupamiento($_POST['tecnico_id']);
                    $agrupamiento=My_Comun::prepararQuery("Ordenreinstalacion o");
                break;
                
                case 4:
                    // Ordenes de reconexion
                    $ordenes = Ordendesreconexion::obtenerOrdenesDeTecnicoPorAgrupamiento($_POST['tecnico_id']);
                    $agrupamiento=My_Comun::prepararQuery("Ordendesreconexion o");
                break;
                
                case 5:
                    // Ordenes de cambio de domicilio retirar
                    $ordenes = Ordencambiodomicilio::obtenerOrdenesDeTecnicoRetirarPorAgrupamiento($_POST['tecnico_id']);
                    $agrupamiento=My_Comun::prepararQuery("Ordencambiodomicilio o");
                break;

                case 6:
                    // Ordenes de cambio de domicilio instalar
                    $ordenes = Ordencambiodomicilio::obtenerOrdenesDeTecnicoInstalarPorAgrupamiento($_POST['tecnico_id']);
                    $agrupamiento=My_Comun::prepararQuery("Ordencambiodomicilio o");
                break;

                case 7:
                    // Ordenes de extension adicional
                    $ordenes = Ordenextensionadicional::obtenerOrdenesDeTecnicoPorAgrupamiento($_POST['tecnico_id']);
                    $agrupamiento=My_Comun::prepararQuery("Ordenextensionadicional o");
                break;

                case 8:
                    // Ordenes de baja voluntaria
                    $ordenes = Bajavoluntaria::obtenerOrdenesDeTecnicoPorAgrupamiento($_POST['tecnico_id']);
                    $agrupamiento=My_Comun::prepararQuery("Bajavoluntaria o");
                break;

                case 9:
                    // Fallas reportadas
                    $ordenes = ReportarFallas::obtenerOrdenesDeTecnicoPorAgrupamiento($_POST['tecnico_id']);
                    $agrupamiento=My_Comun::prepararQuery("ReportarFallas o");
                break;
            }


            if($_POST['agrupar_por']>0)
            {
                $agrupamiento->innerJoin("o.Cliente c");
                $agrupamiento=$this->whereAction($agrupamiento, $_POST['filtro'],'','','','','', $_POST['tecnico_id'], $_POST['tipo']);

                //echo $agrupamiento->getSqlQuery(); exit;
                if($_POST['tipo'] != 9)
                {
                    $orden=My_Comun::configurarOrden($_POST['nivel_1'],$_POST['nivel_2'],$_POST['nivel_3'],"o.created_at desc");
                }
                else
                {
                    $orden=My_Comun::configurarOrden($_POST['nivel_1'],$_POST['nivel_2'],$_POST['nivel_3'],"o.fecha_reporte desc");
                }
                $agrupamiento->orderBy($orden);                
                $grupos=My_Comun::configurarAgrupamiento($agrupamiento,$_POST['agrupar_por']);
            }


            if(count($grupos)==0)
                $grupos[]=-1;


            foreach($grupos as $titulo=>$grupo)
            {
                if($_POST['tipo'] != 9)
                {
                    $orden=My_Comun::configurarOrden($_POST['nivel_1'],$_POST['nivel_2'],$_POST['nivel_3'],"o.created_at desc");
                }
                else
                {
                    $orden=My_Comun::configurarOrden($_POST['nivel_1'],$_POST['nivel_2'],$_POST['nivel_3'],"o.fecha_reporte desc");
                }
                
                $ordenes->orderBy($orden);


                if($grupo!=-1)
                {
                    switch($_POST['agrupar_por'])
                    {
                        case 1: $colonia=$grupo; $titulo="Colonia: "; break;
                        case 2: $calle=$grupo; $titulo="Calle: "; break;
                        case 3: $etiqueta=$grupo; $titulo="Etiqueta: "; break;
                        case 4: $nodo=$grupo; $titulo="Nodo: "; break;
                        case 5: $poste=$grupo; $titulo="Poste: "; break;
                    }
                    $xml.='<grupo valor="'.$this->reformarString($titulo).'" nombre="'.$this->reformarString($grupo).'">';
                }
                else
                    $xml.='<grupo valor="" nombre="">';

                $ordenes=$this->whereAction($ordenes,$_POST['filtro'],$colonia,$calle, $etiqueta,$nodo,$poste,$_POST['tecnico_id'], $_POST['tipo']);

            $mail = new Zend_Mail();
            $mail->setFrom('info@supertv.com.mx', 'Ultracable');
            $mail->addTo('ricardo_sonora@avansys.com.mx', 'Richard');
            $mail->setSubject('WHEREACTION');
            $infodata = $ordenes->getSqlQuery();
            $mail->setBodyText($infodata);
            $mail->send();

                $registros=  $ordenes->execute();

                if($_POST['tipo'] == 5)
                {
                    foreach($registros as $orden)
                    {
                        $xml .= '<orden calle="' . $this->reformarString($orden->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString( Colonia::obtenerColonia($orden->colonia_id)) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->no_exterior) . '" numero_interior="' . $this->reformarString($orden->no_interior) . '" tecnico="' . $this->reformarString(Ordencambiodomicilio::obtenerTecnico($this->usuario->id)) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '" idCliente="' . $orden->Cliente->id . '" etiqueta="' . $orden->Cliente->etiqueta . '"  nodo="' . $orden->Cliente->nodo . '"  poste="' . $orden->Cliente->poste . '"></orden>'; 
                    }
                }else if($_POST['tipo'] == 6)
                {
                    foreach($registros as $orden)
                    {
                        $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString($orden->Cliente->Colonia->nombre) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString(Ordencambiodomicilio::obtenerTecnico($this->usuario->id)) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '" idCliente="' . $orden->Cliente->id . '"  etiqueta="' . $orden->Cliente->etiqueta . '"  nodo="' . $orden->Cliente->nodo . '"  poste="' . $orden->Cliente->poste . '"></orden>'; 
                    }
                }else if($_POST['tipo'] == 1)
                {
                    foreach($registros as $orden)
                    {
                        $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString($orden->Cliente->Colonia->nombre) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . ", Col: " . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . ", Et: " . $this->reformarString($orden->Cliente->etiqueta) . ", Nod: " . $this->reformarString($orden->Cliente->nodo) . ", Pos: " . $this->reformarString($orden->Cliente->poste) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString($orden->Tecnico->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '" idCliente="' . $orden->Cliente->id . '"  etiqueta="' . $orden->Cliente->etiqueta . '"  nodo="' . $orden->Cliente->nodo . '"  poste="' . $orden->Cliente->poste . '"></orden>';
                    }
                }else if($_POST['tipo'] == 9)
                {
                    foreach($registros as $orden)
                    {
                        $xml .= '<orden cliente="' . $this->reformarString($orden->Cliente->nombre) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . '" tipo_falla="' . $this->reformarString(TipoDeFalla::obtenerTipoDeFallaPorId($orden->tipo_falla)) . '" identificador="' . $this->reformarString($orden->id) . '" tecnico="' . $this->reformarString($orden->Tecnico->nombre) .'" idCliente="' . $orden->Cliente->id . '" etiqueta="' . $orden->Cliente->etiqueta . '"  nodo="' . $orden->Cliente->nodo . '"  poste="' . $orden->Cliente->poste . '"></orden>';
                    }
                }else if($_POST['tipo'] == 7)
                {
                    foreach($registros as $orden)
                    {
                        $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString($orden->Cliente->Colonia->nombre) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString($orden->Tecnico->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '" idCliente="' . $orden->Cliente->id . '" tvs="' . $this->reformarString($orden->televisores) . '"  etiqueta="' . $orden->Cliente->etiqueta . '"  nodo="' . $orden->Cliente->nodo . '"  poste="' . $orden->Cliente->poste . '"></orden>';
                    }
                }else if($_POST['tipo'] == 8)
                {
                    foreach($registros as $orden)
                    {
                        $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString($orden->Cliente->Colonia->nombre) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString($orden->Tecnico->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '" idCliente="' . $orden->Cliente->id . '" etiqueta="' . $orden->Cliente->etiqueta . '"  nodo="' . $orden->Cliente->nodo . '"  poste="' . $orden->Cliente->poste . '"></orden>';
                    }
                }else
                {
                    foreach($registros as $orden)
                    {
                        $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString($orden->Cliente->Colonia->nombre) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString($orden->Tecnico->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '" idCliente="' . $orden->Cliente->id . '"  etiqueta="' . $orden->Cliente->etiqueta . '"  nodo="' . $orden->Cliente->nodo . '"  poste="' . $orden->Cliente->poste . '"></orden>';
                    }
                }
                

                $xml.="</grupo>";
            }
            
            // Enviamos la respuesta
            $xml .= '</respuesta>';
            //header ("Content-Type:text/xml");
            echo $xml;
        }
        else 
        {
            echo "algo";
            // El usuario no es correcto
        }
    }

    public function whereAction($query, $filtro, $colonia, $calle, $etiqueta, $nodo, $poste, $tecnico=0, $tipo)
    {
        //Hacemos la validación de qué tipo de orden provienen los datos
        switch ($tipo) 
        {
            case 0:
                // Ordenes de instalación
                $query->where('o.estatus=0');
                if($tecnico>0)
                    $query->andWhere('o.tecnico_id="'.$tecnico.'"');
            break;

            case 1:
                // Ordenes de desconexión
                $query->where('o.status=0 AND c.status = 2');
                if($tecnico>0)
                    $query->andWhere('o.tecnico_id="'.$tecnico.'"');
            break;

            case 2:
                // Ordenes de retiro
                $query->where('o.estatus=0 AND c.status = 4');
                if($tecnico>0)
                    $query->andWhere('o.tecnico_id="'.$tecnico.'"');
            break;

            case 3:
                // Ordenes de reinstalación
                $query->where('o.estatus=0')->andWhere('c.status=10');
                if($tecnico>0)
                    $query->andWhere('o.tecnico_id="'.$tecnico.'"');
            break;
            
            case 4:
                // Ordenes de reconexion
                $query->where('o.status=0');
                if($tecnico>0)
                    $query->andWhere('o.tecnico_id="'.$tecnico.'"');
            break;
            
            case 5:
                // Ordenes de cambio de domicilio retirar
                $query->where('o.status_retiro=0');
                if($tecnico>0)
                    $query->andWhere('o.tecnico_retiro="'.$tecnico.'"');
            break;

            case 6:
                // Ordenes de cambio de domicilio instalar
                $query->where('o.status=0');
                if($tecnico>0)
                    $query->andWhere('o.tecnico_instalo="'.$tecnico.'"');
            break;

            case 7:
                // Ordenes de extension adicional
                $query->where('o.status=0');
                if($tecnico>0)
                    $query->andWhere('o.tecnico_id="'.$tecnico.'"');
            break;

            case 8:
                // Ordenes de baja voluntaria
                $query->where('o.estatus=0');
                if($tecnico>0)
                    $query->andWhere('o.tecnico_id="'.$tecnico.'"');
            break;

            case 9:
                // Fallas reportadas
                $query->where('o.fecha_solucion = \'0000-00-00\'');
                if($tecnico>0)
                    $query->andWhere('o.tecnico_id="'.$tecnico.'"');
            break;
        }

        if($filtro != '')
            $query->andWhere("c.nombre LIKE '%$filtro%' OR  c.contrato LIKE '%$filtro%'");

        if($calle != '')
        {
            //$query->andWhere("c.calle = '".$calle."'");
			if($calle == "INDEFINIDO")
                $query->andWhere("(c.calle = '".$calle."' OR c.calle='' OR c.calle IS NULL)");
            else
                $query->andWhere("c.calle = '".$calle."'");
        }

        if($etiqueta != '')
        {
            //$query->andWhere("c.etiqueta = '".$etiqueta."'");
			if($etiqueta == "INDEFINIDO")
                $query->andWhere("(c.etiqueta = '".$etiqueta."' OR c.etiqueta='' OR c.etiqueta IS NULL)");
            else
                $query->andWhere("c.etiqueta = '".$etiqueta."'");
        }

        if($nodo != '')
        {
            //$query->andWhere("c.nodo = '".$nodo."'");
			if($nodo == "INDEFINIDO")
                $query->andWhere("(c.nodo = '".$nodo."' OR c.nodo='' OR c.nodo IS NULL)");
            else
                $query->andWhere("c.nodo = '".$nodo."'");
        }

        if($poste != '')
        {
            //$query->andWhere("c.poste = '".$poste."'");
			if($poste == "INDEFINIDO")
                $query->andWhere("(c.poste = '".$poste."' OR c.poste='' OR c.poste IS NULL)");
            else
                $query->andWhere("c.poste = '".$poste."'");
        }

        if($colonia != '')
        {
            //$query->andWhere("c.Colonia.nombre = '".$colonia."'");
			if($colonia == "INDEFINIDO")
                $query->andWhere("(c.Colonia.nombre = '".$colonia."' OR c.Colonia.nombre='' OR c.Colonia.nombre IS NULL)");
            else
                $query->andWhere("c.Colonia.nombre = '".$colonia."'");
        }

        return $query;
    }


    ########################################################



    public function obtenerordenAction()
    {
        // Verificamos que el usuario sea correcto
        if(is_object($this->usuario))
        {
            // El usuario es correcto

            // Formamos el XML de respuesta
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';

            // Obtenemos las órdenes
            switch($_POST['tipo']){
                case 0:
                    // Ordenes de instalación
                    $orden = Ordeninstalacion::obtenerDetalleOrden($_POST['idOrdenSeleccionada']);
                break;

                case 1:
                    // Ordenes de desconexión
                    $orden = Ordendesconexion::obtenerDetalleOrden($_POST['idOrdenSeleccionada']);
                break;

                case 2:
                    // Ordenes de retiro
                    $orden = Ordendesretiro::obtenerDetalleOrden($_POST['idOrdenSeleccionada']);
                break;

                case 3:
                    // Ordenes de reinstalación
                    $orden = Ordenreinstalacion::obtenerDetalleOrden($_POST['idOrdenSeleccionada']);
                break;

                case 4:
                    // Ordenes de reconexion
                    $orden = Ordendesreconexion::obtenerDetalleOrden($_POST['idOrdenSeleccionada']);
                break;
                
                case 5:
                    // Ordenes de cambio de domicilio retirar
                    $orden = Ordencambiodomicilio::obtenerDetalleOrden($_POST['idOrdenSeleccionada']);
                break;

                case 6:
                    // Ordenes de cambio de domicilio instalar
                    $orden = Ordencambiodomicilio::obtenerDetalleOrden($_POST['idOrdenSeleccionada']);
                break;

                case 7:
                    // Ordenes de extension adicional
                    $orden = Ordenextensionadicional::obtenerDetalleOrden($_POST['idOrdenSeleccionada']);
                break;

                case 8:
                    // Ordenes de baja voluntaria
                    $orden = Bajavoluntaria::obtenerDetalleOrden($_POST['idOrdenSeleccionada']);
                break;

                case 9:
                    // Reportes de fallas
                    $orden = ReportarFallas::obtenerDetalleOrden($_POST['idOrdenSeleccionada']);
                break;
            }
            
            if($_POST['tipo'] == 6) //Orden de instalación
            {
                // Formamos la respuesta
                $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . ', Etiq:' . $this->reformarString($orden->Cliente->etiqueta) . ', Nodo: ' . $this->reformarString($orden->Cliente->nodo) .', Poste: ' . $this->reformarString($orden->Cliente->poste) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString($orden->TecnicoI->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '" id_cliente="' . $this->reformarString($orden->Cliente->id) . '"></orden>';
            }
            else if($_POST['tipo'] == 5) //Orden de retiro
            {
                // Formamos la respuesta
                $xml .= '<orden calle="' . $this->reformarString($orden->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($orden->colonia_id)) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . ', Etiq:' . $this->reformarString($orden->Cliente->etiqueta) . ', Nodo: ' . $this->reformarString($orden->Cliente->nodo) .', Poste: ' . $this->reformarString($orden->Cliente->poste) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->no_exterior) . '" numero_interior="' . $this->reformarString($orden->no_interior) . '" tecnico="' . $this->reformarString($orden->TecnicoR->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '" id_cliente="' . $this->reformarString($orden->Cliente->id) . '"></orden>';
            }
            else if($_POST['tipo'] == 7) //Extension adicional
            {
                // Formamos la respuesta
                $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . ', Etiq:' . $this->reformarString($orden->Cliente->etiqueta) . ', Nodo: ' . $this->reformarString($orden->Cliente->nodo) .', Poste: ' . $this->reformarString($orden->Cliente->poste) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString($orden->Tecnico->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '" id_cliente="' . $this->reformarString($orden->Cliente->id) . '" tvs="' . $this->reformarString($orden->televisores) . '"></orden>';
            }
            else if($_POST['tipo'] == 9)
            {
                $xml .= '<orden identificador="' . $this->reformarString($orden->id) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" calle="' . $this->reformarString($orden->Cliente->calle) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . '" status="' . $this->reformarString(Cliente::obtenerEstatus($orden->Cliente->status)) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->ultimomescubierto) . '" fecha_falla="' . $this->reformarString($orden->fecha_falla) . '" fecha_reporte="' . $this->reformarString($orden->fecha_reporte) . '" tipo_falla="' . $this->reformarString(TipoDeFalla::obtenerTipoDeFallaPorId($orden->tipo_falla)) . '" status_falla="' . $this->reformarString($orden->status_falla) . '"></orden>';
            }
            else
            {
                // Formamos la respuesta
                $xml .= '<orden calle="' . $this->reformarString($orden->Cliente->calle) . '" cliente="' . $this->reformarString($orden->Cliente->nombre) . '" colonia="' . $this->reformarString(Colonia::obtenerColonia($orden->Cliente->colonia_id)) . '" contrato="' . $this->reformarString($orden->Cliente->contrato) . ', Etiq:' . $this->reformarString($orden->Cliente->etiqueta) . ', Nodo: ' . $this->reformarString($orden->Cliente->nodo) .', Poste: ' . $this->reformarString($orden->Cliente->poste) . '" identificador="' . $this->reformarString($orden->id) . '" numero_exterior="' . $this->reformarString($orden->Cliente->no_exterior) . '" numero_interior="' . $this->reformarString($orden->Cliente->no_interior) . '" tecnico="' . $this->reformarString($orden->Tecnico->nombre) . '" ultimo_mes_cubierto="' . $this->reformarString($orden->Cliente->Ultimomescubierto) . '" id_cliente="' . $this->reformarString($orden->Cliente->id) . '"></orden>';
            }

            // Enviamos la respuesta
            $xml .= '</respuesta>';
            echo $xml;
        }

        else {
            // El usuario no es correcto
        }
    }

    public function obtenerstatusdefallaAction()
    {
        // Verificamos que el usuario sea correcto
        if(is_object($this->usuario))
        {
            // El usuario es correcto
            // Formamos el XML de respuesta
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            $tipos = EstatusDeFalla::obtenerEstatusDeFalla();
            foreach($tipos as $tipo)
            {
                $xml .= '<tipo identificador="' . $this->reformarString($tipo->id) . '" nombre="' . $this->reformarString($tipo->nombre) . '"></tipo>';
            }
            // Enviamos la respuesta
            $xml .= '</respuesta>';
            echo $xml;
        }

        else {
            // El usuario no es correcto
        }
    }

    public function registrarordenAction()
    {
        // Verificamos que el usuario sea correcto
        if(is_object($this->usuario))
        {
            if(array_key_exists('orden', $_POST))
            {
                $ordenes = $_POST['orden'];
    
                $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
                $xml .= '<respuesta>';
                
                if(count($ordenes) > 0)
                {
                    // Obtenemos las órdenes
                    switch($_POST['tipo']){
                        case 0:
                            foreach($ordenes AS $orden)
                            {
                                // Ordenes de instalación
                                $status_orden = "ok";
                                $orden['tecnico_id'] = $this->usuario->id;
                                //$orden['fecha_instalacion'] = date('Y-m-d H:i:s');
                                $orden['token'] = $_POST['token'];
                                $status_orden .= Ordeninstalacion::actualiza($orden);
                                $cliente_orden = Cliente::obtenerCliente("id = ". $orden['cliente_id']);
                                $xml .= '<orden id="' . $orden['id'] . '" contrato="' . $cliente_orden->contrato . '" cliente="' . $this->reformarString($cliente_orden->nombre) . '" status="' . $status_orden . '"></orden>';
                            }
                        break;

                        case 1:
                            foreach($ordenes AS $orden)
                            {
                                // Ordenes de desconexión
                                $status_orden = "ok";
                                $orden['tecnico_id'] = $this->usuario->id;
                                //$orden['fecha_desconexion'] = date('Y-m-d H:i:s');
                                $orden['token'] = $_POST['token'];
                                //echo "<pre>"; print_r($orden); exit;
                                $status_orden .= Ordendesconexion::actualiza($orden);
                                $cliente_orden = Cliente::obtenerCliente("id = ". $orden['cliente_id']);
                                $xml .= '<orden id="' . $orden['id'] . '" contrato="' . $cliente_orden->contrato . '" cliente="' . $this->reformarString($cliente_orden->nombre) . '" status="' . $status_orden . '"></orden>';
                            }
                        break;

                        case 2:
                            foreach($ordenes AS $orden)
                            {
                                // Ordenes de retiro
                                $status_orden = "ok";
                                $orden['tecnico_id'] = $this->usuario->id;
                                //$orden['fecha_retiro']=date('Y-m-d H:i:s');
                                $orden['token'] = $_POST['token'];
                                $status_orden .= Ordendesretiro::actualiza($orden);
                                $cliente_orden = Cliente::obtenerCliente("id = ". $orden['cliente_id']);
                                $xml .= '<orden id="' . $orden['id'] . '" contrato="' . $cliente_orden->contrato . '" cliente="' . $this->reformarString($cliente_orden->nombre) . '" status="' . $status_orden . '"></orden>';
                            }
                        break;

                        case 3:
                            foreach($ordenes AS $orden)
                            {
                                // Ordenes de reinstalación
                                $status_orden = "ok";
                                $orden['tecnico_id'] = $this->usuario->id;
                                //$orden['fecha_instalacion']=date('Y-m-d H:i:s');
                                $orden['token'] = $_POST['token'];
                                $status_orden .= Ordenreinstalacion::actualiza($orden);
                                $cliente_orden = Cliente::obtenerCliente("id = ". $orden['cliente_id']);
                                $xml .= '<orden id="' . $orden['id'] . '" contrato="' . $cliente_orden->contrato . '" cliente="' . $this->reformarString($cliente_orden->nombre) . '" status="' . $status_orden . '"></orden>';
                            }
                        break;

                        case 4:
                            foreach($ordenes AS $orden)
                            {
                                // Ordenes de reconexion
                                $status_orden = "ok";
                                //$orden['fecha_reconexion'] = date('Y-m-d H:i:s');
                                $orden['token'] = $_POST['token'];
                                $orden['tecnico_id'] = $this->usuario->id;
                                $status_orden .=  Ordendesreconexion::actualiza($orden);
                                $cliente_orden = Cliente::obtenerCliente("id = ". $orden['cliente_id']);
                                $xml .= '<orden id="' . $orden['id'] . '" contrato="' . $cliente_orden->contrato . '" cliente="' . $this->reformarString($cliente_orden->nombre) . '" status="' . $status_orden . '"></orden>';
                            }
                        break;
                        
                        case 5:
                            foreach($ordenes AS $orden)
                            {
                                // Ordenes de cambio de domicilio retirar
                                $status_orden = "ok";
                                //$orden['fecha_retiro']=date('Y-m-d H:i:s');
                                $orden['tecnico_id'] = $this->usuario->id;
                                $orden['token'] = $_POST['token'];
                                $status_orden .=  Ordencambiodomicilio::actualizaRetirar($orden);
                                $cliente_orden = Cliente::obtenerCliente("id = ". $orden['cliente_id']);
                                $xml .= '<orden id="' . $orden['id'] . '" contrato="' . $cliente_orden->contrato . '" cliente="' . $this->reformarString($cliente_orden->nombre) . '" status="' . $status_orden . '"></orden>';
                            }
                        break;

                        case 6:
                            foreach($ordenes AS $orden)
                            {
                                // Ordenes de cambio de domicilio instalar
                                $status_orden = "ok";
                                //$orden['fecha_instalacion']=date('Y-m-d H:i:s');
                                $orden['tecnico_id'] = $this->usuario->id;
                                $orden['token'] = $_POST['token'];
                                $status_orden .=  Ordencambiodomicilio::actualizaInstalar($orden);
                                $cliente_orden = Cliente::obtenerCliente("id = ". $orden['cliente_id']);
                                $xml .= '<orden id="' . $orden['id'] . '" contrato="' . $cliente_orden->contrato . '" cliente="' . $this->reformarString($cliente_orden->nombre) . '" status="' . $status_orden . '"></orden>';
                            }
                        break;

                        case 7:
                            foreach($ordenes AS $orden)
                            {
                                // Ordenes de extension adicional
                                $status_orden = "ok";
                                //$orden['fecha_instalacion']=date('Y-m-d H:i:s');
                                $orden['tecnico_id'] = $this->usuario->id;
                                $orden['token'] = $_POST['token'];
                                $status_orden .=  Ordenextensionadicional::actualiza($orden);
                                $cliente_orden = Cliente::obtenerCliente("id = ". $orden['cliente_id']);
                                $xml .= '<orden id="' . $orden['id'] . '" contrato="' . $cliente_orden->contrato . '" cliente="' . $this->reformarString($cliente_orden->nombre) . '" status="' . $status_orden . '"></orden>';
                            }
                        break;

                        case 8:
                            foreach($ordenes AS $orden)
                            {
                                // Ordenes de baja voluntaria
                                $status_orden = "ok";
                                //$orden['fecha_retiro']=date('Y-m-d H:i:s');
                                $orden['tecnico_id'] = $this->usuario->id;
                                $orden['token'] = $_POST['token'];
                                $status_orden .=  Bajavoluntaria::actualiza($orden);
                                $cliente_orden = Cliente::obtenerCliente("id = ". $orden['cliente_id']);
                                $xml .= '<orden id="' . $orden['id'] . '" contrato="' . $cliente_orden->contrato . '" cliente="' . $this->reformarString($cliente_orden->nombre) . '" status="' . $status_orden . '"></orden>';
                            }
                        break;

                        case 9:
                            // Reportes de fallas
                            $ordenes = ReportarFallas::actualiza($_POST);
                        break;
                    }
                }

                $xml .= '</respuesta>';
                echo $xml;
            }
            else
            {
                if(count($_POST) >= 3)
                {
                    // Completamos el $_POST
                    $_POST['tecnico_id'] = $this->usuario->id;

                    
                    // Obtenemos las órdenes
                    switch($_POST['tipo']){
                        case 0:
                            // Ordenes de instalación
                            $_POST['fecha_instalacion']=date('Y-m-d H:i:s');
                            Ordeninstalacion::actualiza($_POST);
                        break;

                        case 1:
                            // Ordenes de desconexión
                            $_POST['fecha_desconexion']=date('Y-m-d H:i:s');
                            Ordendesconexion::actualiza($_POST);
                        break;

                        case 2:
                            // Ordenes de retiro
                            $_POST['fecha_retiro']=date('Y-m-d H:i:s');
                            Ordendesretiro::actualiza($_POST);
                        break;

                        case 3:
                            // Ordenes de reinstalación
                            $_POST['fecha_instalacion']=date('Y-m-d H:i:s');
                            Ordenreinstalacion::actualiza($_POST);
                        break;

                        case 4:
                            // Ordenes de reconexion
                            $_POST['fecha_reconexion']=date('Y-m-d H:i:s');
                            $ordenes = Ordendesreconexion::actualiza($_POST);
                        break;
                        
                        case 5:
                            // Ordenes de cambio de domicilio retirar
                            $_POST['fecha_retiro']=date('Y-m-d H:i:s');
                            $ordenes = Ordencambiodomicilio::actualizaRetirar($_POST);
                        break;

                        case 6:
                            // Ordenes de cambio de domicilio instalar
                            $_POST['fecha_instalacion']=date('Y-m-d H:i:s');
                            $ordenes = Ordencambiodomicilio::actualizaInstalar($_POST);
                        break;

                        case 7:
                            // Ordenes de extension adicional
                            $_POST['fecha_instalacion']=date('Y-m-d H:i:s');
                            $ordenes = Ordenextensionadicional::actualiza($_POST);
                        break;

                        case 8:
                            // Ordenes de baja voluntaria
                            $_POST['fecha_retiro']=date('Y-m-d H:i:s');
                            $ordenes = Bajavoluntaria::actualiza($_POST);
                        break;

                        case 9:
                            // Reportes de fallas
                            $ordenes = ReportarFallas::actualiza($_POST);
                        break;
                    }

                    echo 'OK';
                }
                else
                {
                    $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
                    $xml .= '<respuesta>';
                    $xml .= '</respuesta>';
                    echo $xml;
                }
            }
        }
    }

    public function obtenerhistorialdefallaAction()
    {
        // Verificamos que el usuario sea correcto
        if(is_object($this->usuario))
        {
            // Obtenemos el historial por id
            $historiales = HistorialDeFallas::obtenerHistorial($_POST['id']);

            // Recorremos los historiales
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';

            foreach($historiales as $historial){
                $xml .= '<historial fecha="'.$historial->fecha.'" status="'.$this->reformarString($historial->EstatusDeFalla->nombre).'" tecnico="'.$this->reformarString($historial->Tecnico->nombre).'" comentarios="'.$this->reformarString($historial->comentarios).'"></historial>';
            }

            $xml .= '</respuesta>';

            echo $xml;
        }
    }

    public function registrarubicacionAction()
    {
        // Verificamos que el usuario sea correcto
        if(is_object($this->usuario))
        {
            // Recorremos las ubicaciones
            $ubicaciones = $_POST['ubicacion'];

            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';

            foreach($ubicaciones AS $ubicacion){
                if(isset($ubicacion['tipo']))
                {
                    if(Ubicacion::guardar($ubicacion['id'], $ubicacion['fecha'], $ubicacion['latitud'], $ubicacion['longitud'], $this->usuario->id, $ubicacion['tipo'], $ubicacion['sesion'])){
                        $xml .= '<ubicacion id="' . $ubicacion['id'] . '"></ubicacion>';
                    }
                }
                else
                {
                    if(Ubicacion::guardar($ubicacion['id'], $ubicacion['fecha'], $ubicacion['latitud'], $ubicacion['longitud'], $this->usuario->id, 0, 0)){
                        $xml .= '<ubicacion id="' . $ubicacion['id'] . '"></ubicacion>';
                    }
                }
            }

            $xml .= '</respuesta>';

            echo $xml;
        }
    }

    public function almacenarimagenftpAction()
    {
        // Verificamos que el usuario sea correcto
        if(is_object($this->usuario))
        {
            $image_content = str_replace(" ", "+", $_POST['archivo']);
            $image_decoded = base64_decode($image_content);
            
            $xml  = '';

            $ifp = fopen("../public/expediente_clientes/".$_POST['datos'], "w+");
            @file_put_contents("../public/expediente_clientes/".$_POST['datos'], $image_decoded);
            fclose($ifp);
            
            if(file_exists("../public/expediente_clientes/".$_POST['datos']))
            {
                $xml .= 'OK';
            }
            else
            {
                $xml .= 'ERROR';
            }
            
            echo $xml;
        }
    }


    ##################################
    ############CLIENTE###############
    ##################################

    ########### LOGIN ################

    public function loginclienteAction()
    {
        if(isset($_POST['numeroContrato']) && isset($_POST['clave']) && isset($_POST['plaza']))
        {
            $cliente=Cliente::iniciarSesion($_POST);
            if(is_object($cliente))
            {
                $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
                $xml .= '<respuesta>';
                $xml .= '<cliente nombre="'.$this->reformarString($cliente->nombre).'" token="'.md5($cliente->id).'" statuslogin="'.$cliente->status_login_appcliente.'" email="'.$cliente->email.'"></cliente>';
                $xml .= '</respuesta>';
                echo $xml;
            }
            else
            {
                echo "ERROR";
            }
        }
    }

    public function sesioniniciadaAction()
    {
        $this->cliente = Cliente::obtenerPorToken($_POST['token']);
        $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '<respuesta>';
        
        if(is_object($this->cliente))
        {
            $xml .= '<sesion status="'.$this->cliente->status_login_appcliente.'"></sesion>';
        }
        
        $xml .= '</respuesta>';
        echo $xml;
    }
    
     public function cambiarstatusloginappAction()
    {
        $this->cliente = Cliente::obtenerPorToken($_POST['token']);
        $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '<respuesta>';
        if(is_object($this->cliente))
        {
            if(isset($_POST['status']))
            {
                $this->cliente->status_login_appcliente = $_POST['status'];
                $this->cliente->gcm_id = "";
                $this->cliente->save();       
            }
            $xml .= '<sesion status="'.$this->cliente->status_login_appcliente.'"></sesion>';
        }
        
        $xml .= '</respuesta>';
        echo $xml;
    }

    public function cerrarsesionforzadoAction()
    {
        $cliente=Cliente::iniciarSesion($_POST);
        $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '<respuesta>';
        if(is_object($cliente))
        {
            $mensaje = array("titulo"=>"Logout","cuerpo"=>"","seccion"=>"");
            My_Comun::pushnotification($cliente->gcm_id,$mensaje);
            $cliente->status_login_appcliente = 0;
            $cliente->gcm_id = "";
            $cliente->save();
            $xml .= '<sesion status="'.$cliente->status_login_appcliente.'" gcm="'.$cliente->gcm_id.'"></sesion>'; 
        }
        $xml .= '</respuesta>';
        echo $xml;
    }


    ############### RECIBOS ##################



    public function historialdepagosAction()
    {
        $this->cliente = Cliente::obtenerPorToken($_POST['token']);
       
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';

        if(is_object($this->cliente)) {

            $registros = Cobranza::obtenerPorCliente($this->cliente->id, $fecha);

            $xml .= '<respuesta>';

            foreach ($registros as $registro) {
                                
                $xml .= '<pago folio="'.$registro->folio.'" monto="'.$registro->total.'" fecha="'.substr($registro->created_at, 0, 10).'" recibo="http://desarrollo.supertv.com.mx/transacciones/recibo/imprimir/id/'.$registro->id.'" ></pago>';                
            }

            $xml .= '</respuesta>';
        }        

        echo $xml; 
    }

    public function enviodereciboAction()
    {
        $this->cliente = Cliente::obtenerPorToken($_POST['token']);
        if(is_object($this->cliente)) {
            $pdf_content = str_replace(" ", "+", $_POST['base']);
            $pdf_decoded = base64_decode($pdf_content);

            $mail = new Zend_Mail('UTF-8');

            $attachment = $mail->createAttachment($pdf_decoded);
            $attachment->type = 'application/pdf';
            $attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
            $attachment->encoding = Zend_Mime::ENCODING_BASE64;
            $attachment->filename = 'recibo.pdf';

            $mail->setBodyHtml("Esta es una copia del recibo de pago con el número de folio : <b>". $_POST['folio'] ."</b><br><br>Usted puede generarlo de nuevo yendo al apartado “Historial de pagos” en su dispositivo móvil.<br><br><b>SuperTv</b>");
            $mail->setFrom('pagos@supertv.com.mx', 'Ultracable');
            $mail->addTo($_POST['email'], $_POST['nombre']);
            $mail->setSubject('SuperTv: Copia de Recibo de pago Folio: '. $_POST['folio']);
            $mail->send();

        }  
    }


    ################## PROMOCIONES ########################

    public function promocionesAction()
    {
        $this->cliente = Cliente::obtenerPorToken($_POST['token']);
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        if(is_object($this->cliente)) 
        {
            $registros = Promocion::obtenerPorPlaza($this->cliente->plaza_id, $_POST['fecha']);
            $xml .= '<respuesta>';
            foreach ($registros as $registro) 
            {
                $xml .= '<promocion id="'.$registro->id.'" nombre="'. $this->reformarString($registro->nombre) .'" descripcion="'. $this->reformarString($registro->descripcion) .'" fecha_inicio="'. $this->reformarString($registro->PromocionPlaza[0]->inicio) .'" fecha_fin="'. $this->reformarString($registro->PromocionPlaza[0]->fin) .'" imagen="http://desarrollo.supertv.com.mx/images/promociones/'.$registro->imagen.'"></promocion>';
            }
            $xml .= '</respuesta>';
        }   
        echo $xml;        
    }

    public function promocionAction()
    {
        $this->cliente = Cliente::obtenerPorToken($_POST['token']);
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        if(is_object($this->cliente)) 
        {
            $registros = Promocion::obtenerPorPlazaAndId($this->cliente->plaza_id, $_POST['idpromo']);
            $xml .= '<respuesta>';
            foreach ($registros as $registro) 
            {
                $xml .= '<promocion id="'.$registro->id.'" nombre="'.$this->reformarString($registro->nombre).'" descripcion="'. $this->reformarString($registro->descripcion) .'" fecha_inicio="'. $this->reformarString($registro->PromocionPlaza[0]->inicio) .'" fecha_fin="'. $this->reformarString($registro->PromocionPlaza[0]->fin) .'" imagen="http://desarrollo.supertv.com.mx/images/promociones/'.$registro->imagen.'"></promocion>';
            }
            $xml .= '</respuesta>';
        }   
        echo $xml;        
    }

    public function detallepromocionAction()
    {
        $this->cliente = Cliente::obtenerPorToken($_POST['token']);
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        if(is_object($this->cliente)) 
        {
            $promocion = Promocion::obtenerpromocion($_POST['promocion_id']);
            $xml .= '<respuesta>';
            $xml .= '<promocion id="'.$promocion->id.'" nombre="'.$this->reformarString($promocion->nombre).'" descripcion="'.$this->reformarString($promocion->descripcion).'" fecha_inicio="'.$promocion->PromocionPlaza[0]->inicio.'" fecha_fin="'.$promocion->PromocionPlaza[0]->fin.'" imagen="http://desarrollo.supertv.com.mx/images/promociones/'.$promocion->imagen.'"></promocion>';
            $xml .= '</respuesta>';
        }   
        echo $xml;        
    }

    ##################### ESTADO DE CUENTA ####################################3

    public function estadodecuentaAction()
    {
        //$_POST['token']=md5(15361);
        ###Nota: Falta interar la tarifa de recuperación....

        $cliente = Cliente::obtenerPorToken($_POST['token']);

        $ultimoMesCubierto=$cliente->ultimomescubierto;
        ### Obtenemos el número de meses que hay entre la fecha actual y el último mes cubierto
        $meses=My_Comun::numero_de_meses(date('Y-m-d'),$ultimoMesCubierto);
        
        if($cliente->status!='ACTIVO')
        {
            $servicios=array();
            ### Obtenemos los servicios que se le cobrarán al cliente
            $dServicios=Doctrine_Query::create()->from('ClienteServicio')->where('cliente_id=?',$cliente->id)->execute();
            ### Recorremos todos los servicios del cliente para conformar el total a pagar
            foreach($dServicios as $servicio)
            {
                ### Recordar que falta calcular el precio en caso de una tarifa especial
                $servicios[]=Servicio::obtenerPrecioCobranza($servicio->servicio_id, $servicio->tarifa_especial_id ,$servicio->periodos_tarifa_especial ,$servicio->periodos_restan_tarifa_especial, $meses, $cliente);// $this->view->meses,$this->view->cliente);
            }
        }

        if(count($servicios)>0)
        {
            $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= '<respuesta>';
            
            $xml .= '<servicios>';
            $total=0;
            foreach($servicios as $servicio)
            {
                $xml.='<servicio cantidad="'.$meses.'" nombre="'.$this->reformarString($servicio['nombre']).'" monto="'.$servicio['tarifa'].'" subtotal="'.$meses*$servicio['tarifa'].'" recargo="'.$servicio['recargo'].'" total="'.($servicio['recargo']+($servicio['tarifa']*$meses)).'"></servicio>';
                $total+=($servicio['recargo']+($servicio['tarifa']*$meses));
            }
            $xml.='</servicios>';
            $xml .= '<cliente nombre="'.$this->reformarString($cliente->nombre).'" contrato="'.$cliente->contrato.'" ultimo_mes_cubierto="'.$ultimoMesCubierto.'" adeudo="'.$total.'" status="'.$cliente->status.'" fecha="'.date('d/m/Y').'" telefono="'.$cliente->telefono.'"></cliente>';
            $xml .= '</respuesta>';
            echo $xml;
        }
        else
        {
            echo "ERROR";
        }
    }

    public function intentodepagoAction()
    {
       // $_POST['token']=md5(15234);
        $cliente = Cliente::obtenerPorToken($_POST['token']);

        if(is_object($cliente))
        {
            $id=Cobranza::cobrarAdeudo($cliente->id, array(),0,0,2);
            if($id>0)
            {
                $xml  = '<?xml version="1.0" encoding="UTF-8" ?>';
                $xml .= '<respuesta>';
                $xml .= '<cobro id="'.$id.'"></cobro>';
                $xml .= '</respuesta>';
                echo $xml; 
            }
            else
            {
                echo "ERROR";
            }
        }

                
    }

    public function confirmarpagoAction()
    {

        //$_POST['token']=md5(15361);
        $cliente = Cliente::obtenerPorToken($_POST['token']);

        //$_POST['cobro_id']=83401;
        //$_POST['status']=1;
        
        /*$body = $cliente->nombre. " | " .$_POST['cobro_id']. " | " .$_POST['status']. " | FIN";

        $to      = 'ricardo_sonora@avansys.com.mx';
        $subject = 'confirmarpago';
        $message = $body;
        mail($to, $subject, $message);*/

        ###Primero obtenemos el registro de cobro
        $cobro=Doctrine_Query::create()->from("Cobranza")->where("id=?",$_POST['cobro_id'])->execute()->getFirst();
        if(is_object($cobro))
        {
            if($_POST['status']==1)
            {
                $cobro->pagada=1;
                $cobro->save();

                if($cliente->estatus=="DESCONECTADO")
                {
                    $orden= new Ordendesreconexion();
                        $orden->cliente_id=$dCliente->id;
                    $orden->save(); 
                    
                    $cliente->status=11;
                    
                }
                else
                {
                    $cliente->status=1;
                }

                $cliente->save();
            }
            else
            {
                $cobro->cancelado=1;
                $cobro->save();
            }
        }
           
    }


    ################### FALLAS ###################################

    public function registrarfallaAction()
    {
        $this->cliente = Cliente::obtenerPorToken($_POST['token']);

        if(is_object($this->cliente))
        {
            //print_r($_POST); exit;
            if(isset($_POST['tipodefalla']) && isset($_POST['fechadefalla']) && isset($_POST['comentario']))
            {
                $_POST['status_falla'] = 1;
                $_POST['cliente_id'] = $this->cliente->id;
                $_POST['tipo_falla'] = $_POST['tipodefalla'];
                $_POST['fecha_falla'] = $_POST['fechadefalla'];
                $_POST['comentarios'] = $_POST['comentario'];
                $_POST['fecha_reporte'] = date('Y-m-d H:i:s');
                $id = My_Comun::guardar("ReportarFallas", $_POST, NULL, NULL, 'status_falla');
                if($id > 0){
                    $_POST['fecha']= date("Y-m-d");
                    $_POST['reportar_falla_id'] = $id;
                    My_Comun::guardar("HistorialDeFallas",$_POST,NULL,'','fecha');
                    echo "ok";
                }else{
                    echo "error";
                }
            }
        }
    }

    public function obtenertiposfallaAction()
    {
        $tipos = TipoDeFalla::obtenerTipoDeFalla();
        
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';

        $xml .= '<respuesta>';

        foreach ($tipos as $tipo) 
        {
            $xml .= '<tipo id="'.$tipo->id.'" nombre="'.$this->reformarString($tipo->nombre).'"></tipo>';
        }

        $xml .= '</respuesta>';
        
        echo $xml;
    }


    ###################### OTROS #######################################

    public function registrationidAction()
    {

        if(isset($_POST['registrationid']))
        {
            
            $this->cliente = Cliente::obtenerPorToken($_POST['token']);


            $this->cliente->gcm_id=$_POST['registrationid'];
            $this->cliente->save();            
        }

        
    }

    public function cronAction()
    {
        $qPromociones=Doctrine_Query::create()->from('PromocionPlaza')->where('inicio="'.date('Y-m-d').'" AND status=1')->groupBy('plaza_id');
        $dPromocion=$qPromociones->execute();
        foreach($dPromocion as $promocion)
        {
            $qClientes=Doctrine_Query::create()->from('Cliente')->where('plaza_id='.$promocion->plaza_id.' AND gcm_id!="" AND (status=1 OR status=2 OR status=8)');
            $dClientes=$qClientes->execute();
            foreach ($dClientes as $cliente) 
            {
                //echo $cliente->nombre;
                $mensaje=array("titulo"=>"Disfruta de nuestras promociones","cuerpo"=>"Super tv tiene nuevas promociones para ti.","seccion"=>"promociones");
                
                if($cliente->gcm_id != Null && $cliente->gcm_id != "")
                {
                    My_Comun::pushnotification($cliente->gcm_id, $mensaje);
                }
            }
        }

        $to      = 'christian_lopez@avansys.com.mx';
        $subject = 'Prueba cron';
        $message = 'hello';
        mail($to, $subject, $message);
    }

    public function versionAction()
    {
        var_dump(Version::obtenerVersion(4));
    }

    public function recordatoriopagoAction()
    {
        $qClientes = Doctrine_Query::create()->from('Cliente')->where('status=2');
        $dCliente = $qClientes->execute();
        foreach($dCliente as $cliente)
        {
            $fecha_vence = "11-".date("m")."-".date("Y");
            $fecha = str_replace("-", "/", $fecha_vence);
            $fecha_hoy = strtotime('now');
            $fecha_vence = strtotime($fecha_vence);
            
            if($fecha_hoy > $fecha_vence){
                $mensaje = array("titulo"=>"Recordatorio de pago","cuerpo"=>"Estimado cliente, la fecha limite para el pago de su mensualidad ha vencido","seccion"=>"notificaciones");
            }else{
                $mensaje = array("titulo"=>"Recordatorio de pago","cuerpo"=>"Estimado cliente, la fecha limite para el pago de su mensualidad es el ".$fecha."","seccion"=>"notificaciones");
            }
            
            if($cliente->gcm_id != Null && $cliente->gcm_id != "")
            {
                 My_Comun::pushnotification($cliente->gcm_id, $mensaje);
            }
        }

        $to      = 'ricardo_sonora@avansys.com.mx';
        $subject = 'Recordatorio de pago';
        $message = 'Se ha realizado el recordatorio de pago';
        mail($to, $subject, $message);
    }

    public function crearcarpetasAction()
    {
        ini_set("memory_limit", "530M");
        ini_set('max_execution_time', 0);

        $qClientes = Doctrine_Query::create()->from('Cliente');
        $dCliente = $qClientes->execute();

        $id_ftp = ftp_connect('desarrollo.supertv.com.mx', 21); //Obtiene un manejador del Servidor FTP
        ftp_login($id_ftp, 'sissutv', 'ftp.sys13');

        $errores_ftp = ""; //Variable para controlar errores cuando se crean las carpetas 
        $existen = "";
        $porcrear = "";

        foreach($dCliente as $cliente)
        {
            //Creamos las carpetas del cliente registrado
            $fplaza = $cliente->plaza_id;
            $fcontrato = $cliente->contrato;

            if(ftp_chdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato))
            {
                $existen = $existen.$fplaza."_".$fcontrato." | ";
            }
            else
            {
                $porcrear = $porcrear.$fplaza."_".$fcontrato." | ";
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato) == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato." | "; 
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato."/Registro/") == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato."/Registro/"." | ";
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato."/Instalacion/") == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato."/Instalacion/"." | ";
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato."/Desconexion/") == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato."/Desconexion/"." | ";
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato."/Retiro/") == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato."/Retiro/"." | ";
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato."/Reinstalacion/") == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato."/Reinstalacion/"." | ";
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato."/Reconexion/") == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato."/Reconexion/"." | ";
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato."/Cambio de domicilio/") == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato."/Cambio de domicilio/"." | ";
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato."/Cambio de domicilio/Retiro/") == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato."/Cambio de domicilio/Retiro/"." | ";
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato."/Cambio de domicilio/Instalacion") == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato."/Cambio de domicilio/Instalacion"." | ";
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato."/Extensiones/") == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato."/Extensiones/"." | ";
                if(ftp_mkdir($id_ftp, "/httpdocs/public/expediente_clientes/".$fplaza."_".$fcontrato."/Baja/") == False) $errores_ftp = $errores_ftp.$fplaza."_".$fcontrato."/Baja/"." | ";
            }
        }

        if(ftp_close($id_ftp) == False) $errores_ftp = $errores_ftp."Cerrar FTP"; //Cierra la conexion FTP

        $mail = new Zend_Mail();
        $mail->setBodyHtml('Log Carpetas:<br><br>ERRORES<br><br>' .$errores_ftp. '<br><br>EXISTEN<br><br>' .$existen. '<br><br>POR CREAR<br><br>' .$porcrear);
        $mail->setFrom('info@supertv.com.mx', 'Ultracable');
        $mail->addTo('ricardo_sonora@avansys.com.mx', 'Richard');
        $mail->setSubject('Log Carpetas');
        $mail->send();
    }

    private function reformarString($string)
    {
        $string = utf8_encode($string);
        //$string = $this->reformarString( $string);
        $string = str_replace('"', '', $string);
		$string = str_replace("'", '', $string);
        $string = str_replace('&', 'Y', $string);
        return $string;
    }

    public function enviarnotificacionAction()
    {
        //Enviamos las notificaciones periodicas
        $notificaciones = Notificacion::obtenerNotificacionesPeriodicas(date('Y-m-d H:00:00'));
        foreach ($notificaciones as $notificacion)
        {
            //echo "<br>1Notificacion: ".$notificacion->nombre."<br>"."Descripcion: ".$notificacion->descripcion."<br>";
            $estatus = NotificacionStatus::obtenerStatus($notificacion->id);
            $plazas = NotificacionPlaza::obtenerPlazas($notificacion->id);

            $filtro = "";
            foreach ($estatus as $estado)
            {
                $filtro .= $estado->status.",";
            }
            $filtro = trim($filtro, ',');

            $filtro2 = "";
            foreach ($plazas as $plaza)
            {
                $filtro2 .= $plaza->plaza_id.",";
            }
            $filtro2 = trim($filtro2, ',');
            //echo "Estatus: ".$filtro."<br>";
            $clientes = Cliente::obtenerClientes('status IN('.$filtro.') AND plaza_id IN('.$filtro2.') AND (gcm_id IS NOT NULL AND gcm_id != "")');
            foreach ($clientes as $cliente)
            {
                //echo "<br>".$cliente->nombre;
                $mensaje=array("titulo"=>$notificacion->nombre,"cuerpo"=>$notificacion->descripcion,"seccion"=>"notificaciones");
                My_Comun::pushnotification($cliente->gcm_id, $mensaje);
            }
        }

        //echo "<br><br>";

        $notificaciones = Notificacion::obtenerNotificaciones(date('Y-m-d H:00:00'));
        foreach ($notificaciones as $notificacion)
        {
            //echo "<br>2Notificacion: ".$notificacion->nombre."<br>"."Descripcion: ".$notificacion->descripcion."<br>";
            $estatus = NotificacionStatus::obtenerStatus($notificacion->id);
            $plazas = NotificacionPlaza::obtenerPlazas($notificacion->id);

            $filtro = "";
            foreach ($estatus as $estado)
            {
                $filtro .= $estado->status.",";
            }
            $filtro = trim($filtro, ',');

            $filtro2 = "";
            foreach ($plazas as $plaza)
            {
                $filtro2 .= $plaza->plaza_id.",";
            }
            $filtro2 = trim($filtro2, ',');
            //echo "Estatus: ".$filtro."<br>";
            $clientes = Cliente::obtenerClientes('status IN('.$filtro.') AND plaza_id IN('.$filtro2.') AND (gcm_id IS NOT NULL AND gcm_id != "")');
            foreach ($clientes as $cliente)
            {
                //echo "<br>".$cliente->nombre;
                $mensaje=array("titulo"=>$notificacion->nombre,"cuerpo"=>$notificacion->descripcion,"seccion"=>"notificaciones");
                My_Comun::pushnotification($cliente->gcm_id, $mensaje);
            }
        }
    }

    public function testfunctionAction()
    {
        //echo My_Comun::numero_de_meses2("2015-02-05","2015-01-31")-1;
        //print_r(My_Comun::obtenerRangoFechaDesdePeriodo(My_Comun::numero_de_meses2("2015-02-05","2015-01-31")-1,"2015-01-31", false));
        //echo Doctrine_Query::create()->from('ClienteServicio')->where('cliente_id=?',"22986")->orderBy("id DESC")->execute()->getFirst()->servicio_id;
        //echo Doctrine_Query::create()->from('ServicioPlaza')->where('plaza_id=? AND servicio_id=?',array("13", "1"))->execute()->getFirst()->id;
        //echo Cliente::obtenerMesCubiertoPorInstalacion("22986");
        //echo My_Comun::numero_de_meses2("2015-02-05","2014-12-31");
        echo My_Comun::numero_de_meses2("2015-02-05",$mesCubiertoPorRecuperacion)-1;
    }

}