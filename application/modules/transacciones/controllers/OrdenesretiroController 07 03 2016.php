<?php

class Transacciones_OrdenesretiroController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/ordenesretiro.js'));
    }

    public function indexAction()
    {
        $this->view->plazas=Plaza::obtenerPlazas();
    }
	
    public function gridAction()
    {

        Cliente::ordenesDeRetiro();
	
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $query=My_Comun::prepararQuery("Ordendesretiro o");

        $query->innerJoin("o.Cliente c");
        
        ### Cachamos las variables para conformar el filtro
        $contrato = $this->_getParam('contrato');
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');   
        $sucursal = $this->_getParam('sucursal');
        $plaza = $this->_getParam('plaza');            
        $calle='';
        $etiqueta='';
        $nodo='';
        $poste='';
        $orden_primer_nivel=$this->_getParam('orden_primer_nivel');
        $orden_segundo_nivel=$this->_getParam('orden_segundo_nivel');
        $orden_tercer_nivel=$this->_getParam('orden_tercer_nivel');
        
        $query=$this->whereAction($query, $nombre, $contrato, $calle, $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza);

        $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"o.created_at desc"); 
                           
        $query->orderBy($orden);

        
        ### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::registrosGrid2($query); 
		
		$grid=array();		
		$i=0;
		
		foreach($registros['registros'] as $registro)
		{
            $grid[$i]['contrato']=$registro->Cliente->contrato;
            $grid[$i]['nombre']=$registro->Cliente->nombre;
            $grid[$i]['plaza']=$registro->Cliente->Plaza->nombre;
            $grid[$i]['telefono']=$registro->Cliente->telefono;
            $grid[$i]['celular']=$registro->Cliente->celular1;
            $grid[$i]['estatus']=$registro->Cliente->estatus;
            $grid[$i]['cobrar']='<span onclick="descargar('.$registro->id.');" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png"/></span>';
            $i++;
		}
		
        My_Comun::grid2($registros,$grid);	
    }

    public function whereAction($query, $nombre, $contrato, $calle, $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza)
    {
        
        $query->where("o.estatus=0");
        $query->andWhere("c.status = 4");
        //echo $query->getSqlQuery(); exit;

        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $query->andWhere("c.Plaza.id=".Usuario::plaza());

        if($nombre != '')
        {
            $query->andWhere("(c.nombre LIKE '%$nombre%' OR c.telefono LIKE '%$nombre%'  OR c.calle LIKE '%$nombre%' OR c.contrato LIKE '%$nombre%')");
        }

        if($contrato != '')
        {
            $query->andWhere("c.contrato = '".$contrato."'");
        }

        if($calle != '')
        {
            $query->andWhere("c.calle = '".$calle."'");
        }

        if($etiqueta != '')
        {
            $query->andWhere("c.etiqueta = '".$etiqueta."'");
        }

        if($nodo != '')
        {
            $query->andWhere("c.nodo = '".$nodo."'");
        }

        if($poste != '')
        {
            $query->andWhere("c.poste = '".$poste."'");
        }
        
        if($desde != '')
        {    
            $query->andWhere("o.created_at >=  '$desde 00:00:00'");
        }    
        if($hasta != '')
        {    
            $query->andWhere("o.created_at <= '$hasta 23:59:59'");
        }
        
        if($sucursal != 0)
        {
            $query->andWhere("c.sucursal_id = ".$sucursal);
        }   
        
        else if($plaza >0)
        {
            $query->andWhere("c.Plaza.id =" .$plaza);
        }

        return $query;
    }

    public function descargarAction()
    {
		$this->_helper->layout->disableLayout();
		
		$this->view->registro=My_Comun::obtener('Ordendesretiro',$this->_getParam('id'));
		$this->view->tecnicos=Tecnico::obtenerTecnicos(" plaza_id=".Usuario::plaza()."");
    }
	
    public function guardarAction()
    {
	### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   

        Ordendesretiro::actualiza($_POST);
		
		
    }

    public function imprimirAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);


        ### Cachamos las variables para conformar el filtro
        $contrato = $this->_getParam('contrato');
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');   
        $sucursal = $this->_getParam('sucursal');
        $plaza = $this->_getParam('plaza');   
        $calle='';
        $etiqueta='';
        $nodo='';
        $poste='';
        $orden_primer_nivel=$this->_getParam('orden_primer_nivel');
        $orden_segundo_nivel=$this->_getParam('orden_segundo_nivel');
        $orden_tercer_nivel=$this->_getParam('orden_tercer_nivel');

        ### Inicializamos el objeto PDF
        $pdf= new My_Fpdf_Pdf();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->Header("IMPRESIÓN DE ÓRDENES DE RETIRO");
        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(30,42,36,36,36));
        $pdf->SetFont('Arial','',10);


        if($this->_getParam('agrupamiento_primer_nivel')>0)
        {
            $agrupamiento=My_Comun::prepararQuery("Ordendesretiro o");
            $agrupamiento->innerJoin("o.Cliente c");
            $agrupamiento=$this->whereAction($agrupamiento, $nombre, $contrato, $calle, $etiqueta, $nodo, $poste, $desde, $hasta, $sucursal, $plaza);
            $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"o.created_at desc");
            $agrupamiento->orderBy($orden);
            $grupos=My_Comun::configurarAgrupamiento($agrupamiento,$this->_getParam('agrupamiento_primer_nivel'));
       }


       if(count($grupos)==0)
        $grupos[]=-1;
        
        
       foreach($grupos as $grupo)
       {
            $query=My_Comun::prepararQuery("Ordendesretiro o");
            $query->innerJoin("o.Cliente c");

            if($grupo!=-1)
            {
                switch($this->_getParam('agrupamiento_primer_nivel'))
                {
                    case 2: $calle=$grupo; $titulo="Calle: "; break;
                    case 3: $etiqueta=$grupo; $titulo="Etiqueta: "; break;
                    case 4: $nodo=$grupo; $titulo="Nodo: "; break;
                    case 5: $poste=$grupo; $titulo="Poste: "; break;
                }
            }


            $query=$this->whereAction($query, $nombre, $contrato, $calle, $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza);
            
            //Configuramos el orden
            $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"o.created_at desc");
            
            $query->orderBy($orden);

            //echo $query->getSqlQuery(); echo "<br><br>";

            // Ejecitamos la consulta
            $registros=  $query->execute();
            
            $i=1;
            $pdf->Header($titulo." ".$grupo);
       
            foreach($registros as $registro)
            {
                $domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
    			
                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFillColor(220,220,220);
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('FECHA DE SOLICITUD','CONTRATO','CLIENTE','PLAZA'),1,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array($registro->created_at,$registro->Cliente->contrato,$registro->Cliente->nombre,$registro->Cliente->Plaza->nombre),1,1);

                $pdf->SetFillColor(220,220,220);
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('ESTATUS','ETIQUETA','NODO','POSTE'),1,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array($registro->Cliente->estatus,$registro->Cliente->etiqueta,$registro->Cliente->nodo,$registro->Cliente->poste,''),1,1);
    			$pdf->SetWidths(array(45,135));
    			$pdf->Row(array('DIRECCIÓN:',$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id)),1,1);

                $pdf->SetWidths(array(45,135));
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array('REFERENCIA',$registro->Cliente->referencia),1,1);
                
                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFillColor(220,220,220);
                $pdf->SetFont('Arial','',8);
                $pdf->Row(array('TELEFONO',$registro->Cliente->telefono,'CELULAR',$registro->Cliente->celular1),1,1);
    			
                $pdf->Ln(2);
                $pdf->SetWidths(array(45,45,45,45));
    			$pdf->SetWidths(array(180));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('MATERIAL RETIRADO: '.$domicilioretirar),1,1);
    			
                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('NO. DE TELEVISIONES','CABLE','CONECTOR','DIVISOR 2'),0,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array($registro->Cliente->televisores,'','',''),0,1);
    			
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('DIVISOR 3','DIVISOR 4','GRAPAS','FECHA DE RETIRO'),0,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array('','','',''),0,1);
    			
                $pdf->SetWidths(array(90,90));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('TÉCNICO QUE RETIRÓ:',''),0,1);	
                $pdf->Ln(2);
            }
        }   
        
       $pdf->Output();	       
    }
	
	function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        ### Establecemos el filtro por default
        $filtro=" Ordendesretiro.estatus =0 AND Ordendesretiro.Cliente.status=4";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Ordendesretiro.Cliente.plaza_id=".Usuario::plaza();
        
        ### Cachamos las variables para conformar el filtro
	$contrato = $this->_getParam('contrato');
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $sucursal = $this->_getParam('sucursal');
        $plaza = $this->_getParam('plaza');

	if($contrato != '')
        {   
            $filtro .= " AND Ordendesretiro.Cliente.contrato LIKE '%".$contrato."%'";
        }          
	
	if($nombre != '')
        {            
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);   
            $filtro .= " AND (Ordendesretiro.Cliente.telefono LIKE '%".$nombre."%' OR Ordendesretiro.Cliente.nombre LIKE '%".$nombre."%' OR Ordendesretiro.Cliente.celular1  LIKE '%".$nombre."%')";
        }          
        if($desde != '')
        {    
            $filtro .= " AND (Ordendesretiro.created_at >=  '$desde 00:00:00')";
        }       
        if($hasta != '')
        {    
            $filtro .= " AND (Ordendesretiro.created_at <= '$hasta 23:59:59')";
        }
        
        if($sucursal != 0)
        {
            $filtro .= " AND Ordendesretiro.Cliente.sucursal_id = ".$sucursal;
        }
                	
	if($plaza >0)
        {
            $filtro .= " AND Ordendesretiro.Cliente.plaza_id =" .$plaza;
        }
        	    
        $registros=  My_Comun::obtenerFiltro("Ordendesretiro", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 11
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 35
                        ),
				"C$i" => array(
                        "name" => 'PLAZA',
                        "width" => 7
                        ),
				"D$i" => array(
                        "name" => 'DOMICILIO',
                        "width" => 40
                        ),
				"E$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 18
                        ),
				"F$i" => array(
                        "name" => 'ETIQUETA',
                        "width" => 11
                        ),
				"G$i" => array(
                        "name" => 'NODO',
                        "width" => 8
                        ),
				"H$i" => array(
                        "name" => 'POSTE',
                        "width" => 9
                        )											
        );

        //Datos tabla
        $data = array();

        foreach($registros as $registro)
        {
									
            $i++;
            $data[] = array(				
                "A$i" =>str_pad((int)$registro->Cliente->contrato,0,"0",STR_PAD_LEFT),
                "B$i" =>$registro->Cliente->nombre,
                "C$i" =>$registro->Cliente->Plaza->nombre,
                "D$i" =>$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id),
                "E$i" =>$registro->Cliente->estatus,
				"F$i" =>$registro->Cliente->etiqueta,
				"G$i" =>$registro->Cliente->nodo,
				"H$i" =>$registro->Cliente->poste
            );
        }
				
	$objPHPExcel->createExcel('Ordendesretiro', $columns_name, $data, 10, array('rango'=>'A4:H4','size'=>14,'texto'=>'ÓRDENES DE RETIRO'));
    }
    
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }
}