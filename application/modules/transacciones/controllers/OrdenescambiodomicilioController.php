<?php

class Transacciones_OrdenescambiodomicilioController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/ordenescambiodomicilio.js'));
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $query=My_Comun::prepararQuery("Ordencambiodomicilio o");

        $query->innerJoin("o.Cliente c");
        
        

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');   
        $sucursal = $this->_getParam('sucursal');
        $plaza = $this->_getParam('plaza');            
        $calle='';
        $calle='';
        $etiqueta='';
        $nodo='';
        $poste='';
        $orden_primer_nivel=$this->_getParam('orden_primer_nivel');
        $orden_segundo_nivel=$this->_getParam('orden_segundo_nivel');
        $orden_tercer_nivel=$this->_getParam('orden_tercer_nivel');
        
        $query=$this->whereAction($query, $nombre, $calle, $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza);

        $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"c.created_at desc");                    
						   
        $query->orderBy($orden);

        	
        ### Extraemos los registros para formar el arreglo del grid
		$registros=My_Comun::registrosGrid2($query); 
				
		$grid=array();		
		$i=0;
		
		foreach($registros['registros'] as $registro)
		{
            $grid[$i]['created_at']=$registro->created_at;
            $grid[$i]['contrato']=$registro->Cliente->contrato;
            $grid[$i]['nombre']=$registro->Cliente->nombre;
            $grid[$i]['plaza']=$registro->Cliente->Plaza->nombre;
            $grid[$i]['telefono']=$registro->Cliente->telefono;        
            $grid[$i]['celular']=$registro->Cliente->celular1;
            $grid[$i]['status']=$registro->Cliente->estatus;	
            $grid[$i]['descargar']='<span onclick="descargar('.$registro->id.');" title="Descargar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
            if($registro->status==0)
                $grid[$i]['cancelar']='<span onclick="cancelar('.$registro->id.');" title="Cancelar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';					
            $i++;
		}
		
        My_Comun::grid2($registros,$grid);	

    }

    public function whereAction($query, $nombre, $calle, $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza)
    {
        
        $query->where("o.status=0");

        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $query->andWhere("c.Plaza.id=".Usuario::plaza());

        if($nombre != '')
        {
            $query->andWhere("(c.nombre LIKE '%$nombre%' OR c.telefono LIKE '%$nombre%'  OR c.calle LIKE '%$nombre%' OR c.contrato LIKE '%$nombre%')");
        }

        if($calle != '')
        {
            $query->andWhere("c.calle = '".$calle."'");
        }

        if($etiqueta != '')
        {
            $query->andWhere("c.etiqueta = '".$etiqueta."'");
        }

        if($nodo != '')
        {
            $query->andWhere("c.nodo = '".$nodo."'");
        }

        if($poste != '')
        {
            $query->andWhere("c.poste = '".$poste."'");
        }
        
        if($desde != '')
        {    
            $query->andWhere("o.created_at >=  '$desde 00:00:00'");
        }    
        if($hasta != '')
        {    
            $query->andWhere("o.created_at <= '$hasta 23:59:59'");
        }
        
        if($sucursal != 0)
        {
            $query->andWhere("c.sucursal_id = ".$sucursal);
        }   
        
        else if($plaza >0)
        {
            $query->andWhere("c.Plaza.id =" .$plaza);
        }

        return $query;
    }
    
    public function cancelarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);    
        
        Ordencambiodomicilio::cancela($this->_getParam('id'));	 	   
    }

    public function descargarAction()
    {
	    ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();  
        
        $this->view->tecnicos=Tecnico::obtenerTecnicos(' plaza_id='.Usuario::plaza().'');    
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Ordencambiodomicilio", $this->_getParam('id'));
            $this->view->instalacion = Ordeninstalacion::obtenerOrdenenInstalacionCliente($this->view->registro->cliente_id);

        }
		
    }
		
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	  
        ### Actualizamos el contrato del cliente y la orden de instalación
        Ordencambiodomicilio::actualiza($_POST);	   	   
    }
    
    public function imprimirAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);


        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');   
        $sucursal = $this->_getParam('sucursal');
        $plaza = $this->_getParam('plaza');   
        $calle='';
        $etiqueta='';
        $nodo='';
        $poste='';
        $orden_primer_nivel=$this->_getParam('orden_primer_nivel');
        $orden_segundo_nivel=$this->_getParam('orden_segundo_nivel');
        $orden_tercer_nivel=$this->_getParam('orden_tercer_nivel');

        ### Inicializamos el objeto PDF
        $pdf= new My_Fpdf_Pdf();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->Header("IMPRESIÓN DE ÓRDENES DE CAMBIO DE DOMICILIO");
        $pdf->SetWidths(array(45,45,45,45)); 


        if($this->_getParam('agrupamiento_primer_nivel')>0)
       {
            $agrupamiento=My_Comun::prepararQuery("Ordencambiodomicilio o");
            $agrupamiento->innerJoin("o.Cliente c");
            $agrupamiento=$this->whereAction($agrupamiento, $nombre, $calle, $etiqueta, $nodo, $poste, $desde, $hasta, $sucursal, $plaza);
            $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"o.created_at desc");
            $agrupamiento->orderBy($orden);
            $grupos=My_Comun::configurarAgrupamiento($agrupamiento,$this->_getParam('agrupamiento_primer_nivel'));
       }


       if(count($grupos)==0)
        $grupos[]=-1;
        
        
       foreach($grupos as $titulo=>$grupo)
       {
            $query=My_Comun::prepararQuery("Ordencambiodomicilio o");
            $query->innerJoin("o.Cliente c");

            if($grupo!=-1)
            {
                switch($this->_getParam('agrupamiento_primer_nivel'))
                {
                    case 2: $calle=$grupo; $titulo="Calle: "; break;
                    case 3: $etiqueta=$grupo; $titulo="Etiqueta: "; break;
                    case 4: $nodo=$grupo; $titulo="Nodo: "; break;
                    case 5: $poste=$grupo; $titulo="Poste: "; break;
                }
                
            }


            $query=$this->whereAction($query, $nombre, $calle, $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza);
            
            //Configuramos el orden
            $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"o.created_at desc");
            
            $query->orderBy($orden);

            //echo $query->getSqlQuery(); echo "<br><br>";

            // Ejecitamos la consulta
            $registros=  $query->execute();
            
            $i=1;
            $pdf->Header($titulo.": ".$grupo);
       
            foreach($registros as $registro)
            {
                $domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
                $domicilioretirar=$registro->calle." #".$registro->no_exterior.", Col.:".$registro->colonia;
    			
                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFillColor(220,220,220);
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('FECHA DE SOLICITUD','CONTRATO','CLIENTE','PLAZA'),1,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array($registro->created_at,$registro->Cliente->contrato,$registro->Cliente->nombre,$registro->Cliente->Plaza->nombre),1,1);

                $pdf->SetFillColor(220,220,220);
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('ESTATUS','ETIQUETA','NODO','POSTE'),1,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array($registro->Cliente->estatus,$registro->Cliente->etiqueta,$registro->Cliente->nodo,$registro->Cliente->poste,''),1,1);
    			
                $pdf->Ln(4);
                $pdf->SetWidths(array(180));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('RETIRAR DE: '.$domicilioretirar),1,1);
    			
                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('NO. DE TELEVISIONES','CABLE','CONECTOR','DIVISOR 2'),0,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array($registro->Cliente->televisores,'','',''),0,1);
    			
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('DIVISOR 3','DIVISOR 4','GRAPAS','FECHA DE RETIRO'),0,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array('','','',''),0,1);
    			
                $pdf->SetWidths(array(90,90));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('TÉCNICO QUE RETIRÓ:',''),0,1);				
    			
                $pdf->Ln(4);
                $pdf->SetWidths(array(180));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('INSTALAR EN: '.$domicilio),1,1);
    			
                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('NO. DE TELEVISIONES','CABLE','CONECTOR','DIVISOR 2'),0,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array($registro->Cliente->televisores,'','',''),0,1);
    			
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('DIVISOR 3','DIVISOR 4','GRAPAS','ETIQUETA'),0,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array('','','',''),0,1);
                $pdf->SetWidths(array(45,45,90));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('NODO','POSTE','FECHA DE INSTALACIÓN'),0,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array('','','',''),0,1);
    			
                $pdf->SetWidths(array(90,90));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('TÉCNICO QUE INSTALÓ:',''),0,1);
    			
                $pdf->SetWidths(array(45,135));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('VISTO BUENO DEL CLIENTE:',''),0,1);			
                $pdf->Ln(6);
            }//foreach registros
        }//foreach grupos
               
       $pdf->Output();	
       
    }	  

    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }
}