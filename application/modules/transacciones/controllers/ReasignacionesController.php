<?php

class Transacciones_ReasignacionesController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/reasignaciones.js'));
    }

    public function indexAction()
    {
        $this->view->plazas=Plaza::obtenerPlazas();
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        
		
		if(Usuario::tipo()==0){
        	$plaza_id = $this->_getParam('plaza');
		}else{
			$plaza_id = Usuario::plaza();
		}

        $this->view->tecnicos = Tecnico::obtenerTecnicos("status = 1 AND plaza_id=".$plaza_id);

        ### Cachamos las variables para conformar la consulta
        $tipo = $this->_getParam('tipo');
        $agrupar = $this->_getParam('agrupar');
        
        if($tipo == 1) //Instalación
        {
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Ordeninstalacion o')
            ->innerJoin('o.Cliente c')
            ->where('o.estatus=0')
            ->andWhere('c.plaza_id = '.$plaza_id);
        }
        else if($tipo == 2) //Desconexión
        {
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Ordendesconexion o')
            ->innerJoin('o.Cliente c')
            ->where('o.status=0')
            ->andWhere('c.status=2')
            ->andWhere('c.plaza_id = '.$plaza_id);
        }else if($tipo == 3) //Retiro
        {
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Ordendesretiro o')
            ->innerJoin('o.Cliente c')
            ->where('o.estatus=0')
            ->andWhere('c.status=4')
            ->andWhere('c.plaza_id = '.$plaza_id);
        }
        else if($tipo == 4) //Bajas - Deshabilidado.
        { 
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Bajavoluntaria o')
            ->innerJoin('o.Cliente c')
            ->where('o.estatus=0')
            ->andWhere('c.colonia_id >0')
            ->andWhere('c.plaza_id = '.$plaza_id);          
        }
        else if($tipo == 5) //Reinstalación
        {  
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Ordenreinstalacion o')
            ->innerJoin('o.Cliente c')
            ->where('o.estatus=0')
            ->andWhere('c.status=10')
            ->andWhere('c.plaza_id = '.$plaza_id);         
        }
        else if($tipo == 6) //Reconexiones
        {   
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Ordendesreconexion o')
            ->innerJoin('o.Cliente c')
            ->where('o.status=0')
            ->andWhere('c.status=11')
            ->andWhere('c.plaza_id = '.$plaza_id);         
        }
        else if($tipo == 7) //Cambios de domicilio (retirar)
        {
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Ordencambiodomicilio o')
            ->innerJoin('o.Cliente c')
            ->where('o.status_retiro=0')
            ->andWhere('c.plaza_id = '.$plaza_id);        
        }
        else if($tipo == 10) //Cambios de domicilio (instalar)
        {   
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Ordencambiodomicilio o')
            ->innerJoin('o.Cliente c')
            ->where('o.status=0')
            ->andWhere('c.plaza_id = '.$plaza_id);      
        }
        else if($tipo == 8) //Extensiones adicionales 
        { 
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Ordenextensionadicional o')
            ->innerJoin('o.Cliente c')
            ->where('o.status=0')
            ->andWhere('c.plaza_id = '.$plaza_id);  
        }
        else if($tipo == 9) //Extensiones adicionales
        {     
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Ordendescorte o')
            ->innerJoin('o.Cliente c')
            ->where('o.estatus=0')
            ->andWhere('c.status=3')
            ->andWhere('c.plaza_id = '.$plaza_id);      
        }
        else if($tipo == 11) //Fallas
        {       
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('ReportarFallas o')
            ->innerJoin('o.Cliente c')
            ->where('o.status_falla=1')
            ->andWhere('o.Cliente.plaza_id = '.$plaza_id);   
        }
        else if($tipo == 12) //Cambio de servicio retirar
        {       
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Ordencambioservicio o')
            ->innerJoin('o.Cliente c')
            ->where('o.status_retiro = 0 AND o.status = 0')
            ->andWhere('o.Cliente.plaza_id = '.$plaza_id);   
        }
        else if($tipo == 13) ///Cambio de servicio instalar
        {       
            // Definimos la estructura para la busqueda
            $qRegistros=Doctrine_Query::create()
            ->from('Ordencambioservicio o')
            ->innerJoin('o.Cliente c')
            ->where('o.status = 0')
            ->andWhere('o.Cliente.plaza_id = '.$plaza_id);
        }
        // Definimos el agrupamiento
        if($agrupar == '1') /*Colonia*/ $qRegistros->orderBy("c.Colonia.nombre asc");
        else if($agrupar == '2') /*Calle*/ $qRegistros->orderBy("c.calle asc");
        else if($agrupar == '3') /*Etiqueta*/ $qRegistros->orderBy("c.etiqueta asc");
        else if($agrupar == '4') /*Nodo*/ $qRegistros->orderBy("c.nodo asc");
        else if($agrupar == '5') /*Poste*/ $qRegistros->orderBy("c.poste asc"); 

        $this->view->total=$qRegistros->count();

        $grupo=array("tipo"=>$agrupar,"data"=>array());

        //Cofiguramos el orden
        if($tipo!=11)
            $orden=My_Comun::configurarOrden($this->_getParam("ordenar1"),$this->_getParam("ordenar2"),$this->_getParam("ordenar3"),"o.created_at DESC");
        else
            $orden=My_Comun::configurarOrden($this->_getParam("ordenar1"),$this->_getParam("ordenar2"),$this->_getParam("ordenar3"),"o.fecha_reporte DESC");
        $qRegistros->orderBy($orden);

        //echo $qRegistros->getSqlQuery();
        $registros=$qRegistros->execute();

		$this->view->agrupar=$agrupar;

        foreach($registros as $registro)
        {
            if($agrupar==1) // Colonia
            {
                $grupo['data'][$registro->Cliente->colonia_id]['colonia']=$registro->Cliente->Colonia->nombre;
                if($tipo == 7 || $tipo == 12) /*Domicilio/Servicio Retirar*/ $grupo['data'][$registro->Cliente->colonia_id]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, Usuario::obtenerNombrePorId($registro->tecnico_retiro), $registro->tecnico_retiro);
                else if($tipo == 10 || $tipo == 13) /*Domicilio/Servicio Instalar*/ $grupo['data'][$registro->Cliente->colonia_id]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, Usuario::obtenerNombrePorId($registro->tecnico_instalo), $registro->tecnico_instalo);
                else /*Otros*/ $grupo['data'][$registro->Cliente->colonia_id]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, $registro->Tecnico->nombre, $registro->tecnico_id);
            }
            else if($agrupar==2) // Calle
            {
                $grupo['data'][$registro->Cliente->calle]['colonia']=$registro->Cliente->calle;
                if($tipo == 7 || $tipo == 12) /*Domicilio/Servicio Retirar*/ $grupo['data'][$registro->Cliente->calle]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, Usuario::obtenerNombrePorId($registro->tecnico_retiro), $registro->tecnico_retiro);
                else if($tipo == 10 || $tipo == 13) /*Domicilio/Servicio Instalar*/ $grupo['data'][$registro->Cliente->calle]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, Usuario::obtenerNombrePorId($registro->tecnico_instalo), $registro->tecnico_instalo);
                else /*Otros*/ $grupo['data'][$registro->Cliente->calle]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, $registro->Tecnico->nombre, $registro->tecnico_id);
            }
            else if($agrupar==3) // Etiqueta
            {
                $grupo['data'][$registro->Cliente->etiqueta]['colonia']=$registro->Cliente->etiqueta;
                if($tipo == 7 || $tipo == 12) /*Domicilio/Servicio Retirar*/ $grupo['data'][$registro->Cliente->etiqueta]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, Usuario::obtenerNombrePorId($registro->tecnico_retiro), $registro->tecnico_retiro);
                else if($tipo == 10 || $tipo == 13) /*Domicilio/Servicio Instalar*/ $grupo['data'][$registro->Cliente->etiqueta]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, Usuario::obtenerNombrePorId($registro->tecnico_instalo), $registro->tecnico_instalo);
                else /*Otros*/ $grupo['data'][$registro->Cliente->etiqueta]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, $registro->Tecnico->nombre, $registro->tecnico_id);
            }
            else if($agrupar==4) // Nodo
            {
                $grupo['data'][$registro->Cliente->nodo]['colonia']=$registro->Cliente->nodo;
                if($tipo == 7 || $tipo == 12) /*Domicilio/Servicio Retirar*/ $grupo['data'][$registro->Cliente->nodo]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, Usuario::obtenerNombrePorId($registro->tecnico_retiro), $registro->tecnico_retiro);
                else if($tipo == 10 || $tipo == 13) /*Domicilio/Servicio Instalar*/ $grupo['data'][$registro->Cliente->nodo]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, Usuario::obtenerNombrePorId($registro->tecnico_instalo), $registro->tecnico_instalo);
                else /*Otros*/ $grupo['data'][$registro->Cliente->nodo]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, $registro->Tecnico->nombre, $registro->tecnico_id);
            }
            else if($agrupar==5) // Poste
            {
                $grupo['data'][$registro->Cliente->poste]['colonia']=$registro->Cliente->poste;
                if($tipo == 7 || $tipo == 12) /*Domicilio/Servicio Retirar*/ $grupo['data'][$registro->Cliente->poste]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, Usuario::obtenerNombrePorId($registro->tecnico_retiro), $registro->tecnico_retiro);
                else if($tipo == 10 || $tipo == 13) /*Domicilio/Servicio Instalar*/ $grupo['data'][$registro->Cliente->poste]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, Usuario::obtenerNombrePorId($registro->tecnico_instalo), $registro->tecnico_instalo);
                else /*Otros*/ $grupo['data'][$registro->Cliente->poste]['ordenes'][$registro->id]=array($registro->Cliente->contrato, $registro->Cliente->nombre, $registro->Cliente->Plaza->nombre, $registro->Tecnico->nombre, $registro->tecnico_id);
            }
		}

        $this->view->grupo=$grupo;
        $this->view->tipo=$tipo;

    }

    public function guardarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        $tipo=$_POST['tipo'];

        foreach($_POST['tecnico'] as $colonia=>$tecnico)
        {
            if($tecnico!=0)
            {

                if($tipo == 1) //Instalación
                {
                    $mensaje=array("titulo"=>"Nueva orden de instalación","mensaje"=>"Tienes una nueva orden de instalación asignada","seccion"=>"ordenesdeinstalacion");
                    $tabla = 'Ordeninstalacion';
                }
                elseif($tipo == 2) //Desconexión
                {
                    $mensaje=array("titulo"=>"Nueva orden de desconexión","mensaje"=>"Tienes una nueva orden de desconexion asignada","seccion"=>"ordenesdedesconexion");
                    $tabla = 'Ordendesconexion';
                }
                elseif($tipo == 3) //Retiro
                {
                    $mensaje=array("titulo"=>"Nueva orden de retiro","mensaje"=>"Tienes una nueva orden de retiro asignada","seccion"=>"ordenesderetiro");
                    $tabla = 'Ordendesretiro';
                }
                elseif($tipo == 4) //Bajas - Deshabilidado.
                {
                    $mensaje=array("titulo"=>"Nueva orden de baja","mensaje"=>"Tienes una nueva orden de baja asignada","seccion"=>"ordenesdebaja");
                    $tabla = 'Bajavoluntaria';
                }
                elseif($tipo == 5) //Reinstalación
                {
                    $mensaje=array("titulo"=>"Nueva orden de reinstalación","mensaje"=>"Tienes una nueva orden de reinstalación asignada","seccion"=>"ordenesdereinstalacion");
                    $tabla = 'Ordenreinstalacion';
                }
                elseif($tipo == 6) //Reconexión
                {
                    $mensaje=array("titulo"=>"Nueva orden de reconexión","mensaje"=>"Tienes una nueva orden de reconexión","seccion"=>"ordenesdereconexion");
                    $tabla = 'Ordendesreconexion';
                }
                elseif($tipo == 7) //Cambio de domicilio
                {
                    $mensaje=array("titulo"=>"Nueva orden de cambio de domicilio (retirar)","mensaje"=>"Tienes una nueva orden de cambio de domicilio","seccion"=>"ordenesdecamiodedomicilio");
                    $tabla = 'Ordencambiodomicilio';
                }
                elseif($tipo == 8) //Extensiones adicionales
                {
                    $mensaje=array("titulo"=>"Nueva orden de extensión adicional","mensaje"=>"Tienes una nueva orden de extensión adicional","seccion"=>"ordenesdeextensionesadicionales");
                    $tabla = 'Ordenextensionadicional';
                }
                elseif($tipo == 9) //Ordenes de corte
                {
                    $mensaje=array("titulo"=>"Nueva orden de corte","mensaje"=>"Tienes una nueva orden de corte","seccion"=>"ordenesdeextensionesadicionales");
                    $tabla = 'Ordendescorte';
                }
                elseif($tipo == 10) //Cambio de domicilio
                {
                    $mensaje=array("titulo"=>"Nueva orden de cambio de domicilio (instalar)","mensaje"=>"Tienes una nueva orden de cambio de domicilio","seccion"=>"ordenesdecamiodedomicilio");
                    $tabla = 'Ordencambiodomicilio';
                }
                elseif($tipo == 11) //Orden de falla
                {
                    $mensaje=array("titulo"=>"Nueva falla por atender","mensaje"=>"Tienes una nueva falla por atender","seccion"=>"ordenesdefalla");
                    $tabla = 'ReportarFallas';
                }
                elseif($tipo == 12) //Orden cambio de servicio retirar
                {
                    $mensaje=array("titulo"=>"Nueva orden de cambio de servicio (retirar)","mensaje"=>"Tienes una nueva orden de cambio de servicio","seccion"=>"ordenesdecambiodeservicio");
                    $tabla = 'Ordencambioservicio';
                }
                elseif($tipo == 13) //Orden cambio de servicio instalar
                {
                    $mensaje=array("titulo"=>"Nueva orden de cambio de servicio (instalar)","mensaje"=>"Tienes una nueva orden de cambio de servicio","seccion"=>"ordenesdecambiodeservicio");
                    $tabla = 'Ordencambioservicio';
                }

                $asignadas=0;
                foreach($_POST['orden'][$colonia] as $orden)
                {
                    $o = Doctrine_Query::create()->from($tabla)->where("id=".$orden)->execute()->getFirst();
                    $t = Usuario::obtenerPorId($tecnico);

                    if($o->Cliente->plaza_id != $t->plaza_id)
                    {
                        $mail = new Zend_Mail('UTF-8');
                        $mail->setBodyHtml("Usuario(1|".$tipo."): [".Usuario::id()."]".Usuario::obtenerPorId(Usuario::id())->nombre." ||| El tecnico [".$tecnico."]".$t->nombre." de la plaza [".$t->plaza_id."] ".$t->Plaza->nombre." no es de la plaza [".$o->Cliente->plaza_id."] ".$o->Cliente->Plaza->nombre." del cliente ".$o->Cliente->nombre);
                        $mail->setFrom('pagos@supertv.com.mx', 'Ultracable');
                        $mail->addTo("ricardo_sonora@avansys.com.mx", "Ricardo");
                        $mail->setSubject("Asignacion de orden a un tecnico de una plaza diferente");
                        $mail->send();
                    }

                    if($tipo!=7 && $tipo!=10 && $tipo!=12 && $tipo!=13)
                    {
                        if($_POST['tipoasignacion']==1)
                            $q=Doctrine_Query::create()->update($tabla)->set('tecnico_id',$tecnico)->where("id=".$orden)->execute();
                        else
                            $q=Doctrine_Query::create()->update($tabla)->set('tecnico_id',$tecnico)->where("id=".$orden." AND tecnico_id IS NULL")->execute();
                    }
                    else
                    {
                        if($_POST['tipoasignacion']==1)
                        {
                            if($tipo==7 || $tipo==12)
                                $q=Doctrine_Query::create()->update($tabla)->set('tecnico_retiro',$tecnico)->where("id=".$orden)->execute();
                            if($tipo==10 || $tipo==13)
                                $q=Doctrine_Query::create()->update($tabla)->set('tecnico_instalo',$tecnico)->where("id=".$orden)->execute();
                        }
                        else
                        {
                            if($tipo==7 || $tipo==12)
                                $q=Doctrine_Query::create()->update($tabla)->set('tecnico_retiro',$tecnico)->where("id=".$orden." AND tecnico_retiro IS NULL")->execute();
                            if($tipo==10 || $tipo==12)
                            {
                                $q=Doctrine_Query::create()->update($tabla)->set('tecnico_instalo',$tecnico)->where("id=".$orden." AND tecnico_instalo IS NULL")->execute();
                            }
                        }
                    }
                }

                $usuario = My_Comun::obtener('Usuario', $tecnico);
                if($usuario->gcm_id != '' && ($tipo != 12 && $tipo != 13))
                    My_Comun::pushnotification($usuario->gcm_id, $mensaje);
            }
        }
        
        echo "Las reasignaciones se han guardado con éxito.";
    }


    public function asignarindividualAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);  

        $tipo=$this->_getParam('tipo');
        $tecnico=$this->_getParam('tecnico');
        $orden=$this->_getParam('orden');

        if($tipo == 1) //Instalación
        {
            $mensaje=array("titulo"=>"Nueva orden de instalación","mensaje"=>"Tienes una nueva orden de instalación asignada","seccion"=>"ordenesdeinstalacion");
            $tabla = 'Ordeninstalacion';
        }
        elseif($tipo == 2) //Desconexión
        {
            $mensaje=array("titulo"=>"Nueva orden de desconexión","mensaje"=>"Tienes una nueva orden de desconexion asignada","seccion"=>"ordenesdedesconexion");
            $tabla = 'Ordendesconexion';
        }
        elseif($tipo == 3) //Retiro
        {
            $mensaje=array("titulo"=>"Nueva orden de retiro","mensaje"=>"Tienes una nueva orden de retiro asignada","seccion"=>"ordenesderetiro");
            $tabla = 'Ordendesretiro';
        }
        elseif($tipo == 4) //Bajas - Deshabilidado.
        {
            $mensaje=array("titulo"=>"Nueva orden de baja","mensaje"=>"Tienes una nueva orden de baja asignada","seccion"=>"ordenesdebaja");
            $tabla = 'Bajavoluntaria';
        }
        elseif($tipo == 5) //Reinstalación
        {
            $mensaje=array("titulo"=>"Nueva orden de reinstalación","mensaje"=>"Tienes una nueva orden de reinstalación asignada","seccion"=>"ordenesdereinstalacion");
            $tabla = 'Ordenreinstalacion';
        }
        elseif($tipo == 6) //Reconexión
        {
            $mensaje=array("titulo"=>"Nueva orden de reconexión","mensaje"=>"Tienes una nueva orden de reconexión","seccion"=>"ordenesdereconexion");
            $tabla = 'Ordendesreconexion';
        }
        elseif($tipo == 7) //Cambio de domicilio
        {
            $mensaje=array("titulo"=>"Nueva orden de cambio de domicilio (retirar)","mensaje"=>"Tienes una nueva orden de cambio de domicilio","seccion"=>"ordenesdecamiodedomicilio");
            $tabla = 'Ordencambiodomicilio';
        }
        elseif($tipo == 8) //Extensiones adicionales
        {
            $mensaje=array("titulo"=>"Nueva orden de extensión adicional","mensaje"=>"Tienes una nueva orden de extensión adicional","seccion"=>"ordenesdeextensionesadicionales");
            $tabla = 'Ordenextensionadicional';
        }
        elseif($tipo == 9) //Ordenes de corte
        {
            $mensaje=array("titulo"=>"Nueva orden de corte","mensaje"=>"Tienes una nueva orden de corte","seccion"=>"ordenesdeextensionesadicionales");
            $tabla = 'Ordendescorte';
        }
        elseif($tipo == 10) //Cambio de domicilio
        {
            $mensaje=array("titulo"=>"Nueva orden de cambio de domicilio (instalar)","mensaje"=>"Tienes una nueva orden de cambio de domicilio","seccion"=>"ordenesdecamiodedomicilio");
            $tabla = 'Ordencambiodomicilio';
        }
        elseif($tipo == 11) //Orden de falla
        {
            $mensaje=array("titulo"=>"Nueva falla por atender","mensaje"=>"Tienes una nueva falla por atender","seccion"=>"ordenesdefalla");
            $tabla = 'ReportarFallas';
        }
        elseif($tipo == 12) //Orden cambio de servicio retirar
        {
            $mensaje=array("titulo"=>"Nueva orden de cambio de servicio (retirar)","mensaje"=>"Tienes una nueva orden de cambio de servicio","seccion"=>"ordenesdecambiodeservicio");
            $tabla = 'Ordencambioservicio';
        }
        elseif($tipo == 13) //Orden cambio de servicio instalar
        {
            $mensaje=array("titulo"=>"Nueva orden de cambio de servicio (instalar)","mensaje"=>"Tienes una nueva orden de cambio de servicio","seccion"=>"ordenesdecambiodeservicio");
            $tabla = 'Ordencambioservicio';
        }

        $o = Doctrine_Query::create()->from($tabla)->where("id=".$orden)->execute()->getFirst();
        $t = Usuario::obtenerPorId($tecnico);

        if($o->Cliente->plaza_id != $t->plaza_id)
        {
            $mail = new Zend_Mail('UTF-8');
            $mail->setBodyHtml("Usuario(2|".$tipo."): [".Usuario::id()."]".Usuario::obtenerPorId(Usuario::id())->nombre." ||| El tecnico [".$tecnico."]".$t->nombre." de la plaza [".$t->plaza_id."] ".$t->Plaza->nombre." no es de la plaza [".$o->Cliente->plaza_id."] ".$o->Cliente->Plaza->nombre." del cliente ".$o->Cliente->nombre);
            $mail->setFrom('pagos@supertv.com.mx', 'Ultracable');
            $mail->addTo("ricardo_sonora@avansys.com.mx", "Ricardo");
            $mail->setSubject("Asignacion de orden a un tecnico de una plaza diferente");
            $mail->send();
        }

        if($tipo!=7 && $tipo!=10 && $tipo!=12 && $tipo!=13)
        {
            $q=Doctrine_Query::create()->update($tabla)->set('tecnico_id',$tecnico)->where("id=".$orden)->execute();
        }
        else
        {
            if($tipo==7 || $tipo==12)
                $q=Doctrine_Query::create()->update($tabla)->set('tecnico_retiro',$tecnico)->where("id=".$orden)->execute();
            if($tipo==10 || $tipo==13)
                $q=Doctrine_Query::create()->update($tabla)->set('tecnico_instalo',$tecnico)->where("id=".$orden)->execute();            
        }

        $usuario = My_Comun::obtener('Usuario', $tecnico);
        if($usuario->gcm_id != '' && ($tipo != 12 && $tipo != 13))
            My_Comun::pushnotification($usuario->gcm_id, $mensaje);
    }
}