<?php
class Transacciones_GastosController extends Zend_Controller_Action
{

    public function init()
    {
    	$this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/gastos.js?v='.time()));
    }

    public function indexAction()
    {
		$this->view->plazas=Plaza::obtenerPlazas();
        //$this->view->sucursales=Sucursal::obtenerSucursal(); 
    }
		
    public function calcularAction()
    {	
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);    
		
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" plaza_id=".Usuario::plaza()." ";
				
        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
	$hasta = $this->_getParam('hasta');
	$plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal'); 
        
        if($sucursal != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal."  ";
        } 
        
        if($desde != '')
        {
           $filtro .= " AND fecha >= '$desde 00:00:00'";
        }
		
	if($hasta != '')
        {
           $filtro .= " AND fecha <= '$hasta 23:59:59'";
        }

        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
		
	$this->view->calcular=Gasto::obtenerGastos($filtro);
	
        echo "$".number_format($this->view->calcular->monto,2,".",",");
    }
	
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);    
		
        ### Establecemos el filtro por default
        $filtro = "cancelado = 0 ";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" plaza_id=".Usuario::plaza()." ";
			
        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
	$hasta = $this->_getParam('hasta');
	$plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal'); 
        
        if($sucursal != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal."  ";
        }           
        if($desde != '')
        {
           $filtro .= " AND fecha >= '$desde 00:00:00'";
        }		
	if($hasta != '')
        {
           $filtro .= " AND fecha <= '$hasta 23:59:59'";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
		
	### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Gasto",$filtro); 
		
	$grid=array();
		
	$i=0;
	$editar=false;
	if(My_Permisos::tienePermiso('EDITAR_CLIENTES') == 1)
            $editar=true;			
			
            foreach($registros['registros'] as $registro)
            {
                $grid[$i]['fecha']=$registro->fecha;
                $grid[$i]['concepto']=$registro->concepto;
				$grid[$i]['monto']="$".number_format($registro->monto,2,".",",");
					
				$grid[$i]['editar']='<span onclick="agregarGasto('.$registro->id.','.Usuario::tipo().');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png"/></span>';
				if(Usuario::tipo()==0)
					$grid[$i]['eliminar']='<span onclick="eliminarGastos('.$registro->id.');" title="Cancelar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png"/></span>';
				else
					$grid[$i]['eliminar']='<span  title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar_off.png"/></span>';
				$i++;
            }
		
        My_Comun::grid2($registros,$grid);	
    }

    public function agregarAction()
    {
     	### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
		
        $this->view->plazas=Plaza::obtenerPlazas();
       
	### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Gasto", $this->_getParam('id'));
        }
    }

    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);

       $_POST['created_at']=$_POST['fecha'];
       $_POST['usuario_id']=Usuario::id();
       $_POST['sucursal_id']=Usuario::sucursal();
       if($_POST['id']>0)
       {
        unset($_POST['sucursal_id']);
        unset($_POST['usuario_id']);
        $_POST['modifico_id']=Usuario::id();
       }
       
	   
       echo My_Comun::guardar("Gasto",$_POST,NULL,$_POST['id'],'concepto');
    }
	
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
 	### Establecemos el filtro por default
        $filtro = "cancelado = 0 ";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" plaza_id=".Usuario::plaza()." ";
				
        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
	$hasta = $this->_getParam('hasta');
	$plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal'); 
        
        if($sucursal != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal."  ";
        } 
        if($desde != '')
        {
           $filtro .= " AND fecha >= '$desde'";
        }
		
	if($hasta != '')
        {
           $filtro .= " AND fecha <= '$hasta'";
        }

        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        
        $registros=  My_Comun::obtenerFiltro("Gasto", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE GASTOS");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(45,90,45));
        $pdf->Row(array('FECHA','CONCEPTO','MONTO'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
           $monto= "$".number_format($registro->monto,2,".",",");
           $pdf->Row
           (
                array
                (
                    $registro->fecha,
                    $registro->concepto,
                    $monto
                ),0,1			
           );
        }
        
       $pdf->Output();	
    }
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
 	### Establecemos el filtro por default
        $filtro = "cancelado = 0 ";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" plaza_id=".Usuario::plaza()." ";
				
        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
	$hasta = $this->_getParam('hasta');
	$plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal'); 
        
        if($sucursal != 0)
        {
            $filtro .= " AND sucursal_id = ".$sucursal."  ";
        } 
        if($desde != '')
        {
           $filtro .= " AND fecha >= '$desde'";
        }
		
	if($hasta != '')
        {
           $filtro .= " AND fecha <= '$hasta'";
        }

        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }

        $registros=  My_Comun::obtenerFiltro("Gasto", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
				
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'CONCEPTO',
                        "width" => 40
                        ),
				"C$i" => array(
                        "name" => 'MONTO',
                        "width" => 20
                        )			
        );
		
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
            $monto= "$".number_format($registro->monto,2,".",",");
            $i++;
            $data[] = array(
		"A$i" =>$registro->fecha,
               	"B$i" =>$registro->concepto,
               	"C$i" =>$monto
            );
        }
		
        $objPHPExcel->createExcel('ReporteGastos', $columns_name, $data, 10,array('rango'=>'A4:C4','texto'=>'REPORTE DE GASTOS'));
		
    }
     
	public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }

    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
            
        $q = Doctrine_Query::create()->update("Gasto")->set('cancelado','1')->where('id='.$this->_getParam('id'));
        //echo $q->getSqlQuery(); exit;
        $q->execute();
        
        $accion="Eliminó el registro: ".$this->_getParam('id');
        Bitacora::registrarEvento("Gasto", $accion, Zend_Auth::getInstance()->getIdentity()->nombre);  
    }
	
}