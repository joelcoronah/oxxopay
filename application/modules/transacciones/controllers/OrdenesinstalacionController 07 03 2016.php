<?php

class Transacciones_OrdenesinstalacionController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/ordenesinstalacion.js'));
    }

    public function indexAction()
    {
		//Doctrine_Query::create()->from('Ordeninstalacion O')
        ### Obtenemos los estados para alimentar el filtro
    	$this->view->plazas=Plaza::obtenerPlazas();
        if(Usuario::plaza()==null)
            $this->view->servicios=Servicio::obtenerServicios('status=1 AND tipo=1');
        else
            $this->view->servicios=Servicio::obtenerServiciosAContratar();
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        $query=My_Comun::prepararQuery("Ordeninstalacion o");

        $query->innerJoin("o.Cliente c");
        //$query->innerJoin("c.Colonia co");


        ### Establecemos el filtro por default		
		//$filtro=" Ordeninstalacion.estatus=0 AND Ordeninstalacion.Cliente.status=6";
        if($this->_getParam('estatus'))
            $query->where("o.estatus=".$this->_getParam('estatus'));
        else
            $query->where("o.estatus=0");
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $query->andWhere("c.Plaza.id=".Usuario::plaza());

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');  
        $sucursal = $this->_getParam('sucursal');   
        $plaza = $this->_getParam('plaza');
		$estatus = $this->_getParam('estatus');
		$ordenar = $this->_getParam('ordenar');
		$calle = $this->_getParam('calle');
		$colonia = $this->_getParam('colonia');                   
        $servicio = $this->_getParam('servicio'); 
        $orden_primer_nivel=$this->_getParam('orden_primer_nivel');
        $orden_segundo_nivel=$this->_getParam('orden_segundo_nivel');
        $orden_tercer_nivel=$this->_getParam('orden_tercer_nivel');

        //$query->andWhere("c.colonia_id IS NOT NULL");

        if($nombre != '')
        {
            $query->andWhere("(c.nombre LIKE '%$nombre%' OR c.telefono LIKE '%$nombre%'  OR c.calle LIKE '%$nombre%' OR c.contrato LIKE '%$nombre%')");
        }
		
		if($calle != '')
		{
			$query->andWhere("c.calle LIKE '%$calle%'");
		}
		
		if($colonia != '')
		{
			$query->andWhere("c.Colonia.nombre LIKE '%$colonia%'");
		}
        
        if($desde != '')
        {    
            $query->andWhere("o.created_at >=  '$desde 00:00:00'");
        }    
        if($hasta != '')
        {    
            $query->andWhere("o.created_at <= '$hasta 23:59:59'");
        }
        
        if($sucursal != 0)
        {
            $query->andWhere("c.sucursal_id = ".$sucursal);
        }   
        
        if($plaza >0)
        {
            $query->andWhere("c.Plaza.id =" .$plaza);
        }
        if($servicio >0)
        {
            $query->andWhere("c.ClienteServicio.servicio_id =" .$servicio);
        }
				
		


        //Configuramos el orden
        $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"c.fecha_contratacion desc");
        
        $query->orderBy($orden);

        //echo $query->getSqlQuery(); exit;
			
		### Extraemos los registros para formar el arreglo del grid
		$registros=My_Comun::registrosGrid2($query); 
		
		$grid=array();		
		$i=0;
		
		foreach($registros['registros'] as $registro)
		{
            $grid[$i]['created_at']=$registro->created_at;
            $grid[$i]['contrato']=$registro->Cliente->contrato;
            $grid[$i]['nombre']=$registro->Cliente->nombre;
            $grid[$i]['servicio']=$registro->Cliente->TarifaConceptoContratacion->ConceptoDeContratacion->nombre.": ".$registro->Cliente->ClienteServicio[0]->Servicio->nombre;
            $grid[$i]['plaza']=$registro->Cliente->Plaza->nombre;
            $grid[$i]['telefono']=$registro->Cliente->telefono;
            $grid[$i]['celular']=$registro->Cliente->celular1;
            $grid[$i]['status']=$registro->Cliente->estatus;
            if(Usuario::tipo()==0)
            {
                
            }
            else
            {
                $grid[$i]['descargar']='<span onclick="descargarInstalacion('.$registro->id.');" title="Descargar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
                if($registro->estatus==0)
                $grid[$i]['cancelar']='<span onclick="cancelarInstalacion('.$registro->id.');" title="Cancelar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';    
            }
					
		    $i++;
	    }
		
        My_Comun::grid2($registros,$grid);	
    }

    public function descargarinstalacionAction()
    {
	### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos las plazas
        $this->view->plazas=Plaza::obtenerPlazas();
        ### Obtenemos los estados 
        $this->view->comunidades=Comunidad::obtenerComunidades(1);
        
        $this->view->tecnicos=Tecnico::obtenerTecnicos(' plaza_id='.Usuario::plaza());
        
        $this->view->concepto1=Servicio::obtenerServicios("status=1 AND tipo=1");
        $this->view->concepto2=Servicio::obtenerServicios("status=1 AND tipo=2");
        
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Ordeninstalacion", $this->_getParam('id'));
        }
		
	}
	
	public function cancelarinstalacionAction()
	{
	   ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
       Ordeninstalacion::cancela($this->_getParam('id'));	 
	   
    }
		
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	  

	   ### Actualizamos el contrato del cliente y la orden de instalación
       Ordeninstalacion::actualiza($_POST);	   
	   
    }
    
    public function imprimirAction()
    {

       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);

       ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');  
        $sucursal = $this->_getParam('sucursal');   
        $plaza = $this->_getParam('plaza');
        $estatus = $this->_getParam('estatus');
        $ordenar = $this->_getParam('ordenar');
        $calle = $this->_getParam('calle');
        $colonia = $this->_getParam('colonia'); 
        $servicio = $this->_getParam('servicio');                 
        $orden_primer_nivel=$this->_getParam('orden_primer_nivel');
        $orden_segundo_nivel=$this->_getParam('orden_segundo_nivel');
        $orden_tercer_nivel=$this->_getParam('orden_tercer_nivel');

       ### Inicializamos el objeto PDF
       $pdf= new My_Fpdf_Pdf();
       $pdf->AliasNbPages();
       $pdf->AddPage();
       $pdf->Header("IMPRESIÓN DE ÓRDENES DE INSTALACIÓN");
       $pdf->SetWidths(array(45,45,45,45));


       if($this->_getParam('agrupamiento_primer_nivel')>0)
       {
            $agrupamiento=My_Comun::prepararQuery("Ordeninstalacion o");
            $agrupamiento->innerJoin("o.Cliente c");
            $agrupamiento=$this->whereAction($agrupamiento, $nombre, $calle, $colonia, $desde, $hasta, $sucursal, $plaza,$this->_getParam('estatus'), $servicio);
            $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"c.fecha_contratacion desc");
            $agrupamiento->orderBy($orden);
            $grupos=My_Comun::configurarAgrupamiento($agrupamiento,$this->_getParam('agrupamiento_primer_nivel'));
            /*echo "<pre>";
            print_r($grupos); exit;*/
       }

       if(count($grupos)==0)
        $grupos[]=-1;
       


       foreach($grupos as $grupo)
       {
            if(empty($grupo))
                continue;

            $query=My_Comun::prepararQuery("Ordeninstalacion o");
            $query->innerJoin("o.Cliente c");

            if($grupo!=-1)
            {
                switch($this->_getParam('agrupamiento_primer_nivel'))
                {
                    case 1: $colonia=$grupo; $titulo="Colonia: "; break;
                    case 2: $calle=$grupo; $titulo="Calle: "; break;
                    case 3: $etiqueta=$grupo; $titulo="Etiqueta: "; break;
                    case 4: $nodo=$grupo; $titulo="Nodo: "; break;
                    case 5: $poste=$grupo; $titulo="Poste: "; break;
                }
                
            }


            $query=$this->whereAction($query, $nombre, $calle, $colonia, $desde, $hasta, $sucursal, $plaza,$this->_getParam('estatus'), $servicio);
            
            //Configuramos el orden
            $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"c.fecha_contratacion desc");
            
            $query->orderBy($orden);

            //echo $query->getSqlQuery(); echo "<br><br>"; continue;

            // Ejecitamos la consulta
    		$registros=  $query->execute();
    		
            
            $i=1;
            $pdf->Header($titulo.": ".$grupo);
            foreach($registros as $registro)
            {
                if($i==4)
                {
                    $i=1;
    		        $pdf->AddPage();
                }
                $domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".$registro->Cliente->Colonia->nombre;
    			
                

                $pdf->SetWidths(array(45,30,45,20,40));
                $pdf->SetFillColor(220,220,220);
                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('FECHA DE CONTRATACIÓN','CONTRATO','CLIENTE','PLAZA','ESTATUS'),1,1);
                $pdf->SetFont('Arial','',8);
                $pdf->Row(array($registro->Cliente->fecha_contratacion,$registro->Cliente->contrato,$registro->Cliente->nombre,$registro->Cliente->Plaza->nombre,$registro->Cliente->estatus),1,1);

                $pdf->SetWidths(array(45,135));
                $pdf->SetFont('Arial','B',8);
                $servicioContratado=$registro->Cliente->ClienteServicio[0]->Servicio->nombre;
                $pdf->Row(array('SERVICIO',$registro->Cliente->TarifaConceptoContratacion->ConceptoDeContratacion->nombre.": ".$servicioContratado),1,1);
    			
                $pdf->SetWidths(array(45,135));
                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('DOMICILIO',$domicilio),1,1);

                $pdf->SetWidths(array(45,135));
                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('REFERENCIA',$registro->Cliente->referencia),1,1);

                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFillColor(220,220,220);
                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('TELEFONO',$registro->Cliente->telefono,'CELULAR',$registro->Cliente->celular1),1,1);
                
    			
                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('NO. DE TELEVISIONES','CABLE','CONECTOR','DIVISOR 2'),0,1);
                $pdf->SetFont('Arial','',8);
                $pdf->Row(array($registro->Cliente->televisores,$registro->cable,$registro->conector,$registro->divisor2),0,1);
    			
                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('DIVISOR 3','DIVISOR 4','GRAPAS','ETIQUETA'),0,1);
                $pdf->SetFont('Arial','',8);
                $pdf->Row(array($registro->divisor3,$registro->divisor4,$registro->grapas,$registro->Cliente->etiqueta),0,1);
    			
                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('MAC ADDRESS DEL ROUTER','AC ADDRESS DEL SWITCH','MAC ADDRESS DEL ROKU','SALIDA DEL SWITCH'),0,1);
                $pdf->SetFont('Arial','',8);
                $pdf->Row(array($registro->mac_router,$registro->mac_switch,$registro->mac_roku,$registro->salida_switch),0,1);

                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('NODO','FECHA DE INSTALACIÓN','HORA INICIO','HORA DE TÉRMINO'),0,1);
                $pdf->SetFont('Arial','',8);
                $pdf->Row(array($registro->Cliente->nodo,$registro->fecha_instalacion,'',''),0,1);
    			
                $pdf->SetWidths(array(90,90));
                $pdf->SetFont('Arial','B',11);
                $pdf->Row(array('NOMBRE DEL TÉCNICO INSTALADOR:',''),0,1);	
    			
                $pdf->SetWidths(array(45,135));
                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('VISTO BUENO DEL CLIENTE:',''),0,1);			
                $pdf->Ln(3);
                $i++;
            }
        }
            

       $pdf->Output();	
       
    }

    public function whereAction($query, $nombre, $calle, $colonia, $desde, $hasta, $sucursal, $plaza, $estatus, $servicio)
    {
        // Establecemos el filtro por default      
        if($estatus)
            $query->where("o.estatus=".$estatus);
        else
            $query->where("o.estatus=0");
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $query->andWhere("c.Plaza.id=".Usuario::plaza());

        if($nombre != '')
        {
            $query->andWhere("(c.nombre LIKE '%$nombre%' OR c.telefono LIKE '%$nombre%'  OR c.calle LIKE '%$nombre%' OR c.contrato LIKE '%$nombre%')");
        }
        
        if($calle != '')
        {
            $query->andWhere("c.calle = '".$calle."'");
        }
        
        if($colonia != '')
        {
            $query->andWhere("c.Colonia.nombre = '".$colonia."'");
        }
        
        if($desde != '')
        {    
            $query->andWhere("o.created_at >=  '$desde 00:00:00'");
        }    
        if($hasta != '')
        {    
            $query->andWhere("o.created_at <= '$hasta 23:59:59'");
        }
        
        if($sucursal != 0)
        {
            $query->andWhere("c.sucursal_id = ".$sucursal);
        }   
        
        if($plaza >0)
        {
            $query->andWhere("c.Plaza.id =" .$plaza);
        }

        if($servicio >0)
        {
            $query->andWhere("c.ClienteServicio.servicio_id =" .$servicio);
        }

        

        return $query;

    }
		
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        

        $query=My_Comun::prepararQuery("Ordeninstalacion o");

        $query->innerJoin("o.Cliente c");
        //$query->innerJoin("c.Colonia co");


        ### Establecemos el filtro por default      
        //$filtro=" Ordeninstalacion.estatus=0 AND Ordeninstalacion.Cliente.status=6";
        if($this->_getParam('estatus'))
            $query->where("o.estatus=".$this->_getParam('estatus'));
        else
            $query->where("o.estatus=0");
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $query->andWhere("c.Plaza.id=".Usuario::plaza());

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');  
        $sucursal = $this->_getParam('sucursal');   
        $plaza = $this->_getParam('plaza');
        $estatus = $this->_getParam('estatus');
        $ordenar = $this->_getParam('ordenar');
        $calle = $this->_getParam('calle');
        $colonia = $this->_getParam('colonia'); 
        $servicio = $this->_getParam('servicio');                    
        $orden_primer_nivel=$this->_getParam('orden_primer_nivel');
        $orden_segundo_nivel=$this->_getParam('orden_segundo_nivel');
        $orden_tercer_nivel=$this->_getParam('orden_tercer_nivel');

        //$query->andWhere("c.colonia_id IS NOT NULL");

        if($nombre != '')
        {
            $query->andWhere("(c.nombre LIKE '%$nombre%' OR c.telefono LIKE '%$nombre%'  OR c.calle LIKE '%$nombre%' OR c.contrato LIKE '%$nombre%')");
        }
        
        if($calle != '')
        {
            $query->andWhere("c.calle LIKE '%$calle%'");
        }
        
        if($colonia != '')
        {
            $query->andWhere("c.Colonia.nombre LIKE '%$colonia%'");
        }
        
        if($desde != '')
        {    
            $query->andWhere("o.created_at >=  '$desde 00:00:00'");
        }    
        if($hasta != '')
        {    
            $query->andWhere("o.created_at <= '$hasta 23:59:59'");
        }
        
        if($sucursal != 0)
        {
            $query->andWhere("c.sucursal_id = ".$sucursal);
        }   
        
        if($plaza >0)
        {
            $query->andWhere("c.Plaza.id =" .$plaza);
        }
        
        if($servicio >0)
        {
            $query->andWhere("c.ClienteServicio.servicio_id =" .$servicio);
        }


        //Configuramos el orden
        $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"c.fecha_contratacion desc");
        
        $query->orderBy($orden);

        $registros=  $query->execute();
				
        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 11
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 35
                        ),      
				"C$i" => array(
                        "name" => 'PLAZA',
                        "width" => 10
                        ),
				"D$i" => array(
                        "name" => 'CALLE',
                        "width" => 35
                        ),
                "E$i" => array(
                        "name" => 'NO. EXTERIOR',
                        "width" => 10
                        ),
                "F$i" => array(
                        "name" => 'NO. INTERIOR',
                        "width" => 10
                        ),
                "G$i" => array(
                        "name" => 'REFERENCIA',
                        "width" => 50
                        ),
                "H$i" => array(
                        "name" => 'COLONIA',
                        "width" => 35
                        ),
                "I$i" => array(
                        "name" => 'CELULAR',
                        "width" => 25
                        ),
                "J$i" => array(
                        "name" => 'NO. TVS',
                        "width" => 25
                        ),
                "K$i" => array(
                        "name" => 'ETIQUETA',
                        "width" => 10
                        ),
                "L$i" => array(
                        "name" => 'NODO',
                        "width" => 10
                        ),
                "M$i" => array(
                        "name" => 'POSTE',
                        "width" => 10
                        ),
                "N$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 18
                        ),	
                "O$i" => array(
                        "name" => 'SERVICIO',
                        "width" => 18
                        )   		



        );
       
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
            $domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
                $i++;
                $data[] = array(			
		            "A$i" =>$registro->Cliente->contrato,
                    "B$i" =>$registro->Cliente->nombre,
                    "C$i" =>$registro->Cliente->Plaza->nombre,
                    "D$i" =>$registro->Cliente->calle,
                    "E$i" =>$registro->Cliente->no_exterior,
                    "F$i" =>$registro->Cliente->no_interior,
                    "G$i" =>$registro->Cliente->referencia,
                    "H$i" =>$registro->Cliente->Colonia->nombre,
                    "I$i" =>$registro->Cliente->celular1,
                    "J$i" =>$registro->Cliente->televisores,
                    "K$i" =>$registro->Cliente->etiqueta,
                    "L$i" =>$registro->Cliente->nodo,
                    "M$i" =>$registro->Cliente->poste,
                    "N$i" =>$registro->Cliente->estatus,
                    "O$i" =>$grid[$i]['servicio']=$registro->Cliente->TarifaConceptoContratacion->ConceptoDeContratacion->nombre.": ".$registro->Cliente->ClienteServicio[0]->Servicio->nombre



                );
        }
		
        $objPHPExcel->createExcel('Ordenes_de_instalacion', $columns_name, $data, 10,array('rango'=>'A4:E4','size'=>14,'texto'=>'ÓRDENES DE INSTALACIÓN'));
		
    }
    
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }
}