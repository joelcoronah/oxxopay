<?php

class Transacciones_ReporteconfirmacionmetasController extends Zend_Controller_Action
{
    public function indexAction()
    {	    
        $this->view->headScript()->appendFile('/js/transacciones/reportes/confirmacionmetas.js');
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
	
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);  
		
		### Establecemos el filtro por default
        $filtro = " status =1 AND tipo=1 ";
		$estatus = $this->_getParam('estatus');
		$plazas = $this->_getParam('plaza');
		
		if($this->_getParam('fecha'))
            $fecha = $this->_getParam('fecha');
        else
            $fecha=date('Y-m-01'); 

        if($plazas!='')
        {
			$plazas = str_replace('_', ',', $plazas);
            $filtro .= " AND plaza_id in (".$plazas.")";            
        }
 
        

		$registros=My_Comun::registrosGrid("Usuario",$filtro);      
        
		$grid=array();
		$i=0;
				
		foreach($registros['registros'] as $registro)
		{
			
			if($estatus == "") // todos los registros
			{
				$grid[$i]['meta']=$fecha;
				$grid[$i]['plaza']=$registro->Plaza->nombre;  
				$grid[$i]['usuario']=$registro->nombre;
				
				if(Metas::yaConfirmo($fecha,$registro->id))
					$grid[$i]['estatus']='<img src="/images/success.png" width="25px" />';
				else
					$grid[$i]['estatus']='<img src="/images/alert.png" width="25px" />';
					
				$grid[$i]['fecha']=Metas::fechaConfirmacion($fecha,$registro->id);
				
				$i++;
			}
			elseif($estatus == 1) // confirmados
			{	
				if(Metas::yaConfirmo($fecha,$registro->id))
				{
					$grid[$i]['meta']=$fecha;
					$grid[$i]['plaza']=$registro->Plaza->nombre;  
					$grid[$i]['usuario']=$registro->nombre;
					$grid[$i]['estatus']='<img src="/images/success.png" width="25px" />';
					$grid[$i]['fecha']=Metas::fechaConfirmacion($fecha,$registro->id);
					
					$i++;
				}
			}
			elseif($estatus == 2) // no confirmados
			{	
			
				if(!(Metas::yaConfirmo($fecha,$registro->id)))
				{
					
					$grid[$i]['meta']=$fecha;
					$grid[$i]['plaza']=$registro->Plaza->nombre;  
					$grid[$i]['usuario']=$registro->nombre;
					$grid[$i]['estatus']='<img src="/images/alert.png" width="25px" />';
					$grid[$i]['fecha']=Metas::fechaConfirmacion($fecha,$registro->id);
					
					$i++;
				}
			}
			
            
        }
		
		$registros['total'] = $i;
			
        My_Comun::grid2($registros,$grid);
    }
	
    /*public function imprimircobranzaAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";        
		
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Cobranza.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
		$contrato = $this->_getParam('contrato');
        $fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
		$estatus = $this->_getParam('estatus');
        $sucursal = $this->_getParam('sucursal');
        $concepto = $this->_getParam('concepto');
		
		             
        if($nombre != '')
        {
           $filtro .= " AND (Cobranza.Cliente.nombre LIKE '%$nombre%' OR Cobranza.Cliente.telefono LIKE '%$nombre%' OR Cobranza.Cliente.calle LIKE '%$nombre%')";		   
        }
			            
        if($contrato != '')
        {		   
		   $filtro .= " AND Cobranza.Cliente.contrato = ".$contrato;
        }
        
        if($fecha != '')
        {
           $filtro .= " AND (Cobranza.created_at >= '$fecha 00:00:00' ) ";
        }
						
        if($hasta != '')
        {
           $filtro .= " AND ( Cobranza.created_at <= '$hasta 23:59:59' ) ";
        }		
		        
        if($estatus != 0)
        {
            $filtro .= " AND Cobranza.Cliente.status = ".$estatus;
        }
        
        if($plaza >0)
        {
            $filtro .= " AND Cobranza.plaza_id =" .$plaza;
        }	
        
        if($sucursal != 0)
        {
            $filtro .= " AND Cobranza.Cliente.sucursal_id = ".$sucursal;
        }
        
		if($concepto == 1)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%Cambio de domicilio%'";
        }
					
        if($concepto == 2)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.servicio_adicional_id != 'null'";
        }
		
        if($concepto == 3)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%Recuperaci%'";
        }
				
        if($concepto == 4)
        {
            $filtro .= " AND Cobranza.fecha_inicio != 'null' AND Cobranza.fecha_fin != 'null'";
        }
				
        if($concepto == 5)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%extensiones adicionales%'";
        }
		
        $registros=  My_Comun::obtenerFiltro("Cobranza", $filtro);
       
        $pdf= new My_Fpdf_Pdf('L');
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE COBRANZA");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(15,40,25,65,52,18,40,22));
        $pdf->Row(array('FOLIO','FECHA DE COBRO','CONTRATO','CLIENTE','CAJERO','MONTO', 'CONCEPTO', 'FECHA'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {    
		
			$concepto =" ";	
			
			if($registro->fecha_inicio != null && $registro->fecha_fin  != null)
				$concepto.="Pago de mensualidad ";
						
			if(0 == strcmp($registro->CobranzaDetalle[0]->otro_concepto,"Cambio de domicilio"))       					
				$concepto.=", Cambio de domicilio";
        				
			if($registro->CobranzaDetalle[0]->servicio_adicional_id != null )       	
				$concepto.=", Televisión adicionales";        	
									
        	if(0 == strcmp($registro->CobranzaDetalle[0]->otro_concepto,"Recuperación"))        	
				$concepto.=", Recuperación";
			
			$fin=$registro->fecha_fin;
			if($registro->fecha_fin==NULL)
				$fin= "N/A";
			
           $pdf->Row
           (
                array
                (
                    str_pad((int)$registro->id,0,"0",STR_PAD_LEFT),
                    $registro->created_at,
                    str_pad((int)$registro->Cliente->contrato,0,"0",STR_PAD_LEFT),
                    $registro->Cliente->nombre,
                    $registro->Usuario->nombre,
                    "$".number_format($registro->total,2,".",","),
					$concepto,
					$fin
                ),0,1			
           );
        }	
		
       $pdf->Output();	 
    }
	
    function exportarcobranzaAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
        
        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Cobranza.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
		$contrato = $this->_getParam('contrato');
        $fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
		$estatus = $this->_getParam('estatus');
        $sucursal = $this->_getParam('sucursal');
        $concepto = $this->_getParam('concepto');
		             
        if($nombre != '')
        {
           $filtro .= " AND (Cobranza.Cliente.nombre LIKE '%$nombre%' OR Cobranza.Cliente.telefono LIKE '%$nombre%' OR Cobranza.Cliente.calle LIKE '%$nombre%')";		   
        }
			            
        if($contrato != '')
        {		   
		   $filtro .= " AND Cobranza.Cliente.contrato = ".$contrato;
        }
		
        if($fecha != '')
        {
           $filtro .= " AND (Cobranza.created_at >= '$fecha 00:00:00' ) ";
        }
						
        if($hasta != '')
        {
           $filtro .= " AND ( Cobranza.created_at <= '$hasta 23:59:59' ) ";
        }		
        
        if($estatus != 0)
        {
            $filtro .= " AND Cobranza.Cliente.status = ".$estatus;
        }
        
        if($plaza >0)
        {
            $filtro .= " AND Cobranza.plaza_id =" .$plaza;
        }	
        
        if($sucursal != 0)
        {
            $filtro .= " AND Cobranza.Cliente.sucursal_id = ".$sucursal;
        }
		
		if($concepto == 1)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%Cambio de domicilio%'";
        }
					
        if($concepto == 2)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.servicio_adicional_id != 'null'";
        }
		
        if($concepto == 3)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%Recuperaci%'";
        }
				
        if($concepto == 4)
        {
            $filtro .= " AND Cobranza.fecha_inicio != 'null' AND Cobranza.fecha_fin != 'null'";
        }
				
        if($concepto == 5)
        {
            $filtro .= " AND Cobranza.CobranzaDetalle.otro_concepto LIKE '%extensiones adicionales%'";
        }
		
        $registros=  My_Comun::obtenerFiltro("Cobranza", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FOLIO',
                        "width" => 11
                        ),
                "B$i" => array(
                        "name" => 'FECHA DE COBRO',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 11
                        ),
				"D$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 30
                        ),
				"E$i" => array(
                        "name" => 'CAJERO',
                        "width" => 30
                        ),
				"F$i" => array(
                        "name" => 'MONTO',
                        "width" => 11
                        ),
				"G$i" => array(
                        "name" => 'CONCEPTO',
                        "width" => 20
                        ),
				"H$i" => array(
                        "name" => 'MES CUBIERTO',
                        "width" => 15
                        )										
        );

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
			
			$concepto =" ";	
			
			if($registro->fecha_inicio != null && $registro->fecha_fin  != null)
				$concepto.="Pago de mensualidad ";
						
			if(0 == strcmp($registro->CobranzaDetalle[0]->otro_concepto,"Cambio de domicilio"))       					
				$concepto.=", Cambio de domicilio";
        				
			if($registro->CobranzaDetalle[0]->servicio_adicional_id != null )       	
				$concepto.=", Televisión adicionales";        	
									
        	if(0 == strcmp($registro->CobranzaDetalle[0]->otro_concepto,"Recuperación"))        	
				$concepto.=", Recuperación";
			
			
			$fin=$registro->fecha_fin;
			if($registro->fecha_fin==NULL)
				$fin= "N/A";
			
						
            $i++;
            $data[] = array(				
                "A$i" =>str_pad((int)$registro->id,0,"0",STR_PAD_LEFT),
                "B$i" =>$registro->created_at,
                "C$i" =>str_pad((int)$registro->Cliente->contrato,0,"0",STR_PAD_LEFT),
                "D$i" =>$registro->Cliente->nombre,
                "E$i" =>$registro->Usuario->nombre,
                "F$i" =>"$".number_format($registro->total,2,".",","),
				"G$i" =>$concepto,
				"H$i" =>$fin
            );
        }
				
        $objPHPExcel->createExcel('Cobranza', $columns_name, $data, 10, array('rango'=>'A4:F4','size'=>14,'texto'=>'COBRANZA'));
    } 	*/

	
  }