<?php
class Transacciones_FallasyacometidasController extends Zend_Controller_Action
{
    public function init()
    {
    	$this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/fallasyacometidas.js'));
    }

    public function indexAction()
    {
	//$this->view->plazas=Plaza::obtenerPlazas();
        $this->view->sucursales=Sucursal::obtenerSucursal();
        //echo Usuario::plaza(); exit;
        $this->view->tecnicos=Tecnico::obtenerTecnicos(" plaza_id=".Usuario::plaza()."");
    }
	
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);    
		
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" plaza_id=".Usuario::plaza()." ";
			
        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
	$hasta = $this->_getParam('hasta');
	$plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal'); 
        
        /*if($sucursal != 0)
        {
            $filtro .= " AND Gasto.Plaza.sucursal_id = ".$sucursal."  ";
        }           
        if($desde != '')
        {
           $filtro .= " AND fecha >= '$desde'";
        }		
	if($hasta != '')
        {
           $filtro .= " AND fecha <= '$hasta'";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }*/
		
	### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Plaza",$filtro); 
		
	$grid=array();
		
	$i=0;
	$editar=false;
	if(My_Permisos::tienePermiso('EDITAR_CLIENTES') == 1)
            $editar=true;	
        
			
        foreach($registros['registros'] as $registro)
        {
            $grid[$i]['fecha']="04-25-2013";
            $grid[$i]['plaza']="BMJ";
            $grid[$i]['tecnico']="Nelson Esparza Guevara";
            $grid[$i]['nodo']="9";
            $grid[$i]['poste']="34";
            $grid[$i]['descripcion']="El cable está roto";
            $grid[$i]['editar']='<span onclick="agregarGasto('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png"/></span>';
            $grid[$i]['eliminar']='<span onclick="eliminarAcometidas('.$registro->id.');" title="Eliminar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png"/></span>';
            $i++;
        }
		
        My_Comun::grid2($registros,$grid);	
    }

    public function agregarAction()
    {
     	### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
		
        $this->view->tecnicos=Tecnico::obtenerTecnicos(" plaza_id=".Usuario::plaza()."");
        
	### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            //$this->view->registro=My_Comun::obtener("Gasto", $this->_getParam('id'));
        }
    }

    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
       //echo My_Comun::guardar("Gasto",$_POST,NULL,$_POST['id'],'concepto');
    }
	
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
 	### Establecemos el filtro por default
        $filtro = "1 = 1 ";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" plaza_id=".Usuario::plaza()." ";
				
        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
	$hasta = $this->_getParam('hasta');
	$plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal'); 
        
        if($sucursal != 0)
        {
            $filtro .= " AND Gasto.Plaza.sucursal_id = ".$sucursal."  ";
        } 
        if($desde != '')
        {
           $filtro .= " AND fecha >= '$desde'";
        }
		
	if($hasta != '')
        {
           $filtro .= " AND fecha <= '$hasta'";
        }

        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        
        $registros=  My_Comun::obtenerFiltro("Gasto", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE GASTOS");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(45,90,45));
        $pdf->Row(array('FECHA','CONCEPTO','MONTO'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
           $monto= "$".number_format($registro->monto,2,".",",");
           $pdf->Row
           (
                array
                (
                    $registro->fecha,
                    $registro->concepto,
                    $monto
                ),0,1			
           );
        }
        
       $pdf->Output();	
    }
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
 	### Establecemos el filtro por default
        $filtro = "1 = 1 ";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            //$filtro=" plaza_id=".Usuario::plaza()." ";
				
        ### Cachamos las variables para conformar el filtro
        $desde = $this->_getParam('desde');
	$hasta = $this->_getParam('hasta');
	$plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal'); 
        
        /*if($sucursal != 0)
        {
            $filtro .= " AND Gasto.Plaza.sucursal_id = ".$sucursal."  ";
        } 
        if($desde != '')
        {
           $filtro .= " AND fecha >= '$desde'";
        }
		
	if($hasta != '')
        {
           $filtro .= " AND fecha <= '$hasta'";
        }

        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        } */
        
        $registros=  My_Comun::obtenerFiltro("Plaza", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
				
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'FECHA',
                        "width" => 11
                        ),
                "B$i" => array(
                        "name" => 'PLAZA',
                        "width" => 10
                        ),
		"C$i" => array(
                        "name" => 'TÉCNICO',
                        "width" => 35
                        ),
		"D$i" => array(
                        "name" => 'NODO',
                        "width" => 10
                        ),
		"E$i" => array(
                        "name" => 'POSTE',
                        "width" => 10
                        ),
		"F$i" => array(
                        "name" => 'DESCRIPCIÓN',
                        "width" => 30
                        )
        );
		
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
            $i++;
            $data[] = array(
		"A$i" =>"04-25-2013",
               	"B$i" =>"BMJ",
               	"C$i" =>"Nelson Esparza Guevara",
                "D$i" =>"9",
                "E$i" =>"34",
                "F$i" =>"El cable está roto"
            );
        }
		
        $objPHPExcel->createExcel('AuditoriasDeAcometidas', $columns_name, $data, 10,array('rango'=>'A4:F4','texto'=>'REPORTE DE AUDITORÍAS DE ACOMETIDAS'));		
    }
     
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboPlazas($this->_getParam('id'));
    }
}