<?php
class Transacciones_ServicioplazaController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/servicioplaza.js'));
    }

    public function indexAction()
    {
		if(isset($_POST['tarifas']))
		{
			ServicioPlaza::procesarTarifas($_POST['tarifas']);
			ServicioPlazaTarifaespecial::procesarTarifas($_POST['tarifas']);
			header("Location: /transacciones/servicioplaza/index/estado/" . $_POST['estado_actual']);
		}
		else
		{
			$this->view->EstadosPlazas=Estado::obtenerEstadosPlazas();
			$this->view->tarifasespeciales=Tarifaespecial::obtenerTarifasEspeciales();
			$this->view->estado_actual = $this->_getParam('estado');
			
			$e=array();
			/*foreach($this->view->EstadosPlazas as $estado)
			{   */
				$this->view->plazas=TarifaConceptoContratacion::obtenerPlazas($this->view->estado_actual);
				$e[$this->view->estado_actual] = $this->view->plazas; 
			/*}*/
			$this->view->e=$e; 
			
			/*Muestra todos los servicios*/
			$this->view->Servicios=Servicio::obtenerServicios(' (tipo=1 OR tipo=3) AND status=1');  	   
			
			### Vamos a armar un arreglo para poder editar tarifas no especiales
			$serviciosPlazas=ServicioPlaza::obtenerServicioPlaza(); 
			
			$arrServicioPlaza=array();
			foreach($serviciosPlazas as $servicioPlaza)
			{
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['1a5']=$servicioPlaza->precio_1a5;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['6a10']=$servicioPlaza->precio_6a10;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['11']=$servicioPlaza->precio_11;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['reconexion']=$servicioPlaza->reconexion;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['2meses']=$servicioPlaza->meses2;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['3meses']=$servicioPlaza->meses3;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['4meses']=$servicioPlaza->meses4;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['6meses']=$servicioPlaza->meses6;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['8meses']=$servicioPlaza->meses8;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['10meses']=$servicioPlaza->meses10;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['12meses']=$servicioPlaza->meses12;
			}
			$this->view->servicioplaza=$arrServicioPlaza;
			
			
			### Vamos a armar un arreglo para poder editar tarifas  especiales
			$serviciosPlazasTarifaespecial=ServicioPlazaTarifaespecial::obtenerServicioPlazaTarifaespecial("1=1"); 
			
			$arrServicioPlazaTarifaespecial=array();
			foreach($serviciosPlazasTarifaespecial as $servicioPlazaTarifaespecial)
			{
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['1a5']=$servicioPlazaTarifaespecial->precio_1a5;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['6a10']=$servicioPlazaTarifaespecial->precio_6a10;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['11']=$servicioPlazaTarifaespecial->precio_11;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['reconexion']=$servicioPlazaTarifaespecial->reconexion;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['status']=$servicioPlazaTarifaespecial->status;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['2meses']=$servicioPlazaTarifaespecial->meses2;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['3meses']=$servicioPlazaTarifaespecial->meses3;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['4meses']=$servicioPlazaTarifaespecial->meses4;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['6meses']=$servicioPlazaTarifaespecial->meses6;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['8meses']=$servicioPlazaTarifaespecial->meses8;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['10meses']=$servicioPlazaTarifaespecial->meses10;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['12meses']=$servicioPlazaTarifaespecial->meses12;
			}
			$this->view->servicioplazatarifaespecial=$arrServicioPlazaTarifaespecial;

		}
    }
 
    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("ServicioPlaza", $this->_getParam('id'));
        }
    }
	
	public function guardarAction()
	{
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
		echo "<pre>";
		print_r($_POST);
		
	}


}