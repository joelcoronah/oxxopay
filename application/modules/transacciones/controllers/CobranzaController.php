<?php

class Transacciones_CobranzaController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/cobranza.js'));
    }

    public function indexAction()
    {
        // action body
    }
	
	
	 public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);              
        
        ### Establecemos el filtro por default
        $filtro=" 1=1 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
			$filtro=" plaza_id=".Usuario::plaza()." ";
        
        ### Cachamos las variables para conformar el filtro
		$contrato = $this->_getParam('contrato');
        $nombre = $this->_getParam('nombre');
		$telefono = $this->_getParam('telefono');
        
		if($contrato != '')
        {
            
            $filtro .= " AND contrato LIKE '%".$contrato."%'";
        }  
		
        if($nombre != '')
        {
            $nombre=str_replace(array("'","\"",),array("´","´"),$nombre);
            
            $filtro .= " AND nombre LIKE '%".$nombre."%'";
        } 
		
		
		if($telefono != '')
        {
            
            $filtro .= " AND (telefono LIKE '%".$telefono."%' OR celular1 LIKE '%".$telefono."%' OR celular2  LIKE '%".$telefono."%')";
        }   
		
		 ### Extraemos los registros para formar el arreglo del grid
		$registros=My_Comun::registrosGrid("Cliente",$filtro); 
		
		
		$grid=array();
		
		$i=0;
		
		foreach($registros['registros'] as $registro)
		{
			$grid[$i]['contrato']=$registro->contrato;
			$grid[$i]['nombre']=$registro->nombre;
			$grid[$i]['telefono']=$registro->telefono;
			$grid[$i]['celular']=$registro->celular1;
			$grid[$i]['estatus']=$registro->estatus;
			if(Cliente::tieneAdeudoAdicional($registro->id))
					$grid[$i]['cobrar']='<span onclick="cobrarAdicional('.$registro->id.');" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png" /></span>';
			else	
			{
				if($registro->estatus=="POR INSTALAR")
				$grid[$i]['cobrar']='<span onclick="cobrar(-1);" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png" /></span>';

				else
					$grid[$i]['cobrar']='<span onclick="cobrar('.$registro->id.');" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png" /></span>';
			}
			$i++;
		}
		
        My_Comun::grid2($registros,$grid);	
    }
	
	public function aplicarpagoadicionalAction()
    {
		### Deshabilitamos el layout
        $this->_helper->layout->disableLayout();
		
		### Obtenemos la información del cliente al que vamos a cobrar
		$this->view->cliente=My_Comun::obtener("Cliente",$this->_getParam('id'));
		
		### Extraemos el cobro a realizar
		$this->view->cobranza=Cliente::extraerAdeudoPendiente($this->_getParam('id'));
	}

    public function aplicarpagoAction()
    {
		### Deshabilitamos el layout
        $this->_helper->layout->disableLayout();
		
		### Obtenemos la información del cliente al que vamos a cobrar
		$this->view->cliente=My_Comun::obtener("Cliente",$this->_getParam('id'));
		
		### Obtenemos la información de sus servicios para conformar la tabla de cobranza
			### Obtenemos el último mes cubierto
			$ultimoMesCubierto=$this->view->cliente->ultimomescubierto;
			### Obtenemos el número de meses que hay entre la fecha actual y el último mes cubierto
			$this->view->meses=My_Comun::numero_de_meses2(date('Y-m-d'),$ultimoMesCubierto)-1;

			//echo "Meses: ".date('Y-m-d')." vs ".$ultimoMesCubierto." Meses: ".$this->view->meses; exit;
			
			if($this->view->cliente->status!='ACTIVO')
			{
				$this->view->servicios=array();
				### Obtenemos los servicios que se le cobrarán al cliente
				//$dServicios=Doctrine_Query::create()->from('ClienteServicio')->where('cliente_id=?',$this->view->cliente->id)->execute();
				$servicio = Doctrine_Query::create()->from('ClienteServicio')->where('cliente_id=?',$this->view->cliente->id)->orderBy("id DESC")->execute()->getFirst();
				### Recorremos todos los servicios del cliente para conformar el total a pagar
				/*foreach($dServicios as $servicio)
				{
					### Recordar que falta calcular el precio en caso de una tarifa especial
					$this->view->servicios[]=Servicio::obtenerPrecioCobranza($servicio->servicio_id,$servicio->tarifa_especial_id,$servicio->periodos_tarifa_especial, $servicio->periodos_restan_tarifa_especial,$this->view->meses,$this->view->cliente);
				}*/
				$this->view->servicios[]=Servicio::obtenerPrecioCobranza($servicio->servicio_id,$servicio->tarifa_especial_id,$servicio->periodos_tarifa_especial, $servicio->periodos_restan_tarifa_especial,$this->view->meses,$this->view->cliente);
				//echo "<pre>"; print_r($this->view->servicios); exit;
			}	
		
    }
	
	public function calculamesesporadelantadoAction()
    {
		### Deshabilitamos el layout
        $this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(TRUE);   
		
		### Obtenemos la información del cliente al que vamos a cobrar
		$this->view->cliente=My_Comun::obtener("Cliente",$this->_getParam('id'));
		
		### Obtenemos la información de sus servicios para conformar la tabla de cobranza
			### Obtenemos el último mes cubierto
			$ultimoMesCubierto=$this->view->cliente->ultimomescubierto;
			### Obtenemos el periodo a cubrir
			$this->view->fechas=My_Comun::obtenerRangoFechaDesdePeriodo(date('Y-m-d'),$ultimoMesCubierto);
			
			$this->view->servicios=array();
			### Obtenemos los servicios que se le cobrarán al cliente
			//$dServicios=Doctrine_Query::create()->from('ClienteServicio')->where('cliente_id=?',$this->view->cliente->id)->execute();
			$servicio = Doctrine_Query::create()->from('ClienteServicio')->where('cliente_id=?',$this->view->cliente->id)->orderBy("id DESC")->execute()->getFirst();
			### Recorremos todos los servicios del cliente para conformar el total a pagar
			/*foreach($dServicios as $servicio)
			{
				### Recordar que falta calcular el precio en caso de una tarifa especial
				$this->view->servicios[]=Servicio::obtenerPrecioCobranzaAdelantado($servicio->servicio_id,$servicio->tarifa_especial_id,$servicio->periodos_tarifa_especial,$servicio->periodos_restan_tarifa_especial,$this->_getParam('periodos'),$this->view->cliente->estatus,$this->view->cliente);
			}*/
			$this->view->servicios[]=Servicio::obtenerPrecioCobranzaAdelantado($servicio->servicio_id,$servicio->tarifa_especial_id,$servicio->periodos_tarifa_especial,$servicio->periodos_restan_tarifa_especial,$this->_getParam('periodos'),$this->view->cliente->estatus,$this->view->cliente);
			$this->view->periodos=$this->_getParam('periodos');
			$this->view->saldo_a_favor = $this->view->cliente->saldo_a_favor;
				
			//echo "<pre>";
			//print_r($this->view->servicios);	
	}
	
	public function calculamesesporadelantadodesconectarAction()
    {
		### Deshabilitamos el layout
        $this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(TRUE);   
		
		### Obtenemos la información del cliente al que vamos a cobrar
		$this->view->cliente=My_Comun::obtener("Cliente",$this->_getParam('id'));
		
		### Obtenemos la información de sus servicios para conformar la tabla de cobranza
			### Obtenemos el último mes cubierto
			$ultimoMesCubierto=$this->view->cliente->ultimomescubierto;
			### Obtenemos el periodo a cubrir
			$this->view->fechas=My_Comun::obtenerRangoFechaDesdePeriodo(date('Y-m-d'),$ultimoMesCubierto);
			
			$this->view->servicios=array();
			### Obtenemos los servicios que se le cobrarán al cliente
			$dServicios=Doctrine_Query::create()->from('ClienteServicio')->where('cliente_id=?',$this->view->cliente->id)->execute();
			### Recorremos todos los servicios del cliente para conformar el total a pagar
			foreach($dServicios as $servicio)
			{
				### Recordar que falta calcular el precio en caso de una tarifa especial
				$this->view->servicios[]=Servicio::obtenerPrecioCobranzaAdelantado($servicio->servicio_id,$servicio->tarifa_especial_id,$servicio->periodos_tarifa_especial,$servicio->periodos_restan_tarifa_especial,$this->_getParam('periodos'), $this->view->cliente->estatus,$this->view->cliente);
			}
			$this->view->periodos=$this->_getParam('periodos');
			
				
			/*echo "<pre>";
			print_r($this->view->servicios);	*/
	}
	
	
	public function anadirserviciosadicionalesAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   
	   $this->view->servicioAContratar=Servicio::obtenerServiciosAdicionalesContratar();
	}
	
	public function estableceservicioadicionalAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   
	   $qServicio=Doctrine_Query::create()->from('TarifaServicioadicional')->where('servicioadicional_id=? AND plaza_id=?',array($this->_getParam('id'),Usuario::plaza()));
	   
	   $this->view->servicio=$qServicio->execute()->getFirst();
	}
	
	
	
	public function  guardarpagoAction()
	{
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE); 


		if(isset($_POST['selectPagarMesesAdelantado'])) 
		{
			echo Cobranza::cobraMesesPorAdelantado($_POST['selectPagarMesesAdelantado'],$_POST['cliente_id'],$_POST['servicioadicional'],$_POST['descuento']);
	    }
		else
		{
			echo Cobranza::cobrarAdeudo($_POST['cliente_id'],$_POST['servicioadicional'],$_POST['descuento']);
		}
	}
	
	public function  guardarpagoadicionalAction()
	{
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE); 
		
		Cobranza::cobrarAdicional($_POST['id']);
		echo $_POST['id'];

	}
	
	public function cancelarpagoAction()
    {
		### Deshabilitamos el layout
        $this->_helper->layout->disableLayout();
		
		if(isset($_POST['motivo_cancelacion']))
		{
			$this->_helper->viewRenderer->setNoRender(TRUE);
			
			Cobranza::cancelarPago($_POST);
			
		}
		else
		{
			### Obtenemos la información del cliente al que vamos a cobrar
			$this->view->registro=My_Comun::obtener("Cobranza",$this->_getParam('id'));			
		}
	}
	


}