<?php
class Transacciones_ReporteservicioController extends Zend_Controller_Action
{

  	public function init(){
		$this->view->headScript()->appendFile('/js/transacciones/reportes/servicio.js');	
    }
	
	public function indexAction(){
		$this->view->plazas=Plaza::obtenerPlazas();
	}
	
    public function gridAction(){
		 ### Deshabilitamos el layout y la vista
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
		
		ini_set("memory_limit", "500M");
        ini_set('max_execution_time', 0);     
		
		### Recibimos los parámetros de paginación y ordenamiento.
        if (isset($_POST['page']) != ""){$page = $_POST['page'];}
        if (isset($_POST['sortname']) != ""){$sortname =$_POST['sortname'];}
        if (isset($_POST['sortorder']) != ""){$sortorder = $_POST['sortorder'];}
        if (isset($_POST['qtype']) != ""){$qtype = $_POST['qtype'];}
        if (isset($_POST['query']) != ""){$query = $_POST['query'];}
        if (isset($_POST['rp']) != ""){$rp = $_POST['rp'];}

        $con = Doctrine_Manager::getInstance()->connection();

        ### Establecemos el filtro por default
        $filtro = " 1 =1";
 
        /*if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Cobranza.plaza_id=".Usuario::plaza()." ";*/

        ### Cachamos las variables para conformar el filtro
        $fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');	
		$vendedor = $this->_getParam('vendedor');	             
        
        if($fecha != '')
        {
           $filtro .= " AND (C.fecha_contratacion >= '$fecha 00:00:00' ) ";
        }
						
        if($hasta != '')
        {
           $filtro .= " AND ( C.fecha_contratacion <= '$hasta 23:59:59' ) ";
        }
		 
        if($plaza != '' && $plaza != 'undefined')
        {
            $plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND C.plaza_id in (".$plaza.")";
        }	
        
        if($sucursal != 0)
        {
            $filtro .= " AND C.sucursal_id = ".$sucursal;
        }
		
		if($vendedor != 0)
        {
            $filtro .= " AND C.usuario_id = ".$vendedor;
        }

	    $consulta ="
					SELECT S.nombre AS nombre,S.id AS id, count(distinct CS.cliente_id) AS total  
					FROM cliente_servicio CS 
					INNER JOIN servicio S on CS.servicio_id=S.id
					INNER JOIN cliente AS C ON C.id = CS.cliente_id
					WHERE ".$filtro."
					GROUP BY S.nombre";		
		
		 $q = $con->execute($consulta)->fetchAll();

        ### Incializamos el arreglo de registros
        $registros = array();

        $registros['total']=count($q);
        $paginas=ceil($registros['total']/$rp);
        if($page>$paginas)
            $page=1;

        $consulta .= "
                    order by
                        $sortname $sortorder
                    limit ".(($page-1)*$rp).", $rp";

        ### Extraemos los registros para formar el arreglo del grid
        $registros['registros']=$con->execute($consulta)->fetchAll();  
        $registros['pagina']=$page;     

        $grid=array();
        $i=0;

		foreach($registros['registros'] as $registro)
		{		
            $grid[$i]['nombre']=$registro['nombre'];
			$grid[$i]['total']=$registro['total'];	
			$grid[$i]['detalle']='<span onclick="detalle('.$registro['id'].');" title="Detalle"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/lupa.png" /></span>';        
            $i++;
        }
	
        My_Comun::grid2($registros,$grid);
    }
	
	public function detalleAction(){
		 ### Deshabilitamos el layout y la vista
		$this->_helper->layout->disableLayout();
		
		
		$this->view->servicio = My_Comun::obtener("Servicio",$this->_getParam('id'));
		
	}
	
	public function detallegridAction(){
		 ### Deshabilitamos el layout y la vista
		$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
		
		ini_set("memory_limit", "500M");
        ini_set('max_execution_time', 0);     
		
		### Recibimos los parámetros de paginación y ordenamiento.
        if (isset($_POST['page']) != ""){$page = $_POST['page'];}
        if (isset($_POST['sortname']) != ""){$sortname =$_POST['sortname'];}
        if (isset($_POST['sortorder']) != ""){$sortorder = $_POST['sortorder'];}
        if (isset($_POST['qtype']) != ""){$qtype = $_POST['qtype'];}
        if (isset($_POST['query']) != ""){$query = $_POST['query'];}
        if (isset($_POST['rp']) != ""){$rp = $_POST['rp'];}

        $con = Doctrine_Manager::getInstance()->connection();

        ### Cachamos las variables para conformar el filtro
        $fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');	
		$vendedor = $this->_getParam('vendedor');	            
        $id = $this->_getParam('id');
		
		### Establecemos el filtro por default
        $filtro = " servicio_id = '".$id."'";
		
        if($fecha != '')
        {
           $filtro .= " AND (C.fecha_contratacion >= '$fecha 00:00:00' ) ";
        }
						
        if($hasta != '')
        {
           $filtro .= " AND ( C.fecha_contratacion <= '$hasta 23:59:59' ) ";
        }
		 
        if($plaza != '' && $plaza != 'undefined')
        {
            $plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND C.plaza_id in (".$plaza.")";
        }	
        
        if($sucursal != 0)
        {
            $filtro .= " AND C.sucursal_id = ".$sucursal;
        }
		
		if($vendedor != 0)
        {
            $filtro .= " AND C.usuario_id = ".$vendedor;
        }
				
	    $consulta ="
					SELECT DISTINCT CS.cliente_id, C.contrato AS contrato, C.nombre AS nombre, U.nombre AS promotor
					FROM cliente_servicio CS 
					INNER JOIN servicio S on CS.servicio_id=S.id
					INNER JOIN cliente AS C ON C.id = CS.cliente_id
					LEFT JOIN usuario AS U ON U.id=C.promotor
					WHERE ".$filtro."
					";		
		
		 $q = $con->execute($consulta)->fetchAll();

        ### Incializamos el arreglo de registros
        $registros = array();

        $registros['total']=count($q);
        $paginas=ceil($registros['total']/$rp);
        if($page>$paginas)
            $page=1;

        $consulta .= "
                    order by
                        $sortname $sortorder
                    limit ".(($page-1)*$rp).", $rp";

        ### Extraemos los registros para formar el arreglo del grid
        $registros['registros']=$con->execute($consulta)->fetchAll();  
        $registros['pagina']=$page;     

        $grid=array();
        $i=0;

		foreach($registros['registros'] as $registro)
		{		
			$grid[$i]['contrato']=$registro['contrato'];
            $grid[$i]['nombre']=$registro['nombre'];
			$grid[$i]['promotor']=$registro['promotor'];
            $i++;
        }
	
        My_Comun::grid2($registros,$grid);
    }
	
	public function imprimirAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
        $con = Doctrine_Manager::getInstance()->connection();
 	
		ini_set("memory_limit", "500M");
        ini_set('max_execution_time', 0);     


        ### Establecemos el filtro por default
         $filtro = " 1 =1";
 
        /*if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Cobranza.plaza_id=".Usuario::plaza()." ";*/

        ### Cachamos las variables para conformar el filtro
        $fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');		
		$vendedor = $this->_getParam('vendedor');           
        
        if($fecha != '')
        {
           $filtro .= " AND (C.fecha_contratacion >= '$fecha 00:00:00' ) ";
        }
						
        if($hasta != '')
        {
           $filtro .= " AND ( C.fecha_contratacion <= '$hasta 23:59:59' ) ";
        }
		 
        if($plaza != '' && $plaza != 'undefined')
        {
            $plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND C.plaza_id in (".$plaza.")";
        }	
        
        if($sucursal != 0)
        {
            $filtro .= " AND C.sucursal_id = ".$sucursal;
        }
		
		if($vendedor != 0)
        {
            $filtro .= " AND C.usuario_id = ".$vendedor;
        }
		
	    $consulta ="
					SELECT S.nombre AS nombre, count(distinct CS.cliente_id) AS total  
					FROM cliente_servicio CS 
					INNER JOIN servicio S on CS.servicio_id=S.id
					INNER JOIN cliente AS C ON C.id = CS.cliente_id
					WHERE ".$filtro."
					GROUP BY S.nombre";		
			

        ### Extraemos los registros para formar el arreglo del grid
        $registros=$con->execute($consulta)->fetchAll();

        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE REPORTE DE VENTAS");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(139,46));
        $pdf->Row(array('SERVICIO','CANTIDAD'),0,1);
        
        $pdf->SetFont('Arial','',10);

        $tecnico = "";
        $tecnico_id = 0;
        $i=0;
        
        foreach($registros as $registro)
        {

            $pdf->Row
            (
                array
                (
                    $registro['nombre'],
                    $registro['total']
                ),0,1           
            );
            
            $i++;
        }
    
        $pdf->Output();
    }

    public function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);   
        
       $con = Doctrine_Manager::getInstance()->connection();
 	
		ini_set("memory_limit", "500M");
        ini_set('max_execution_time', 0);     


        ### Establecemos el filtro por default
         $filtro = " 1 =1";
 
        /*if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro.=" AND Cobranza.plaza_id=".Usuario::plaza()." ";*/

        ### Cachamos las variables para conformar el filtro
        $fecha = $this->_getParam('fecha');
		$hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');		
		$vendedor = $this->_getParam('vendedor');           
        
        if($fecha != '')
        {
           $filtro .= " AND (C.fecha_contratacion >= '$fecha 00:00:00' ) ";
        }
						
        if($hasta != '')
        {
           $filtro .= " AND ( C.fecha_contratacion <= '$hasta 23:59:59' ) ";
        }
		 
        if($plaza != '' && $plaza != 'undefined')
        {
            $plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND C.plaza_id in (".$plaza.")";
        }	
        
        if($sucursal != 0)
        {
            $filtro .= " AND C.sucursal_id = ".$sucursal;
        }
		
		if($vendedor != 0)
        {
            $filtro .= " AND C.usuario_id = ".$vendedor;
        }
		
	    $consulta ="
					SELECT S.nombre AS nombre, count(distinct CS.cliente_id) AS total  
					FROM cliente_servicio CS 
					INNER JOIN servicio S on CS.servicio_id=S.id
					INNER JOIN cliente AS C ON C.id = CS.cliente_id
					WHERE ".$filtro."
					GROUP BY S.nombre";		
					
        ### Extraemos los registros para formar el arreglo del grid
        $registros=$con->execute($consulta)->fetchAll();
		$objPHPExcel = new My_PHPExcel_Excel();     
   
        $i=5;
        //Titulos columna
        $columns_name = array
        (
            "A$i" => array(
                    "name" => 'SERVICIO',
                    "width" => 48
                    ),
            "B$i" => array(
                    "name" => 'CANTIDAD',
                    "width" => 20
                    )                                  
        );

        $tecnico = "";
        $tecnico_id = 0;
        //Datos tabla
        $data = array();

        foreach($registros as $registro)
        {

            $i++;
            $data[] = array(                
                "A$i" =>$registro['nombre'],
                "B$i" =>$registro['total']
            );
        }
    
        $objPHPExcel->createExcel('servicios', $columns_name, $data, 10, array('rango'=>'A4:B4','size'=>14,'texto'=>'REPORTE DE VENTAS'));
    }
	
	public function vendedoresAction()
    {
       	### Deshabilitamos el layout y la vista
       	$this->_helper->layout->disableLayout();
       	$this->_helper->viewRenderer->setNoRender(TRUE);
       
       	$registros= My_Comun::obtenerFiltro("Usuario", "promotor ='1' and status ='1' and plaza_id='".$this->_getParam('id')."'"); //$this->_getParam('id')
	    
	   	$options='<option value="0">Seleccione una sucursal</option>';
            
    	foreach($registros as $registro)
       	{               
      		$options.='<option value="'.$registro->id.'" >'.$registro->nombre.'</option>';
     	}
		
      	echo $options; 
    }   

} // FIN DE CONTROLLER