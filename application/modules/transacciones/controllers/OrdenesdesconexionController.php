<?php

class Transacciones_OrdenesdesconexionController extends Zend_Controller_Action
{

    public function init()
    {
         $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/ordenesdesconexion.js'));
    }

    public function indexAction()
    {
        $this->view->plazas=Plaza::obtenerPlazas();
    }
	
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $query=My_Comun::prepararQuery("Ordendesconexion o");

        $query->innerJoin("o.Cliente c");
        
        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');   
        $sucursal = $this->_getParam('sucursal');
        $plaza = $this->_getParam('plaza');            
        $calle='';
        $etiqueta='';
        $nodo='';
        $poste='';
        $orden_primer_nivel=$this->_getParam('orden_primer_nivel');
        $orden_segundo_nivel=$this->_getParam('orden_segundo_nivel');
        $orden_tercer_nivel=$this->_getParam('orden_tercer_nivel');
        
        $query=$this->whereAction($query, $nombre, $calle, "", $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza);

        $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"o.created_at desc"); 
                           
        $query->orderBy($orden);

        
        ### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::registrosGrid2($query); 
				
    	$grid=array();		
    	$i=0;
    		
    	foreach($registros['registros'] as $registro)
    	{
            $grid[$i]['contrato']=$registro->Cliente->contrato;
            $grid[$i]['nombre']=$registro->Cliente->nombre;
            $grid[$i]['plaza']=$registro->Cliente->Plaza->nombre;
            $grid[$i]['colonia']=$registro->Cliente->Colonia->nombre;
            $grid[$i]['telefono']=$registro->Cliente->telefono;
            $grid[$i]['celular']=$registro->Cliente->celular1;
            $grid[$i]['estatus']=$registro->Cliente->estatus;
            if($registro->status==0)
                $grid[$i]['descargar']='<span onclick="descargar('.$registro->id.');" title="Descargar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
            $i++;
        }
		
        My_Comun::grid2($registros,$grid);	
    }

    public function whereAction($query, $nombre, $calle, $colonia, $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza)
    {
        
        $query->where("o.status=0 AND c.status=2");
        //echo $query->getSqlQuery(); exit;

        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $query->andWhere("c.Plaza.id=".Usuario::plaza());

        if($nombre != '')
        {
            $query->andWhere("(c.nombre LIKE '%$nombre%' OR c.telefono LIKE '%$nombre%'  OR c.calle LIKE '%$nombre%' OR c.contrato LIKE '%$nombre%')");
        }

        if($calle != '')
        {
            $query->andWhere("c.calle = '".$calle."'");
        }

        if($colonia != '')
        {
            $query->andWhere("c.Colonia.nombre LIKE '%$colonia%'");
        }

        if($etiqueta != '')
        {
            $query->andWhere("c.etiqueta = '".$etiqueta."'");
        }

        if($nodo != '')
        {
            $query->andWhere("c.nodo = '".$nodo."'");
        }

        if($poste != '')
        {
            $query->andWhere("c.poste = '".$poste."'");
        }
        
        if($desde != '')
        {    
            $query->andWhere("o.created_at >=  '$desde 00:00:00'");
        }    
        if($hasta != '')
        {    
            $query->andWhere("o.created_at <= '$hasta 23:59:59'");
        }
        
        if($sucursal != 0)
        {
            $query->andWhere("c.sucursal_id = ".$sucursal);
        }   
        
        else if($plaza >0)
        {
            $query->andWhere("c.Plaza.id =" .$plaza);
        }

        return $query;
    }

    public function descargarAction()
    {
    	$this->_helper->layout->disableLayout();
    		
    	$this->view->registro=My_Comun::obtener('Ordendesconexion',$this->_getParam('id'));
    	$this->view->tecnicos=Tecnico::obtenerTecnicos(" plaza_id=".Usuario::plaza()."");
        $this->view->instalacion = Ordeninstalacion::obtenerOrdenenInstalacionCliente($this->view->registro->cliente_id);
    }
	
    public function guardarAction()
    {
	   ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);

      // echo "<pre>"; print_r($_POST); exit;

       Ordendesconexion::actualiza($_POST);

    }
		
    public function imprimirAction()
    {
        ini_set("memory_limit", "600M");
        ini_set('max_execution_time', 0);
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);


        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');   
        $sucursal = $this->_getParam('sucursal');
        $plaza = $this->_getParam('plaza');   
        $calle='';
        $etiqueta='';
        $nodo='';
        $poste='';
        $orden_primer_nivel=$this->_getParam('orden_primer_nivel');
        $orden_segundo_nivel=$this->_getParam('orden_segundo_nivel');
        $orden_tercer_nivel=$this->_getParam('orden_tercer_nivel');

        ### Inicializamos el objeto PDF
        $pdf= new My_Fpdf_Pdf();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->Header("IMPRESIÓN DE ÓRDENES DE DESCONEXIÓN");
        $pdf->SetWidths(array(45,45,45,45)); 


        if($this->_getParam('agrupamiento_primer_nivel')>0)
       {
            $agrupamiento=My_Comun::prepararQuery("Ordendesconexion o");
            $agrupamiento->innerJoin("o.Cliente c");
            $agrupamiento=$this->whereAction($agrupamiento, $nombre, $calle, "", $etiqueta, $nodo, $poste, $desde, $hasta, $sucursal, $plaza);
            $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"o.created_at desc");
            $agrupamiento->orderBy($orden);
            $grupos=My_Comun::configurarAgrupamiento($agrupamiento,$this->_getParam('agrupamiento_primer_nivel'));
       }


       if(count($grupos)==0)
        $grupos[]=-1;
        
        
       foreach($grupos as $titulo=>$grupo)
       {
            $query=My_Comun::prepararQuery("Ordendesconexion o");
            $query->innerJoin("o.Cliente c");

            if($grupo!=-1)
            {
                switch($this->_getParam('agrupamiento_primer_nivel'))
                {
                    case 1: $colonia=$grupo; $titulo="Colonia: "; break;
                    case 2: $calle=$grupo; $titulo="Calle: "; break;
                    case 3: $etiqueta=$grupo; $titulo="Etiqueta: "; break;
                    case 4: $nodo=$grupo; $titulo="Nodo: "; break;
                    case 5: $poste=$grupo; $titulo="Poste: "; break;
                }
                
            }



            $query=$this->whereAction($query, $nombre, $calle, $colonia, $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza);
            
            //Configuramos el orden
            $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"o.created_at desc");
            
            $query->orderBy($orden);


            //echo $query->getSqlQuery(); echo "<br><br>";

            // Ejecitamos la consulta
            $registros=  $query->execute();
            
            $i=1;
            $pdf->Header($titulo.": ".$grupo);
       
            foreach($registros as $registro)
            {
                $domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".$registro->Cliente->Colonia->nombre;
    			
                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFillColor(220,220,220);
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('CONTRATO','CLIENTE','PLAZA','ESTATUS'),1,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array($registro->Cliente->contrato,$registro->Cliente->nombre,$registro->Cliente->Plaza->nombre,$registro->Cliente->estatus),1,1);
    			
                $pdf->SetWidths(array(45,135));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('ULTIMO MES CUBIERTO','DOMICILIO'),1,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array($registro->Cliente->ultimomescubierto,$domicilio),1,1);

                $pdf->SetWidths(array(45,135));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('REFERENCIA',$registro->Cliente->referencia),1,1);
                
                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFillColor(220,220,220);
                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('TELEFONO',$registro->Cliente->telefono,'CELULAR',$registro->Cliente->celular1),1,1);
                
                
                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('NO. DE TELEVISIONES','ETIQUETA','NODO','POSTE 2'),0,1);
                $pdf->SetFont('Arial','',10);
                if(strlen($registro->Cliente->Ordeninstalacion[0]->mac_switch)>3)
                    $etiqueta=$registro->Cliente->Ordeninstalacion[0]->mac_switch;
                else
                    $etiqueta=$registro->Cliente->etiqueta;
                if(strlen($registro->Cliente->Ordeninstalacion[0]->mac_router)>3)
                    $nodo=$registro->Cliente->Ordeninstalacion[0]->mac_router;
                else
                    $nodo=$registro->Cliente->nodo;
                if(strlen($registro->Cliente->Ordeninstalacion[0]->mac_roku)>3)
                    $poste=$registro->Cliente->Ordeninstalacion[0]->mac_roku;
                else
                    $poste=$registro->Cliente->poste;
                $pdf->Row(array($registro->Cliente->televisores,$etiqueta,$nodo,$poste,''),1,1);
                
    			
                $pdf->SetWidths(array(90,90));
                $pdf->SetFont('Arial','B',10);
                $pdf->Row(array('FECHA DE DESCONEXIÓN','TÉCNICO QUE DESCONECTÓ'),0,1);
                $pdf->SetFont('Arial','',10);
                $pdf->Row(array('',''),0,1);
            }	
        }
           
       $pdf->Output();	
       
    }
    
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }
}