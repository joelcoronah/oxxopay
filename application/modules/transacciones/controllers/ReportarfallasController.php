<?php
class Transacciones_ReportarfallasController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/reportarfallas.js'));
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
        ### Obtenemos los estados para alimentar el filtro
        $this->view->estados=Estado::obtenerEstados();	
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);    
		
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
		$filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');
        $comunidad = $this->_getParam('comunidad');
        $plaza = $this->_getParam('plaza');
		
		if($nombre != '')
        {
           $filtro .= " AND (nombre LIKE '%$nombre%' OR telefono LIKE '%$nombre%' OR calle LIKE '%$nombre%' OR contrato LIKE '%$nombre%')";
        }
		
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        if($estado >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.Estado.id =" .$estado;
        }
        if($municipio >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.id =" .$municipio;
        }
        if($comunidad >0)
        {
            $filtro .= " AND comunidad_id =" .$comunidad;
        }
		
		### Extraemos los registros para formar el arreglo del grid
		$registros=My_Comun::registrosGrid("Cliente",$filtro); 
		
		$grid=array();
		
		$i=0;
			$editar=true;
			
		foreach($registros['registros'] as $registro)
		{
			$grid[$i]['contrato']=$registro->contrato;
			$grid[$i]['nombre']=$registro->nombre;
			$grid[$i]['telefono']=$registro->telefono;
			$grid[$i]['celular']=$registro->celular1;
			
			if(count($registro->Autorizacion)==1 && $registro->Autorizacion[0]->status!=1)
				$grid[$i]['estatus']=($registro->Autorizacion[0]->status==0)?"POR AUTORIZAR":"NO AUTORIZADO";
			else
				$grid[$i]['estatus']=$registro->estatus;
			if($editar)
				$grid[$i]['editar']='<span onclick="reportarFalla('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png"/></span>';
			else
				$grid[$i]['editar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';
			$i++;
		}
		
        My_Comun::grid2($registros,$grid);	

    }
	
    public function agregarfallasAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();    
		
		$this->view->fallas=TipoDeFalla::obtenerTipoDeFalla();
		$this->view->estatusfallas=EstatusDeFalla::obtenerEstatusDeFalla();
			    
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->cliente=My_Comun::obtener("Cliente", $this->_getParam('id'));
        }
		
    }
	
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);

       $_POST['fecha_reporte'] = date('Y-m-d H:i:s');
		
       echo $id= My_Comun::guardar("ReportarFallas",$_POST,NULL,$_POST['id'],'status_falla');
	   
	   	$_POST['fecha']= date("Y-m-d");
		$_POST['reportar_falla_id']=$id;
		
		echo My_Comun::guardar("HistorialDeFallas",$_POST,NULL,'','fecha');
	   
    }
	
	///////////////////////////// 
}