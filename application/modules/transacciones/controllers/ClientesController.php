<?php

class Transacciones_ClientesController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/clientes.js?v=2'));
		$this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/cobranza.js?v=2'));
		
		/*$fecha='2013-02-28';
		$periodos=6;
		$suma=true;
		$f=My_Comun::obtenerRangoFechaDesdePeriodo($periodos,$fecha,$suma);
		print_r($f);
		exit;*/
		
		/*$periodos=12;
		$f=My_Comun::obtenerRangoFechaDesdePeriodo($periodos);
		print_r($f);
		exit;*/
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
        ### Obtenemos los estados para alimentar el filtro
        $this->view->estados=Estado::obtenerEstados();

        

		
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);    
		
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
		$filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
		$colonia=$this->_getParam('colonia');
		$estatus = $this->_getParam('estatus');
		$etiqueta = $this->_getParam('etiqueta');
		$nodo = $this->_getParam('nodo');
		$poste = $this->_getParam('poste');
        
        if($nombre != '')
        {
           $filtro .= " AND (nombre LIKE '%$nombre%' OR telefono LIKE '%$nombre%'  OR calle LIKE '%$nombre%' OR contrato LIKE '%$nombre%') ";
        }
        if($plaza != '')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND plaza_id in (" .$plaza.")";
        }
		if($colonia!='')
		{
			$colonia = str_replace('_', ',', $colonia);
            $filtro .= " AND colonia_id in (" .$colonia.")";
		}
		if($estatus != '')
        {
			$estatus = str_replace('_', ',', $estatus);
            $filtro .= " AND status in (" .$estatus.")";
        }
		
		if($etiqueta != '')
        	$filtro .= " AND etiqueta = '$etiqueta'";
			
		if($nodo != '')
        	$filtro .= " AND nodo = '$nodo'";
			
		if($poste != '')
        	$filtro .= " AND poste = '$poste' ";
		
		### Extraemos los registros para formar el arreglo del grid
		$registros=My_Comun::registrosGrid("Cliente",$filtro); 
		
		$grid=array();
		
		$i=0;
		$editar=false;
		if(My_Permisos::tienePermiso('EDITAR_CLIENTES') == 1)
			$editar=true;
			
			
		foreach($registros['registros'] as $registro)
		{
			$domicilio=$registro->calle." No.".$registro->no_exterior;
			if($registro->no_interior!='')
				$domicilio.=', Int. '.$registro->no_interior;
			$grid[$i]['contrato']=$registro->contrato;
			$grid[$i]['nombre']=$registro->nombre;			
			$grid[$i]['domicilio']=$domicilio;
			$grid[$i]['plaza']=$registro->Plaza->nombre;
			$grid[$i]['telefono']=$registro->telefono;
			$grid[$i]['celular']=$registro->celular1;
			if(strlen($registro->Ordeninstalacion[0]->mac_switch)>3)
				$grid[$i]['etiqueta']=$registro->Ordeninstalacion[0]->mac_switch;
			else
				$grid[$i]['etiqueta']=$registro->etiqueta;
			if(strlen($registro->Ordeninstalacion[0]->mac_router)>3)
				$grid[$i]['nodo']=$registro->Ordeninstalacion[0]->mac_router;
			else
				$grid[$i]['nodo']=$registro->nodo;
			if(strlen($registro->Ordeninstalacion[0]->mac_roku)>3)
				$grid[$i]['poste']=$registro->Ordeninstalacion[0]->mac_roku;
			else
				$grid[$i]['poste']=$registro->poste;
			if(count($registro->Autorizacion)==1 && $registro->Autorizacion[0]->status!=1)
				$grid[$i]['estatus']=($registro->Autorizacion[0]->status==0)?"POR AUTORIZAR":"NO AUTORIZADO";
			else
				$grid[$i]['estatus']=$registro->estatus;
				
			if(Usuario::tipo()!=0)				
				if(Cliente::tieneAdeudoAdicional($registro->id))
						$grid[$i]['cobrar']='<span onclick="cobrarAdicional('.$registro->id.');" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png" /></span>';
				else	
				{
					if($registro->estatus=="POR INSTALAR")
					$grid[$i]['cobrar']='<span onclick="cobrar(-1);" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png" /></span>';
	
					else
						$grid[$i]['cobrar']='<span onclick="cobrar('.$registro->id.');" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cobrar.png" /></span>';
				}	
				
			if(Usuario::tipo()!=0)	
				if(Ordencambiodomicilio::tieneOrdenPendiente($registro->id))	
				{
					$grid[$i]['cambiardomicilio']='<span onclick="cambiarDomicilio(-3);" title="Cambio de domicilio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/map.png" /></span>';
				}
				else if(Ordencambioservicio::tieneOrdenPendiente($registro->id))	
				{
					$grid[$i]['cambiardomicilio']='<span onclick="cambiarDomicilio(-4);" title="Cambio de domicilio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/map.png" /></span>';
				}
				else
				{
					if($registro->estatus=='ACTIVO')	
						$grid[$i]['cambiardomicilio']='<span onclick="cambiarDomicilio('.$registro->id.');" title="Cambio de domicilio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/map.png" /></span>';
					elseif($registro->estatus=='POR INSTALAR')	
						$grid[$i]['cambiardomicilio']='<span onclick="cambiarDomicilio(-2);" title="Cambio de domicilio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/map.png" /></span>';
					else
						$grid[$i]['cambiardomicilio']='<span onclick="cambiarDomicilio(-1);" title="Cambio de domicilio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/map.png" /></span>';
				}
			
			if(Usuario::tipo()!=0)
				if(Ordenextensionadicional::tieneOrdenPendiente($registro->id))	
				{
					$grid[$i]['tv']='<span onclick="anadirTv(-3);" title="Añadir Televisión"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/television_add.png" /></span>';	
				}
				else
				{
					if($registro->estatus=='ACTIVO')	
						$grid[$i]['tv']='<span onclick="anadirTv('.$registro->id.');" title="Añadir Televisión"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/television_add.png" /></span>';
					elseif($registro->estatus=='POR INSTALAR')	
						$grid[$i]['tv']='<span onclick="anadirTv(-2);" title="Añadir Televisión"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/television_add.png" /></span>';	
					else
						$grid[$i]['tv']='<span onclick="anadirTv(-1);" title="Añadir Televisión"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/television_add.png" /></span>';	
				}

			if(Usuario::tipo()!=0)
				if(Ordenextensionadicional::tieneOrdenPendiente($registro->id))	
				{
					$grid[$i]['cambiarservicio']='<span onclick="cambiarServicio(-5);" title="Cambiar servicio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/service_change.png" /></span>';	
				}
				else if(Ordencambiodomicilio::tieneOrdenPendiente($registro->id))	
				{
					$grid[$i]['cambiarservicio']='<span onclick="cambiarServicio(-3);" title="Cambiar servicio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/service_change.png" /></span>';	
				}
				else if(Ordencambioservicio::tieneOrdenPendiente($registro->id))
				{
					$grid[$i]['cambiarservicio']='<span onclick="cambiarServicio(-4);" title="Cambiar servicio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/service_change.png" /></span>';	
				}
				else
				{
					if($registro->estatus=='ACTIVO')
					{
						$grid[$i]['cambiarservicio']='<span onclick="cambiarServicio('.$registro->id.');" title="Cambiar servicio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/service_change.png" /></span>';
					}
					elseif($registro->estatus=='POR INSTALAR')	
						$grid[$i]['cambiarservicio']='<span onclick="cambiarServicio(-2);" title="Cambiar servicio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/service_change.png" /></span>';	
					else
						$grid[$i]['cambiarservicio']='<span onclick="cambiarServicio(-1);" title="Cambiar servicio"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/service_change.png" /></span>';	
				}

			if($registro->estatus=='ACTIVO')
			{	
				$grid[$i]['baja']='<span onclick="solicitarBaja('.$registro->id.');" title="Solicitar baja"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png"/></span>';
				
			}
			else if($registro->estatus=='POR INSTALAR')	
				$grid[$i]['baja']='<span onclick="solicitarBaja('.$registro->id.');" title="Solicitar baja"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png"/></span>';	
			else
				$grid[$i]['baja']='<span onclick="solicitarBaja(-1);" title="Solicitar baja"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png"/></span>';	
							
            $grid[$i]['editar']='<span onclick="editarCliente('.$registro->id.');" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';		
				
			$i++;
		}
		
        My_Comun::grid2($registros,$grid);	

    }

    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos las plazas
        $this->view->plazas=Plaza::obtenerPlazas();
		
        ### Obtenemos las comunidades de la plaza
        $this->view->comunidades=Comunidad::obtenerComunidadesDeMunicipio(Zend_Auth::getInstance()->getIdentity()->Plaza->municipio_id);
        
        $this->view->promotores=My_Comun::obtenerPromotores();
		
		$this->view->comisiones=Comisiones::obtenerComisiones();
        
        $this->view->conceptoContratacion=ConceptoDeContratacion::obtenerConceptoParaContratacion();
        $this->view->servicioAContratar=Servicio::obtenerServicios("status=1 AND tipo=1");

        $this->view->colonias=Colonia::obtenerColoniasMunicipio(Zend_Auth::getInstance()->getIdentity()->Plaza->municipio_id);
        
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Cliente", $this->_getParam('id'));
        }
    }
		
    public function editarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        

        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        $this->view->registro=My_Comun::obtener("Cliente", $this->_getParam('id'));
        $this->view->comunidades=Comunidad::obtenerComunidadesDeMunicipio($this->view->registro->Plaza->municipio_id);
        $this->view->colonias=Colonia::obtenerColoniasMunicipio($this->view->registro->Plaza->municipio_id);
        
    }
	
	
	
	public function establecerconceptocontratacionAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   $conceptoContratacion=ConceptoDeContratacion::obtenerConceptoContratacion($this->_getParam('id'));
	   
	   echo $conceptoContratacion->ConceptoDeContratacion->nombre."_".$conceptoContratacion->tarifa;
		
	}
	
	public function establecertvsconceptocontratacionAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   $conceptoContratacion=ConceptoDeContratacion::obtenerConceptoContratacion($this->_getParam('id'));
	   
	   echo $conceptoContratacion->tv;
		
	}
	
	public function establecerserviciocontratacionAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	    $servicio=Servicio::obtenerServicios("id='".$this->_getParam('id')."'");
		$comision=Servicio::obtenerComision($this->_getParam('id'));
		

		
		### Primero verificamos si es un servicio o un servicio adicional
		
		### Si es un servicio
		if($servicio[0]->tipo!=2)
		{
			$tarifa=Servicio::obtenerTarifa($this->_getParam('id'));
			###Buscamos las tarifas especiales asociadas a este servicio
			$tarifas=Servicio::obtenerTarifasEspecialesServicio($tarifa->servicio_id);
			
			### Si encontramos tarifas especiales asociadas al servicio en la plaza actual, entonces armamos un select de tarifas
			if(count($tarifas)>0)
			{
				$opt.='<option value="0">Tarifa base</option>';
				foreach($tarifas as $t)
				{
					$opt.='<option value="'.$t->id.'">'.$t->Tarifaespecial->nombre.'</option>';
				}
				
				$optPeriodo='<option value="0">Indefinido</option>';
				for($i=1;$i<=12;$i++)
				{
					$optPeriodo.='<option value="'.$i.'">'.$i.' periodo(s)</option>';
				}

				
				echo '<div class="cuerpo" id="divCuerpo'.$tarifa->id.'"><div>1</div><div>'.$servicio[0]->nombre.'</div> <div class="divComision">$'.sprintf("%0.2f",$comision).'</div><div><select name="tipotarifa[]" id="select_'.$tarifa->id.'" onchange="estableceTipoTarifa(this.id,'.$tarifa->precio_1a5.');" class="span10">'.$opt.'</select></div><div id="precio'.$tarifa->id.'" class="divMontoServicio">$'.sprintf("%0.2f",$tarifa->precio_1a5).'</div><div class="eliminar"><img src="/images/png/cancelar.png" width="20px" onclick="eliminarServicio('.$tarifa->id.');" /></div><input type="hidden" name="tarifas[]" value="'.$tarifa->id.'" class="hdnTarifa" /><input type="hidden" name="servicios[]" value="'.$tarifa->servicio_id.'" class="hdnServicio" /><input type="hidden" name="autorizacion[]" value="0" class="hdnServicio" id="hdnautorizacion'.$tarifa->id.'" /></div>';
			}
			else
			{
				echo '<div class="cuerpo" id="divCuerpo'.$tarifa->id.'"><div>1</div><div>'.$servicio[0]->nombre.'</div><div class="divComision">$'.sprintf("%0.2f",$comision).'</div><div>Base</div><div class="divMontoServicio">$'.sprintf("%0.2f",$tarifa->precio_1a5).'</div><div class="eliminar"><img src="/images/png/cancelar.png" width="20px" onclick="eliminarServicio('.$tarifa->id.');" /></div><input type="hidden" name="tarifas[]" value="'.$tarifa->id.'" class="hdnTarifa" /><input type="hidden" name="servicios[]" value="'.$tarifa->servicio_id.'" class="hdnServicio" /><input type="hidden" name="autorizacion[]" value="0" class="hdnServicio" id="hdnautorizacion'.$tarifa->id.'" /></div>';
			}
			
		}
		else
		{
			$tarifa=Servicio::obtenerTarifaAdicional($this->_getParam('id'));
			echo '<div class="cuerpo" id="divCuerpo'.$tarifa->id.'"><div>1</div><div>Servicio adicional '.$servicio[0]->nombre.'</div> <div class="divComision">$'.sprintf("%0.2f",$comision).'</div><div>Base</div><div class="divMonto">$'.sprintf("%0.2f",$tarifa->tarifa).'</div><div class="eliminar"><img src="/images/png/cancelar.png" width="20px" onclick="eliminarServicio('.$tarifa->id.');" /></div><input type="hidden" name="tarifas[]" value="'.$tarifa->id.'"  /><input type="hidden" name="serviciosAdicionales[]" value="'.$tarifa->servicioadicional_id.'" class="hdnServicio" /></div>';
		}
		
		
		
		
	}
	
	public function obtenermontotarifaespecialAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   $q=Doctrine_Query::create()->from('ServicioPlazaTarifaespecial')->where('id=?',$this->_getParam('id'))->execute()->getFirst();
	   
	   echo "$".sprintf("%.2f",$q->precio_1a5)."_".$q->Tarifaespecial->autorizacion;
		
	}
	
	public function agregarservicioAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   
	   $this->view->servicioAContratar=Servicio::obtenerServiciosAContratar();
		
	}
	
	public function tiposervicioAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   ### Obtenemos servicios
	   if($this->_getParam('id')==1)
	   {
		   	$servicios=Servicio::obtenerServiciosAContratar();
			foreach($servicios as $servicio)
			{
				echo '<option value="'.$servicio->servicio_id.'">'.$servicio->Servicio->nombre.'</option>';
			}
	   }
	   else
	   {
		   ### Obtenemos servicios adicionales
		   $servicios=Servicio::obtenerServicios(" tipo=2 AND status=1 AND Servicio.id=Servicio.TarifaServicioadicional.servicioadicional_id AND Servicio.TarifaServicioadicional.plaza_id=".Usuario::plaza()."");
			foreach($servicios as $servicio)
			{
				echo '<option value="'.$servicio->id.'">'.$servicio->nombre.'</option>';
			}
	   }
	   
	   $this->view->servicioAContratar=Servicio::obtenerServiciosAContratar();
		
	}

    
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);

       echo Cliente::guardar("Cliente",$_POST,NULL,$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Cliente", $_POST['id']);
    }
	
	
	public function cambiodomicilioAction()
	{
		### Deshabilitamos el layout
       $this->_helper->layout->disableLayout();
	   $this->view->registro=My_Comun::obtener("Cliente",$this->_getParam('id'));
	   $this->view->colonias=Colonia::obtenerColoniasMunicipio(Zend_Auth::getInstance()->getIdentity()->Plaza->municipio_id);
	}
	
	public function guardarcambiodomicilioAction()
	{
		 ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   echo Cliente::cambiarDomicilio($_POST['id'],$_POST);
	}

	public function cambioservicioAction()
	{
		### Deshabilitamos el layout
       	$this->_helper->layout->disableLayout();
	   	### Obtenemos los servicios que tiene el cliente
	   	$this->view->servicios = array();
	   	$dServicios = Doctrine_Query::create()->from('ClienteServicio')->where('cliente_id='.$this->_getParam('id'))->orderBy("id DESC")->execute();
		
		###obtenemos el concepto de contratcion actual
		###primero obtenemos al cliente para obtener el id de la tarifa de contratacion
		$cliente = $this->_getParam('id');
		$Cli = Cliente::obtenerCliente("id = ".$cliente);
		### por medio de la tarifa de contratacion obtenemos el concepto de contratacion
		$tarifa_actual = ConceptoDeContratacion::obtenerConceptoContratacion($Cli->tarifa_concepto_de_contratacion_id);
		$conceptoContratacion = (object) array();
		$conceptoContratacion->id = $tarifa_actual->ConceptoDeContratacion->id;
		$conceptoContratacion->nombre = $tarifa_actual->ConceptoDeContratacion->nombre;
		$this->view->concepto_contratacion_actual = $conceptoContratacion;
		###obtenemos los conceptos de contrartacion disponibles
		$conceptos_contratacion = ConceptoDeContratacion::obtenerConceptoParaContratacion();
	   	
	   	foreach($dServicios as $servicio)
	   	{
       		$this->view->servicios[] = $servicio->servicio_id;
	   	}
		$ccarray = array();
		foreach($conceptos_contratacion as $cc)
	   	{
			//print_r($cc->concepto_de_contratacion_id);
			$concepto = Doctrine_Query::create()->from('ConceptoDeContratacion')->where('id ="'.$cc->concepto_de_contratacion_id.'" AND status=1')->execute()->getFirst();
			//echo $concepto->nombre; exit;
			if($concepto->status==1){
				$ccarray[$concepto->id]["id"] = $concepto->id;
				$ccarray[$concepto->id]["nombre"] = $concepto->nombre;
			}
	   	}
		ksort($ccarray);
		$this->view->conceptos_contratacion = $ccarray;
	   	### Obtenemos los servicios disponibles
	   	$this->view->l_servicios;
	   	$this->view->l_servicios = Doctrine_Query::create()->from('Servicio')->where('status=1')->andWhere('tipo=1')->execute();
	   	$this->view->registro = My_Comun::obtener("Cliente",$this->_getParam('id'));
	}

	public function guardarcambioservicioAction()
	{
		### Deshabilitamos el layout y la vista
       	$this->_helper->layout->disableLayout();
       	$this->_helper->viewRenderer->setNoRender(TRUE);
	   	echo Cliente::cambiarServicio($_POST['id'],$_POST);
	}
	
	public function anadirtvAction()
	{
		### Deshabilitamos el layout
       $this->_helper->layout->disableLayout();
	   $this->view->registro=My_Comun::obtener("Cliente",$this->_getParam('id'));
	}
	
	public function guardaranadirtvAction()
	{
		 ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   echo Cliente::anadirTv($_POST['id'],$_POST);
	}
	
	public function costotvadicionalAction()
	{
	   ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   echo Parametros::obtenerParametros('extensiones');
	}
	/*
	* Function Added by chmurillo at 04/07/18
	*/
	public function obtenercoloniasAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   echo json_encode(Municipio::obtenerColonias($this->_getParam('colonias')));
	}
	
	public function bajaAction()
	{
		### Deshabilitamos el layout
       $this->_helper->layout->disableLayout();
	   $this->view->registro=My_Comun::obtener("Cliente",$this->_getParam('id'));
	}
	
	public function guardarbajaAction()
	{
		 ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
		Cliente::baja($_POST['id'],$_POST);
	}
    
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
		$filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        /*$estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');
        $comunidad = $this->_getParam('comunidad');*/
        $plaza = $this->_getParam('plaza');
		$colonia=$this->_getParam('colonia');
		$estatus = $this->_getParam('estatus');
		$etiqueta = $this->_getParam('etiqueta');
		$nodo = $this->_getParam('nodo');
		$poste = $this->_getParam('poste');
        
        if($nombre != '')
        {
           $filtro .= " AND (nombre LIKE '%$nombre%' OR telefono LIKE '%$nombre%'  OR calle LIKE '%$nombre%' OR contrato LIKE '%$nombre%') ";
        }
        if($plaza != '' && $plaza!='undefined')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND plaza_id in (" .$plaza.")";
        }
		if($colonia!='')
		{
			$colonia = str_replace('_', ',', $colonia);
            $filtro .= " AND colonia_id in (" .$colonia.")";
		}
		if($estatus != '')
        {
			$estatus = str_replace('_', ',', $estatus);
            $filtro .= " AND status in (" .$estatus.")";
        }
		
		if($etiqueta != '')
        	$filtro .= " AND etiqueta = '$etiqueta' ";
			
		if($nodo != '')
        	$filtro .= " AND nodo = '$nodo' ";
			
		if($poste != '')
        	$filtro .= " AND poste = '$poste' ";
        
        $registros=  My_Comun::obtenerFiltro("Cliente", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE CLIENTES");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(80,15,35,30,30));
        $pdf->Row(array('CLIENTE','PLAZA','TELEFONO','CELULAR','ESTATUS'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
            
           $pdf->Row
           (
                array
                (
                    $registro->nombre,
					$registro->Plaza->nombre,
                    $registro->telefono,
                    $registro->celular1,
                    $registro->estatus
                ),0,1			
           );
        }
        
        
       $pdf->Output();	
       
    }
	
	
	
    function exportarAction()
    {
    	ini_set("memory_limit", "230M");
        ini_set('max_execution_time', 0);
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
		$filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $plaza = $this->_getParam('plaza');
		$colonia=$this->_getParam('colonia');
		$estatus = $this->_getParam('estatus');
		$etiqueta = $this->_getParam('etiqueta');
		$nodo = $this->_getParam('nodo');
		$poste = $this->_getParam('poste');
        
              if($nombre != '')
        {
           $filtro .= " AND (c.nombre LIKE '%$nombre%' OR c.telefono LIKE '%$nombre%'  OR c.calle LIKE '%$nombre%' OR c.contrato LIKE '%$nombre%') ";
        }
        if($plaza != '' && $plaza!='undefined')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND c.plaza_id in (" .$plaza.")";
        }
		if($colonia!='')
		{
			$colonia = str_replace('_', ',', $colonia);
            $filtro .= " AND colonia_id in (" .$colonia.")";
		}
		if($estatus != '')
        {
			$estatus = str_replace('_', ',', $estatus);
            $filtro .= " AND c.status in (" .$estatus.")";
        }
		
		if($etiqueta != '')
        	$filtro .= " AND c.etiqueta = '$etiqueta' ";
			
		if($nodo != '')
        	$filtro .= " AND c.nodo = '$nodo' ";
			
		if($poste != '')
        	$filtro .= " AND c.poste = '$poste' ";

		$query = "SELECT 
					(CASE
						WHEN c.status = 1 THEN 'ACTIVO'
						WHEN c.status = 2 THEN 'DESCONECTAR'
						WHEN c.status = 3 THEN 'CORTAR'
						WHEN c.status = 4 THEN 'RETIRAR'
						WHEN c.status = 5 THEN 'BAJA'
						WHEN c.status = 6 THEN 'POR INSTALAR'
						WHEN c.status = 7 THEN 'CORTADO'
						WHEN c.status = 8 THEN 'DESCONECTADO'
						WHEN c.status = 9 THEN 'RETIRADO'
						WHEN c.status= 10 THEN 'POR REINSTALAR'
						WHEN c.status= 11 THEN 'POR RECONECTAR'
						WHEN c.status= 12 THEN 'BAJA VOLUNTARIA'
					END) as estatus,
					c.contrato AS contrato,
					c.nombre AS nombre,
					c.telefono AS telefono,
					c.celular1 AS celular1,
					c.celular2 AS celular2,
					c.calle AS calle,
					c.no_exterior AS no_exterior,
					c.no_interior AS no_interior,
					c.colonia_id AS colonia_id,
					(SELECT fecha_instalacion FROM ordeninstalacion WHERE cliente_id=c.id LIMIT 1) as fecha_instalacion,
					(SELECT MAX(fecha_fin) FROM cobranza WHERE cliente_id=c.id AND cancelado=0 and pagada=1 LIMIT 1) as ultimo_mes_cubierto,
					c.televisores AS televisores,
                  	c.etiqueta AS etiqueta,
                    c.nodo AS nodo,
                    c.poste AS poste,
                    c.fecha_contratacion AS fecha_contratacion,
                    p.nombre AS plaza,
                    c.comentarios AS comentarios,
                    c.id AS id,
					(SELECT servicio.nombre 
						FROM cliente_servicio 
						INNER JOIN servicio 
						ON servicio.id=cliente_servicio.servicio_id 
						WHERE cliente_servicio.cliente_id=c.id 
						ORDER BY cliente_servicio.id DESC LIMIT 1)as servicio_contratado,
					
					(SELECT concepto_de_contratacion.nombre 
						FROM cobranza 
						INNER JOIN cobranza_detalle 
							ON cobranza_detalle.cobranza_id = cobranza.id 
						INNER JOIN concepto_de_contratacion 
							ON cobranza_detalle.concepto_contratacion_id =concepto_de_contratacion.id 
						WHERE cobranza.cliente_id=c.id ORDER BY cobranza.id DESC LIMIT 1)as concepto_contratado
             	FROM cliente AS c                      
                                                                              
              	INNER JOIN plaza AS p
                    ON c.plaza_id = p.id
              	WHERE(".$filtro.")		
					 ";//max(cs.id) AS maximo
					//c.estatus AS estatus,
					//ORDER BY cs.id DESC 
			
        //recuperamos el singleton de la conexión
        $con = Doctrine_Manager::getInstance()->connection();    
        //ejecutamos la consulta
        $st = $con->execute($query);        
        //recuperamos los resultados en un arreglo
        $registros = $st->fetchAll();           


        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
				"A$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 11
                        ),
                "B$i" => array(
                        "name" => 'NOMBRE',
                        "width" => 20
                        ),
                "C$i" => array(
                        "name" => 'TELEFONO',
                        "width" => 20
                        ),
                "D$i" => array(
                        "name" => 'CELULAR1',
                        "width" => 20
                        ),
                "E$i" => array(
                        "name" => 'CELULAR2',
                        "width" => 20
                        ),
				"F$i" => array(
                        "name" => 'CALLE',
                        "width" => 20
                        ),
                "G$i" => array(
                        "name" => 'NO. EXTERIOR',
                        "width" => 20
                        ),
                "H$i" => array(
                        "name" => 'NO. INTERIOR',
                        "width" => 20
                        ),
                "I$i" => array(
                        "name" => 'COLONIA',
                        "width" => 20
                        ),                        		
				"J$i" => array(
                        "name" => 'FECHA INSTALACION',
                        "width" => 20
                        ),
				"K$i" => array(
                        "name" => 'ULTIMO MES CUBIERTO',
                        "width" => 20
                        ),		
				"L$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 20
                        ),		
				"M$i" => array(
                        "name" => 'NO. TVS',
                        "width" => 10
                        ),
				"N$i" => array(
                        "name" => 'ETIQUETA',
                        "width" => 10
                        ),										
				"O$i" => array(
                        "name" => 'NODO',
                        "width" => 10
                        ),		
				"P$i" => array(
                        "name" => 'POSTE',
                        "width" => 10
                        ),		
				"Q$i" => array(
                        "name" => 'FECHA DE CONTRATACIÓN',
                        "width" => 20
                        ),
				"R$i" => array(
                        "name" => 'PLAZA',
                        "width" => 20
                        ),		
				"S$i" => array(
                        "name" => 'OBSERVACIÓN',
                        "width" => 30
                        ),
                "T$i" => array(
                        "name" => 'MONTO CONTRATACIÓN',
                        "width" => 30
                        ), 
                "U$i" => array(
                        "name" => 'CONCEPTO DE CONTRATACIÓN',
                        "width" => 30
                        ),    
                "V$i" => array(
                        "name" => 'SERVICIO CONTRADO',
                        "width" => 30
                        )          		
        );
        

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {

				//$domicilio=$registro->calle.", ".$registro->no_exterior;
				//if($registro->no_interior!='')
				//	$domcilio.=", ".$registro->no_interior;

				
				$fi=Cliente::fechaInstalacion($registro['id']);
                $i++;
                
                $data[] = array(				
		    		"A$i" =>$registro['contrato'],
					"B$i" =>$registro['nombre'],
                    "C$i" =>$registro['telefono'],
                    "D$i" =>$registro['celular1'],
                    "E$i" =>$registro['celular2'],
					"F$i" =>$registro['calle'],
					"G$i" =>$registro['no_exterior'],
					"H$i" =>$registro['no_interior'],
					"I$i" =>Colonia::obtenerColonia($registro['colonia_id']),
                    "J$i" =>$fi,
                    "K$i" =>$registro['ultimo_mes_cubierto'],
					"L$i" =>$registro['estatus'],					
					"M$i" =>$registro['televisores'],
					"N$i" =>$registro['etiqueta'],				
					"O$i" =>$registro['nodo'],
					"P$i" =>$registro['poste'],
					"Q$i" =>$registro['fecha_contratacion'],
					"R$i" =>$registro['plaza'],
					"S$i" =>$registro['comentarios'],
					"T$i" =>Cliente::montoContratacion($registro['id']),
					"U$i" =>$registro['concepto_contratado'],
					"V$i" =>$registro['servicio_contratado']);
        }
        //echo "<pre>";
        //echo var_dump($data2); EXIT;
		
        $objPHPExcel->createExcel('Clientes', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'CLIENTES'));
    }
	/****
	    function exportarAction()
    {
    	ini_set("memory_limit", "230M");
        ini_set('max_execution_time', 0);
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
		$filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        /*$estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');
        $comunidad = $this->_getParam('comunidad');
        $plaza = $this->_getParam('plaza');
		$estatus = $this->_getParam('estatus');
		$etiqueta = $this->_getParam('etiqueta');
		$nodo = $this->_getParam('nodo');
		$poste = $this->_getParam('poste');
        
        if($nombre != '')
        {
           $filtro .= " AND (nombre LIKE '%$nombre%' OR telefono LIKE '%$nombre%'  OR calle LIKE '%$nombre%' OR contrato LIKE '%$nombre%') ";
        }
        if($plaza != '' && $plaza!='undefined')
        {
			$plaza = str_replace('_', ',', $plaza);
            $filtro .= " AND plaza_id in (" .$plaza.")";
        }
		if($estatus != '')
        {
			$estatus = str_replace('_', ',', $estatus);
            $filtro .= " AND status in (" .$estatus.")";
        }
		
		if($etiqueta != '')
        	$filtro .= " AND etiqueta = '$etiqueta' ";
			
		if($nodo != '')
        	$filtro .= " AND nodo = '$nodo' ";
			
		if($poste != '')
        	$filtro .= " AND poste = '$poste' ";

        $registros=  My_Comun::obtenerFiltro("Cliente", $filtro);


        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
				"A$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 11
                        ),
                "B$i" => array(
                        "name" => 'NOMBRE',
                        "width" => 20
                        ),
                "C$i" => array(
                        "name" => 'TELEFONO',
                        "width" => 20
                        ),
                "D$i" => array(
                        "name" => 'CELULAR1',
                        "width" => 20
                        ),
                "E$i" => array(
                        "name" => 'CELULAR2',
                        "width" => 20
                        ),
				"F$i" => array(
                        "name" => 'CALLE',
                        "width" => 20
                        ),
                "G$i" => array(
                        "name" => 'NO. EXTERIOR',
                        "width" => 20
                        ),
                "H$i" => array(
                        "name" => 'NO. INTERIOR',
                        "width" => 20
                        ),
                "I$i" => array(
                        "name" => 'COLONIA',
                        "width" => 20
                        ),                        		
				"J$i" => array(
                        "name" => 'FECHA INSTALACION',
                        "width" => 20
                        ),
				"K$i" => array(
                        "name" => 'ULTIMO MES CUBIERTO',
                        "width" => 20
                        ),		
				"L$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 20
                        ),		
				"M$i" => array(
                        "name" => 'NO. TVS',
                        "width" => 10
                        ),
				"N$i" => array(
                        "name" => 'ETIQUETA',
                        "width" => 10
                        ),										
				"O$i" => array(
                        "name" => 'NODO',
                        "width" => 10
                        ),		
				"P$i" => array(
                        "name" => 'POSTE',
                        "width" => 10
                        ),		
				"Q$i" => array(
                        "name" => 'FECHA DE CONTRATACIÓN',
                        "width" => 20
                        ),
				"R$i" => array(
                        "name" => 'PLAZA',
                        "width" => 20
                        ),		
				"S$i" => array(
                        "name" => 'OBSERVACIÓN',
                        "width" => 30
                        ),
                "T$i" => array(
                        "name" => 'MONTO CONTRATACIÓN',
                        "width" => 30
                        ), 
                "U$i" => array(
                        "name" => 'CONCEPTO DE CONTRATACIÓN',
                        "width" => 30
                        ),    
                "V$i" => array(
                        "name" => 'SERVICIO CONTRADO',
                        "width" => 30
                        )          		
        );
        

        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {

				$domicilio=$registro->calle.", ".$registro->no_exterior;
				if($registro->no_interior!='')
					$domcilio.=", ".$registro->no_interior;

				
				$fi=Cliente::fechaInstalacion($registro->id);
				/*if(Cliente::fechaInstalacion($registro->id)!='')
				{
					var_dump(Cliente::fechaInstalacion($registro->id));

					/*
					$f=explode(" ",Cliente::fechaInstalacion($registro->id));
					$fi=str_replace("-","/",$f[0]);
					$mes=explode("/",$fi);


					switch($mes[1])
					{
						case "01": $fi=$mes[2]."/"."Ene/".$mes[0]; break;
						case "02": $fi=$mes[2]."/"."Feb/".$mes[0]; break;
						case "03": $fi=$mes[2]."/"."Mar/".$mes[0]; break;
						case "04": $fi=$mes[2]."/"."Abr/".$mes[0]; break;
						case "05": $fi=$mes[2]."/"."May/".$mes[0]; break;
						case "06": $fi=$mes[2]."/"."Jun/".$mes[0]; break;
						case "07": $fi=$mes[2]."/"."Jul/".$mes[0]; break;
						case "08": $fi=$mes[2]."/"."Ago/".$mes[0]; break;
						case "09": $fi=$mes[2]."/"."Sep/".$mes[0]; break;
						case "10": $fi=$mes[2]."/"."Oct/".$mes[0]; break;
						case "11": $fi=$mes[2]."/"."Nov/".$mes[0]; break;
						case "12": $fi=$mes[2]."/"."Dic/".$mes[0]; break;

					}
					$fi="Enero";
				//}
                $i++;
                
                $data[] = array(				
		    		"A$i" =>$registro->contrato,
					"B$i" =>$registro->nombre,
                    "C$i" =>$registro->telefono,
                    "D$i" =>$registro->celular1,
                    "E$i" =>$registro->celular2,
					"F$i" =>$registro->calle,
					"G$i" =>$registro->no_exterior,
					"H$i" =>$registro->no_interior,
					"I$i" =>Colonia::obtenerColonia($registro->colonia_id),
                    "J$i" =>$fi,
                    "K$i" =>$registro->getUltimomescubierto(),
					"L$i" =>$registro->estatus,					
					"M$i" =>$registro->televisores,
					"N$i" =>$registro->etiqueta,				
					"O$i" =>$registro->nodo,
					"P$i" =>$registro->poste,
					"Q$i" =>$registro->fecha_contratacion,
					"R$i" =>$registro->Plaza->nombre,
					"S$i" =>$registro->comentarios,
					"T$i" =>Cliente::montoContratacion($registro->id),
					"U$i" =>Cliente::conceptoContratacion($registro->id),
					"V$i" =>Cliente::montoContratacion($registro->id));
        }
        //echo "<pre>";
        //echo var_dump($data2); EXIT;
		
        $objPHPExcel->createExcel('Clientes', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'CLIENTES'));
    }
	
	
	****/
	
	
	
	
	public function  cronAction()
	{
		ini_set("memory_limit", "530M");
        ini_set('max_execution_time', 0);
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
		### Asignamos el dia actual
		$d=date('d'); 
		//$d=1;
		
		$qClientes=Doctrine_Query::create()->from('Cliente')->where('status!=6 AND status!=10 AND status!=11 AND status!=5 AND status!=12');
		$clientes=$qClientes->execute();
		
		### Generamos un nuevo registro de ejecución del cronjob
		$cron= new Cronjob();
			$cron->fecha=date('Y-m-d h:i:s');
		$cron->save();	
		
		foreach($clientes as $cliente)
		{
			
			
			$cronjob= new Cronjob();
			### Si no se ha ejecutado el cambio de estatus para este cliente entonces lo ejecutamos
			if($d==1)
			{

				Cliente::actualizarEstatus($cliente);
				
			}//ifcronjob
			
			### Si el dia actual es igual o mayor al 11 intentamos generar la orden  correspondiente.
			if($d==6)
			{	
				//echo $cliente->estatus;			
				### Si el dia es 11, entonces ya están actualizados los estatus y habrá que generar órdenes

				 if($cliente->estatus=="DESCONECTAR")	
				 {
					 echo "Id: ".$cliente->id;
					echo "<br>Cliente: ".$cliente->nombre;
					echo "<br>Ultimo mes: ".$ultimoMesCubierto;
					echo "<br>Estatus: ".$cliente->estatus;
					echo "<hr>";
					Ordendesconexion::generar($cliente->id);
				 }
			}
	   }//foreach	
	}// end function

	public function  cronpruebaAction()
	{
		ini_set("memory_limit", "530M");
        ini_set('max_execution_time', 0);
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
		### Asignamos el dia actual
		$d=date('d'); 
		$d=1;
		
		$qClientes=Doctrine_Query::create()->from('Cliente')->where('status!=6 AND status!=10 AND status!=11 AND status!=5 AND status!=12');
		$clientes=$qClientes->execute();
		
		### Generamos un nuevo registro de ejecución del cronjob
		$cron= new Cronjob();
			$cron->fecha=date('Y-m-d h:i:s');
		$cron->save();	
		
		foreach($clientes as $cliente)
		{
			
			
			$cronjob= new Cronjob();
			### Si no se ha ejecutado el cambio de estatus para este cliente entonces lo ejecutamos
			if($d==1)
			{

				Cliente::actualizarEstatusPrueba($cliente);
				
			}//ifcronjob
			
			### Si el dia actual es igual o mayor al 11 intentamos generar la orden  correspondiente.
			if($d==6)
			{	
				//echo $cliente->estatus;			
				### Si el dia es 11, entonces ya están actualizados los estatus y habrá que generar órdenes

				 if($cliente->estatus=="DESCONECTAR")	
				 {
					 echo "Id: ".$cliente->id;
					echo "<br>Cliente: ".$cliente->nombre;
					echo "<br>Ultimo mes: ".$ultimoMesCubierto;
					echo "<br>Estatus: ".$cliente->estatus;
					echo "<hr>";
					Ordendesconexion::generar($cliente->id);
				 }
			}
	   }//foreach	
	}// end function
	
	
	 public function importarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        
        ini_set("memory_limit", "230M");
        ini_set('max_execution_time', 0);

        //$objReader  = new My_PHPExcel_Importar();
		//Estas dos lineas arriba y abajo son las unicas que estan en productivo
		//$objPHPExcel = $objReader->loadalterno("excel/cuidado/SMO1.xlsx",6,136,86684,631);
		
		//$objPHPExcel = $objReader->load("excel/QMC3.xlsx",11,151,25568,1006);
		//($archivo, $plaza, $usuario, $comunidad,$cantidad)
		
		### Deshabilitamos el layout y la vista
        //$this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(TRUE);
        
        
        /*ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);
        $objReader  = new My_PHPExcel_Importar();
		$objPHPExcel = $objReader->loadalterno("excel/SMO1.xlsx");*/
    }
	
	public function restamesAction()
    {
    	/*$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        #### Determinamos los ids de los clientes a los que se les cambiará el ultimo mes cubierto
        $contratos="123,124,3600,3542,3691,3702,3597,3654,3813,3677,3780,3891,3893,3825,3799,3899,3816,3958,3967,3932,3001,3002,3003,3004,3005,3006,3007,3008,3009,3010,3011,3012,3013,3014,3015,3016,3017,3018,3019,3020,3021,3022,3023,3024,3025,3026,3027,3028,3029,3030,3031,3032,3046,3840,3996,3048,3051,3052,3873,3154,3035,3192,3170,3172,3105,3106,3987,3990,3144,3179,3128,3249,3204,3213,3223,3199,3195,3182,3037,3214,3218,3295,3056,3241,3243,3245,3217,3279,3269,3320,3237,3326,3328,3329,3261,3293,3294,3481,3384,3499,3419,2057,2053,3465,3448,2009,2087";
        $contratos=explode(",",$contratos);
        echo "<pre>";
        print_r($contratos);

        foreach($contratos as $contrato)
        {
   			$q=Doctrine_Query::create()->from("Cobranza")->where("plaza_id=19 AND Cobranza.Cliente.contrato='".$contrato."'");
   			echo $q->getSqlQuery(); echo "<br>";
   			$cobro=$q->execute()->getLast();
			$cobro->fecha_fin='2013-10-31';
			$cobro->save();
        }
        exit;*/

    }

    public function cronmetasAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        Metas::generarMetas();

        ### Actualizamos la sucursal de todos los cobros
        /*$cobranza=Doctrine_Query::create()->from("Cobranza")->where('created_at>="2014-01-01 00:00:00" AND sucursal_id=0')->execute();

        foreach($cobranza as $cobro)
        {
        	echo $cobro->id;
        	$sucursal=Doctrine_Query::create()->from("Usuario")->where("id=?",$cobro->usuario_id)->execute()->getFirst();

        	$cobro->sucursal_id=$sucursal->sucursal_id;
        	$cobro->save();
        }*/

        ### Actualizamos la sucursal de todos los gastos
       /* $gastos=Doctrine_Query::create()->from("Gasto")->where('created_at>="2014-01-01 00:00:00" AND sucursal_id=0')->execute();

        foreach($gastos as $gasto)
        {
        	$sucursal=Doctrine_Query::create()->from("Usuario")->where("id=?",$gasto->usuario_id)->execute()->getFirst();

        	$gasto->sucursal_id=$sucursal->sucursal_id;
        	$gasto->save();
        }*/

         $auth =  Zend_Auth::getInstance();
        echo $auth->getIdentity()->sucursal_id;
    }

    public function pidecelularesAction()
    {
    	$this->_helper->layout->disableLayout();

    	$this->view->servicio=Servicio::obtenerDetalleServicioPorId($this->_getParam('id'));

    }

    public function testsmsAction()
    {
    	$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        ini_set('max_execution_time', 150);
        /*include('My/nusoap/lib/nusoap.php');

    	$client = new nusoap_client('http://www.masmensajes.com.mx/server.php?wsdl','wsdl');
        $err = $client->getError();

        if(!$err)
        {
	        $param = array('usuario' =>'sai','contrasena' =>'sai123','sms' =>'Mensaje SOAP','destinatario' =>'+523310072319','fecha' =>date('Y-m-s'),'hora' =>date('H:i:s'));
	        $result = $client->call('Set_Sms', $param);
    	}*/

    	//echo file_get_contents('https://www.masmensajes.com.mx/wss/smsapi13.php?usuario=sai&password=sai123&celular=+523310072319&mensaje=MENSAJEENVIARHTTP');
    	/*$ch = curl_init(); 
 
	    curl_setopt($ch,CURLOPT_URL,'https://www.masmensajes.com.mx/wss/smsapi13.php?usuario=sai&password=sai123&celular=+523310072319&mensaje=MENSAJE%20A%20ENVIARHTTP');
	    // User agent
	    curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
	 
	    // Include header in result? (0 = yes, 1 = no)
	    curl_setopt($ch, CURLOPT_HEADER, 1);
	 
	    // Should cURL return or print out the data? (true = return, false = print)
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
	 
	    // Timeout in seconds
	    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	 
	    // Download the given URL, and return output
	    $output = curl_exec($ch);
	 
	 
	    curl_close($ch);
	    print_r($output);*/
    }

    public function reasignarvoucherAction()
    {
    	$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        //Reasignamos el voucher
        ClienteVoucherCloudtrax::reasignarVoucher($this->_getParam('id'));

    }


    public function reenviarsmsAction()
    {
    	$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $voucher=ClienteVoucherCloudtrax::obtenerRegistro($this->_getParam('id'));

        //echo $voucher->celular;

        

        $mensaje="Ingresa tu codigo de acceso e inicia a disfrutar el servicio, dudas sobre como registrar tu codigo, llama al 37980049. Codigo de acceso ".$voucher->VouchersCloudtrax->voucher;
        $mensaje=urlencode(utf8_encode($mensaje));
        $response=file_get_contents('https://www.masmensajes.com.mx/wss/smsapi13.php?usuario=sai&password=sai123&celular=+52'.$voucher->celular.'&mensaje='.$mensaje);

        

        //Obtenemos los mensajes disponibles
        $prexml='<SMSWSS10><USER>sai</USER><PASS>sai123</PASS><SERVICE>12</SERVICE></SMSWSS10>';
		

		$ch = curl_init("http://www.masmensajes.com.mx/wss/smswss10.php");
		//curl_setopt($ch, CURLOPT_URL,);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,"xml=".urlencode($prexml)."");
		curl_setopt($ch, CURLOPT_HEADER, false); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$respuesta= curl_exec ($ch);

		
		curl_close ($ch);

		$respuesta=explode("|",trim($respuesta));

		$json=array();
		$json['celular']=$voucher->celular;
		$json['mensajesrestantes']=str_replace("Credito:", "", $respuesta[2]);

		echo json_encode($json);

    }
	

}