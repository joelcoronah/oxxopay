<?php
class Transacciones_MetasController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/metas.js'));
    }

    public function indexAction()
    {
    	if(isset($_POST['metas']))
		{
            Metas::procesarMetas($_POST['metas']);
            header("Location: /transacciones/metas/index/");
		}
		else
		{									
            ### Obtenemos todos los estados en loa que hay plazas
            $this->view->EstadosPlazas=Estado::obtenerEstadosPlazas();

            ### Este bloque me permitirá determinar si muestro o no el botón para guardar    
            $dia=date('d');
            //$dia=2;
            $diaSemana=date('N');
            //$diaSemana=1;
            if($dia==1 || ($dia==2 && $diaSemana==1))
                $this->view->puedeguardar=true;
            else
                $this->view->puedeguardar=false;

            $this->view->puedeguardar=true;
					
            ### Inicializamos el arreglo de estados
            $e=array();
            foreach($this->view->EstadosPlazas as $estado)
            {
                $this->view->plazas=Plaza::obtenerPlazasDeUnEstado($estado->estado_id);
                $e[$estado->estado_id] = $this->view->plazas; 
            }
            ### Mandamos a la vista la lista de estados con sus plazas         
            $this->view->e=$e;  
					
            ### Obtenemos la última fecha en la que se insertaron metas
			$fecha = Metas::obtenerMetasFecha()->fecha; 

			
            ### Obtenemos el registro de las metas establecidas en esta fecha
			$metasPlaza=Metas::obtenerMetasPlaza("fecha ="."'".$fecha."'");

            ### Inicializamos un arreglo en el que se almacenarán las metas
            $arrMetasPlaza=array();
            foreach($metasPlaza as $metaPlaza)
            {	
                $arrMetasPlaza[$metaPlaza->plaza_id]=$metaPlaza;
            }
			
            ### Mandamos el arreglo a la vista
            $this->view->metasplaza=$arrMetasPlaza;
		}
    }

    public function mostraralertametaAction()
    {
        $this->_helper->layout->disableLayout();
    }

    public function mostraralertaconfirmarmetaAction()
    {
        $this->_helper->layout->disableLayout();
        $this->view->meta=ConfirmacionEnteradoMetas::obtenerMetaPorfConfirmar(); 
    }

    public function confirmarmetaAction()
    {
        $this->_helper->layout->disableLayout();
        ConfirmacionEnteradoMetas::confirmar();
    }

    public function mostraralertaretrasadaadministradorAction()
    {
        $this->_helper->layout->disableLayout();
        $this->view->plazas=Metas::algunaPlazaVaPorDebajo(NULL,true);
    }

    public function mostraralertaretrasadaplazaAction()
    {
        $this->_helper->layout->disableLayout();

    }

    public function plazasretrasadasAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
        $sheet = $objPHPExcel->getActiveSheet();


        $i=5;
        //Titulos columna
        $columns_name = array
        (
            "B$i" => array(
                "name" => 'VENTAS',
                "width" => 20
                ),
            "G$i" => array(
                "name" => 'RECUPERACIONES',
                "width" => 20
                ),
            "L$i" => array(
                "name" => 'CLIENTES',
                "width" => 20
                ),
            "Q$i" => array(
                "name" => 'MENSUALIDADES',
                "width" => 20
                )                 
        );

        $objPHPExcel->objPHPExcel->getActiveSheet()->mergeCells('B5:F5');
        $objPHPExcel->objPHPExcel->getActiveSheet()->mergeCells('G5:K5');
        $objPHPExcel->objPHPExcel->getActiveSheet()->mergeCells('L5:P5');
        $objPHPExcel->objPHPExcel->getActiveSheet()->mergeCells('Q5:U5');

        $data=array();

        $data[6]=array
        (
            "A6"=>"PLAZA",
            "B6"=>"META TOTAL",
            "C6"=>"META DESEADA AL DIA DE HOY",
            "D6"=>"CONSEGUIDO",
            "E6"=>"CONSEGUIDO(%)",
            "F6"=>"FALTANTES",
            "G6"=>"META TOTAL",
            "H6"=>"META DESEADA AL DIA DE HOY",
            "I6"=>"CONSEGUIDO",
            "J6"=>"CONSEGUIDO(%)",
            "K6"=>"FALTANTES",
            "L6"=>"META TOTAL",
            "M6"=>"META DESEADA AL DIA DE HOY",
            "N6"=>"CONSEGUIDO",
            "O6"=>"CONSEGUIDO(%)",
            "P6"=>"FALTANTES",
            "Q6"=>"META TOTAL",
            "R6"=>"META DESEADA AL DIA DE HOY",
            "S6"=>"CONSEGUIDO",
            "T6"=>"CONSEGUIDO(%)",
            "U6"=>"FALTANTES"
        );

        
        ### Primero extraemos toda las plazas
        $i=7;
        $plaza_id=NULL;
        if($this->_getParam('plaza'))
            $plaza_id=$this->_getParam('plaza');
        $plazas=Metas::algunaPlazaVaPorDebajo(NULL,true,$plaza_id);

        foreach($plazas as $plaza)
        {
            $data[$i]=array
            (
                "A$i"=>$plaza['plaza'],
                "B$i"=>$plaza['ventas'],
                "C$i"=>$plaza['ventasesperados'],
                "D$i"=>$plaza['ventaslogradas'],
                "E$i"=>sprintf("%0.2f",$plaza['ventaslogradas']/$plaza['ventasesperados']*100)." %",
                "F$i"=>$plaza['ventasesperados']-$plaza['ventaslogradas'],
                "G$i"=>$plaza['recuperaciones'],
                "H$i"=>$plaza['recuperacionesperados'],
                "I$i"=>$plaza['recuperacioneslogradas'],
                "J$i"=>sprintf("%0.2f",$plaza['recuperacioneslogradas']/$plaza['recuperacionesperados']*100)." %",
                "K$i"=>$plaza['recuperacionesperados']-$plaza['recuperacioneslogradas'],
                "L$i"=>$plaza['clientes'],
                "M$i"=>$plaza['clientesesperados'],
                "N$i"=>$plaza['clienteslogrados'],
                "O$i"=>sprintf("%0.2f",$plaza['clienteslogrados']/$plaza['clientesesperados']*100)." %",
                "P$i"=>$plaza['clientesesperados']-$plaza['clienteslogrados'],
                "Q$i"=>$plaza['mensualidades'],
                "R$i"=>$plaza['mensualidadesesperados'],
                "S$i"=>$plaza['mensualidadeslogradas'],
                "T$i"=>sprintf("%0.2f",$plaza['mensualidadeslogradas']/$plaza['mensualidadesesperados']*100)." %",
                "U$i"=>$plaza['mensualidadesesperados']-$plaza['mensualidadeslogradas']
            );

            $i++;
        }


        $objPHPExcel->createExcel('Metas', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'Metas'));
    }


   

}