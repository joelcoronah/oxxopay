<?php

class Transacciones_OrdenreinstalacionController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/ordenreinstalacion.js'));
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $query=My_Comun::prepararQuery("Ordenreinstalacion o");

        $query->innerJoin("o.Cliente c");
        
        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');   
        $sucursal = $this->_getParam('sucursal');
        $plaza = $this->_getParam('plaza');            
        $calle='';
        $etiqueta='';
        $nodo='';
        $poste='';
        $orden_primer_nivel=$this->_getParam('orden_primer_nivel');
        $orden_segundo_nivel=$this->_getParam('orden_segundo_nivel');
        $orden_tercer_nivel=$this->_getParam('orden_tercer_nivel');
        
        $query=$this->whereAction($query, $nombre, $calle, $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza);

        $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"o.created_at desc"); 
                           
        $query->orderBy($orden);
        
        ### Extraemos los registros para formar el arreglo del grid
        $registros=My_Comun::registrosGrid2($query); 
		
    	$grid=array();		
    	$i=0;
    		
    	foreach($registros['registros'] as $registro)
    	{
                $grid[$i]['created_at']=$registro->Cliente->fecha_contratacion;
                $grid[$i]['contrato']=$registro->Cliente->contrato;
                $grid[$i]['nombre']=$registro->Cliente->nombre;
                $grid[$i]['plaza']=$registro->Cliente->Plaza->nombre;
                $grid[$i]['colonia']=$registro->Cliente->Colonia->nombre;
                $grid[$i]['telefono']=$registro->Cliente->telefono;
                $grid[$i]['celular']=$registro->Cliente->celular1;
                $grid[$i]['status']=$registro->Cliente->estatus;	
                $grid[$i]['descargar']='<span onclick="descargarInstalacion('.$registro->id.');" title="Descargar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
                if($registro->estatus==0)
                    $grid[$i]['cancelar']='<span onclick="cancelarInstalacion('.$registro->id.');" title="Cancelar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/cancelar.png" /></span>';	
    					
    		$i++;
    	}
		
        My_Comun::grid2($registros,$grid);	
    }

    public function whereAction($query, $nombre, $calle, $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza)
    {
        $query->where("o.estatus=0 AND c.status=10");
        //echo $query->getSqlQuery(); exit;

        if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $query->andWhere("c.Plaza.id=".Usuario::plaza());

        if($nombre != '')
        {
            $query->andWhere("(c.nombre LIKE '%$nombre%' OR c.telefono LIKE '%$nombre%'  OR c.calle LIKE '%$nombre%' OR c.contrato LIKE '%$nombre%')");
        }

        if($calle != '')
        {
            $query->andWhere("c.calle = '".$calle."'");
        }

        if($etiqueta != '')
        {
            $query->andWhere("c.etiqueta = '".$etiqueta."'");
        }

        if($nodo != '')
        {
            $query->andWhere("c.nodo = '".$nodo."'");
        }

        if($poste != '')
        {
            $query->andWhere("c.poste = '".$poste."'");
        }
        
        if($desde != '')
        {    
            $query->andWhere("o.created_at >=  '$desde 00:00:00'");
        }    
        if($hasta != '')
        {    
            $query->andWhere("o.created_at <= '$hasta 23:59:59'");
        }
        
        if($sucursal != 0)
        {
            $query->andWhere("c.sucursal_id = ".$sucursal);
        }   
        
        else if($plaza >0)
        {
            $query->andWhere("c.Plaza.id =" .$plaza);
        }

        return $query;
    }

	public function descargarinstalacionAction()
	{
		### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos las plazas
        $this->view->plazas=Plaza::obtenerPlazas();
        ### Obtenemos los estados 
        $this->view->comunidades=Comunidad::obtenerComunidades(1);
        
        $this->view->tecnicos=Tecnico::obtenerTecnicos(' plaza_id='.Usuario::plaza());
        
        $this->view->concepto1=Servicio::obtenerServicios("status=1 AND tipo=1");
        $this->view->concepto2=Servicio::obtenerServicios("status=1 AND tipo=2");
        
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Ordenreinstalacion", $this->_getParam('id'));
            //$this->view->instalacion = Ordeninstalacion::obtenerOrdenenInstalacionCliente($this->view->registro->cliente_id);
        }
		
	}
	
	public function cancelarinstalacionAction()
	{
	   ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
        Ordenreinstalacion::cancela($this->_getParam('id'));	 	   
    }
		
    public function guardarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);	  

        ### Actualizamos el contrato del cliente y la orden de instalación
        Ordenreinstalacion::actualiza($_POST);	   
	   
    }
    
    public function imprimirAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);


        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');   
        $sucursal = $this->_getParam('sucursal');
        $plaza = $this->_getParam('plaza');   
        $calle='';
        $etiqueta='';
        $nodo='';
        $poste='';
        $orden_primer_nivel=$this->_getParam('orden_primer_nivel');
        $orden_segundo_nivel=$this->_getParam('orden_segundo_nivel');
        $orden_tercer_nivel=$this->_getParam('orden_tercer_nivel');

        ### Inicializamos el objeto PDF
        $pdf= new My_Fpdf_Pdf();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->Header("IMPRESIÓN DE ÓRDENES DE REINSTALACIÓN");
        $pdf->SetWidths(array(45,45,45,45));


        if($this->_getParam('agrupamiento_primer_nivel')>0)
       {
            $agrupamiento=My_Comun::prepararQuery("Ordenreinstalacion o");
            $agrupamiento->innerJoin("o.Cliente c");
            $agrupamiento=$this->whereAction($agrupamiento, $nombre, $calle, $etiqueta, $nodo, $poste, $desde, $hasta, $sucursal, $plaza);
            $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"o.created_at desc");
            $agrupamiento->orderBy($orden);
            $grupos=My_Comun::configurarAgrupamiento($agrupamiento,$this->_getParam('agrupamiento_primer_nivel'));
       }


       if(count($grupos)==0)
        $grupos[]=-1;
        
        
       foreach($grupos as $titulo=>$grupo)
       {
            $query=My_Comun::prepararQuery("Ordenreinstalacion o");
            $query->innerJoin("o.Cliente c");

            if($grupo!=-1)
            {
                switch($this->_getParam('agrupamiento_primer_nivel'))
                {
                    case 2: $calle=$grupo; $titulo="Calle: "; break;
                    case 3: $etiqueta=$grupo; $titulo="Etiqueta: "; break;
                    case 4: $nodo=$grupo; $titulo="Nodo: "; break;
                    case 5: $poste=$grupo; $titulo="Poste: "; break;
                }
                
            }


            $query=$this->whereAction($query, $nombre, $calle, $etiqueta, $nodo, $poste,  $desde, $hasta, $sucursal, $plaza);
            
            //Configuramos el orden
            $orden=My_Comun::configurarOrden($orden_primer_nivel,$orden_segundo_nivel,$orden_tercer_nivel,"o.created_at desc");
            
            $query->orderBy($orden);

            //echo $query->getSqlQuery(); echo "<br><br>";

            // Ejecitamos la consulta
            $registros=  $query->execute();
            
            $i=1;
            $pdf->Header($titulo.": ".$grupo);
       
            foreach($registros as $registro)
            {
    			if($i==4)
    			{
    				$i=1;
    				$pdf->AddPage();
    			}
    			$domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
    			
    			$pdf->SetWidths(array(45,45,45,45));
    			$pdf->SetFillColor(220,220,220);
    			$pdf->SetFont('Arial','B',8);
    			$pdf->Row(array('FECHA DE CONTRATACIÓN','CONTRATO','CLIENTE','PLAZA'),1,1);
    			$pdf->SetFont('Arial','',8);
    			$pdf->Row(array($registro->Cliente->fecha_contratacion,$registro->Cliente->contrato,$registro->Cliente->nombre,$registro->Cliente->Plaza->nombre),1,1);
    			
    			$pdf->SetWidths(array(45,135));
    			$pdf->SetFont('Arial','B',8);
    			$pdf->Row(array('ESTATUS','DOMICILIO'),1,1);
                $pdf->SetFont('Arial','',8);
                $pdf->Row(array($registro->Cliente->estatus,$domicilio),1,1);

                $pdf->SetWidths(array(45,135));
                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('REFERENCIA',$registro->Cliente->referencia),1,1);

                $pdf->SetWidths(array(45,45,45,45));
                $pdf->SetFillColor(220,220,220);
                $pdf->SetFont('Arial','B',8);
                $pdf->Row(array('TELEFONO',$registro->Cliente->telefono,'CELULAR',$registro->Cliente->celular1),1,1);
    			
    			$pdf->SetWidths(array(45,45,45,45));
    			$pdf->SetFont('Arial','B',8);
    			$pdf->Row(array('NO. DE TELEVISIONES','CABLE','CONECTOR','DIVISOR 2'),0,1);
    			$pdf->SetFont('Arial','',8);
    			$pdf->Row(array($registro->Cliente->televisores,$registro->cable,$registro->conector,$registro->divisor2),0,1);
    			
    			$pdf->SetFont('Arial','B',8);
    			$pdf->Row(array('DIVISOR 3','DIVISOR 4','GRAPAS','ETIQUETA'),0,1);
    			$pdf->SetFont('Arial','',8);
    			$pdf->Row(array($registro->divisor3,$registro->divisor4,$registro->grapas,$registro->Cliente->etiqueta),0,1);
    			
    			$pdf->SetFont('Arial','B',8);
    			$pdf->Row(array('NODO','FECHA DE INSTALACIÓN','HORA INICIO','HORA DE TÉRMINO'),0,1);
    			$pdf->SetFont('Arial','',8);
    			$pdf->Row(array($registro->Cliente->nodo,$registro->fecha_instalacion,'',''),0,1);
    			
    			$pdf->SetWidths(array(90,90));
    			$pdf->SetFont('Arial','B',11);
    			$pdf->Row(array('NOMBRE DEL TÉCNICO INSTALADOR:',''),0,1);	
    			
    			$pdf->SetWidths(array(45,135));
    			$pdf->SetFont('Arial','B',8);
    			$pdf->Row(array('VISTO BUENO DEL CLIENTE:',''),0,1);			
    			$pdf->Ln(6);
    			$i++;
            }
        }
        
       $pdf->Output();	
       
    }
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "estatus = 0  AND Ordenreinstalacion.Cliente.status=10";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" estatus IS  NULL  AND Ordenreinstalacion.Cliente.plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $sucursal = $this->_getParam('sucursal');
        $plaza = $this->_getParam('plaza');
          
        if($nombre != '')
        {
            $filtro .= " AND (Ordenreinstalacion.Cliente.nombre LIKE '%$nombre%' OR Ordenreinstalacion.Cliente.telefono LIKE '%$nombre%'  OR Ordenreinstalacion.Cliente.calle LIKE '%$nombre%' OR Ordenreinstalacion.Cliente.contrato LIKE '%$nombre%') ";
        }
        
        if($desde != '')
        {    
            $filtro .= " AND (Ordenreinstalacion.created_at >=  '$desde 00:00:00')";
        }       
        if($hasta != '')
        {    
            $filtro .= " AND (Ordenreinstalacion.created_at <= '$hasta 23:59:59')";
        }
        
        if($sucursal != 0)
        {
            $filtro .= " AND Ordenreinstalacion.Cliente.sucursal_id = ".$sucursal;
        }
        
        else if($plaza >0)
        {
            $filtro .= " AND Ordenreinstalacion.Cliente.Plaza.id =" .$plaza;
        }
        
        $registros=  My_Comun::obtenerFiltro("Ordenreinstalacion", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
				
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 11
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 30
                        ),
		"C$i" => array(
                        "name" => 'PLAZA',
                        "width" => 8
                        ), 
		"D$i" => array(
                        "name" => 'DOMICILIO',
                        "width" => 50
                        ),
		"E$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 15
                        )					
        );
        
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
		$domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
                $i++;
                $data[] = array(				
		    "A$i" =>$registro->Cliente->contrato,
                    "B$i" =>$registro->Cliente->nombre,
                    "C$i" =>$registro->Cliente->Plaza->nombre,
                    "D$i" =>$domicilio,
                    "E$i" =>$registro->Cliente->estatus

                );
        }
		
        $objPHPExcel->createExcel('Ordenreinstalacion', $columns_name, $data, 10, array('rango'=>'A4:E4','size'=>14,'texto'=>'ÓRDENES DE REINSTALACIÓN'));
    }
    
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }
}