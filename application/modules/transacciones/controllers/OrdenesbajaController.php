<?php

class Transacciones_OrdenesbajaController extends Zend_Controller_Action
{

    public function init()
    {
       $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/ordenesbaja.js'));
    }

    public function indexAction()
    {
       ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
        $this->view->estados=Estado::obtenerEstados();
    }
	
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
        ### Establecemos el filtro por default
        $filtro = " Ordenbaja.Cliente.status=5 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
			$filtro.="  AND plaza_id=".Usuario::plaza();

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');
        $comunidad = $this->_getParam('comunidad');
        $plaza = $this->_getParam('plaza');
        
        if($nombre != '')
        {
            $filtro .= " AND (nombre LIKE '%$nombre%' OR telefono LIKE '%$nombre%'  OR calle LIKE '%$nombre%' OR contrato LIKE '%$nombre%') ";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        if($estado >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.estado_id =" .$estado;
        }
        if($municipio >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.id =" .$municipio;
        }
        if($comunidad >0)
        {
            $filtro .= " AND comunidad_id =" .$comunidad;
        }
		
		### Extraemos los registros para formar el arreglo del grid
		$registros=My_Comun::registrosGrid("Cliente",$filtro); 
		
		$grid=array();
		
		$i=0;
		
		foreach($registros['registros'] as $registro)
		{
			$grid[$i]['created_at']=$registro->updated_at;
			$grid[$i]['contrato']=$registro->contrato;
			$grid[$i]['nombre']=$registro->nombre;
			$grid[$i]['telefono']=$registro->telefono;
			$grid[$i]['celular']=$registro->celular1;
			$grid[$i]['status']=$registro->estatus;	
			//$grid[$i]['descargar']='<span onclick="descargar('.$registro->id.');" title="Descargar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
					
			$i++;
		}
		
        My_Comun::grid2($registros,$grid);	
    }	

     public function descargarAction()
    {
		$this->_helper->layout->disableLayout();
		
		$this->view->registro=My_Comun::obtener('Ordenbaja',$this->_getParam('id'));
		$this->view->tecnicos=Tecnico::obtenerTecnicos(" plaza_id=".Usuario::plaza()."");
    }
	
	public function guardarAction()
    {
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);    
		
		### Actualizamos la orden de cambio de domicilio
		$ordenTable = Doctrine_Core::getTable('Ordenbaja');
		$orden=$ordenTable->findOneById($_POST['id']);
			$orden->tecnico_id=$_POST['tecnico_id'];
			$orden->fecha_baja=$_POST['fecha_baja'];			
			$orden->estatus =1;

		$orden->save();	
		
    }
	
	    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        ### Establecemos el filtro por default
        $filtro = " Ordenbaja.Cliente.status=5 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
			$filtro="1=1  AND Ordenbaja.Cliente.plaza_id=".Usuario::plaza()." AND  Ordenbaja.fecha_baja IS NULL ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');
        $comunidad = $this->_getParam('comunidad');
        $plaza = $this->_getParam('plaza');

        
        if($nombre != '')
        {
            $filtro .= " AND (Ordenbaja.Cliente.nombre LIKE '%$nombre%' OR Ordenbaja.Cliente.telefono LIKE '%$nombre%'  OR Ordenbaja.Cliente.calle LIKE '%$nombre%' OR Ordenbaja.Cliente.contrato LIKE '%$nombre%') ";
        }
        if($plaza >0)
        {
            $filtro .= " AND Ordenbaja.Cliente.plaza_id =" .$plaza;
        }
        if($estado >0)
        {
            $filtro .= " AND Ordenbaja.Cliente.Comunidad.Municipio.estado_id =" .$estado;
        }
        if($municipio >0)
        {
            $filtro .= " AND Ordenbaja.Cliente.Comunidad.Municipio.id =" .$municipio;
        }
        if($comunidad >0)
        {
            $filtro .= " AND Ordenbaja.Cliente.comunidad_id =" .$comunidad;
        }
		
        $registros=  My_Comun::obtenerFiltro("Ordenbaja", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE ÓRDENES DE BAJA");

        
        $pdf->SetWidths(array(45,45,45,45));
        
        
        foreach($registros as $registro)
        {	
			$domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
			
			$pdf->SetWidths(array(45,45,45,45));
			$pdf->SetFillColor(220,220,220);
			$pdf->SetFont('Arial','B',10);
			$pdf->Row(array('FECHA DE SOLICITUD','CONTRATO','CLIENTE','ESTATUS'),1,1);
			$pdf->SetFont('Arial','',10);
			$pdf->Row(array($registro->created_at,$registro->Cliente->contrato,$registro->Cliente->nombre,$registro->Cliente->estatus),1,1);

			$pdf->SetFillColor(220,220,220);
			$pdf->SetFont('Arial','B',10);
			$pdf->Row(array('ETIQUETA','NODO','POSTE',''),1,1);
			$pdf->SetFont('Arial','',10);
			$pdf->Row(array($registro->Cliente->etiqueta,$registro->Cliente->nodo,$registro->Cliente->poste,''),1,1);
			
			$pdf->Ln(3);
			$pdf->SetWidths(array(180));
			$pdf->SetFont('Arial','B',10);
			
			$pdf->SetWidths(array(80,100));		
			$pdf->SetFont('Arial','B',10);
			$pdf->Row(array('FECHA DE RETIRO',''),0,1);
			$pdf->Row(array('TÉCNICO QUE RETIRÓ:',''),0,1);
			$pdf->Ln(5);
        }
           
       $pdf->Output();	
       
    }
}