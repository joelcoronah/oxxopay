<?php

class Transacciones_FallasreportadasController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/fallasreportadas.js'));
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
        $this->view->estatusDeFallas=EstatusDeFalla::obtenerEstatusDeFalla();
    }
	
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);              
        
        ### Establecemos el filtro por default
        $filtro=" 1 = 1 ";
	    if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" ReportarFallas.Cliente.plaza_id=".Usuario::plaza();
        
        ### Cachamos las variables para conformar el filtro	
	    $nombre = $this->_getParam('nombre');
        $folio = $this->_getParam('folio');  
	    $falla = $this->_getParam('falla');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $sucursal = $this->_getParam('sucursal');  
	    $plaza = $this->_getParam('plaza');	

        if($nombre != '')
        {
            $filtro .= " AND (ReportarFallas.Cliente.contrato LIKE '%$nombre%' OR ReportarFallas.Cliente.nombre LIKE '%$nombre%') ";
            //$filtro .= " AND (ReportarFallas.Cliente.nombre LIKE '%$nombre%' OR ReportarFallas.Cliente.telefono LIKE '%$nombre%'  OR ReportarFallas.Cliente.contrato LIKE '%$nombre%' OR ReportarFallas.Cliente.celular1 LIKE '%$nombre%') "; 
        }

        if($folio != '')
        {    
            $filtro .= " AND ReportarFallas.id LIKE '%".$folio."%'";
        }
 
        if($falla >0)
        {    
            $filtro .= " AND ReportarFallas.status_falla = '".$falla."'";
	    }
        else
        {
            $filtro .= " AND ReportarFallas.status_falla != 2 ";   
        }
        

        if($desde != '')
        {    
            $filtro .= " AND ReportarFallas.fecha_falla >=  '$desde'";
            //echo $filtro; exit;
        }    
        if($hasta != '')
        {    
            $filtro .= " AND ReportarFallas.fecha_falla <= '$hasta'";
        }                   
        if($sucursal != 0)
        {
            $filtro .= " AND ReportarFallas.Cliente.sucursal_id = ".$sucursal."  ";
        }   
	    else if($plaza >0)
        {
            $filtro .= " AND ReportarFallas.Cliente.plaza_id =" .$plaza;
        } 
        		
    	### Extraemos los registros para formar el arreglo del grid
    	$registros=My_Comun::registrosGrid("ReportarFallas",$filtro); 
    	//echo $filtro; exit;	
    	$grid=array();
    	$i=0;
    		
    	foreach($registros['registros'] as $registro)
    	{
                $grid[$i]['folio']=$registro->id;
                $grid[$i]['fecha']=$registro->fecha_falla;
                $grid[$i]['contrato']=$registro->Cliente->contrato;
                $grid[$i]['nombre']=$registro->Cliente->nombre;
                $grid[$i]['plaza']=$registro->Cliente->Plaza->nombre;
                $grid[$i]['telefono']=$registro->Cliente->telefono;
                $grid[$i]['celular']=$registro->Cliente->celular1;
                $grid[$i]['estatus']=$registro->Cliente->estatus;
                $grid[$i]['ultimoestatus']=$registro->EstatusDeFalla->nombre;			
                $grid[$i]['cobrar']='<span onclick="fallaReportada('.$registro->id.');" title="Cobrar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png"/></span>';
                $i++;
    	}
		
        My_Comun::grid2($registros,$grid);	
    }

    public function editarfallaAction()
    {
        ### Deshabilitamos el layout
        $this->_helper->layout->disableLayout();
		
	### obtenemos la descripción de la falla 
	$this->view->descripcionFallas=ReportarFallas::obtenerFalla($this->_getParam('id'));
		
	### obtenemos el historial
	$this->view->historiales=HistorialDeFallas::obtenerHistorial($this->_getParam('id'));
		
	### Obtenemos la información del cliente 
	$this->view->cliente=My_Comun::obtener("Cliente",$this->view->descripcionFallas->cliente_id);
		
	$this->view->fallas=TipoDeFalla::obtenerTipoDeFalla();
	$this->view->estatusfallas=EstatusDeFalla::obtenerEstatusDeFalla();
	$filtro=" plaza_id=".Usuario::plaza();
	$this->view->tecnicos=Tecnico::obtenerTecnicos($filtro);
    }
	
    public function  guardarAction()
    {
	### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE); 
		
	echo My_Comun::guardar("ReportarFallas",$_POST,NULL,$_POST['id'],'status_falla');

	$_POST['fecha']= date("Y-m-d");
	$_POST['reportar_falla_id']=$_POST['id'];
		
	echo My_Comun::guardar("HistorialDeFallas",$_POST,NULL,'','fecha');
    }
	
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
                $filtro=" 1 = 1 ";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" ReportarFallas.Cliente.plaza_id=".Usuario::plaza();
        
        ### Cachamos las variables para conformar el filtro	
	$nombre = $this->_getParam('nombre');
        $folio = $this->_getParam('folio');  
	$falla = $this->_getParam('falla');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $sucursal = $this->_getParam('sucursal');  
	$plaza = $this->_getParam('plaza');	

        if($nombre != '')
        {
            $filtro .= " AND (ReportarFallas.Cliente.contrato LIKE '%$nombre%' OR ReportarFallas.Cliente.nombre LIKE '%$nombre%') ";
            //$filtro .= " AND (ReportarFallas.Cliente.nombre LIKE '%$nombre%' OR ReportarFallas.Cliente.telefono LIKE '%$nombre%'  OR ReportarFallas.Cliente.contrato LIKE '%$nombre%' OR ReportarFallas.Cliente.celular1 LIKE '%$nombre%') "; 
        }

        if($folio != '')
        {    
            $filtro .= " AND ReportarFallas.id LIKE '%".$folio."%'";
        }
 
        if($falla >0)
        {    
            $filtro .= " AND ReportarFallas.status_falla = '".$falla."'";
        }
        else
        {
            $filtro .= " AND ReportarFallas.status_falla != 2";   
        }
        
	    /*if($falla == 2)
        {    
            $filtro .= " AND ReportarFallas.fecha_solucion != '0000-00-00'";
	    } */
        if($desde != '')
        {    
            $filtro .= " AND ReportarFallas.fecha_falla >=  '$desde'";
            //echo $filtro; exit;
        }    
        if($hasta != '')
        {    
            $filtro .= " AND ReportarFallas.fecha_falla <= '$hasta'";
        }                   
        if($sucursal != 0)
        {
            $filtro .= " AND ReportarFallas.Cliente.sucursal_id = ".$sucursal."  ";
        }   
	else if($plaza >0)
        {
            $filtro .= " AND ReportarFallas.Cliente.plaza_id =" .$plaza;
        } 
        
        $registros=  My_Comun::obtenerFiltro("ReportarFallas", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE FALLAS REPORTADAS");

        
        $pdf->SetWidths(array(45,45,45,45));
                
        foreach($registros as $registro)
        {
            $domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
			
            $pdf->SetWidths(array(35,45,20,45,35));
            $pdf->SetFillColor(220,220,220);
            $pdf->SetFont('Arial','B',10);
			$pdf->Row(array('CONTRATO','CLIENTE','PLAZA','ESTATUS DE LA FALLA','TELÉFONO'),1,1);
			$pdf->SetFont('Arial','',10);
			$pdf->Row(array($registro->Cliente->contrato,$registro->Cliente->nombre,$registro->Cliente->Plaza->nombre,$registro->EstatusDeFalla->nombre,$registro->Cliente->telefono),1,1);
			
			$pdf->SetWidths(array(45,135));
			$pdf->SetFont('Arial','B',10);
			$pdf->Row(array('DOMICILIO',$domicilio),1,1);
			
			$pdf->SetWidths(array(45,45,45,45));
			$pdf->SetFont('Arial','B',10);
			$pdf->Row(array('CELULAR','ETIQUETA','NODO','POSTE'),0,1);
			$pdf->SetFont('Arial','',10);
			$pdf->Row(array($registro->Cliente->celular1,$registro->Cliente->etiqueta,$registro->Cliente->nodo,$registro->Cliente->poste),0,1);
			
			$pdf->SetFont('Arial','B',10);
			$pdf->Row(array('FECHA DE LA FALLA','FECHA REPORTADA','FECHA DE SOLUCIÓN',''),0,1);
			$pdf->SetFont('Arial','',10);
			if($registro->status_falla != 1)
			     $pdf->Row(array($registro->fecha_falla,$registro->fecha_reporte,$registro->fecha_solucion ),0,1);
            else
                $pdf->Row(array($registro->fecha_falla,$registro->fecha_reporte),0,1);
			
			$pdf->SetWidths(array(90,90));
			$pdf->SetFont('Arial','B',11);
			$pdf->Row(array('NOMBRE DEL TÉCNICO QUE ATENDIÓ:',''),0,1);
                        $pdf->Row(array('DESCRIPCIÓN DE LA FALLA:',''),0,1);
			
			$pdf->SetWidths(array(45,135));
			$pdf->SetFont('Arial','B',11);
			$pdf->Row(array('COMENTARIOS:',''),0,1);	
			
			$pdf->SetFont('Arial','B',11);
			$pdf->Row(array('VISTO BUENO DEL CLIENTE:',''),0,1);			
			$pdf->Ln(6);
        }
                
       $pdf->Output();	
       
    }
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
                $filtro=" 1 = 1 ";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" ReportarFallas.Cliente.plaza_id=".Usuario::plaza();
        
        ### Cachamos las variables para conformar el filtro	
	$nombre = $this->_getParam('nombre');
        $folio = $this->_getParam('folio');  
	$falla = $this->_getParam('falla');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $sucursal = $this->_getParam('sucursal');  
	$plaza = $this->_getParam('plaza');	

        if($nombre != '')
        {
            $filtro .= " AND (ReportarFallas.Cliente.contrato LIKE '%$nombre%' OR ReportarFallas.Cliente.nombre LIKE '%$nombre%') ";
            //$filtro .= " AND (ReportarFallas.Cliente.nombre LIKE '%$nombre%' OR ReportarFallas.Cliente.telefono LIKE '%$nombre%'  OR ReportarFallas.Cliente.contrato LIKE '%$nombre%' OR ReportarFallas.Cliente.celular1 LIKE '%$nombre%') "; 
        }

        if($folio != '')
        {    
            $filtro .= " AND ReportarFallas.id LIKE '%".$folio."%'";
        }
 
        if($falla == 0)
        {    
            $filtro .= " AND ReportarFallas.fecha_solucion = '0000-00-00'";
	}
        
	if($falla == 2)
        {    
            $filtro .= " AND ReportarFallas.fecha_solucion != '0000-00-00'";
	} 
        if($desde != '')
        {    
            $filtro .= " AND ReportarFallas.fecha_falla >=  '$desde'";
        }    
        if($hasta != '')
        {    
            $filtro .= " AND ReportarFallas.fecha_falla <= '$hasta'";
        }                   
        if($sucursal != 0)
        {
            $filtro .= " AND ReportarFallas.Cliente.sucursal_id = ".$sucursal."  ";
        }   
	else if($plaza >0)
        {
            $filtro .= " AND ReportarFallas.Cliente.plaza_id =" .$plaza;
        } 
        
        $registros=  My_Comun::obtenerFiltro("ReportarFallas", $filtro);

        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
	//$grid[$i]['plaza']=$registro->Cliente->Plaza->nombre;	
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'CONTRATO',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'CLIENTE',
                        "width" => 20
                        ),  
		"C$i" => array(
                        "name" => 'PLAZA',
                        "width" => 15
                        ),        
		"D$i" => array(
                        "name" => 'DOMICILIO',
                        "width" => 20
                        ),
		"E$i" => array(
                        "name" => 'ESTATUS DE LA FALLA',
                        "width" => 20
                        ),
		"F$i" => array(
                        "name" => 'TELÉFONO',
                        "width" => 20
                        ),
		"G$i" => array(
                        "name" => 'CELULAR',
                        "width" => 20
                        ),
		"H$i" => array(
                        "name" => 'ETIQUETA',
                        "width" => 20
                        ),
		"I$i" => array(
                        "name" => 'NODO',
                        "width" => 20
                        ),
		"J$i" => array(
                        "name" => 'POSTE',
                        "width" => 20
                        ),
		"K$i" => array(
                        "name" => 'FECHA DE LA FALLA',
                        "width" => 20
                        ),						
        );
        
        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
            
		$domicilio=$registro->Cliente->calle." #".$registro->Cliente->no_exterior.", Col.:".Colonia::obtenerColonia($registro->Cliente->colonia_id);
                $i++;
                $data[] = array(				
		    "A$i" =>$registro->Cliente->contrato,
                    "B$i" =>$registro->Cliente->nombre,
                    "C$i" =>$registro->Cliente->Plaza->nombre,
                    "D$i" =>$domicilio,
                    "E$i" =>$registro->EstatusDeFalla->nombre,
                    "F$i" =>$registro->Cliente->telefono,
                    "G$i" =>$registro->Cliente->celular1,
                    "H$i" =>$registro->Cliente->etiqueta,
                    "I$i" =>$registro->Cliente->nodo,
                    "J$i" =>$registro->Cliente->poste,
                    "K$i" =>$registro->fecha_falla,
                );
        }
		
        $objPHPExcel->createExcel('ReportarFallas', $columns_name, $data, 10);
    }
    
    public function plazasAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::comboSucursal($this->_getParam('id'));
    }
}