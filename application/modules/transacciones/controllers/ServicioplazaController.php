<?php
class Transacciones_ServicioplazaController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/servicioplaza.js'));
    }

    public function indexAction()
    {
		if(isset($_POST['tarifas']))
		{
			$_POST["servicios"] = $_POST["servicio_id"];
			ServicioPlaza::procesarTarifas($_POST['tarifas']);
			ServicioPlazaTarifaespecial::procesarTarifas($_POST['tarifas']);
			//header("Location: /transacciones/servicioplaza/index/estado/" . $_POST['estado_actual']);
		}
			
		//$this->view->EstadosPlazas=Estado::obtenerEstadosPlazas();
		$this->view->tarifasespeciales=Tarifaespecial::obtenerTarifasEspeciales();
		//$this->view->estado_actual = $this->_getParam('estado');
		$this->view->plazas=Plaza::obtenerPlazas();
		$this->view->serviciosPlaza=Servicio::obtenerServicios(' (tipo=1 OR tipo=3) AND status=1');
		
		/*else
		{
			$this->view->EstadosPlazas=Estado::obtenerEstadosPlazas();
			$this->view->tarifasespeciales=Tarifaespecial::obtenerTarifasEspeciales();
			$this->view->estado_actual = $this->_getParam('estado');
			
			$e=array();

				$this->view->plazas=TarifaConceptoContratacion::obtenerPlazas($this->view->estado_actual);
				$e[$this->view->estado_actual] = $this->view->plazas; 

			$this->view->e=$e; 
			
			### Muestra todos los servicios
			$this->view->Servicios=Servicio::obtenerServicios(' (tipo=1 OR tipo=3) AND status=1');  	   
			
			### Vamos a armar un arreglo para poder editar tarifas no especiales
			$serviciosPlazas=ServicioPlaza::obtenerServicioPlaza(); 
			
			$arrServicioPlaza=array();
			foreach($serviciosPlazas as $servicioPlaza)
			{
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['1a5']=$servicioPlaza->precio_1a5;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['6a10']=$servicioPlaza->precio_6a10;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['11']=$servicioPlaza->precio_11;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['reconexion']=$servicioPlaza->reconexion;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['2meses']=$servicioPlaza->meses2;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['3meses']=$servicioPlaza->meses3;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['4meses']=$servicioPlaza->meses4;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['6meses']=$servicioPlaza->meses6;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['8meses']=$servicioPlaza->meses8;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['10meses']=$servicioPlaza->meses10;
				$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['12meses']=$servicioPlaza->meses12;
			}
			$this->view->servicioplaza=$arrServicioPlaza;
			
			
			### Vamos a armar un arreglo para poder editar tarifas  especiales
			$serviciosPlazasTarifaespecial=ServicioPlazaTarifaespecial::obtenerServicioPlazaTarifaespecial("1=1"); 
			
			$arrServicioPlazaTarifaespecial=array();
			foreach($serviciosPlazasTarifaespecial as $servicioPlazaTarifaespecial)
			{
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['1a5']=$servicioPlazaTarifaespecial->precio_1a5;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['6a10']=$servicioPlazaTarifaespecial->precio_6a10;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['11']=$servicioPlazaTarifaespecial->precio_11;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['reconexion']=$servicioPlazaTarifaespecial->reconexion;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['status']=$servicioPlazaTarifaespecial->status;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['2meses']=$servicioPlazaTarifaespecial->meses2;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['3meses']=$servicioPlazaTarifaespecial->meses3;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['4meses']=$servicioPlazaTarifaespecial->meses4;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['6meses']=$servicioPlazaTarifaespecial->meses6;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['8meses']=$servicioPlazaTarifaespecial->meses8;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['10meses']=$servicioPlazaTarifaespecial->meses10;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['12meses']=$servicioPlazaTarifaespecial->meses12;
			}
			$this->view->servicioplazatarifaespecial=$arrServicioPlazaTarifaespecial;

		}*/
    }
 	public function cargarServiciosPlazaAction(){
 		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $plaza = $this->_getParam('plaza');

        $serviciosPlaza=Servicio::obtenerServicios(' (tipo=1 OR tipo=3) AND status=1'); 
        foreach($serviciosPlaza as $servicio){
        	$cadena.='<option value="'.$servicio->id.'">'.$servicio->nombre.'</option>';
        }

        echo $cadena;

 	}
 	public function cargarServiciosAction(){
 		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $pla = $_POST['plaza'];
        $ser = $_POST['servicios'];
        //$estado_actual = $_POST['estado'];
       // $EstadosPlazas=Estado::obtenerEstadosPlazas();
        $e=array();
		$plazas=Plaza::obtenerPlazas($pla);
		$tarifasespeciales = Tarifaespecial::obtenerTarifasEspeciales();
		//$e[$estado_actual] = $plazas; 
        
        $Servicios=Servicio::obtenerServicios(' (tipo=1 OR tipo=3) AND status=1'); 
        $serviciosPlazas=ServicioPlaza::obtenerServicioPlaza(); 
			
		$arrServicioPlaza=array();
		foreach($serviciosPlazas as $servicioPlaza){
			if($pla==$servicioPlaza->plaza_id){
				if(in_array($servicioPlaza->servicio_id, $ser)){
					$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['1a5']=$servicioPlaza->precio_1a5;
					$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['6a10']=$servicioPlaza->precio_6a10;
					$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['11']=$servicioPlaza->precio_11;
					$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['reconexion']=$servicioPlaza->reconexion;
					$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['2meses']=$servicioPlaza->meses2;
					$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['3meses']=$servicioPlaza->meses3;
					$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['4meses']=$servicioPlaza->meses4;
					$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['6meses']=$servicioPlaza->meses6;
					$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['8meses']=$servicioPlaza->meses8;
					$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['10meses']=$servicioPlaza->meses10;
					$arrServicioPlaza[$servicioPlaza->plaza_id][$servicioPlaza->servicio_id]['12meses']=$servicioPlaza->meses12;
				}
			}
		}
		$servicioplaza=$arrServicioPlaza;

		### Vamos a armar un arreglo para poder editar tarifas  especiales
		$serviciosPlazasTarifaespecial=ServicioPlazaTarifaespecial::obtenerServicioPlazaTarifaespecial("1=1"); 	
		$arrServicioPlazaTarifaespecial=array();
		foreach($serviciosPlazasTarifaespecial as $servicioPlazaTarifaespecial){
			if($pla == $servicioPlazaTarifaespecial->plaza_id){
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['1a5']=$servicioPlazaTarifaespecial->precio_1a5;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['6a10']=$servicioPlazaTarifaespecial->precio_6a10;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['11']=$servicioPlazaTarifaespecial->precio_11;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['reconexion']=$servicioPlazaTarifaespecial->reconexion;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['status']=$servicioPlazaTarifaespecial->status;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['2meses']=$servicioPlazaTarifaespecial->meses2;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['3meses']=$servicioPlazaTarifaespecial->meses3;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['4meses']=$servicioPlazaTarifaespecial->meses4;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['6meses']=$servicioPlazaTarifaespecial->meses6;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['8meses']=$servicioPlazaTarifaespecial->meses8;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['10meses']=$servicioPlazaTarifaespecial->meses10;
				$arrServicioPlazaTarifaespecial[$servicioPlazaTarifaespecial->plaza_id][$servicioPlazaTarifaespecial->servicio_id][$servicioPlazaTarifaespecial->tarifaespecial_id]['12meses']=$servicioPlazaTarifaespecial->meses12;
			}
		}
		$servicioplazatarifaespecial=$arrServicioPlazaTarifaespecial;
$cadena='';
                foreach($Servicios as $servicio){
                	if(in_array($servicio->id, $ser)){
			                $cadena.='<div class="nombreServicioTarifas tituloFiltro">'.$servicio->nombre.'</div>
			                    <div class="contenedorPlazasTarifas">';
			                    	foreach($plazas as $plaza){
			                    		if($plaza->id == $pla){
					                    		$cadena.='<div class="contenedorPlazaTarifas identificador'.$plaza->id.'" id="divPlaza_'.$plaza->id.'_'.$servicio->id.'">
					                       	  	<h3>'.$plaza->nombre.'</h3>
					                        	<table class="table table-bordered">
					                                <thead><tr><th colspan="4" align="center">Pagando 1 mes</th>
					                                  <th align="center"><input type="button" class="btn btn-success" onclick="validarMatriz('.$plaza->id.');" value="Guardar" /></th>
					                                </tr>
					                                </thead>
					                                <tbody>
					                                    <tr>
					                                        <td class="etiqueta"><strong>Tarifa</strong></td>
					                                        <td class="etiqueta"><strong>1 al 5</strong></td>
					                                        <td class="etiqueta"><strong>6 al 10</strong></td>
					                                        <td class="etiqueta"><strong>A partir del 11</strong></td>
					                                        <td class="etiqueta"><strong>Reconexión</strong></td>
					                                        <td class="etiqueta"><strong>Activa</strong></td>
					                                    </tr>
					                                    
					                                    <tr id="base_'.$plaza->id.'_'.$servicio->id.'">
					                                        <td class="etiqueta"><strong>Base</strong></td>
					                                        <td align="center"><input type="text" class="formato base" value="'.$servicioplaza[$plaza->id][$servicio->id]['1a5'].'" name="tarifas['.$plaza->id.'][servicio]['.$servicio->id.'][1a5]"></td>
					                                        <td align="center"><input type="text" class="formato base" value="'.$servicioplaza[$plaza->id][$servicio->id]['6a10'].'" name="tarifas['.$plaza->id.'][servicio]['.$servicio->id.'][6a10]"></td>
					                                        <td align="center"><input type="text" class="formato base" value="'.$servicioplaza[$plaza->id][$servicio->id]['11'].'" name="tarifas['.$plaza->id.'][servicio]['.$servicio->id.'][11]"></td>
					                                        <td align="center"><input type="text" class="formato base" value="'.$servicioplaza[$plaza->id][$servicio->id]['reconexion'].'" name="tarifas['.$plaza->id.'][servicio]['.$servicio->id.'][reconexion]"></td>
					                                        <td></td>
					                                    </tr>';
					                                   foreach($tarifasespeciales as $tarifasespecial){ 
					                                    $cadena.='<tr class="especial1mes" id="especial1mes_'.$plaza->id.'_'.$servicio->id.'_'.$tarifasespecial->id.'">
					                                        <td class="etiqueta"><strong>'.$tarifasespecial->nombre.'</strong></td>
					                                        <td align="center"><input type="text" class="formato especial" value="'.$servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['1a5'].'" name="tarifas['.$plaza->id.'][especiales]['.$tarifasespecial->id.'][servicio]['.$servicio->id.'][1a5]"></td>
					                                        <td align="center"><input type="text" class="formato especial" value="'.$servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['6a10'].'" name="tarifas['.$plaza->id.'][especiales]['.$tarifasespecial->id.'][servicio]['.$servicio->id.'][6a10]"></td>
					                                        <td align="center"><input type="text" class="formato especial" value="'.$servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['11'].'" name="tarifas['.$plaza->id.'][especiales]['.$tarifasespecial->id.'][servicio]['.$servicio->id.'][11]"></td>
					                                        <td align="center"><input type="text" class="formato especial" value="'.$servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['reconexion'].'" name="tarifas['.$plaza->id.'][especiales]['.$tarifasespecial->id.'][servicio]['.$servicio->id.'][reconexion]"></td>
					                                        <td align="center"><input type="checkbox" class="formato"'.(($servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['status']==1)?"checked":'').' value="1" name="tarifas['.$plaza->id.'][especiales]['.$tarifasespecial->id.'][servicio]['.$servicio->id.'][status]"></td>
					                                    </tr>';
					                                    }
					                                $cadena.='</tbody>
					                            </table>
					                            <br/>
					                            
					                            <table class="table table-bordered" >
					                              <thead><tr><th colspan="2" align="center">Pagando </th></tr></thead>
					                              <tbody>
					                                <tr>
					                                  <td class="etiqueta"><strong>Tarifa</strong></td>
					                                  <td class="etiqueta"><strong>2 meses</strong></td>
					                                  <td class="etiqueta"><strong>3 meses</strong></td>
					                                  <td class="etiqueta"><strong>4 meses</strong></td>
					                                  <td class="etiqueta"><strong>6 meses</strong></td>
					                                  <td class="etiqueta"><strong>8 meses</strong></td>
					                                  <td class="etiqueta"><strong>10 meses</strong></td>
					                                  <td class="etiqueta"><strong>12 meses</strong></td>
					                                </tr>
					                                <tr id="adicional_'.$plaza->id.'_'.$servicio->id.'">
					                                  <td class="etiqueta"><strong>Base</strong></td>
					                                  <td align="center">
					                                      <div id="precio_1a">
					                                            <input type="text" class="formato base" value="'.$servicioplaza[$plaza->id][$servicio->id]['2meses'].'" name="tarifas['.$plaza->id.'][servicio]['.$servicio->id.'][2meses]" id="precio_1a5_'.$plaza->id.'2" />
					                                      </div>
					                                  </td>
					                                  <td align="center">
					                                      <div id="precio_1a">
					                                            <input type="text" class="formato base" value="'.$servicioplaza[$plaza->id][$servicio->id]['3meses'].'" name="tarifas['.$plaza->id.'][servicio]['.$servicio->id.'][3meses]" id="precio_1a5_'.$plaza->id.'2" />
					                                      </div>
					                                  </td>
					                                  <td align="center">
					                                      <div id="precio_1a">
					                                            <input type="text" class="formato base" value="'.$servicioplaza[$plaza->id][$servicio->id]['4meses'].'" name="tarifas['.$plaza->id.'][servicio]['.$servicio->id.'][4meses]" id="precio_1a5_'.$plaza->id.'2" />
					                                      </div>
					                                  </td>
					                                  <td align="center">
					                                      <div id="precio_1a">
					                                            <input type="text" class="formato base" value="'.$servicioplaza[$plaza->id][$servicio->id]['6meses'].'" name="tarifas['.$plaza->id.'][servicio]['.$servicio->id.'][6meses]" id="precio_1a5_'.$plaza->id.'2" />
					                                      </div>
					                                  </td>
					                                  <td align="center">
					                                      <div id="precio_1a">
					                                            <input type="text" class="formato base" value="'.$servicioplaza[$plaza->id][$servicio->id]['8meses'].'" name="tarifas['.$plaza->id.'][servicio]['.$servicio->id.'][8meses]" id="precio_1a5_'.$plaza->id.'2" />
					                                      </div>
					                                  </td>
					                                  <td align="center">
					                                      <div id="precio_1a">
					                                            <input type="text" class="formato base" value="'.$servicioplaza[$plaza->id][$servicio->id]['10meses'].'" name="tarifas['.$plaza->id.'][servicio]['.$servicio->id.'][10meses]" id="precio_1a5_'.$plaza->id.'2" />
					                                      </div>
					                                  </td>
					                                  <td align="center">
					                                      <div id="precio_1a">
					                                            <input type="text" class="formato base" value="'.$servicioplaza[$plaza->id][$servicio->id]['12meses'].'" name="tarifas['.$plaza->id.'][servicio]['.$servicio->id.'][12meses]" id="precio_1a5_'.$plaza->id.'2" />
					                                      </div>
					                                  </td>
					                                </tr>';
					                                foreach($tarifasespeciales as $tarifasespecial){
					                                $cadena.='<tr class="especialAdcional" id="especialAdcional_'.$plaza->id.'_'.$servicio->id .'_'.$tarifasespecial->id.'">
					                                  <td class="etiqueta"><strong>'.$tarifasespecial->nombre.'</strong></td>
					                                  <td align="center"><input type="text" class="formato especial" value="'.$servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['2meses'].'" name="tarifas['.$plaza->id.'][especiales]['.$tarifasespecial->id.'][servicio]['.$servicio->id.'][2meses]"   /></td>
					                                  <td align="center"><input type="text" class="formato especial" value="'.$servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['3meses'].'" name="tarifas['.$plaza->id.'][especiales]['. $tarifasespecial->id.'][servicio]['. $servicio->id.'][3meses]"   /></td>
					                                  <td align="center"><input type="text" class="formato especial" value="'.$servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['4meses'].'" name="tarifas['.$plaza->id.'][especiales]['. $tarifasespecial->id.'][servicio]['. $servicio->id.'][4meses]"   /></td>
					                                  <td align="center"><input type="text" class="formato especial" value="'.$servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['6meses'].'" name="tarifas['.$plaza->id.'][especiales]['. $tarifasespecial->id.'][servicio]['. $servicio->id.'][6meses]" /></td>
					                                  <td align="center"><input type="text" class="formato especial" value="'.$servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['8meses'].'" name="tarifas['.$plaza->id.'][especiales]['. $tarifasespecial->id.'][servicio]['. $servicio->id.'][8meses]"   /></td>
					                                  <td align="center"><input type="text" class="formato especial" value="'.$servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['10meses'].'" name="tarifas['.$plaza->id.'][especiales]['. $tarifasespecial->id.'][servicio]['. $servicio->id.'][10meses]"   /></td>
					                                  <td align="center"><input type="text" class="formato especial" value="'.$servicioplazatarifaespecial[$plaza->id][$servicio->id][$tarifasespecial->id]['12meses'].'" name="tarifas['.$plaza->id.'][especiales]['. $tarifasespecial->id.'][servicio]['. $servicio->id.'][12meses]" /></td>
					                                </tr>';
					                                }

					                              $cadena.='</tbody>
					                            </table>
					                        </div></div>';
					                    }
			                        }
			                        $cadena.='</div>';
			             }
			        }       






    echo $cadena;

 	}
    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("ServicioPlaza", $this->_getParam('id'));
        }
    }
	
	public function guardarAction()
	{
		### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
		
		echo "<pre>";
		print_r($_POST);
		
	}


}