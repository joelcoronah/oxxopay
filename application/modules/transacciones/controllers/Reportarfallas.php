<?php

class Transacciones_ReportarfallasController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/reportefallas.js'));
    }

    public function indexAction()
    {
        ### Obtenemos los estados para alimentar el filtro
        $this->view->plazas=Plaza::obtenerPlazas();
        ### Obtenemos los estados para alimentar el filtro
        $this->view->estados=Estado::obtenerEstados();	
    }
    
    public function gridAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);    
		
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
	if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
            $filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');
        $comunidad = $this->_getParam('comunidad');
        $plaza = $this->_getParam('plaza');
        
        if($nombre != '')
        {
           $filtro .= " AND (nombre LIKE '%$nombre%' OR telefono LIKE '%$nombre%'  OR calle LIKE '%$nombre%' OR contrato LIKE '%$nombre%') ";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        if($estado >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.Estado.id =" .$estado;
        }
        if($municipio >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.id =" .$municipio;
        }
        if($comunidad >0)
        {
            $filtro .= " AND comunidad_id =" .$comunidad;
        }
		
	### Extraemos los registros para formar el arreglo del grid
	$registros=My_Comun::registrosGrid("Cliente",$filtro); 
		
	$grid=array();
		
	$i=0;
	$editar=false;
	if(My_Permisos::tienePermiso('EDITAR_CLIENTES') == 1)
            $editar=true;
			
        foreach($registros['registros'] as $registro)
	{
            $grid[$i]['contrato']=$registro->contrato;
            $grid[$i]['nombre']=$registro->nombre;
			$grid[$i]['telefono']=$registro->telefono;
			$grid[$i]['celular']=$registro->celular1;
			if(count($registro->Autorizacion)==1 && $registro->Autorizacion[0]->status!=1)
				$grid[$i]['estatus']=($registro->Autorizacion[0]->status==0)?"POR AUTORIZAR":"NO AUTORIZADO";
			else
				$grid[$i]['estatus']=$registro->estatus;
			if($editar)
				$grid[$i]['editar']='<span onclick="agregarCliente(%s);" title="Editar"><img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar.png" /></span>';
			else
				$grid[$i]['editar']='<img style="cursor:pointer; width: 25px; heigth:25px;" src="'.$this->view->baseUrl('').'images/png/editar-off.png" />';
			$i++;
		}
		
        My_Comun::grid2($registros,$grid);	

    }
	
    public function agregarAction()
    {
        ### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
        
        ### Obtenemos las plazas
        $this->view->plazas=Plaza::obtenerPlazas();
		
        ### Obtenemos las comunidades de la plaza
        $this->view->comunidades=Comunidad::obtenerComunidadesDeMunicipio(Zend_Auth::getInstance()->getIdentity()->Plaza->municipio_id);
        
        $this->view->promotores=My_Comun::obtenerPromotores();
		
		$this->view->comisiones=Comisiones::obtenerComisiones();
        
        $this->view->conceptoContratacion=ConceptoDeContratacion::obtenerConceptoParaContratacion();
        $this->view->servicioAContratar=Servicio::obtenerServicios("status=1 AND tipo=1");
            
        ### Cachamos el id del registro; esto se da solo si se está editando
        $this->view->id=($this->_getParam('id')>0)?$this->_getParam('id'):"";
        
        ### Si recibimos el id es porque estamos editando, asi que extraemos los datos
        if(is_numeric($this->_getParam('id')) && $this->_getParam('id')>0)
        {   
            $this->view->registro=My_Comun::obtener("Cliente", $this->_getParam('id'));
        }
    }
	
	
	public function establecerconceptocontratacionAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   $conceptoContratacion=ConceptoDeContratacion::obtenerConceptoContratacion($this->_getParam('id'));
	   
	   echo $conceptoContratacion->ConceptoDeContratacion->nombre."_".$conceptoContratacion->tarifa;		
	}
	
	public function establecerserviciocontratacionAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	    $servicio=Servicio::obtenerServicios("id='".$this->_getParam('id')."'");
		$comision=Servicio::obtenerComision($this->_getParam('id'));
		
		### Primero verificamos si es un servicio o un servicio adicional
		
		### Si es un servicio
		if($servicio[0]->tipo!=2)
		{
			$tarifa=Servicio::obtenerTarifa($this->_getParam('id'));
			###Buscamos las tarifas especiales asociadas a este servicio
			$tarifas=Servicio::obtenerTarifasEspecialesServicio($tarifa->servicio_id);
			
			### Si encontramos tarifas especiales asociadas al servicio en la plaza actual, entonces armamos un select de tarifas
			if(count($tarifas)>0)
			{
				$opt.='<option value="0">Tarifa base</option>';
				foreach($tarifas as $t)
				{
					$opt.='<option value="'.$t->id.'">'.$t->Tarifaespecial->nombre.'</option>';
				}
				
				$optPeriodo='<option value="0">Indefinido</option>';
				for($i=1;$i<=12;$i++)
				{
					$optPeriodo.='<option value="'.$i.'">'.$i.' periodo(s)</option>';
				}

				
				echo '<div class="cuerpo" id="divCuerpo'.$tarifa->id.'"><div>1</div><div>'.$servicio[0]->nombre.'</div> <div class="divComision">$'.sprintf("%0.2f",$comision).'</div><div><select name="tipotarifa[]" id="select_'.$tarifa->id.'" onchange="estableceTipoTarifa(this.id,'.$tarifa->precio_1a5.');" class="span10">'.$opt.'</select></div><div id="precio'.$tarifa->id.'">$'.sprintf("%0.2f",$tarifa->precio_1a5).'</div><div class="eliminar"><img src="/images/png/cancelar.png" width="20px" onclick="eliminarServicio('.$tarifa->id.');" /></div><input type="hidden" name="tarifas[]" value="'.$tarifa->id.'" class="hdnTarifa" /><input type="hidden" name="servicios[]" value="'.$tarifa->servicio_id.'" class="hdnServicio" /><input type="hidden" name="autorizacion[]" value="0" class="hdnServicio" id="hdnautorizacion'.$tarifa->id.'" /></div>';
			}
			else
			{
				echo '<div class="cuerpo" id="divCuerpo'.$tarifa->id.'"><div>1</div><div>'.$servicio[0]->nombre.'</div><div class="divComision">$'.sprintf("%0.2f",$comision).'</div><div>Base</div><div>$'.sprintf("%0.2f",$tarifa->precio_1a5).'</div><div class="eliminar"><img src="/images/png/cancelar.png" width="20px" onclick="eliminarServicio('.$tarifa->id.');" /></div><input type="hidden" name="tarifas[]" value="'.$tarifa->id.'" class="hdnTarifa" /><input type="hidden" name="servicios[]" value="'.$tarifa->servicio_id.'" class="hdnServicio" /><input type="hidden" name="autorizacion[]" value="0" class="hdnServicio" id="hdnautorizacion'.$tarifa->id.'" /></div>';
			}
			
		}
		else
		{
			$tarifa=Servicio::obtenerTarifaAdicional($this->_getParam('id'));
			echo '<div class="cuerpo" id="divCuerpo'.$tarifa->id.'"><div>1</div><div>Servicio adicional '.$servicio[0]->nombre.'</div> <div class="divComision">$'.sprintf("%0.2f",$comision).'</div><div>Base</div><div>$'.sprintf("%0.2f",$tarifa->tarifa).'</div><div class="eliminar"><img src="/images/png/cancelar.png" width="20px" onclick="eliminarServicio('.$tarifa->id.');" /></div><input type="hidden" name="tarifas[]" value="'.$tarifa->id.'"  /><input type="hidden" name="serviciosAdicionales[]" value="'.$tarifa->servicioadicional_id.'" class="hdnServicio" /></div>';
		}
		
	}
	
	public function obtenermontotarifaespecialAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   $q=Doctrine_Query::create()->from('ServicioPlazaTarifaespecial')->where('id=?',$this->_getParam('id'))->execute()->getFirst();
	   
	   echo "$".sprintf("%.2f",$q->precio_1a5)."_".$q->Tarifaespecial->autorizacion;
		
	}
	
	public function agregarservicioAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   
	   $this->view->servicioAContratar=Servicio::obtenerServiciosAContratar();
		
	}
	
	public function tiposervicioAction()
	{
		### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
	   $this->_helper->viewRenderer->setNoRender(TRUE);
	   
	   ### Obtenemos servicios
	   if($this->_getParam('id')==1)
	   {
		   	$servicios=Servicio::obtenerServiciosAContratar();
			foreach($servicios as $servicio)
			{
				echo '<option value="'.$servicio->servicio_id.'">'.$servicio->Servicio->nombre.'</option>';
			}
	   }
	   else
	   {
		   ### Obtenemos servicios adicionales
		   $servicios=Servicio::obtenerServicios(" tipo=2 AND status=1 AND Servicio.id=Servicio.TarifaServicioadicional.servicioadicional_id AND Servicio.TarifaServicioadicional.plaza_id=".Usuario::plaza()."");
			foreach($servicios as $servicio)
			{
				echo '<option value="'.$servicio->id.'">'.$servicio->nombre.'</option>';
			}
	   }
	   
	   $this->view->servicioAContratar=Servicio::obtenerServiciosAContratar();
		
	}

    
    public function guardarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);

       
       echo Cliente::guardar("Cliente",$_POST,NULL,$_POST['id']);
    }
    
    public function eliminarAction()
    {
       ### Deshabilitamos el layout y la vista
       $this->_helper->layout->disableLayout();
       $this->_helper->viewRenderer->setNoRender(TRUE);
       
       My_Comun::deshabilitar("Cliente", $_POST['id']);
    }
    
    
    public function imprimirAction()
    {
       ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
		$filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $nombre = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');
        $comunidad = $this->_getParam('comunidad');
        $plaza = $this->_getParam('plaza');

        
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        if($estado >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.Estado.id =" .$estado;
        }
        if($municipio >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.id =" .$municipio;
        }
        if($comunidad >0)
        {
            $filtro .= " AND comunidad_id =" .$comunidad;
        }


        
        $registros=  My_Comun::obtenerFiltro("Cliente", $filtro);
       
        $pdf= new My_Fpdf_Pdf();
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        
        $pdf->Header("IMPRESIÓN DE CLIENTES");

        $pdf->SetFont('Arial','B',11);
        $pdf->SetWidths(array(30,30,35,35));
        $pdf->Row(array('CLIENTE','TELEFONO','CELULAR','ESTATUS'),0,1);
        
        $pdf->SetFont('Arial','',10);
        foreach($registros as $registro)
        {
            
           $pdf->Row
           (
                array
                (
                    $registro->nombre,
                    $registro->telefono,
                    $registro->celular1,
                    $registro->estatus
                ),0,1			
           );
        }
        
        
       $pdf->Output();	
       
    }
	
	
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
        ### Establecemos el filtro por default
        $filtro = "1 = 1 ";
		if(Usuario::tipo()==1 && !is_null(Usuario::plaza()))
		$filtro=" plaza_id=".Usuario::plaza()." ";

        ### Cachamos las variables para conformar el filtro
        $cliente = $this->_getParam('nombre');
        $estado = $this->_getParam('estado');
        $municipio = $this->_getParam('municipio');
        $comunidad = $this->_getParam('comunidad');
        $plaza = $this->_getParam('plaza');

        
        if($nombre != '')
        {
            $filtro .= " AND nombre LIKE '%$nombre%'";
        }
        if($plaza >0)
        {
            $filtro .= " AND plaza_id =" .$plaza;
        }
        if($estado >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.Estado.id =" .$estado;
        }
        if($municipio >0)
        {
            $filtro .= " AND Cliente.Comunidad.Municipio.id =" .$municipio;
        }
        if($comunidad >0)
        {
            $filtro .= " AND comunidad_id =" .$comunidad;
        }

        $registros=  My_Comun::obtenerFiltro("Cliente", $filtro);


        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
		
		
        $i=5;
        //Titulos columna
        $columns_name = array
        (
                "A$i" => array(
                        "name" => 'NOMBRE',
                        "width" => 20
                        ),
                "B$i" => array(
                        "name" => 'TELEFONO',
                        "width" => 20
                        ),
				"C$i" => array(
                        "name" => 'CELUAR',
                        "width" => 20
                        ),
				"D$i" => array(
                        "name" => 'ESTATUS',
                        "width" => 20
                        )					
        );
        
        


        //Datos tabla
        $data = array();
        foreach($registros as $registro)
        {
                $i++;
                $data[] = array(
				
		    "A$i" =>$registro->nombre,
                    "B$i" =>$registro->telefono,
                    "C$i" =>$registro->celular1,
                    "D$i" =>$registro->estatus

                );
        }
		
        $objPHPExcel->createExcel('Clientes', $columns_name, $data, 10, array('rango'=>'A4:D4','size'=>14,'texto'=>'CLIENTES'));
		
    }

}