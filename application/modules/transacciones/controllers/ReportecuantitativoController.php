<?php
class Transacciones_ReportecuantitativoController extends Zend_Controller_Action
{

    public function init()
    {
    	$this->view->headScript()->appendFile($this->view->baseUrl('js/transacciones/reportecuantitativo.js'));
    }

    public function indexAction()
    {
        $this->view->plazas=Plaza::obtenerPlazas();
        $this->view->sucursales=Sucursal::obtenerSucursal();
             
        $dia[1]= "L";
        $dia[2]= "M";
        $dia[3]= "M";
        $dia[4]= "J";
        $dia[5]= "V";
        $dia[6]= "S";
        $dia[7]= "D";      
        $this->view->dia=$dia;
       
        $mes[1]= "Enero";
        $mes[2]= "Febrero";
        $mes[3]= "Marzo";
        $mes[4]= "Abril";
        $mes[5]= "Mayo";
        $mes[6]= "Junio";
        $mes[7]= "Julio";
        $mes[8]= "Agosto";
        $mes[9]= "Septiembre";
        $mes[10]= "Octubre";
        $mes[11]= "Noviembre";
        $mes[12]= "Diciembre";
        $this->view->mes =$mes;
	
        $qp=Doctrine_Query::create()->from('Parametrosdeproductividad');
        $productividades=$qp->execute();                       
        foreach($productividades as $productividad)
        {
            $puntos[1]= $productividad->instalaciones;
            $puntos[2]= $productividad->reconexiones;
            $puntos[3]= $productividad->cambio_domicilio;
            $puntos[4]= $productividad->atencion_a_fallas;
            $puntos[5]= $productividad->extensiones_adicionales;
            $puntos[6]= $productividad->recuperaciones;
            $puntos[7]= $productividad->auditorias_de_acometidas;
            $puntos[8]= $productividad->reinstalaciones;  
            $puntos[9]= $productividad->retiros; 
        }
        
        $this->view->puntos = $puntos;
        
        ### Cachamos las variables
        $desde = date("Y-m-d",mktime(0,0,0,date("m"),1,date("Y")));
          
        $diaF = date("d",mktime(0,0,0,date("m")+1,0,date("Y")));  
        $hasta= date("Y-m-d",mktime(0,0,0,date("m"),$diaF,date("Y")));
          
        if($_POST['fdesde'] != ''){
            $mesI= date("m", strtotime($_POST['fdesde']));
            $anoI= date("Y", strtotime($_POST['fdesde']));
            $desde = $anoI."-".$mesI."-01";
        }
        
        if($_POST['fhasta'] != ''){
            $mesF= date("m", strtotime($_POST['fhasta']));
            $anoF= date("Y", strtotime($_POST['fhasta']));
            $diaF = date("d",mktime(0,0,0,$mesF+1,0,$anoF));        
            $hasta = $anoF."-".$mesF."-".$diaF;
        }

        //echo $desde; echo "<br>"; echo $hasta; exit;
        
        
        $filtro =" 1=1 ";
        $clientes_pagados =" 1=1 ";
        $recuperaciones =" 1=1 ";
        $ventas_nuevas =" 1=1 ";
        $instalaciones =" 1=1 ";
        $desconexiones =" 1=1 ";
        $reconexiones =" 1=1 ";
        $cambio_domicilio =" 1=1 ";
        $extencion_adic =" 1=1 ";
        $retiros=" 1=1 ";
         
        if( $_POST['fplaza'] !=0 && $_POST['fsucursal'] == 0){
            $filtro.= " AND plaza_id =". $_POST['fplaza'];
            $clientes_pagados.= " AND Cobranza.plaza_id =". $_POST['fplaza'];
            $recuperaciones.= " AND CobranzaDetalle.Cobranza.Cliente.Plaza.id =". $_POST['fplaza'];
            $ventas_nuevas.= " AND plaza_id =". $_POST['fplaza'];
            $instalaciones.= " AND Ordeninstalacion.Cliente.plaza_id =". $_POST['fplaza'];
            $desconexiones.= " AND Ordendesconexion.Cliente.Plaza.id =". $_POST['fplaza'];
            $reconexiones.= " AND Ordendesreconexion.Cliente.Plaza.id =". $_POST['fplaza'];
            $cambio_domicilio.= " AND Ordencambiodomicilio.Cliente.Plaza.id =". $_POST['fplaza'];
            $extencion_adic.= " AND Ordenextensionadicional.Cliente.Plaza.id =". $_POST['fplaza']; 
            $retiros.=" AND Ordendesretiro.Cliente.Plaza.id =". $_POST['fplaza'];  
        }       
        
        if($_POST['fsucursal'] != 0)
        {
            $filtro.= " AND sucursal_id =". $_POST['fsucursal'];
            $clientes_pagados.= " AND Cobranza.Cliente.sucursal_id =". $_POST['fsucursal'];
            $recuperaciones.= " AND CobranzaDetalle.Cobranza.Cliente.sucursal_id =". $_POST['fsucursal'];
            $ventas_nuevas.= " AND Cliente.sucursal_id =". $_POST['fsucursal'];
            $instalaciones.= " AND Cliente.sucursal_id =". $_POST['fsucursal'];
            $desconexiones.= " AND Ordendesconexion.Cliente.sucursal_id =". $_POST['fsucursal'];
            $reconexiones.= " AND Ordendesreconexion.Cliente.sucursal_id =". $_POST['fsucursal'];
            $cambio_domicilio.= " AND Ordencambiodomicilio.Cliente.sucursal_id =". $_POST['fsucursal'];
            $extencion_adic.= " AND Ordenextensionadicional.Cliente.sucursal_id =". $_POST['fsucursal'];
            $retiros.=" AND  Ordendesretiro.Cliente.sucursal_id =". $_POST['fsucursal'];  
        }
        
        //$this->view->desde =$desde;
        $this->view->hasta =$hasta;
        $this->view->meses = My_Comun::numero_de_meses1( $hasta, $desde );
        
        $data=array();
        $data=0;
        
        $clientes_pagados=$clientes_pagados." AND Cobranza.pagada=1 AND Cobranza.cancelado=0  AND Cobranza.fecha_inicio != 'null' AND Cobranza.fecha_fin != 'null' AND Cobranza.CobranzaDetalle.otro_concepto='' AND Cobranza.CobranzaDetalle.concepto_contratacion_id IS NULL  AND  CAST(Cobranza.created_at AS DATE) ";
        $this->view->clientes_pagados= Cliente::reporteCuantitativo($desde, $hasta,'Cobranza',$clientes_pagados); 

        //print_r($this->view->clientes_pagados); exit;

             
        $this->view->mens_cobradas = Cliente::mensualidaCobrada($desde, $hasta, $clientes_pagados);        
        
        $recuperaciones.= " AND otro_concepto = 'Recuperación' AND CAST(CobranzaDetalle.Cobranza.created_at AS DATE) ";
        $this->view->recuperaciones = Cliente::reporteCuantitativo($desde, $hasta,'CobranzaDetalle', $recuperaciones);

        
        $ventas_nuevas.=" AND CAST(Cliente.created_at AS DATE) ";
        $this->view->ventas_nuevas = Cliente::reporteCuantitativo($desde, $hasta, 'Cliente',$ventas_nuevas);
         
        $instalaciones.=" AND CAST(fecha_instalacion AS DATE) ";
        $this->view->instalaciones = Cliente::reporteCuantitativo($desde, $hasta, 'Ordeninstalacion',$instalaciones);

        
        $desconexiones.=" AND CAST(Ordendesconexion.fecha_desconexion AS DATE) ";
        $this->view->desconexiones = Cliente::reporteCuantitativo($desde, $hasta,'Ordendesconexion' ,$desconexiones);
        
        $reconexiones.=" AND CAST(fecha_reconexion AS DATE) ";
        $this->view->reconexiones = Cliente::reporteCuantitativo($desde, $hasta,'Ordendesreconexion', $reconexiones);

        
        $this->view->auditoria_acom = $data;
                
        $cambio_domicilio.=" AND CAST(fecha_instalacion AS DATE) ";
        $this->view->cambio_domicilio = Cliente::reporteCuantitativo($desde, $hasta,'Ordencambiodomicilio', $cambio_domicilio);

        
        $extencion_adic.=" AND CAST(fecha_instalacion AS DATE)  ";
        $this->view->extencion_adic = Cliente::reporteCuantitativo($desde, $hasta,'Ordenextensionadicional',$extencion_adic); 


        $retiros.=" AND Ordendesretiro.fecha_retiro"; 
        $this->view->retiros = Cliente::reporteCuantitativo($desde, $hasta,'Ordendesretiro' ,$retiros);

        
        $this->view->notifi_cobranza = $data; 

        
        /**************Tablita****************/
        $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =1 AND ".$filtro);   
        $this->view->activos=$p->execute()->getFirst();                       

		
		$p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status = 2 AND ".$filtro);   
        $this->view->desconectar=$p->execute()->getFirst();  
          
        $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status = 8 AND ".$filtro);   
        $this->view->desconectados=$p->execute()->getFirst();                     


        $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =3 AND ".$filtro);   
        $this->view->cortar=$p->execute()->getFirst();                                
            
        $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =5 AND ".$filtro);   
        $this->view->bajas=$p->execute()->getFirst();                       
             
        $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =4 AND ".$filtro);   
        $this->view->retirar=$p->execute()->getFirst();                       
                       
        $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =9 AND ".$filtro);   
        $this->view->retirado=$p->execute()->getFirst();

		
		$p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =6 AND ".$filtro);   
        $this->view->instalar=$p->execute()->getFirst(); 

		
		$p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =10 AND ".$filtro);   
        $this->view->reinstalar=$p->execute()->getFirst();  
		
		$p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =11 AND ".$filtro);   
        $this->view->reconectar=$p->execute()->getFirst();                       
    }		 
	
    function exportarAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        
 	    $this->view->sucursales=Sucursal::obtenerSucursal();
             
        $dia[1]= "L";
        $dia[2]= "M";
        $dia[3]= "M";
        $dia[4]= "J";
        $dia[5]= "V";
        $dia[6]= "S";
        $dia[7]= "D";      
       
        $nombre_mes[1]= "Enero";
        $nombre_mes[2]= "Febrero";
        $nombre_mes[3]= "Marzo";
        $nombre_mes[4]= "Abril";
        $nombre_mes[5]= "Mayo";
        $nombre_mes[6]= "Junio";
        $nombre_mes[7]= "Julio";
        $nombre_mes[8]= "Agosto";
        $nombre_mes[9]= "Septiembre";
        $nombre_mes[10]= "Octubre";
        $nombre_mes[11]= "Noviembre";
        $nombre_mes[12]= "Diciembre";
	
        $qp=Doctrine_Query::create()->from('Parametrosdeproductividad');
        $productividades=$qp->execute();                       
        foreach($productividades as $productividad)
        {
            $puntos[1]= $productividad->instalaciones;
            $puntos[2]= $productividad->reconexiones;
            $puntos[3]= $productividad->cambio_domicilio;
            $puntos[4]= $productividad->atencion_a_fallas;
            $puntos[5]= $productividad->extensiones_adicionales;
            $puntos[6]= $productividad->recuperaciones;
            $puntos[7]= $productividad->auditorias_de_acometidas;
            $puntos[8]= $productividad->reinstalaciones;  
            $puntos[9]= $productividad->retiros;  
        }
        
        ### Cachamos las variables  
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $plaza = $this->_getParam('plaza');
        $sucursal = $this->_getParam('sucursal');
        
        if( $desde != ''){
            $mesI = date("m", strtotime($desde));
            $anoI = date("Y", strtotime($desde));
            $desde = $anoI."-".$mesI."-01";
        }
        
        else{
            $desde = date("Y-m-d",mktime(0,0,0,date("m"),1,date("Y")));
        }
       
        if($hasta != ''){
            $mesF= date("m", strtotime($this->_getParam('hasta')));
            $anoF= date("Y", strtotime($this->_getParam('hasta')));
            $diaF = date("d",mktime(0,0,0,$mesF+1,0,$anoF));        
            $hasta = $anoF."-".$mesF."-".$diaF;
        } 
        
        else{
            $diaF = date("d",mktime(0,0,0,date("m")+1,0,date("Y")));  
            $hasta= date("Y-m-d",mktime(0,0,0,date("m"),$diaF,date("Y")));
        }
        
        $filtro =" 1 = 1 ";
        $clientes_pagados =" 1=1 ";
        $recuperaciones =" 1=1 ";
        $ventas_nuevas =" 1=1 ";
        $instalaciones =" 1=1 ";
        $desconexiones =" 1=1 ";
        $reconexiones =" 1=1 ";
        $cambio_domicilio =" 1=1 ";
        $extencion_adic =" 1=1 ";
        $ordenes_retiros=" 1=1 ";
         
        if( $plaza !=0){
            $filtro.= " AND plaza_id =". $plaza;
            $clientes_pagados.= " AND Cobranza.plaza_id =". $plaza;
            $recuperaciones.= " AND CobranzaDetalle.Cobranza.Cliente.Plaza.id =". $plaza;
            $ventas_nuevas.= " AND plaza_id =". $plaza;
            $instalaciones.= " AND Ordeninstalacion.Cliente.plaza_id =". $plaza;
            $desconexiones.= " AND Ordendesconexion.Cliente.Plaza.id =". $plaza;
            $reconexiones.= " AND Ordendesreconexion.Cliente.Plaza.id =". $plaza;
            $cambio_domicilio.= " AND Ordencambiodomicilio.Cliente.Plaza.id =". $plaza;
            $extencion_adic.= " AND Ordenextensionadicional.Cliente.Plaza.id =". $plaza;   
            $ordenes_retiros.= " AND Ordendesretiro.Cliente.Plaza.id =". $plaza;   
        }       
        
        if($sucursal != 0){
            $filtro.= " AND sucursal_id =". $sucursal;
            $clientes_pagados.= " AND Cobranza.Plaza.sucursal_id =". $sucursal;
            $recuperaciones.= " AND CobranzaDetalle.Cobranza.Cliente.Plaza.sucursal_id =". $sucursal;
            $ventas_nuevas.= " AND Cliente.Plaza.sucursal_id =". $sucursal;
            $instalaciones.= " AND Cliente.Plaza.sucursal_id =". $sucursal;
            $desconexiones.= " AND Ordendesconexion.Cliente.Plaza.sucursal_id =". $sucursal;
            $reconexiones.= " AND Ordendesreconexion.Cliente.Plaza.sucursal_id =". $sucursal;
            $cambio_domicilio.= " AND Ordencambiodomicilio.Cliente.Plaza.sucursal_id =". $sucursal;
            $extencion_adic.= " AND Ordenextensionadicional.Cliente.Plaza.sucursal_id =". $sucursal;
            $ordenes_retiros.= " AND Ordendesretiro.Cliente.Plaza.sucrusal_id =". $sucursal; 
        }
                
        $meses = My_Comun::numero_de_meses1( $hasta, $desde );  
        $data=array();
        $data=0;
        
        $clientes_pagados=$clientes_pagados." AND Cobranza.fecha_inicio != 'null' AND Cobranza.fecha_fin != 'null' AND Cobranza.CobranzaDetalle.otro_concepto='' AND Cobranza.CobranzaDetalle.concepto_contratacion_id IS NULL AND CAST(Cobranza.created_at AS DATE) ";
        $cliente_pagado = Cliente::reporteCuantitativo($desde, $hasta,'Cobranza',$clientes_pagados); 
        
        $men_cobrada = Cliente::mensualidaCobrada($desde, $hasta, $clientes_pagados);        
        
        $recuperaciones.= " AND otro_concepto = 'Recuperación' AND CAST(CobranzaDetalle.Cobranza.created_at AS DATE) ";
        $recuperacion = Cliente::reporteCuantitativo($desde, $hasta,'CobranzaDetalle', $recuperaciones);
        
        $ventas_nuevas.=" AND CAST(Cliente.created_at AS DATE) ";
        $venta_nueva= Cliente::reporteCuantitativo($desde, $hasta, 'Cliente',$ventas_nuevas);
         
        $instalaciones.=" AND CAST(fecha_instalacion AS DATE) ";
        $instalacion = Cliente::reporteCuantitativo($desde, $hasta, 'Ordeninstalacion',$instalaciones);
        
        $desconexiones.=" AND CAST(Ordendesconexion.fecha_desconexion AS DATE) ";
        $desconexion = Cliente::reporteCuantitativo($desde, $hasta,'Ordendesconexion' ,$desconexiones);
        
        $reconexiones.=" AND CAST(fecha_reconexion AS DATE) ";
        $reconexion = Cliente::reporteCuantitativo($desde, $hasta,'Ordendesreconexion', $reconexiones);
        
        $auditoria_acom = $data;
                
        $cambio_domicilio.=" AND CAST(fecha_instalacion AS DATE) ";
        $domicilio = Cliente::reporteCuantitativo($desde, $hasta,'Ordencambiodomicilio', $cambio_domicilio);

        $ordenes_retiros.=" AND CAST(Ordendesretiro.fecha_retiro AS DATE) ";
        $retiros = Cliente::reporteCuantitativo($desde, $hasta,'Ordendesretiro' ,$ordenes_retiros);
        
        $extencion_adic.=" AND CAST(fecha_instalacion AS DATE)  ";
        $extencion_adi = Cliente::reporteCuantitativo($desde, $hasta,'Ordenextensionadicional',$extencion_adic); 
        
        
       $qp=Doctrine_Query::create()->from('Metas')->where($filtro);
        $metas=$qp->execute();                       
        foreach($metas as $meta)
        {
            $MetaRecupe += $meta->recuperaciones;
            $MetaVentas += $meta->ventas;
        }
        
        $notifi_cobranza = $data; 
        
        ini_set("memory_limit", "130M");
        ini_set('max_execution_time', 0);

        $objPHPExcel = new My_PHPExcel_Excel();
        $objPHPExcel = new PHPExcel();
         
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
        $objPHPExcel->getActiveSheet()->getStyle('A2:AA2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2:AA2')->getFont()->setSize(16);
        $objPHPExcel->getActiveSheet()->getStyle('A2:AA2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    
        $objPHPExcel->getActiveSheet()->mergeCells('A2:AA2');    
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2','REPORTE CUANTITATIVO DE OPERACIÓN');
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);  //0.33

        $letras="B";
        /********TAMAÑO DE LAS CELDAS*******************/
        while($letras != "AP" )
        {  
            $objPHPExcel->getActiveSheet()->getColumnDimension($letras)->setWidth(4);  //5.14
            $letras++;            
        }
              
        $numeros=5;
        $letras="b";
        $mes=0;   
        while($meses > $mes )
        {  
            /*********Nombre del mes ************/
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$numeros.':AA'.$numeros);    
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':AA'.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':AA'.$numeros)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':AA'.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$numeros, $nombre_mes[date("n", strtotime($hasta))]);
            
            $numeros++;
            $dias = date("d", mktime( 0, 0, 0,    date("m", strtotime($hasta))+1  , 0 ,  date("Y", strtotime($hasta))  ));            
            $i=1;               
            $rangoCategorias="Worksheet!$"."B$".$numeros;                
            while($dias >= $i ){
                if(date("N",mktime(0,0,0,  date("m", strtotime($hasta)) ,$i,  date("Y", strtotime($hasta))  )) != 8){       
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $i );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(10);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);                    
                     $letras++;  
                }
                $i++;
            }
            $rangoCategorias.=":$".strtoupper($letras)."$".$numeros; 
            $objPHPExcel->getActiveSheet()->mergeCells($letras.$numeros.':'.$letras.($numeros+1));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, 'TOTAL' );
            $objPHPExcel->getActiveSheet()->getColumnDimension($letras)->setWidth(6);  //5.14  
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $letras++;
            $objPHPExcel->getActiveSheet()->mergeCells($letras.$numeros.':'.$letras.($numeros+1));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, 'METAS' );
            $objPHPExcel->getActiveSheet()->getColumnDimension($letras)->setWidth(6);  //5.14  
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $letras++;
            $objPHPExcel->getActiveSheet()->mergeCells($letras.$numeros.':'.$letras.($numeros+1));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, 'DIFERENCIA' );
            $objPHPExcel->getActiveSheet()->getColumnDimension($letras)->setWidth(8);  //5.14  
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $letras++;
            $objPHPExcel->getActiveSheet()->mergeCells($letras.$numeros.':'.$letras.($numeros+1));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, 'PORCENTAJE' );
            $objPHPExcel->getActiveSheet()->getColumnDimension($letras)->setWidth(9);  //5.14  
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);                        
                        
            /*****************TABLA OBJ*****************/
            
            $letras++; $letras++;
            $letrasA=$letras;
            $letras++;
            
            $objPHPExcel->getActiveSheet()->mergeCells($letrasA.$numeros.':'.$letras.$numeros);  
            $objPHPExcel->getActiveSheet()->setCellValue($letrasA.$numeros, 'OBJ');
            $objPHPExcel->getActiveSheet()->getStyle($letrasA.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letrasA.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letrasA.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '1-5');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '6-10');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '11-15');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '16-20');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '21-25');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '26-'.$dias);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            
            $objPHPExcel->getActiveSheet()->getStyle($letrasA.$numeros.':'.$letras.$numeros)->applyFromArray(
                array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'cccccc')
                    )
                )
            );
            
            /*****************FIN TABLA OBJ*****************/
              
            $numeros++;
            $letras="b";
            $i=1;                                                                  
            while($dias >= $i ){
                if(date("N",mktime(0,0,0,  date("m", strtotime($hasta)) ,$i,  date("Y", strtotime($hasta))  )) != 8){   
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $dia[date("N",mktime(0,0,0, date("m", strtotime($hasta))  ,$i,  date("Y", strtotime($hasta))   ))] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                }
                $i++;
            }
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "CLIENTES PAGADOS" );
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);
            
            $rangoValues="Worksheet!$"."B$".$numeros;
            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $cliente_pagado[date("n",strtotime($hasta))][$i]);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }           
            $rangoValues.=":$".strtoupper($letrasA)."$".$numeros;           
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true); 
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "MENS. COBRADA" );               
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);         
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $men_cobrada[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++; 
                    $letrasA++;
                }
                $i++;
            }            
            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);      
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "RECUPERACIONES" );
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);
            $r1_5=0; 
            $r6_10=0;
            $r11_15=0;
            $r16_20=0;
            $r21_25=0;
            $r26_31=0;
            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $recuperacion[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;    
                    $letrasA++;
                  
                    if( $i <= 5 && $i>0 ){                 
                        $r1_5+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 10  && $i > 5 ){                    
                        $r6_10+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 15  && $i >10 ){                    
                        $r11_15+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 20  && $i >15 ){                    
                        $r16_20+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    } 
                    
                    if( $i <= 25  && $i >20 ){                    
                        $r21_25+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 31  && $i >25 ){                    
                        $r26_31+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                }
                $i++;
            }
            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            
            $letras++;                        
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $MetaRecupe);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);  
            
            $letras++;      $letrasA++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.($letrasA).$numeros.'-'.$MetaRecupe);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);                          
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.($letrasA.$numeros.'/'.$MetaRecupe).'*100');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);   
            
            $letras++; $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $MetaRecupe);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);   
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.$letrasA.$numeros.'/'.$MetaRecupe);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);   
             
            $letras++;            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r1_5);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
           
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r6_10);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);            
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r11_15);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r16_20);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
                        
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r21_25);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
                               
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $r26_31);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
                        
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $v1_5=0; 
            $v6_10=0;
            $v11_15=0;
            $v16_20=0;
            $v21_25=0;
            $v26_31=0;
                        
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "VENTAS NUEVAS" );
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $venta_nueva[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;     
                    $letrasA++;
                    
                    if( $i <= 5 && $i>0 ){                 
                        $v1_5+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 10  && $i > 5 ){                    
                        $v6_10+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 15  && $i >10 ){                    
                        $v11_15+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 20  && $i >15 ){                    
                        $v16_20+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    } 
                    
                    if( $i <= 25  && $i >20 ){                    
                        $v21_25+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 31  && $i >25 ){                    
                        $v26_31+=$venta_nueva[date("n",strtotime($hasta))][$i]; 
                    }
                }
                
                $i++;
            }           
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            
            $letras++;            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $MetaVentas);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
            
            $letras++;$letrasA++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.($letrasA).$numeros.'-'.$MetaVentas);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true); 
            
            $letras++; 
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.($letrasA.$numeros.'/'.$MetaVentas).'*100');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true); 
            
            $letras++; $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $MetaVentas);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);   
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '='.$letrasA.$numeros.'/'.$MetaVentas);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);                          
            
            $letras++;            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v1_5);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
           
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v6_10);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);            
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v11_15);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
            
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v16_20);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
                        
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v21_25);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
                               
            $letras++;
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, $v26_31);            
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);  
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "INSTALACIONES");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $instalacion[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;                
                    $letrasA++;                 
                    
                    if( $i <= 5 && $i>0 ){                 
                        $r1_5+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 10  && $i > 5 ){                    
                        $r6_10+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                    
                    if( $i <= 15  && $i >10 ){                    
                        $r11_15+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 20  && $i >15 ){                    
                        $r16_20+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    } 
                    
                    if( $i <= 25  && $i >20 ){                    
                        $r21_25+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                                       
                    if( $i <= 31  && $i >25 ){                    
                        $r26_31+=$recuperacion[date("n",strtotime($hasta))][$i]; 
                    }
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);            
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;          
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "DESCONEXIONES");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);  
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $desconexion[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;        
                    $letrasA++;
                }
                $i++;
            }            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
                        
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "RECONEXIONES");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $reconexion[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;       
                    $letrasA++;
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);       
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "AUDITORIA ACOM");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $auditoria_acom[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);
                        
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "CAMBIO DOMICILIO");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $domicilio[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;      
                    $letrasA++;
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);            
            
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "EXTENCION ADIC");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $extencion_adi[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);

            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "RETIROS");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);            
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $retiros[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);            
            
            /*$numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "NOTIFI COBRANZA");
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);           
            while($dias >= $i ){
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $notifi_cobranza[date("n",strtotime($hasta))][$i] );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }            
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);*/
                        
            $numeros++;
            $letras="b";
            $letrasA="A";
            $i=1;                        
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->applyFromArray(
                array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FF0000')
                    )
                )
            );
            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "PRODUCT TECNICA");           
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);
            
            while($dias >= $i ){ 
                if(date("N",mktime(0, 0, 0, date("m", strtotime($hasta)), $i, date("Y", strtotime($hasta)))) != 8 ){
                    
                    $resultado =  $instalacion[date("n",strtotime($hasta))][$i] * $puntos[1] ;
                    $resultado += ( $desconexion[date("n",strtotime($hasta))][$i] );
                    $resultado += ( $reconexion[date("n",strtotime($hasta))][$i] * $puntos[2]);
                    $resultado += ( $auditoria_acom[date("n",strtotime($hasta))][$i] * $puntos[7]);
                    $resultado +=( $domicilio[date("n",strtotime($hasta))][$i] * $puntos[3]);
                    $resultado +=( $extencion_adi[date("n",strtotime($hasta))][$i] * $puntos[5]);
                    $resultado +=( $notifi_cobranza[date("n",strtotime($hasta))][$i]);                             
                    
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras.$numeros, $resultado );
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
                    $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $letras++;
                    $letrasA++;
                }
                $i++;
            }      
            $objPHPExcel->getActiveSheet()->setCellValue($letras.$numeros, '=sum(B'.$numeros.':'.$letrasA.$numeros.')');
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle($letras.$numeros)->getFont()->setBold(true);    
            
            $numeros+=2;            
           /********************************/
             $sheet = $objPHPExcel->getActiveSheet();
             $sheet->getPageMargins()->setTop(0.6);
             $sheet->getPageMargins()->setBottom(0.6);
             $sheet->getPageMargins()->setHeader(0.4);
             $sheet->getPageMargins()->setFooter(0.4);
             $sheet->getPageMargins()->setLeft(0.4);
             $sheet->getPageMargins()->setRight(0.4);
             $objPHPExcel->getProperties()->setTitle("Demo");
             $objPHPExcel->getProperties()->setCreator("Demo");
             $objPHPExcel->getProperties()->setLastModifiedBy("Demo");
             $objPHPExcel->getProperties()->setCompany("Demo");
            
             $values = new PHPExcel_Chart_DataSeriesValues('Number', $rangoValues);//Worksheet!$B$8:$AA$8                                 
             $categories = new PHPExcel_Chart_DataSeriesValues('String', $rangoCategorias);//Worksheet!$B$6:$AA$6

             $series = new PHPExcel_Chart_DataSeries(
                PHPExcel_Chart_DataSeries::TYPE_LINECHART,       // plotType
                PHPExcel_Chart_DataSeries::GROUPING_STANDARD,  // plotGrouping
                array(0),                                       // plotOrder
                array(),                                        // plotLabel
                array($categories),                             // plotCategory
                array($values)                                  // plotValues
            );
            $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

            $layout = new PHPExcel_Chart_Layout();
            $plotarea = new PHPExcel_Chart_PlotArea($layout, array($series));

            $title = new PHPExcel_Chart_Title('CLIENTES PAGADOS');
            $chart = new PHPExcel_Chart('sample', $title, null, $plotarea, true);          
            
            $chart->setTopLeftPosition('A'.$numeros);   
            $numeros+=10;
            $chart->setBottomRightPosition('AC'.$numeros);

            $sheet->addChart($chart);       
           /***********fin de la grafica*********************/ 
            
           /*********Inicio de la tablita ************/ 
           if( strtotime($hasta) == strtotime(date("Y-m-".$dias)) ){       
            $numeros+=1; 
                  
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$numeros.':C'.$numeros);    
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->getActiveSheet()->getStyle("A".$numeros)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$numeros, $nombre_mes[date("n", strtotime($hasta))]);

            $numeros++;    
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "ACTIVOS");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);            
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =1 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);   
           
            /*****************************************************************/
			
			$numeros++;  
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "DESCONECTAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);           
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status = 2 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);   
			
            $numeros++;  
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "DESCONECTADOS");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);           
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status = 8 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);            
            
            $numeros++;  
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "CORTAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);   
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =3 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);
               
            $numeros++;  
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "BAJAS");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);                     
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =5 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);
              
            $numeros++; 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "RETIRAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros); 
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =4 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                       
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);
                       
            $numeros++; 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "RETIRADO");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =9 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total); 
			
			$numeros++; 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "POR INSTALAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =6 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);
			
			$numeros++; 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "POR REINSTALAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =10 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);
			
			$numeros++; 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$numeros.':C'.$numeros)->getFont()->setSize(8);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$numeros, "POR RECONECTAR");
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$numeros.':C'.$numeros);
            $p=Doctrine_Query::create()->select('COUNT(*) as total')->from('Cliente')->where(" status =11 AND ".$filtro);   
            $clientes=$p->execute()->getFirst();                            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$numeros, $clientes->total);     
			
			       
            }
                         
            $hasta=date("Y/m/d", strtotime($hasta." -1 month"));
            $letras="b";
            $numeros+=5;
            $mes++;               
        }
        /***********Fin de la tablita*************/
        
        // Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Reporte.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->setIncludeCharts(TRUE);
        $objWriter->save('php://output');
        exit;	
    }
     
    public function sucursalesAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $sucursales=Sucursal::obtenerSucursal("AND plaza_id=".$this->_getParam('id'));

        echo '<option value="0">TODAS LAS SUCURSALES</option>';
        foreach($sucursales as $sucursal)
        {
            echo '<option value="'.$sucursal->id.'">'.$sucursal->nombre.'</option>';
        }
       
    }

    public function detalleAction()
    {
        $this->_helper->layout->disableLayout();

        
        $fecha=$this->_getParam('fecha');
        $this->view->reporte=$this->_getParam('reporte');
        $this->view->tipo = $this->_getParam('tipo'); //Tipo 1 = cable | 2 = internet | 3 = general
        $plaza=$this->_getParam('plaza');
        $plazas = "";

        $f=explode("-",$fecha);

        $fechaInicio=$fecha." 00:00:00";
        $fechaFin=$fecha." 23:59:59";

        if(!($plaza > 0))
        {
            $tmp_plaza = "";
            $tmp_plaza_uno = Permiso::obtienePlazas(Usuario::id());
            foreach ($tmp_plaza_uno as $permiso)
            {
                $tmp_plaza .= $permiso->plaza_id.",";
            }

            if($tmp_plaza != "" && Usuario::tipo() != 0)
            {
                $plazas = trim($tmp_plaza, ',');
            }
        }

        //Si es 2 es porque solo llego el mes y el año y entonces queremos el total
        if(count($f)==2)
        {
            $fechaInicio=$fecha."-01 00:00:00";
            $mes = mktime( 0, 0, 0, $f[1], 1, $f[0] ); 

            $fechaFin=$fecha."-".sprintf("%01d",date("t",$mes))." 23:59:59";
        }

        if($fecha==""){
            $desde=$this->_getParam('desde');
            $hasta=$this->_getParam('hasta'); 
            $fechaInicio=$desde." 00:00:00";
            $fechaFin=$hasta." 23:59:59";
        }

        if($this->_getParam('desde') == ""){
            $fechaInicio=date("Y")."-".date("m")."-1"." 00:00:00";
        }


        if($this->_getParam('hasta') == ""){
             $fechaFin=date("Y")."-".date("m")."-".date("d")." 23:59:59";
        }


        if($this->view->tipo == 3)
        {
            switch($this->view->reporte)
            {
                case "clientesPagados": 
                    $q=Doctrine_Query::create()->from('Cobranza')->where("fecha_inicio != 'null' AND fecha_fin != 'null' AND pagada=1 AND cancelado=0 AND Cobranza.CobranzaDetalle.otro_concepto='' AND Cobranza.CobranzaDetalle.servicio_id IS NOT NULL AND Cobranza.CobranzaDetalle.concepto_contratacion_id IS NULL AND created_at>='".$fechaInicio."' AND created_at<='".$fechaFin."'");
                    
                    if($plaza>0)
                        $q->andWhere('plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('plaza_id IN('.$plazas.')');
                    
                    $this->view->resultado=$q->execute();
                break;

                case "mensualidadesCobradas": 
                    $q=Doctrine_Query::create()->from('Cobranza')->where("fecha_inicio != 'null' AND fecha_fin != 'null' AND pagada=1 AND cancelado=0 AND Cobranza.CobranzaDetalle.otro_concepto='' AND Cobranza.CobranzaDetalle.servicio_id IS NOT NULL AND Cobranza.CobranzaDetalle.concepto_contratacion_id IS NULL AND created_at>='".$fechaInicio."' AND created_at<='".$fechaFin."'");
                    
                    if($plaza>0)
                        $q->andWhere('plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('plaza_id IN('.$plazas.')');
                    
                    $this->view->resultado=$q->execute();
                break;

                case "recuperaciones": 
                    $q=Doctrine_Query::create()->from('Cobranza')->where("fecha_inicio != 'null' AND fecha_fin != 'null' AND pagada=1 AND cancelado=0 AND Cobranza.CobranzaDetalle.otro_concepto='Recuperación' AND Cobranza.CobranzaDetalle.concepto_contratacion_id IS NULL AND created_at>='".$fechaInicio."' AND created_at<='".$fechaFin."'");
                    
                    if($plaza>0)
                        $q->andWhere('plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('plaza_id IN('.$plazas.')');
                    
                    $this->view->resultado=$q->execute();
                break;

                case "ventas": 
                    $q=Doctrine_Query::create()->from('Cobranza')->where("fecha_inicio != 'null' AND fecha_fin != 'null' AND Cobranza.CobranzaDetalle.otro_concepto='' AND Cobranza.CobranzaDetalle.concepto_contratacion_id IS NOT NULL AND created_at>='".$fechaInicio."' AND created_at<='".$fechaFin."'");
                    
                    if($plaza>0)
                        $q->andWhere('plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('plaza_id IN('.$plazas.')');
                    
                    $this->view->resultado=$q->execute();
                break;

                case "instalaciones": 
                    $q=Doctrine_Query::create()->from('Ordeninstalacion')->where("fecha_instalacion>='".$fechaInicio."' AND fecha_instalacion<='".$fechaFin."'");
                    
                    if($plaza>0)
                        $q->andWhere('Ordeninstalacion.Cliente.plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('Ordeninstalacion.Cliente.plaza_id IN('.$plazas.')');
                    
                    $this->view->resultado=$q->execute();
                break;

                case "desconexiones": 
                    $q=Doctrine_Query::create()->from('Ordendesconexion')->where("fecha_desconexion>='".$fechaInicio."' AND fecha_desconexion<='".$fechaFin."'");

                    if($plaza>0)
                        $q->andWhere('Ordendesconexion.Cliente.plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('Ordendesconexion.Cliente.plaza_id IN('.$plazas.')');

                    $this->view->resultado=$q->execute();
                break;

                case "reconexiones": 
                    $q=Doctrine_Query::create()->from('Ordendesreconexion')->where("fecha_reconexion>='".$fechaInicio."' AND fecha_reconexion<='".$fechaFin."'");

                    if($plaza>0)
                        $q->andWhere('Ordendesreconexion.Cliente.plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('Ordendesreconexion.Cliente.plaza_id IN('.$plazas.')');

                    $this->view->resultado=$q->execute();
                break;

                case "cambioDomicilio": 
                    $q=Doctrine_Query::create()->from('Ordencambiodomicilio')->where("fecha_instalacion>='".$fechaInicio."' AND fecha_instalacion<='".$fechaFin."'");

                    if($plaza>0)
                        $q->andWhere('Ordencambiodomicilio.Cliente.plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('Ordencambiodomicilio.Cliente.plaza_id IN('.$plazas.')');

                    $this->view->resultado=$q->execute();
                break;

                case "extensionesAdicionales": 
                    $q=Doctrine_Query::create()->from('Ordenextensionadicional')->where("fecha_instalacion>='".$fechaInicio."' AND fecha_instalacion<='".$fechaFin."'");

                    if($plaza>0)
                        $q->andWhere('Ordenextensionadicional.Cliente.plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('Ordenextensionadicional.Cliente.plaza_id IN('.$plazas.')');

                    $this->view->resultado=$q->execute();
                break;

                case "retiros": 
                    $q=Doctrine_Query::create()->from('Ordendesretiro')->where("fecha_retiro>='".$fechaInicio."' AND fecha_retiro<='".$fechaFin."'");
            
                    if($plaza>0)
                        $q->andWhere('Ordendesretiro.Cliente.plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('Ordendesretiro.Cliente.plaza_id IN('.$plazas.')');

                    $this->view->resultado=$q->execute();
                break;
            }
        }
        else
        {
            if($this->view->tipo == 1)
            {
                $type_1 = "WHERE (s.incluye_internet IS NULL OR s.incluye_internet = 0)";
                $type_2 = " (s.incluye_internet IS NULL OR s.incluye_internet = 0)";
            }
            else
            {
                $type_1 = "WHERE s.incluye_internet = 1";
                $type_2 = " s.incluye_internet = 1";
            }

            switch($this->view->reporte)
            {
                
                case "clientesPagados": 
                    $q = My_Comun::prepararQuery("Cobranza c");
                    $q->innerJoin("c.CobranzaDetalle cd");
                    $q->innerJoin("cd.Servicio s");
                    $q->where("c.fecha_inicio != 'null' AND c.fecha_fin != 'null' AND cd.otro_concepto=''");
                    $q->andWhere("cd.concepto_contratacion_id IS NULL AND cd.servicio_id IS NOT NULL AND c.created_at>='".$fechaInicio."'");
                    if($this->view->tipo == 1)
                        $q->andWhere("c.created_at<='".$fechaFin."' AND (s.incluye_internet IS NULL OR s.incluye_internet = 0)");
                    else
                        $q->andWhere("c.created_at<='".$fechaFin."' AND s.incluye_internet = 1");
                    $q->andWhere("c.pagada = 1 AND c.cancelado = 0");
                    
                    if($plaza>0)
                        $q->andWhere('c.plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('c.plaza_id IN('.$plazas.')');

                    //echo $q->getSqlQuery(); exit;

                    $this->view->resultado=$q->execute();
                break;

                case "mensualidadesCobradas":
                    $q = My_Comun::prepararQuery("Cobranza c");
                    $q->innerJoin("c.CobranzaDetalle cd");
                    $q->innerJoin("cd.Servicio s");
                    $q->where("c.fecha_inicio != 'null' AND c.fecha_fin != 'null' AND cd.otro_concepto=''");
                    $q->andWhere("cd.concepto_contratacion_id IS NULL AND cd.servicio_id IS NOT NULL AND c.created_at>='".$fechaInicio."'");
                    if($this->view->tipo == 1)
                        $q->andWhere("c.created_at<='".$fechaFin."' AND (s.incluye_internet IS NULL OR s.incluye_internet = 0)");
                    else
                        $q->andWhere("c.created_at<='".$fechaFin."' AND s.incluye_internet = 1");
                    $q->andWhere("c.pagada = 1 AND c.cancelado = 0");
                    
                    if($plaza>0)
                        $q->andWhere('c.plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('c.plaza_id IN('.$plazas.')');

                    $this->view->resultado=$q->execute();
                break;

                case "recuperaciones": 
                    $q = My_Comun::prepararQuery("Cobranza c");
                    $q->innerJoin("c.CobranzaDetalle cd");
                    $q->innerJoin("cd.Servicio s");
                    $q->where("c.fecha_inicio != 'null' AND c.fecha_fin != 'null' AND cd.otro_concepto='Recuperación'");
                    $q->andWhere("cd.concepto_contratacion_id IS NULL AND c.created_at>='".$fechaInicio."'");
                    if($this->view->tipo == 1)
                        $q->andWhere("c.created_at<='".$fechaFin."' AND (s.incluye_internet IS NULL OR s.incluye_internet = 0)");
                    else
                        $q->andWhere("c.created_at<='".$fechaFin."' AND s.incluye_internet = 1");
                    $q->andWhere("c.pagada = 1 AND c.cancelado = 0");
                    
                    if($plaza>0)
                        $q->andWhere('c.plaza_id="'.$plaza.'"');
                    else if($plazas != "")
                        $q->andWhere('c.plaza_id IN('.$plazas.')');

                    $this->view->resultado=$q->execute();
                break;

                case "ventas": 
                    $where="1=1";
                    if($plaza>0)
                        $where="plaza_id='".$plaza."'";
                    else if($plazas != "")
                        $where="plaza_id IN(".$plazas.")";
                    
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT cob.*, cl.* FROM cobranza cob 
                            INNER JOIN (
                                SELECT MIN(co.id) co_id FROM cobranza co WHERE co.cliente_id IN (
                                    SELECT c.id
                                    FROM cliente_servicio cs
                                    INNER JOIN (
                                        SELECT MIN( id ) f_id
                                        FROM cliente_servicio
                                        WHERE cliente_id IN (
                                            SELECT id
                                            FROM cliente
                                            WHERE ".$where." AND created_at >=  '".$fechaInicio."'
                                            AND created_at <=  '".$fechaFin."'
                                        )
                                        GROUP BY cliente_id
                                    ) qr ON qr.f_id = cs.id
                                    INNER JOIN cliente c ON cs.cliente_id = c.id
                                    INNER JOIN servicio s ON cs.servicio_id = s.id
                                    ".$type_1.
                                ")
                                GROUP BY co.cliente_id
                            ) qr2 ON cob.id = qr2.co_id
                            INNER JOIN cliente cl ON cob.cliente_id = cl.id";
                            
                    $q = $con->execute($query)->fetchAll();

                    //echo $query; exit;
                    //echo "<pre>"; print_r($q); exit;

                    $this->view->resultado = $q;

                break;

                case "instalaciones": 
                    $where="1=1";
                    if($plaza>0)
                        $where="cl.plaza_id='".$plaza."'";
                    else if($plazas != "")
                        $where="cl.plaza_id IN(".$plazas.")";
                    
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT * FROM ordeninstalacion op 
                            INNER JOIN (
                                SELECT orden.id, qr.o_id qr_o_id
                                FROM cliente_servicio orden 
                                INNER JOIN ( 
                                    SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi, o.id o_id 
                                    FROM ordeninstalacion o 
                                    INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                    WHERE CAST( o.fecha_instalacion AS DATE ) >= '".$fechaInicio."' 
                                    AND CAST( o.fecha_instalacion AS DATE ) <= '".$fechaFin."' 
                                    AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                    GROUP BY o.id 
                                ) qr ON qr.m_id = orden.id
                                INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                                INNER JOIN servicio s ON orden.servicio_id = s.id 
                                WHERE ".$where." AND ".$type_2.
                            ") qr2 ON qr2.qr_o_id = op.id";
                            
                    $q = $con->execute($query)->fetchAll();

                    $this->view->resultado = $q;
                break;

                case "desconexiones": 
                    $where="1=1";
                    if($plaza>0)
                        $where="cl.plaza_id='".$plaza."'";
                    else if($plazas != "")
                        $where="cl.plaza_id IN(".$plazas.")";
                    
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT * FROM ordendesconexion op
                            INNER JOIN (
                                SELECT orden.id, qr.o_id qr_o_id 
                                FROM cliente_servicio orden 
                                INNER JOIN ( 
                                    SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_desconexion o_fi, o.id o_id
                                    FROM ordendesconexion o 
                                    INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                    WHERE CAST( o.fecha_desconexion AS DATE ) >= '".$fechaInicio."' 
                                    AND CAST( o.fecha_desconexion AS DATE ) <= '".$fechaFin."' 
                                    AND CAST( cs.created_at AS DATE ) <= o.fecha_desconexion 
                                    GROUP BY o.id 
                                ) qr ON qr.m_id = orden.id
                                INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                                INNER JOIN servicio s ON orden.servicio_id = s.id 
                                WHERE ".$where." AND ".$type_2. 
                            ") qr2 ON qr2.qr_o_id = op.id";
                            
                    $q = $con->execute($query)->fetchAll();

                    $this->view->resultado = $q;
                break;

                case "reconexiones": 
                    $where="1=1";
                    if($plaza>0)
                        $where="cl.plaza_id='".$plaza."'";
                    else if($plazas != "")
                        $where="cl.plaza_id IN(".$plazas.")";
                    
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT * FROM ordendesreconexion op
                            INNER JOIN (
                                SELECT orden.id, qr.o_id qr_o_id
                                FROM cliente_servicio orden 
                                INNER JOIN ( 
                                    SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_reconexion o_fi, o.id o_id 
                                    FROM ordendesreconexion o 
                                    INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                    WHERE CAST( o.fecha_reconexion AS DATE ) >= '".$fechaInicio."' 
                                    AND CAST( o.fecha_reconexion AS DATE ) <= '".$fechaFin."' 
                                    AND CAST( cs.created_at AS DATE ) <= o.fecha_reconexion 
                                    GROUP BY o.id 
                                ) qr ON qr.m_id = orden.id
                                INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                                INNER JOIN servicio s ON orden.servicio_id = s.id 
                                WHERE ".$where." AND ".$type_2. 
                            ") qr2 ON qr2.qr_o_id = op.id";
                            
                    $q = $con->execute($query)->fetchAll();

                    $this->view->resultado = $q;
                break;

                case "cambioDomicilio": 
                    $where="1=1";
                    if($plaza>0)
                        $where="cl.plaza_id='".$plaza."'";
                    else if($plazas != "")
                        $where="cl.plaza_id IN(".$plazas.")";
                    
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT * FROM ordenescambiodomicilio op 
                            INNER JOIN (
                                SELECT orden.id, qr.o_id qr_o_id 
                                FROM cliente_servicio orden 
                                INNER JOIN ( 
                                    SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi, o.id o_id 
                                    FROM ordenescambiodomicilio o 
                                    INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                    WHERE CAST( o.fecha_instalacion AS DATE ) >= '".$fechaInicio."' 
                                    AND CAST( o.fecha_instalacion AS DATE ) <= '".$fechaFin."' 
                                    AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                    GROUP BY o.id 
                                ) qr ON qr.m_id = orden.id
                                INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                                INNER JOIN servicio s ON orden.servicio_id = s.id 
                                WHERE ".$where." AND ".$type_2.
                            ") qr2 ON qr2.qr_o_id = op.id";
                            
                    $q = $con->execute($query)->fetchAll();

                    $this->view->resultado = $q;
                break;

                case "extensionesAdicionales": 
                    $where="1=1";
                    if($plaza>0)
                        $where="cl.plaza_id='".$plaza."'";
                    else if($plazas != "")
                        $where="cl.plaza_id IN(".$plazas.")";
                    
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT * FROM ordenextensionadicional op 
                            INNER JOIN (
                                SELECT orden.id, qr.o_id qr_o_id 
                                FROM cliente_servicio orden 
                                INNER JOIN ( 
                                    SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_instalacion o_fi, o.id o_id  
                                    FROM ordenextensionadicional o 
                                    INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                    WHERE CAST( o.fecha_instalacion AS DATE ) >= '".$fechaInicio."' 
                                    AND CAST( o.fecha_instalacion AS DATE ) <= '".$fechaFin."' 
                                    AND CAST( cs.created_at AS DATE ) <= o.fecha_instalacion 
                                    GROUP BY o.id 
                                ) qr ON qr.m_id = orden.id
                                INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                                INNER JOIN servicio s ON orden.servicio_id = s.id 
                                WHERE ".$where." AND ".$type_2. 
                            ") qr2 ON qr2.qr_o_id = op.id";
                            
                    $q = $con->execute($query)->fetchAll();

                    $this->view->resultado = $q;
                break;

                case "retiros": 
                    $where="1=1";
                    if($plaza>0)
                        $where="cl.plaza_id='".$plaza."'";
                    else if($plazas != "")
                        $where="cl.plaza_id IN(".$plazas.")";
                    
                    $con = Doctrine_Manager::getInstance()->connection();
                    $query="SELECT * FROM ordendesretiro op 
                            INNER JOIN (
                                SELECT orden.id, qr.o_id qr_o_id  
                                FROM cliente_servicio orden 
                                INNER JOIN ( 
                                    SELECT MAX( cs.id ) m_id, o.cliente_id o_ci, o.fecha_retiro o_fi, o.id o_id 
                                    FROM ordendesretiro o 
                                    INNER JOIN cliente_servicio cs ON cs.cliente_id = o.cliente_id 
                                    WHERE CAST( o.fecha_retiro AS DATE ) >= '".$fechaInicio."' 
                                    AND CAST( o.fecha_retiro AS DATE ) <= '".$fechaFin."' 
                                    AND CAST( cs.created_at AS DATE ) <= o.fecha_retiro 
                                    GROUP BY o.id 
                                ) qr ON qr.m_id = orden.id
                                INNER JOIN cliente cl ON orden.cliente_id = cl.id 
                                INNER JOIN servicio s ON orden.servicio_id = s.id 
                                WHERE ".$where." AND ".$type_2. 
                            ") qr2 ON qr2.qr_o_id = op.id";
                            
                    $q = $con->execute($query)->fetchAll();

                    $this->view->resultado = $q;
                break;
            }
        }
    }



} // FIN DE CONTROLLER