<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Application_Plugin_Login
 */
class Application_Plugin_Login extends Zend_Controller_Plugin_Abstract
{
    
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {		

        $auth =  Zend_Auth::getInstance();
        $controllerName = $request->getControllerName();

        $controller = $request->getControllerName();
        $action = $request->getActionName();

        
        if($request->getModuleName() != "default")
        {
			if(($controller!='prueba' && $controller!='cliente' && $action!='testsms' && $action!='cron' && $action!='restames' && $action!='cronmetas') && $controller!="servicios" && $controller!="recibo" && $controller!="serviciostest")
            {
                if($auth->hasIdentity())
                {
                    
                    $permisos = $auth->getIdentity()->permisos;
                    $permisos = explode("|", $permisos);      

                    $permiso = strtoupper($this->getPermissionName($action)."_".$controller);

                    //echo "Permiso: ".$permiso;

                    if($action == "logout")
                    {
                        $auth->clearIdentity();
                        
                    }
                    /*echo "<!--";
                        print_r($permisos);
                        echo "=====>".$permiso."-->";*/

                   /* if(substr_count($auth->getIdentity()->permisos,$permiso)>0)
                    {
                        echo "<!--";
                        print_r($permisos);
                        echo "=====>".$permiso."-->";
                    }*/



                    if(substr_count($auth->getIdentity()->permisos,$permiso)<=0 && !$request->isXmlHttpRequest())
                    {

                        $request->setModuleName("default");
                        $request->setControllerName("login");
                        $request->setActionName("index");
                    }


                     $layout = Zend_Layout::getMvcInstance();
                     $view = $layout->getView();

                     ### Si es un usuario administrador entonces evaluamos la definición de metas
                     $dia=date('d');
                     //$dia=1;


                     if($auth->getIdentity()->tipo==0 && ($dia==1 || ($dia==2 && date('N')==1)))
                        $view->mostrarAlertaMeta=Metas::metaPorDefinir();



                     $view->seRequiereConfirmar=ConfirmacionEnteradoMetas::seRequiereConfirmar();

                     ### Para mostrar la alerta a administradores en caso de que se vaya abajo de las metas
                     $view->plazasVsMetasAdministrador=false;
                     $view->plazasVsMetasPlaza=false;
                     /*if($auth->getIdentity()->tipo==0)
                     {
                        $view->plazasVsMetasAdministrador=Metas::algunaPlazaVaPorDebajo();
                     }
                     else if($auth->getIdentity()->tipo==1)
                     {
                        $view->plazasVsMetasPlaza=Metas::algunaPlazaVaPorDebajo($auth->getIdentity()->plaza_id);
                     }*/



                }  
                else 
                {   
                    header("Location: /login"); exit;
                }
            }
            else
            {
                
            }    
        }        
			    
    } //function
	
    public function  getPermissionName($action)
    {
            switch($action)
            {
                    case "index"    : return "ver"; break;
                    case "grid"     : return "ver"; break;
                    case "agregar"  : return "agregar"; break;
                    case "eliminar" : return "eliminar"; break;
                    case "exportar" : return "ver"; break;
                    case "imprimir" : return "ver"; break;
                    default: return $action; break;	
            }
    }
}

?>
