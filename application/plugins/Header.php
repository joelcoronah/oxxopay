<?php
  class Application_Plugin_Header extends Zend_Controller_Plugin_Abstract
  { 
	  public $module;
	  
	  public function preDispatch(Zend_Controller_Request_Abstract $request)
	  {
		 $view = new Zend_View();
		 
		 $view->headScript()->appendFile($view->baseUrl('js/jquery.js'));
         $view->headScript()->appendFile($view->baseUrl('js/jquery-ui.js'));
         $view->headScript()->appendFile($view->baseUrl('js/multiselect.js'));
         $view->headScript()->appendFile($view->baseUrl('js/comun.js'));
         $view->headScript()->appendFile($view->baseUrl('js/form/jquery.alphanumeric.js'));
         $view->headScript()->appendFile($view->baseUrl('js/form/jquery.validate.min.js'));
         $view->headScript()->appendFile($view->baseUrl('js/form/ajaxforms.js'));
         //$view->headScript()->appendFile($view->baseUrl('js/bootstrap.js'));
         $view->headScript()->appendFile($view->baseUrl('js/flexgrid/flexigrid.js'));
         $view->headLink()->appendStylesheet($view->baseUrl('css/bootstrap.css'));
         $view->headLink()->appendStylesheet($view->baseUrl('css/principal.css'));
         $view->headLink()->appendStylesheet($view->baseUrl('css/flexigrid.css'));
         $view->headLink()->appendStylesheet($view->baseUrl('css/ui/ui.css'));

        //$view->headLink()->appendStylesheet($view->baseUrl('js/datetimepicker-master/jquery.datetimepicker.css'));
		//$view->headScript()->appendFile($view->baseUrl('js/datetimepicker-master/jquery.datetimepicker.js'));

         $view->headScript()->appendFile($view->baseUrl('js/jquery-ui-timepicker-addon.js'));
         	
                 
                 
		 

		 
		 $view->headTitle('Ultracable::');
		 
		 $viewRenderer=new Zend_Controller_Action_Helper_ViewRenderer();
		 $viewRenderer->setView($view);
		 
		 Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
		 define("BASE_URL", $view->baseUrl());

		 
	  }
	  
	  public function routeStartup(Zend_Controller_Request_Abstract $request)
	  {


	  }
  }
?>
