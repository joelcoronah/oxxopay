<?php

class LoginController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout()->setLayout('login');
    }

    public function indexAction()
    {
        
        ### Si se reciben datos por post, probablemente lleguen el nombre de usuario y password
        if($this->getRequest()->isPost())
        {
            ### Generamos una instancia del adaptador para hacer login
            $authAdapter = new ZC_Auth_Adapter($this->getRequest()->getPost("usuario"),$this->getRequest()->getPost("password"));
            ### Intentamos autentificar al usuario
            $result = Zend_Auth::getInstance()->authenticate($authAdapter);
            
            ### Si el usuario es válido
            if(Zend_Auth::getInstance()->hasIdentity())
            {
                
                
                  $data = My_Comun::obtener("Usuario",Zend_Auth::getInstance()->getIdentity()->id);
                  $SessionUsuario = new Zend_Session_Namespace('idUsuario');
                  $SessionUsuario->idUsuario = $data["id"]; 

                  Bitacora::registrarEvento("Login", "Ingreso", Zend_Auth::getInstance()->getIdentity()->nombre);
                  $permisos=explode("|",Zend_Auth::getInstance()->getIdentity()->permisos);


                  if($data->tipo==2)
                  {
                    header("Location: /transacciones/reportes/cortedecaja");
				          }

                  else if(!in_array("VER_BITACORA",$permisos) )
                  {
                      natsort($permisos);
                      foreach($permisos as $permiso)
                      {
                         if(strpos($permiso,"VER")!==FALSE)
                         {
                             $accion=strtolower(str_replace("VER_","",$permiso));
                         }
                      }
                      header("Location: /transacciones/clientes");
                  }
                  else
                    header("Location: /transacciones/clientes"); 
                  exit;
            }
        }
   
    }


}

