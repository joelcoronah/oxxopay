<?php

/**
 * Area
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Paquetesservicio extends BasePaquetesservicio
{
	public static function obtenerPaquetesservicio($filtro=" 1=1")
    {
	   $q=Doctrine_Query::create()->from('Paquetesservicio')->where($filtro);
       return $q->execute();
    }
	
	public static function guardar($datos)
    {
		Doctrine_Query::create()->delete('Paquetesservicio')->where('paquete_id=?',$datos['paquete_id'])->execute();
		foreach($datos['servicio'] as $servicio)
		{
			$paquetesservicio=new Paquetesservicio();
			$paquetesservicio->servicio_id=$servicio;
			$paquetesservicio->paquete_id=$datos['paquete_id'];
			$paquetesservicio->save();
		}

    }
	
}