<?php

/**
 * BaseColonia
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $zona_id
 * @property string $nombre
 * @property integer $status
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseVouchersCloudtrax extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('vouchers_cloudtrax');
        $this->hasColumn('voucher', 'string', 50, array(
             'type' => 'string',
             'length' => 50
             ));
        $this->hasColumn('usuarios', 'integer', 8, array(
             'type' => 'integer',
             'length' => 8
             ));
        $this->hasColumn('horas', 'integer', 8, array(
             'type' => 'integer',
             'length' => 8
             ));
        $this->hasColumn('horas_restantes', 'integer', 8, array(
             'type' => 'integer',
             'length' => 8
             ));
        
        $this->hasColumn('megas', 'integer', 1, array(
             'type' => 'integer',
             'length' => 1
             ));

        $this->hasColumn('megas_restantes', 'integer', 1, array(
             'type' => 'integer',
             'length' => 1
             ));

        $this->hasColumn('estatus', 'integer', 1, array(
             'type' => 'integer',
             'length' => 1
             ));

        $this->hasColumn('fecha_creacion_cloudtrax', 'date', 1, array(
             'type' => 'date',
             'length' => 1
             ));

        $this->hasColumn('vigencia', 'date', 1, array(
             'type' => 'date',
             'length' => 1
             ));
        $this->hasColumn('asignado', 'integer', 1, array(
             'type' => 'integer',
             'length' => 1
             ));

        $this->option('type', 'INNODB');
        $this->option('collate', 'utf8_general_ci');
        $this->option('charset', 'utf8');
    }

    public function setUp()
    {
        parent::setUp();

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}