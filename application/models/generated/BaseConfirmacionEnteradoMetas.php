<?php
/**
 * BaseEstatusDeFalla
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $nombre
 * @property integer $status
 * @property Doctrine_Collection $Falla
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseConfirmacionEnteradoMetas extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('confirmacion_enterado_metas');
        $this->hasColumn('meta_id', 'integer', 8, array(
             'type' => 'integer',
             'length' => 8,
             ));
        $this->hasColumn('usuario_id', 'integer', 8, array(
             'type' => 'integer',
             'length' => 8,
             ));

        $this->option('type', 'INNODB');
        $this->option('collate', 'utf8_general_ci');
        $this->option('charset', 'utf8');
    }

    public function setUp()
    {
        parent::setUp();
        
        $this->hasOne('Metas', array(
             'local' => 'meta_id',
             'foreign' => 'id'));

        $this->hasOne('Usuario', array(
             'local' => 'usuario_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}