<?php

/**
 * Colonia
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class VouchersCloudtrax extends BaseVouchersCloudtrax
{
    public static function hayVouchers($numero)
    {
    	$f=explode("-",date('Y-m-d'));
    	$fecha=date("Y-m-d", strtotime($f[0]."-".$f[1]."-".$f[2]." +6 month"));
    	$q=Doctrine_Query::create()->from("VouchersCloudtrax")->where("vigencia>'".$fecha."' AND estatus=1 AND asignado=0");

    	return $q->count();
    }

    public static function asignaVoucher($celular,$cliente)
    {
    	$f=explode("-",date('Y-m-d'));
    	$fecha=date("Y-m-d", strtotime($f[0]."-".$f[1]."-".$f[2]." +6 month"));
    	$v=Doctrine_Query::create()->from("VouchersCloudtrax")->where("vigencia>'".$fecha."' AND estatus=1 AND asignado=0")->execute()->getFirst();

    	//Asignamos el voucher
    	$clienteVoucher=new ClienteVoucherCloudtrax();
    		$clienteVoucher->cliente_id=$cliente;
    		$clienteVoucher->voucher_id=$v->id;
    		$clienteVoucher->celular=$celular;
    	$clienteVoucher->save();	

    	//Cambiamos el valor del campo asignación del voucher
    	$v->asignado=1;
    	$v->save();

    	//Enviamos el mensaje
    	ini_set('max_execution_time', 150);
        include('My/nusoap/lib/nusoap.php');

    	$client = new nusoap_client('http://www.masmensajes.com.mx/server.php?wsdl','wsdl');
        $err = $client->getError();

        if(!$err)
        {
	        $param = array('usuario' =>'sai','contrasena' =>'sai123','destinatario' =>'+52'.$celular,'sms' =>'Hola, esta es tu clave para contectarte a la red de SUPERTV: '.$v->voucher,'fecha' =>date('Y-m-s'),'hora' =>date('H:i:s'));
	        $result = $client->call('Set_Sms', $param);
    	}

    }

    public static function preAsignaVoucher($celular,$cliente)
    {

        //Asignamos el voucher
        $clienteVoucher=new ClienteVoucherCloudtrax();
            $clienteVoucher->cliente_id=$cliente;
            $clienteVoucher->celular=$celular;
        $clienteVoucher->save();    

       

    }

}