<?php

/**
 * NotificacionFechas
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class NotificacionFechas extends BaseNotificacionFechas
{
	public static function obtenerFechas($not_id)
    {
       $q = Doctrine_Query::create()->from('NotificacionFechas')->where("notificacion_id=?",$not_id)->orderBy("id asc");
       return $q->execute();
    }

    public static function eliminarFechas($not_id)
    {
    	### Por default la respuesta tiene el valor false
        $respuesta=false;

        ### Instanciamos la conexión para generar una transacción
        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->beginTransaction();
        try
        { 
            ### Intentamos eliminar el registro
            $q = Doctrine_Query::create()->delete('NotificacionFechas')->where("notificacion_id=?",$not_id);
            $respuesta=$q->execute();
            
            ### Hacemo commit a la transacción
            $conn->commit();
        }
        catch (Exception $e)
        { 
            ### Rollback en caso de algún problema
            $conn->rollBack(); 
            ### Entregamos un mensaje de error
            return $e->getPortableCode();
        }
        ### Retornamos la respuesta
        return $respuesta;
    }
}