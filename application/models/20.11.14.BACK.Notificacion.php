<?php

/**
 * Notificacion
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Notificacion extends BaseNotificacion
{
	public static function obtenerNotificaciones($fecha)
    {
    	$q = My_Comun::prepararQuery("Notificacion o");
		$q->innerJoin("o.NotificacionFechas nf");
		$q->where(" o.status = 1 AND nf.fecha = ?",$fecha);
       	$q->orderBy('o.id asc');
       return $q->execute();
    }
}