<?php

/**
 * TipoDeFalla
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Sucursal extends BaseSucursal
{
    public static function obtenerSucursal( $filtro="" )
    {
       $q=Doctrine_Query::create()->from('Sucursal')->orderBy('nombre asc')->where('status=1 '.$filtro);
       //echo $q->getSqlQuery(); exit;
       return $q->execute();      
    }
}